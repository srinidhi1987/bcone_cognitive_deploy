﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.Engine;
    using Automation.CognitiveData.Validator.Model;
    using Automation.CognitiveData.Validator.Service;
    using Automation.CognitiveData.VisionBot.UI;

    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Automation.VisionBotEngine.Model;
    using Contract;
    using Service;
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Windows.Input;
    using System.Windows.Media;
    using ValidatorServices;
    using VisionBotEngine;
    using VisionBotEngine.Validation;
    using Automation.Validator;
    using System.Collections.Generic;
    using System.Windows.Threading;
    using Automation.Validator.Validator.UI.ViewModel;
    using ValidatorServices.ValidatorSerivceConsumer;
    using ValidatorServices.Validator.Model;
    using System.Threading;
    using Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence;

    internal class ValidatorViewModel : ValidatorViewModelBase
    {
        private bool _canExecuteNext = false;
        private bool _canExecutePrevious = false;
        private bool _canExecuteSave = false;
        private ValidatorSettings _settings;
        private ValidatorWorkflowEngine _engine;
        private string _queueName;
        private string _projectName;
        private string _projectId;
        private QSetting _qSetting;
        //private IterativelyCheckingConnectivityNotifierService checkingConnectivity;
        private NextDocumentResponse _response;
        private IList<InvalidReason> _invalidReasonList;

        #region Properties

        private ValidatorHeaderViewModel _validatorHeaderVM;

        public ValidatorHeaderViewModel ValidatorHeaderVM
        {
            get
            {
                return _validatorHeaderVM;
            }

            set
            {
                if (_validatorHeaderVM != value)
                {
                    _validatorHeaderVM = value;
                    OnPropertyChanged(nameof(ValidatorHeaderVM));
                }
            }
        }

        private DocumentEditorViewModel _documentEditorViewModel;

        public DocumentEditorViewModel DocumentEditorViewModel
        {
            get
            {
                return _documentEditorViewModel;
            }
            set
            {
                _documentEditorViewModel = value;
                OnPropertyChanged(nameof(DocumentEditorViewModel));
            }
        }

        private int _correctedDocument;

        public int CorrectedDocument
        {
            get
            {
                return _correctedDocument;
            }
            set
            {
                if (_correctedDocument != value)
                {
                    _correctedDocument = value;
                    OnPropertyChanged(nameof(CorrectedDocument));
                    StatusBarService.ShowCorrectedCount(value);
                }
            }
        }

        private StatusBarViewModel _statusBarViewModel;

        public StatusBarViewModel StatusBarViewModel
        {
            get
            {
                return _statusBarViewModel;
            }
        }

        private IInteractionService<InvalidReasonWindowViewModel> _invalidReasonsService;

        public IInteractionService<InvalidReasonWindowViewModel> InvalidReasonsService
        {
            get
            {
                return _invalidReasonsService;
            }

            set
            {
                _invalidReasonsService = value;
            }
        }

        private IInteractionService<ErrorModel> _popupService;

        public IInteractionService<ErrorModel> PopupService
        {
            get
            {
                return _popupService;
            }

            set
            {
                _popupService = value;
            }
        }

        private IInteractionService<BusyIndicatorViewModel> _busyIndicatorService;

        internal IInteractionService<BusyIndicatorViewModel> BusyIndicatorService
        {
            get
            {
                return _busyIndicatorService;
            }

            set
            {
                _busyIndicatorService = value;
            }
        }

        private ImageCommandViewModel _nextCommandVM;

        public ImageCommandViewModel NextCommandVM
        {
            get
            {
                return _nextCommandVM;
            }
        }

        private ICommand _next;

        public ICommand Next
        {
            get
            {
                return _next;
            }

            set
            {
                _next = value;
            }
        }
        private ICommand _markInvalid;
        public ICommand MarkInvalid
        {
            get { return _markInvalid; }
            set { _markInvalid = value; }
        }
        private ICommand _save;

        public ICommand Save
        {
            get
            {
                return _save;
            }

            set
            {
                _save = value;
            }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler FocusRequested;

        public event EventHandler NetworkStatusChanged;

        public static event Action<QSetting> QueueReconnected;

        public event EventHandler CloseRequested;

        #endregion Event(s)

        #region ctor

        public ValidatorViewModel(ValidatorWorkflowEngine engine, QSetting qSetting, ValidatorSettings settings)
        {
            _settings = settings;
            _queueName = qSetting.QueuePath.ServerPath;
            _projectName = qSetting.ProjectName;
            _projectId = qSetting.ProjectId;
            _qSetting = qSetting;
            _validatorHeaderVM = new ValidatorHeaderViewModel(qSetting);
            _statusBarViewModel = new StatusBarViewModel();
            StatusBarService.Init(_statusBarViewModel);
            StatusBarService.SetIsOnline(true);

            //checkingConnectivity = IterativelyCheckingConnectivityNotifierServiceProvider.GetInstance(qSetting);
            //checkingConnectivity.ConnectivityStatusChanged += OnStatusChanged;
            //checkingConnectivity.ConnectivityReconnected += NetworkReconnected;
            //checkingConnectivity.StartNotification();
            _next = new RelayCommand(nextHandler, canExecuteNext);
            _save = new RelayCommand(saveHandler, canExecuteSave);
            _markInvalid = new RelayCommand(markInvalidHandler);
            _engine = engine;
            CorrectedDocument = 0;
            var userName = qSetting.UserName;//Automation.Common.Routines.GetClientConfiguration().Username;
            StatusBarService.SetUserName(userName);
            _nextCommandVM = new ImageCommandViewModel()
            {
                ActiveImagePath = "pack://siteoforigin:,,,/Images/IQValidator_RightArrowActive.png",
                InactiveImagePath = "pack://siteoforigin:,,,/Images/IQValidator_RightArrowInActive.png",
                Command = _next
            };
        }

        private void NetworkReconnected(ConnectionEndPoint connectionEndPoint, ConnectionStatus oldConnectionStatus, ConnectionStatus newConnectionStatus)
        {
            if (QueueReconnected != null)
            {
                VBotLogger.Trace(() => $"[{nameof(ValidatorViewModel)}->{nameof(NetworkReconnected)}] Reconnected Event Triggered.");
                QueueReconnected(_qSetting);
            }
        }

        private void OnStatusChanged(ConnectionEndPoint connectionEndPoint, ConnectionStatus oldConnectionStatus, ConnectionStatus newConnectionStatus)
        {
            bool status = newConnectionStatus == ConnectionStatus.Online ? true : false;
            StatusBarService.SetIsOnline(status);
            if (DocumentEditorViewModel != null && DocumentEditorViewModel.DataEditorViewModel != null)
            {
                DocumentEditorViewModel.DataEditorViewModel.IsOnline = status;
            }
            //this event should be triggered on reconnect of the queue.
            OnNetworkStatusChanged();
        }

        #endregion ctor

        #region Public Method(s)

        public bool CanCloseValidator()
        {
            bool canClose = false;
            if (ContinueIfDirty())
            {
                canClose = true;
                try
                {
                    ServiceRequest<FailedVBotDocument> closeRequest = new ServiceRequest<FailedVBotDocument>();
                    closeRequest.Model = _response?.Model ?? new FailedVBotDocument();
                    if (closeRequest.Model.Queue == null)
                    {
                        closeRequest.Model.Queue = _qSetting;
                    }
                    _engine.Close(closeRequest);
                    //if (_directoryWatcher != null)
                    //{
                    //    _directoryWatcher.StatusChanged -= OnStatusChanged;
                    //    _directoryWatcher.NetworkReconnected -= _directoryWatcher_NetworkReconnected;
                    //    _directoryWatcher.Dispose();
                    //}
                    //_directoryWatcher = null;
                    //if (checkingConnectivity != null)
                    //{
                    //    checkingConnectivity.ConnectivityStatusChanged -= OnStatusChanged;
                    //    checkingConnectivity.ConnectivityStatusChanged -= NetworkReconnected;
                    //    checkingConnectivity.Dispose();
                    //}
                    //checkingConnectivity = null;
                }
                catch (Exception ex)
                {
                    ex.Log(nameof(ValidatorViewModel), nameof(CanCloseValidator));
                }
                finally
                {
                    _documentEditorViewModel?.Dispose();
                }
            }

            return canClose;
        }
        public void NextWhenEmpty()
        {
            if (IsValidatorEmpty())
                NextDocument();
        }
        public void NextDocument()
        {
            changeNavigationAvailability(false);
            BusyIndicatorViewModel busyIndicatorVM = new BusyIndicatorViewModel();
            busyIndicatorVM.Message = "Loading document";
            _busyIndicatorService.Raise(busyIndicatorVM, nextDocumentCallback);
        }

        public string GetQName()
        {
            return _queueName;
        }

        public bool IsValidatorEmpty()
        {
            return _response?.Model?.FailedDocument == null;
        }

        public bool IsValidatorHasFileAccess()
        {
            if (IsValidatorEmpty())
                return true;
            else
            {
                bool isLocked = true;
                try
                {
                    _response?.Request?.Model?.PackageFileStream?.GetAccessControl();
                    isLocked = false;
                }
                catch
                {
                    isLocked = true;
                    StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(Automation.Cognitive.Validator.Properties.Resources.ValidatorNetworkReconnectDocumentSwitchMessage));
                }
                return isLocked;
            }
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private bool ContinueIfDirty()
        {
            bool shouldContinue = false;
            if (_documentEditorViewModel != null && _documentEditorViewModel.IsDirty)
            {
                ErrorModel context = new ErrorModel();
                context.ErrorMessage = Automation.Cognitive.Validator.Properties.Resources.ValidatorDirtyDocWarning;
                context.Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                    {
                        new ButtonModel
                        {
                            Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionYes,
                            IsDefault = false,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                        },
                        new ButtonModel
                        {
                            Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionNo,
                            IsCancel = true,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                        }
                    };
                _popupService.Raise(context);
                if (context.SelectedAction == context.Buttons[0])
                {
                    shouldContinue = true;
                }
            }
            else
            {
                shouldContinue = true;
            }

            return shouldContinue;
        }

        private void nextHandler(object param)
        {
            if (ContinueIfDirty())
            {
                NextDocument();
            }
        }

        private async void nextDocumentCallback(BusyIndicatorViewModel busyIndicatorVM)
        {
            try
            {
                DocumentEditorViewModel docEditorVM = await GetDocumentEditorVMAsync();
                refreshHeader(_response);
                assignDocumentEditorVM(docEditorVM);
            }
            finally
            {
                changeNavigationAvailability(true);
                busyIndicatorVM.OnCloseRequested();
            }
        }

        private bool canExecuteNext(object param)
        {
            return StatusBarViewModel.IsOnline && _canExecuteNext && gotDocumentFromEngine();
        }

        private bool canExecuteSave(object param)
        {
            return _canExecuteSave && gotDocumentFromEngine();
        }

        private void changeNavigationAvailability(bool available)
        {
            _canExecuteNext = available;
            _canExecutePrevious = available;
            _canExecuteSave = available;
        }

        private void assignDocumentEditorVM(DocumentEditorViewModel docEditorVM)
        {
            DocumentEditorViewModel oldDocEditorVM = this.DocumentEditorViewModel;
            this.DocumentEditorViewModel = null;
            if (oldDocEditorVM != null)
            {
                Task.Run(() => oldDocEditorVM.Dispose());
            }
            this.DocumentEditorViewModel = docEditorVM;
            if (FocusRequested != null)
            {
                FocusRequested(this, EventArgs.Empty);
            }
        }

        private DocumentEditorViewModel GetDocumentEditorVM()
        {
            DocumentEditorViewModel vm = null;
            try
            {
                if (this.DocumentEditorViewModel != null)
                {
                    this.DocumentEditorViewModel?.DocumentViewModel?._uiImageProvide.DisposeNonBlocking();
                }
                NextDocumentRequest request = new NextDocumentRequest();
                request.Model = _response?.Model ?? new FailedVBotDocument();
                if (request.Model.Queue == null)
                {
                    request.Model.Queue = new Automation.ValidatorLite.IntegratorCommon.Model.QSetting()
                    {
                        QueuePath = new Automation.ValidatorLite.IntegratorCommon.Model.QPath()
                        {
                            ServerPath = _queueName
                        },
                        ProjectName = _projectName,
                        ProjectId = _projectId,
                        UserName = _qSetting.UserName,
                        UserToken = _qSetting.UserToken,
                        OrganizationId = _qSetting.OrganizationId
                    };
                }
                _response = _engine.Next(request);

                VBotDocument doc = _response?.Model?.FailedDocument;

                if (doc != null)
                {
                    vm = new DocumentEditorViewModel(_response.Model, this, _settings);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidatorViewModel), nameof(GetDocumentEditorVM));
            }
            return vm;
        }



        private Task<DocumentEditorViewModel> GetDocumentEditorVMAsync()
        {
            return Task.Run<DocumentEditorViewModel>(new Func<DocumentEditorViewModel>(GetDocumentEditorVM));
        }

        private SaveDocumentResponse SaveDocument()
        {
            SaveDocumentResponse response = null;

            try
            {
                _documentEditorViewModel.EditedVBotDocument.Data.IgnoreLowConfidenceIssue();

                SaveDocumentRequest request = new SaveDocumentRequest();
                request.Model = _response.Model;
                request.Model.CorrectedDocument = _documentEditorViewModel.EditedVBotDocument;
                response = _engine.Save(request);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidatorViewModel), nameof(SaveDocument));
            }
            return response;
        }
        private Task<SaveDocumentResponse> SaveDocumentAsync()
        {
            return Task.Run<SaveDocumentResponse>(new Func<SaveDocumentResponse>(SaveDocument));
        }

        private void saveHandler(object param)
        {
            //TODO: check whether there is any error or not? We should use call to Validation logic to check whether any error is there.
            int errorCount = _documentEditorViewModel.DataEditorViewModel.ErrorCount;
            int optionalErrorCount = _documentEditorViewModel.DataEditorViewModel.OptionalErrorCount;

            if (errorCount - optionalErrorCount > 0)
            {
                if (FocusRequested != null)
                {
                    FocusRequested(this, EventArgs.Empty);
                }
            }
            else
            {
                if (CanSaveDocument())
                {
                    BusyIndicatorViewModel busyIndicatorVM = new BusyIndicatorViewModel();
                    string fileName = !string.IsNullOrWhiteSpace(_documentEditorViewModel.DocumentName) ? Path.GetFileNameWithoutExtension(_documentEditorViewModel.DocumentName) : string.Empty;
                    busyIndicatorVM.Message = string.Format(Automation.Cognitive.Validator.Properties.Resources.ValidatorSaveMessage, fileName);
                    _busyIndicatorService.Raise(busyIndicatorVM, saveHandlerCallback);
                }
            }
        }

        private async void saveHandlerCallback(BusyIndicatorViewModel busyIndicatorVM)
        {
            SaveDocumentResponse response = null;
            try
            {
                response = await SaveDocumentAsync();
                if (response != null)
                {
                    CorrectedDocument++;
                    StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(""));
                    StatusBarService.ShowErrorCount(0);
                    if (response.ErrorCode == 1)
                    {
                        showFatalErrorMessage(Cognitive.Validator.Properties.Resources.ProjectNotFoundErrorMessage);
                    }
                }
                else
                {
                    StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(Automation.Cognitive.Validator.Properties.Resources.ValidatorUnexpectedErrorWhileSave));
                }
            }
            finally
            {
                busyIndicatorVM.OnCloseRequested();
            }
            if (response != null && response.ErrorCode != 1)
            {
                NextDocument();
            }
        }

        //private void refreshHeader(QSetting qSetting)
        private void refreshHeader(NextDocumentResponse nextResponse)
        {
            QSetting qSetting = nextResponse?.Model?.Queue;
            if (nextResponse != null && nextResponse.ErrorCode == 103)
            {
                if (qSetting == null)
                {
                    qSetting = new QSetting();
                }
                qSetting.QueueStats = new QStats();
                qSetting.QueueStats.TotalNoOfDocumentsInQ = 0;
            }
            ValidatorHeaderVM.Refresh(qSetting);
        }

        private bool gotDocumentFromEngine()
        {
            return _response?.Model?.FailedDocument != null && _response.ErrorCode != 103;
        }

        private void OnNetworkStatusChanged()
        {
            if (NetworkStatusChanged != null)
            {
                NetworkStatusChanged(this, EventArgs.Empty);
            }
        }

        private bool CanSaveDocument()
        {
            bool canSaveDocument = false;

            ErrorModel context = new ErrorModel();
            string warningMessage = _documentEditorViewModel.DataEditorViewModel.OptionalErrorCount > 0 ?
                Cognitive.Validator.Properties.Resources.LowConfidenceWarning :
                Cognitive.Validator.Properties.Resources.ValidatorSaveDocumentWarning;

            context.ErrorMessage = warningMessage;

            context.Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                {
                    new ButtonModel
                    {
                        Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionYes,
                        IsDefault = false,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                    },
                    new ButtonModel
                    {
                        Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionNo,
                        IsCancel = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                    }
                };
            Dispatcher.CurrentDispatcher.Invoke(new Action(() => { _popupService.Raise(context); }));
            if (context.SelectedAction == context.Buttons[0])
            {
                canSaveDocument = true;
            }
            return canSaveDocument;
        }

        private void showFatalErrorMessage(string message)
        {
            ErrorModel context = new ErrorModel();
            context.ErrorMessage = message;
            context.Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                {
                    new ButtonModel
                    {
                        Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionOK,
                        IsDefault = false,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                    }
                };

            Dispatcher.CurrentDispatcher.Invoke(new Action(() => { _popupService.Raise(context); }));
            if (context.SelectedAction == context.Buttons[0])
            {
                ServiceRequest<FailedVBotDocument> closeRequest = new ServiceRequest<FailedVBotDocument>();
                closeRequest.Model = _response?.Model ?? new FailedVBotDocument();
                if (closeRequest.Model.Queue == null)
                {
                    closeRequest.Model.Queue = _qSetting;
                }

                _engine.Close(closeRequest);

                if (CloseRequested != null)
                {
                    CloseRequested(this, null);
                }
            }
        }

        private bool CanMarkInvalid()
        {
            bool canMarkInvalid = false;

            ErrorModel context = new ErrorModel();
            context.ErrorMessage = Automation.Cognitive.Validator.Properties.Resources.ValidatorMarkDocumentInvalidWarning;
            context.Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
                {
                    new ButtonModel
                    {
                        Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionYes,
                        IsDefault = false,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                    },
                    new ButtonModel
                    {
                        Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionNo,
                        IsCancel = true,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
                    }
                };
            Dispatcher.CurrentDispatcher.Invoke(new Action(() => { _popupService.Raise(context); }));

            if (context.SelectedAction == context.Buttons[0])
            {
                canMarkInvalid = true;
            }

            return canMarkInvalid;
        }
        private InvalidDocumentResponse MarkInvalidDocument(InvalidDocumentRequest invalidDocumentRequest)
        {
            InvalidDocumentResponse response = null;
            try
            {
                response = _engine.Invalid(invalidDocumentRequest);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ValidatorViewModel), nameof(MarkInvalidDocument));
                response = new InvalidDocumentResponse();
                response.ErrorCode = 1;
                response.ResponseCode = -1;
            }
            return response;
        }
        private void showInvalidReasonNotReceivedMessage()
        {
            Boolean clickresult;
            ErrorModel context = new ErrorModel();
            context.ErrorMessage = Automation.Cognitive.Validator.Properties.Resources.InvalidReasonsNotAvailableMessage;
            context.Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
            {
                new ButtonModel
                {
                    Caption = Automation.Cognitive.Validator.Properties.Resources.CaptionOK,
                    IsDefault = true,
                    Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
                }
            };
            Dispatcher.CurrentDispatcher.Invoke(new Action(() => { _popupService.Raise(context); }));
            if (context.SelectedAction == context.Buttons[0])
            {
                clickresult = true;
            }
        }
        private void markInvalidHandler(object param)
        {
            if (_documentEditorViewModel == null)
                return;

            InvalidReasonWindowViewModel invalidReasonWindowViewModel = new InvalidReasonWindowViewModel
            {
                FileId = _response.Model.fileid,
                OrganizationId = _response.Model.Queue.OrganizationId,
                ProjectId = _response.Model.Queue.ProjectId
            };

            invalidReasonWindowViewModel.ReasonList = new List<InvalidReasonModel>();

            IValidatorServiceEndpointConsumer validatorService = new ValidatorServiceEndpointConsumerService(_qSetting);
            EndpointConsumerParameter parameter = new EndpointConsumerParameter(_qSetting.OrganizationId, _qSetting.ProjectId, _response.Model.fileid);
            IList<InvalidReason> reasonList = validatorService.GetInvalidReasons(parameter);
            if (reasonList == null || reasonList.Count == 0)
            {
                VBotLogger.Error(() => string.Format("[ValidatorViewModel -> markInvalidHandler] projectId:{0} : No invalid reasons received.", _response.Model.Queue.ProjectId));
                showInvalidReasonNotReceivedMessage();
                return;
            }
            foreach (InvalidReason reason in reasonList)
            {
                invalidReasonWindowViewModel.ReasonList.Add(
                    new InvalidReasonModel
                    {
                        Id = reason.Id,
                        Text = reason.Text,
                        IsSelected = false
                    });
            }

            InvalidReasonsService.Raise(invalidReasonWindowViewModel, markInvalidConfirmationCallback);
        }

        private void markInvalidConfirmationCallback(InvalidReasonWindowViewModel invalidReasonViewModel)
        {
            if (invalidReasonViewModel.IsCancelled)
                return;

            InvalidDocument invalidDocument = new InvalidDocument
            {
                FileId = invalidReasonViewModel.FileId,
                OrganizationId = invalidReasonViewModel.OrganizationId,
                ProjectId = invalidReasonViewModel.ProjectId
            };

            IList<InvalidReason> reasonList = new List<InvalidReason>();

            foreach (InvalidReasonModel invalidReasonModel in invalidReasonViewModel.ReasonList)
            {
                if (!invalidReasonModel.IsSelected)
                    continue;

                reasonList.Add(
                    new InvalidReason
                    {
                        Id = invalidReasonModel.Id,
                        Text = invalidReasonModel.Text
                    });
            }

            invalidDocument.Reasons = reasonList;

            var response = MarkInvalidDocument(new InvalidDocumentRequest()
            {
                Model = invalidDocument
            });

            if (response.ErrorCode != 1)
            {
                NextDocument();
            }
            else
            {
                showFatalErrorMessage(Cognitive.Validator.Properties.Resources.ProjectNotFoundErrorMessage);
            }
            //BusyIndicatorViewModel busyIndicatorVM = new BusyIndicatorViewModel();
            //string fileName = !string.IsNullOrWhiteSpace(_documentEditorViewModel.DocumentName) ? Path.GetFileNameWithoutExtension(_documentEditorViewModel.DocumentName) : string.Empty;
            //busyIndicatorVM.Message = string.Format(Automation.Validator.Properties.Resources.MarkInvalidMessage, fileName);
            //_busyIndicatorService.Raise(busyIndicatorVM, markInvalidCallback);
        }

        //private async void markInvalidCallback(BusyIndicatorViewModel busyIndicatorVM)
        //{
        //    InvalidDocumentResponse response = null;
        //    try
        //    {
        //        response = await MarkInvalidAsync();
        //        if (response != null)
        //        {
        //            CorrectedDocument++;
        //            StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(""));
        //            StatusBarService.ShowErrorCount(0);
        //        }
        //        else
        //        {
        //            StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(Automation.Validator.Properties.Resources.ValidatorUnexpectedErrorWhileSave));
        //        }
        //    }
        //    finally
        //    {
        //        busyIndicatorVM.OnCloseRequested();
        //    }
        //    if (response != null)
        //    {
        //        NextDocument();
        //    }
        //}
        #endregion Private Method(s)
    }
}