﻿using Automation.VisionBotEngine.Model;

/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    internal class TableValueViewModel
    {
        public TableValueViewModel()
        {
            Rows = new List<DataRecordViewModel>();
        }

        public List<FieldDef> Headers { get; set; }
        public List<DataRecordViewModel> Rows { get; set; }
        public TableDef TableDef { get; set; }
    }
}