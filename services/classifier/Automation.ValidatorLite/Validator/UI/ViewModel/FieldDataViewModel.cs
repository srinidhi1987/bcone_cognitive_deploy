﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.VisionBot.UI.ViewModels;

/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using Automation.VisionBotEngine.Model;

/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    internal class FieldDataViewModel : ViewModelBase
    {
        private FieldData _fieldData;

        private bool _isSelected = false;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public string Id
        {
            get { return _fieldData.Field.Id; }
            set { _fieldData.Field.Id = value; }
        }

        public string Name
        {
            get { return _fieldData.Field.Name; }
            set { _fieldData.Field.Name = value; }
        }

        public Rectangle Bounds
        {
            get { return _fieldData.Value.Field.Bounds; }
            set
            {
                if (_fieldData.Value.Field.Bounds != value)
                {
                    _fieldData.Value.Field.Bounds = value;
                    OnPropertyChanged(nameof(Bounds));
                }
            }
        }

        public string Value
        {
            get { return _fieldData.Value.Field.Text; }
            set
            {
                if (_fieldData.Value.Field.Text != value)
                {
                    _fieldData.Value.Field.Text = value;
                    OnPropertyChanged(nameof(Value));
                }
            }
        }

        public ValidationIssue ValidationIssue
        {
            get { return _fieldData.ValidationIssue; }
            set
            {
                if (_fieldData.ValidationIssue != value)
                {
                    _fieldData.ValidationIssue = value;
                    OnPropertyChanged(nameof(ValidationIssue));
                }
            }
        }

        public FieldDataViewModel(FieldData fieldData)
        {
            _fieldData = fieldData;
        }
    }
}