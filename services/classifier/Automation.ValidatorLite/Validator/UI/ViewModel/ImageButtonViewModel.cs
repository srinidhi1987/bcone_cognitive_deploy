﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using System.Windows.Input;

    internal class ImageCommandViewModel : ValidatorViewModelBase
    {
        #region Properties

        private string _activeImagePath;

        public string ActiveImagePath
        {
            get
            {
                return _activeImagePath;
            }

            set
            {
                if (_activeImagePath != value)
                {
                    _activeImagePath = value;
                    OnPropertyChanged(nameof(ActiveImagePath));
                }
            }
        }

        private string _inactiveImagePath;

        public string InactiveImagePath
        {
            get
            {
                return _inactiveImagePath;
            }

            set
            {
                if (_inactiveImagePath != value)
                {
                    _inactiveImagePath = value;
                    OnPropertyChanged(nameof(InactiveImagePath));
                }
            }
        }

        private ICommand _command;

        public ICommand Command
        {
            get
            {
                return _command;
            }

            set
            {
                if (_command != value)
                {
                    _command = value;
                    OnPropertyChanged(nameof(Command));
                }
            }
        }

        private string _displayText;

        public string DisplayText
        {
            get
            {
                return _displayText;
            }

            set
            {
                if (_displayText != null)
                {
                    _displayText = value;
                    OnPropertyChanged(nameof(DisplayText));
                }
            }
        }

        #endregion Properties
    }
}