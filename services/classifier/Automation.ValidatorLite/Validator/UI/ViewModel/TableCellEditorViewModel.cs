﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using VisionBotEngine.Model;
    using System;
    using System.Drawing;
    using System.Collections.ObjectModel;
    using ValidatorServices.MachineLearningServiceConsumer;
    using Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence;
    using Services.Client;

    internal class TableCellEditorViewModel : ViewModelBase
    {
        private FieldLayout _fieldLayout;

        #region Properties

        private FieldData _fieldData;

        public FieldData FieldData
        {
            get
            {
                return _fieldData;
            }
        }

        public string ValueId
        {
            get
            {
                return _fieldData.Id;
            }
        }

        public string KeyId
        {
            get
            {
                return _fieldLayout.Id;
            }
        }

        public string Name
        {
            get { return _fieldData.Field.Name; }
            set { _fieldData.Field.Name = value; }
        }

        public Rectangle ValueBound
        {
            get { return _fieldData.Value.Field.Bounds; }
            set
            {
                if (_fieldData.Value.Field.Bounds != value)
                {
                    _fieldData.Value.Field.Bounds = value;
                    OnPropertyChanged(nameof(ValueBound));
                }
            }
        }

        public Rectangle LabelBound
        {
            get { return _fieldLayout.Bounds; }
            set
            {
                if (_fieldLayout.Bounds != value)
                {
                    _fieldLayout.Bounds = value;
                    OnPropertyChanged(nameof(LabelBound));
                }
            }
        }

        //TODO: Require some abstraction here. We may have to handle Value of other types in addition to string value.
        public string Value
        {
            get { return _fieldData.Value.Field.Text; }
            set
            {
                if (_fieldData.Value.Field.Text != value)
                {
                    string oldValue = _fieldData.Value.Field.Text;
                    _fieldData.Value.Field.Text = value;
                    OnPropertyChanged(nameof(Value));
                    OnValueChanged(oldValue, value);
                }
            }
        }

        public ValidationIssue ValidationIssue
        {
            get { return _fieldData.ValidationIssue; }
            set
            {
                if (_fieldData.ValidationIssue != value)
                {
                    _fieldData.ValidationIssue = value;
                    OnPropertyChanged(nameof(ValidationIssue));
                }
            }
        }

        private bool _isSelected = false;

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }

            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                    OnSelectionChanged(value);
                }
            }
        }

        public bool isRecommendedOnce
        {
            get; set;
        }

        private ObservableCollection<RecommendationModel> _suggestion;

        public ObservableCollection<RecommendationModel> Suggestion
        {
            get { return this._suggestion; }
            set
            {
                _suggestion = value;
                OnPropertyChanged(nameof(Suggestion));
            }
        }

        #endregion Properties

        #region Events

        public event EventHandler<FieldEditorSelectionChangedArgs> SelectionChanged;

        public event EventHandler<FieldEditorValueChangedArgs> ValueChanged;

        #endregion Events

        #region ctor

        public TableCellEditorViewModel(FieldLayout fieldLayout, FieldData fieldData)
        {
            _fieldLayout = fieldLayout ?? new FieldLayout();
            _fieldData = fieldData;
            _suggestion = new ObservableCollection<RecommendationModel>();
        }

        #endregion ctor

        #region Public Method(s)

        public void RefreshValidationIssue()
        {
            OnPropertyChanged(nameof(ValidationIssue));
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void OnValueChanged(string oldValue, string newValue)
        {
            if (ValueChanged != null)
            {
                _fieldData?.IgnoreLowConfidenceIssue();

                FieldEditorValueChangedArgs args = new FieldEditorValueChangedArgs(ValueId, oldValue, newValue);
                ValueChanged(this, args);
            }
        }

        private void ignoreFieldDataLowConfidenceIssueAsUserChangedValue(FieldData fieldData)
        {
            fieldData.Value.Field.Confidence = VisionBotEngine.Configuration.Constants.CDC.MAX_CONFIDENCE_THRESHOLD;
        }

        private void OnSelectionChanged(bool isSelected)
        {
            if (SelectionChanged != null)
            {
                FieldEditorSelectionChangedArgs args = new FieldEditorSelectionChangedArgs(ValueId, isSelected);
                SelectionChanged(this, args);
            }
        }

        #endregion Private Method(s)
    }
}