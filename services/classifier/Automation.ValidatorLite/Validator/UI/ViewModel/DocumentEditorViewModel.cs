﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using Automation.CognitiveData.Validator.UI.Service;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using Model;
    using Services.Client;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.IO;
    using ValidatorServices.MachineLearningServiceConsumer;
    using ValidatorServices.VisionBotEngine.ValidationWrapper;
    using VisionBotEngine;
    using static DocumentViewModel;

    internal class DocumentEditorViewModel : ValidatorViewModelBase, IDisposable
    {
        private VBotDocument _originalVBotDocument;
        private VBotDocument _clonedVBotDocument;
        private IDocumentPageImageProvider _documentPageImageProvider;
        private ValidatorSettings _settings;
        private FailedVBotDocument _failedVbotDocument;

        #region Propertie(s)

        private DataEditorViewModel _dataEditorViewModel;

        public DataEditorViewModel DataEditorViewModel
        {
            get
            {
                return _dataEditorViewModel;
            }
            set
            {
                _dataEditorViewModel = value;
            }
        }

        private DocumentViewModel _documentViewModel;

        public DocumentViewModel DocumentViewModel
        {
            get
            {
                return _documentViewModel;
            }
        }

        private ValidatorViewModel _parent;

        public ValidatorViewModel Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }

        private bool _isDirty;

        public bool IsDirty
        {
            get
            {
                return _isDirty;
            }

            set
            {
                if (_isDirty != value)
                {
                    _isDirty = value;
                    OnPropertyChanged(nameof(IsDirty));
                }
            }
        }

        public string DocumentName { get { return getDocumentName(); } }

        public VBotDocument EditedVBotDocument
        {
            get
            {
                return _clonedVBotDocument;
            }
        }

        private readonly float _autoCurrectBanchmark = 0.9F;

        #endregion Propertie(s)

        private VisionbotServiceEndPointConsumer _visionBotService;

        #region ctor

        private IMachineLearningServiceEndpointConsumer machineLearningServiceEndpointConsumer;

        public DocumentEditorViewModel(FailedVBotDocument failedVBotDocument, ValidatorViewModel parent, ValidatorSettings settings)
        {
            _settings = settings;
            _visionBotService = new VisionbotServiceEndPointConsumer(new ClassifiedFolderManifest()
            {
                id = failedVBotDocument.MetaData.ClassificationId,
                organizationId = failedVBotDocument.Queue.OrganizationId,
                projectId = failedVBotDocument.Queue.ProjectId
            });
            _failedVbotDocument = failedVBotDocument;
            _originalVBotDocument = failedVBotDocument.FailedDocument;
            _clonedVBotDocument = Utility.Clone<VBotDocument>(_originalVBotDocument);
            _parent = parent;
            _dataEditorViewModel = new DataEditorViewModel(_clonedVBotDocument);
            _dataEditorViewModel.CellValueChanged += _dataEditorViewModel_CellValueChanged;
            _dataEditorViewModel.FieldValueChanged += _dataEditorViewModel_FieldValueChanged;
            _dataEditorViewModel.FieldEditorViewModel.SelectionChanged += FieldEditorViewModel_SelectionChanged;
            _dataEditorViewModel.TableEditorViewModel.SelectionChanged += TableEditorViewModel_SelectionChanged;
            _dataEditorViewModel.RowDeleted += DataEditorViewModel_RowDeleted;
            _dataEditorViewModel.RowInserted += DataEditorViewModel_RowInserted;
            _dataEditorViewModel.RowMoved += DataEditorViewModel_RowMoved;
            _documentPageImageProvider = new DocumentPageImageProvider(_originalVBotDocument?.DocumentProperties, _originalVBotDocument?.Layout?.Settings); //TODO: Think how we can get imagesettings applied in engine.
            _documentViewModel = new DocumentViewModel(_clonedVBotDocument, _documentPageImageProvider, _settings);
            _documentViewModel.DocumentPageImageRequested += _documentViewModel_DocumentPageImageRequested;
            machineLearningServiceEndpointConsumer = new MachineLearningEndpointClientSideConsumerService(_failedVbotDocument.Queue);
            validateDocument();
        }

        private void _documentViewModel_DocumentPageImageRequested(object sender, DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(_documentViewModel_DocumentPageImageRequested)}] pageIndex:{pageIndex} imagePath:{imagePath}");
            try
            {
                _visionBotService.GetImage(documentProperties.Id, pageIndex, imagePath);
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(_documentViewModel_DocumentPageImageRequested)}] pageIndex:{pageIndex} imagePath:{imagePath} Error:{ex.Message}");
            }
        }

        #endregion ctor

        #region Public Method(s)

        public void Dispose()
        {
            _clonedVBotDocument = null;
            _originalVBotDocument = null;
            _failedVbotDocument = null;
            if (_dataEditorViewModel != null)
            {
                _dataEditorViewModel.CellValueChanged -= _dataEditorViewModel_CellValueChanged;
                _dataEditorViewModel.FieldValueChanged -= _dataEditorViewModel_FieldValueChanged;
                _dataEditorViewModel.FieldEditorViewModel.SelectionChanged -= FieldEditorViewModel_SelectionChanged;
                _dataEditorViewModel.TableEditorViewModel.SelectionChanged -= TableEditorViewModel_SelectionChanged;
                _dataEditorViewModel.RowDeleted -= DataEditorViewModel_RowDeleted;
                _dataEditorViewModel.RowInserted -= DataEditorViewModel_RowInserted;
                _dataEditorViewModel.RowMoved -= DataEditorViewModel_RowMoved;
            }
            if (_documentPageImageProvider != null)
            {
                _documentViewModel.Dispose();
                _documentViewModel = null;
                _documentPageImageProvider.Dispose();
                _documentPageImageProvider = null;
            }
            if (machineLearningServiceEndpointConsumer != null)
            {
                machineLearningServiceEndpointConsumer = null;
            }
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void DataEditorViewModel_RowMoved(object sender, EventArgs e)
        {
            makeDocumentDirty();
        }

        private void DataEditorViewModel_RowInserted(object sender, RowInsertedArgs e)
        {
            makeDocumentDirtyAndValidate();
        }

        private void DataEditorViewModel_RowDeleted(object sender, RowDeletedArgs e)
        {
            makeDocumentDirtyAndValidate();
            StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(""));
        }

        private void _dataEditorViewModel_FieldValueChanged(object sender, EventArgs e)
        {
            makeDocumentDirtyAndValidate();
        }

        private void _dataEditorViewModel_CellValueChanged(object sender, EventArgs e)
        {
            makeDocumentDirtyAndValidate();
        }

        private void makeDocumentDirty()
        {
            IsDirty = true;
        }

        private void makeDocumentDirtyAndValidate()
        {
            makeDocumentDirty();
            validateDocument();
        }

        private void validateDocument()
        {
            //Currently document validation is being done on UI thread. Later on we can think about async validation.
            //Challenge is with Tab movement on UI. Because on Tab out we have to perform validation and that validation
            //will deside next error field to be selected. So Async validation is somewhat difficult to achieve.
            //var vbotValidationDataManager = new VBotValidatorDataManager(_failedVbotDocument.PackageFileStream);
            //TODO1: Load Validation details
            VBotValidatorDataManager vBotValidatorDataManagerWrapper = new VBotValidatorDataManager(new ClassifiedFolderManifest()
            {
                id = _failedVbotDocument.MetaData.ClassificationId,
                organizationId = _failedVbotDocument.Queue.OrganizationId,
                projectId = _failedVbotDocument.Queue.ProjectId,
                name = ""
            }, _failedVbotDocument.MetaData.VisionBotId);

            //_clonedVBotDocument.Data.EvaluateAndFillValidationDetails(_clonedVBotDocument.Layout, vbotValidationDataManager);
            VBotDataWraper.EvaluateAndFillValidationDetails(_clonedVBotDocument.Data
                , _clonedVBotDocument.Layout
                , vBotValidatorDataManagerWrapper
                , _failedVbotDocument.Queue.OrganizationId,
                _failedVbotDocument.Queue.ProjectId);

            _dataEditorViewModel.RefreshValidationIssue();
            _dataEditorViewModel.UpdateValidationIssueCount();
        }

        private void TableEditorViewModel_SelectionChanged(object sender, EventArgs e)
        {
            _documentViewModel.SelectValue((sender as TableCellEditorViewModel).ValueId);
            //_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels.
            if ((sender as TableCellEditorViewModel).ValidationIssue == null)
            {
                (sender as TableCellEditorViewModel).Suggestion.Clear();
                return;
            }
            if ((e as FieldEditorSelectionChangedArgs).IsSelected &&
                !(sender as TableCellEditorViewModel).isRecommendedOnce)
            {
                for (int t = 0; t < _dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels.Count; t++)
                {
                    for (int r = 0; r < _dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows.Count; r++)
                    {
                        for (int c = 0; c < _dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray.Length; c++)
                        {
                            if (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c].Equals(sender as TableCellEditorViewModel))
                            {
                                if (_originalVBotDocument.Data.TableDataRecord[t].Rows.Count < r + 1)
                                    return;
                                if ((_originalVBotDocument.Data.TableDataRecord[t].Rows[r].Fields[c - 1].Value.Field.Text !=
                                    (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Value))
                                {
                                    (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Suggestion.Clear();
                                    return;
                                }
                                if ((e as FieldEditorSelectionChangedArgs).IsSelected &&
                                !(_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).isRecommendedOnce &&
                                (_originalVBotDocument.Data.TableDataRecord[t].Rows[r].Fields[c - 1].Value.Field.Text != "") &&
                                (_originalVBotDocument.Data.TableDataRecord[t].Rows[r].Fields[c - 1].Value.Field.Text == (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Value))
                                {
                                    //string suggestion = machineLearningServiceEndpointConsumer.getSuggestion("111",
                                    //  (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).FieldData.Field.Id,
                                    //  (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Value);
                                    try
                                    {
                                        ObservableCollection<RecommendationModel> suggestionCollection = machineLearningServiceEndpointConsumer.GetSuggestionWithConfidence(_failedVbotDocument.Queue.ProjectId,
                                           (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).FieldData.Field.Id,
                                          (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Value);
                                        VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(TableEditorViewModel_SelectionChanged)}] FieldId:{(_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).FieldData.Field.Id} FieldName:{(_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).FieldData.Field.Name}");

                                        if (suggestionCollection != null && suggestionCollection.Count > 0)
                                        {
                                            VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(TableEditorViewModel_SelectionChanged)}] SuggestionCount:{suggestionCollection.Count}");
                                            bool isAutoCurrected = false;
                                            foreach (RecommendationModel suggestion in suggestionCollection)
                                            {
                                                if (suggestion.Confidence >= _autoCurrectBanchmark)
                                                {
                                                    (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Value = suggestion.Value;
                                                    (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Suggestion.Clear();
                                                    isAutoCurrected = true;
                                                    return;
                                                }
                                            }
                                            //(_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Value = suggestion;
                                            if (!isAutoCurrected)
                                                (_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).Suggestion = suggestionCollection;
                                        }
                                        else
                                        {
                                            VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(TableEditorViewModel_SelectionChanged)}] SuggestionCount:{0}");
                                        }
                                        //(_dataEditorViewModel.TableEditorViewModel.SingleTableEditorViewModels[t].DataTable.Rows[r].ItemArray[c] as TableCellEditorViewModel).isRecommendedOnce = true;
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        VBotLogger.Error(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(TableEditorViewModel_SelectionChanged)}] {ex.Message}");
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void FieldEditorViewModel_SelectionChanged(object sender, EventArgs e)
        {
            _documentViewModel.SelectValue((sender as SingleFieldEditorViewModel).ValueId);
            int index = _dataEditorViewModel.FieldEditorViewModel.Fields.IndexOf((sender as SingleFieldEditorViewModel));
            if ((sender as SingleFieldEditorViewModel).ValidationIssue == null)
            {
                _dataEditorViewModel.FieldEditorViewModel.Fields[index].Suggestion.Clear();
                return;
            }
            if (_dataEditorViewModel.FieldEditorViewModel.Fields[index].Value != _originalVBotDocument.Data.FieldDataRecord.Fields[index].Value.Field.Text)
            {
                _dataEditorViewModel.FieldEditorViewModel.Fields[index].Suggestion.Clear();
                return;
            }
            if ((e as FieldEditorSelectionChangedArgs).IsSelected &&
                !_dataEditorViewModel.FieldEditorViewModel.Fields[index].isRecommendedOnce &&
                _originalVBotDocument.Data.FieldDataRecord.Fields[index].Value.Field.Text != ""
                && _dataEditorViewModel.FieldEditorViewModel.Fields[index].Value == _originalVBotDocument.Data.FieldDataRecord.Fields[index].Value.Field.Text)
            {
                //string suggestion = machineLearningServiceEndpointConsumer.getSuggestion("111",
                //  (_dataEditorViewModel.FieldEditorViewModel.Fields[index]).FieldID,
                //  _originalVBotDocument.Data.FieldDataRecord.Fields[index].Value.Field.Text);
                try
                {
                    VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(FieldEditorViewModel_SelectionChanged)}] FieldId:{(_dataEditorViewModel.FieldEditorViewModel.Fields[index]).FieldID} FieldName:{(_dataEditorViewModel.FieldEditorViewModel.Fields[index]).Name}");
                    ObservableCollection<RecommendationModel> suggestionCollection = machineLearningServiceEndpointConsumer.GetSuggestionWithConfidence(_failedVbotDocument.Queue.ProjectId,
                      (_dataEditorViewModel.FieldEditorViewModel.Fields[index]).FieldID,
                      _originalVBotDocument.Data.FieldDataRecord.Fields[index].Value.Field.Text);
                    if (suggestionCollection != null && suggestionCollection.Count > 0)
                    {
                        VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(FieldEditorViewModel_SelectionChanged)}] SuggestionCount:{suggestionCollection.Count}");
                        bool isAutoCurrected = false;
                        foreach (RecommendationModel suggestion in suggestionCollection)
                        {
                            if (suggestion.Confidence >= _autoCurrectBanchmark)
                            {
                                _dataEditorViewModel.FieldEditorViewModel.Fields[index].Value = suggestion.Value;
                                _dataEditorViewModel.FieldEditorViewModel.Fields[index].Suggestion.Clear();
                                isAutoCurrected = true;
                                break;
                            }
                        }
                        //_dataEditorViewModel.FieldEditorViewModel.Fields[index].Value = suggestion;
                        //_dataEditorViewModel.FieldEditorViewModel.Fields[index].isRecommendedOnce = true;
                        if (!isAutoCurrected)
                            _dataEditorViewModel.FieldEditorViewModel.Fields[index].Suggestion = suggestionCollection;
                    }
                    else
                        VBotLogger.Trace(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(FieldEditorViewModel_SelectionChanged)}] SuggestionCount:{0}");
                }
                catch (Exception ex)
                {
                    VBotLogger.Error(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(FieldEditorViewModel_SelectionChanged)}] {ex.Message}");
                }
            }
            else
            {
                _dataEditorViewModel.FieldEditorViewModel.Fields[index].Suggestion.Clear();
            }
        }

        private string getDocumentName()
        {
            string documentName = String.Empty;
            try
            {
                if (_failedVbotDocument?.FailedDocument?.DocumentProperties != null)
                {
                    documentName = _failedVbotDocument.FailedDocument.DocumentProperties.Name;
                }
                else
                {
                    string path = _originalVBotDocument?.DocumentProperties?.Path;
                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        documentName = Path.GetFileName(_originalVBotDocument?.DocumentProperties?.Path);
                    }
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(DocumentEditorViewModel)} -> {nameof(getDocumentName)}]");
            }

            return documentName;
        }

        #endregion Private Method(s)
    }
}