﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using Automation.CognitiveData.VisionBot.UI;
    using Automation.VisionBotEngine.Model;
    using Service;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using VisionBot;
    using VisionBotEngine;

    internal class DocumentViewModel : ValidatorViewModelBase, IDisposable
    {
        private bool _isDisposedCalled = false;
        public UIImageProvider _uiImageProvide = null;
        public DocumentProperties DocumentProperties { get; set; }

        private VBotDocument _document;
        private List<IRegion> _selectedRegion = new List<IRegion>();
        internal delegate void DocumentPageImageRequestHandler(object sender, DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings);
        internal event DocumentPageImageRequestHandler DocumentPageImageRequested;
        #region Properties

        private ObservableCollection<PageViewModel> _pages = new ObservableCollection<PageViewModel>();

        public ObservableCollection<PageViewModel> Pages
        {
            get
            {
                return _pages;
            }

            set
            {
                if (_pages != value)
                {
                    _pages = value;
                    //_pages.CollectionChanged += _pages_CollectionChanged;
                    OnPropertyChanged(nameof(Pages));
                }
            }
        }

        private PageViewModel _currentPage;

        public PageViewModel CurrentPage
        {
            get
            {
                return _currentPage;
            }

            set
            {
                if (_currentPage != value)
                {
                    _currentPage = null;
                    _currentPage = value;
                    OnPropertyChanged(nameof(CurrentPage));
                }
            }
        }

        private ZoomViewModel _zoomViewModel;

        public ZoomViewModel ZoomViewModel
        {
            get
            {
                return _zoomViewModel;
            }

            set
            {
                if (_zoomViewModel != value)
                {
                    _zoomViewModel = value;
                    OnPropertyChanged(nameof(ZoomViewModel));
                }
            }
        }

        private ICommand _pageUP;

        public ICommand PageUP
        {
            get
            {
                return _pageUP;
            }

            set
            {
                _pageUP = value;
            }
        }

        private ICommand _pageDown;

        public ICommand PageDown
        {
            get
            {
                return _pageDown;
            }

            set
            {
                _pageDown = value;
            }
        }

        private ValidatorSettings _settings;

        public ValidatorSettings Settings
        {
            get
            {
                return _settings;
            }
        }

        private Rect _viewRect = new Rect();

        public Rect ViewRect
        {
            get { return _viewRect; }
            set
            {
                if (value != _viewRect)
                {
                    _viewRect = value;
                    OnPropertyChanged(nameof(ViewRect));
                    RefreshLoadingScreen();
                }
            }
        }

        private bool _currentViewIsLoading = true;

        public bool CurrentViewIsLoading
        {
            get { return _currentViewIsLoading /*&& Parent.HasItems*/; }
            set
            {
                if (value != _currentViewIsLoading)
                {
                    _currentViewIsLoading = value;
                    OnPropertyChanged(nameof(CurrentViewIsLoading));
                }
            }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler<PageChangeRequestedArgs> PageChangeRequested;

        public event EventHandler ZoomPercentageChanged;

        #endregion Event(s)

        #region ctor

        public DocumentViewModel(VBotDocument document, IDocumentPageImageProvider documentPageImageProvider, ValidatorSettings settings)
        {
            DocumentProperties = document.DocumentProperties;
            _uiImageProvide = new UIImageProvider(DocumentProperties, document.Layout.Settings);
            _uiImageProvide.DocumentPageImageRequested += _uiImageProvide_DocumentPageImageRequested;
            _settings = settings;
            _document = document;
            _zoomViewModel = new ZoomViewModel(0.1, document.DocumentProperties.Width, _settings.ZoomSettings);
            _zoomViewModel.ZoomPercentageChanged += ZoomViewModel_ZoomPercentageChanged;

            int previousBottom = 0;
            int currentBottom = 0;

            foreach (PageProperties pageProperties in document.DocumentProperties.PageProperties)
            {
                currentBottom += pageProperties.Height;
                RegionsViewModel regionViewModel = CreateRegionsViewModel(document, previousBottom, currentBottom);
                PagePropertiesViewModel pagePropertiesVM = documentPageImageProvider.GetPageProperties(pageProperties.PageIndex);
                _pages.Add(new PageViewModel(
                    pagePropertiesVM, regionViewModel, _zoomViewModel, documentPageImageProvider));

                previousBottom += pageProperties.Height;
            }

            _currentPage = _pages[0];
            _pageUP = new RelayCommand(pageUPHandler, CanExecutePageUp);
            _pageDown = new RelayCommand(pageDownHandler, CanExecutePageDown);
            _uiImageProvide.StartOrResumeProcessing();
        }

        private void _uiImageProvide_DocumentPageImageRequested(object sender, DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            if (DocumentPageImageRequested != null)
            {
                DocumentPageImageRequested(sender, documentProperties, pageIndex, imagePath, settings);
            }
        }

        ~DocumentViewModel()
        {
            this.Dispose();
        }
        private static RegionsViewModel CreateRegionsViewModel(VBotDocument document, int previousBottom, int currentBottom)
        {
            List<IRegion> regions = new List<IRegion>();

            if (document?.SirFields?.Fields != null)
            {
                IList<Field> sifs = (from q in document.SirFields.Fields where q.Bounds.Top >= previousBottom && q.Bounds.Top < currentBottom select q).ToList();
                IList<IRegion> sifRegions = (from q in sifs select (new SIFRegion(q) as IRegion)).ToList();
                regions.AddRange(sifRegions);
            }

            if (document?.Layout?.Fields != null)
            {
                IList<FieldLayout> layoutkeyFields = (from q in document.Layout.Fields where q.Bounds.Top >= previousBottom && q.Bounds.Top < currentBottom select q).ToList();
                IList<IRegion> keyFieldRegions = (from q in layoutkeyFields select (new FieldRegion(q) as IRegion)).ToList();
                regions.AddRange(keyFieldRegions);
            }

            if (document?.Data?.FieldDataRecord?.Fields != null)
            {
                IList<FieldData> valueFieldsForField = (from q in document.Data.FieldDataRecord.Fields where q.Value.Field.Bounds.Top >= previousBottom && q.Value.Field.Bounds.Top < currentBottom select q).ToList();
                IList<IRegion> valuFieldRegionsForField = (from q in valueFieldsForField select (new FieldDataRegion(q) as IRegion)).ToList();
                regions.AddRange(valuFieldRegionsForField);
            }

            if (document?.Layout?.Tables != null)
            {
                foreach (TableLayout tableLayout in document.Layout.Tables)
                {
                    if (tableLayout?.Columns != null)
                    {
                        IList<FieldLayout> layoutkeyColumns = (from q in tableLayout.Columns where q.Bounds.Top >= previousBottom && q.Bounds.Top < currentBottom select q).ToList();
                        IList<IRegion> keyColumnRegions = (from q in layoutkeyColumns select (new ColumnHeaderRegion(q) as IRegion)).ToList();
                        regions.AddRange(keyColumnRegions);
                    }
                }
            }

            if (document?.Data?.TableDataRecord != null)
            {
                foreach (TableValue table in document.Data.TableDataRecord)
                {
                    if (table?.Rows != null)
                    {
                        foreach (DataRecord row in table.Rows)
                        {
                            if (row?.Fields != null)
                            {
                                IList<FieldData> cellValueFields = (from q in row.Fields where q.Value.Field.Bounds.Top >= previousBottom && q.Value.Field.Bounds.Top < currentBottom select q).ToList();
                                IList<IRegion> cellValueFieldsRegions = (from q in cellValueFields select (new TableCellRegion(q) as IRegion)).ToList();
                                regions.AddRange(cellValueFieldsRegions);
                            }
                        }
                    }
                }
            }

            RegionsViewModel regionViewModel = new RegionsViewModel(regions);
            return regionViewModel;
        }

        #endregion ctor

        #region Public Method(s)

        public void GoToPage(int pageIndex)
        {
            if (pageIndex >= 0 && pageIndex < _pages.Count)
            {
                CurrentPage = _pages[pageIndex];
                if (PageChangeRequested != null)
                {
                    PageChangeRequestedArgs args = new PageChangeRequestedArgs(pageIndex);
                    PageChangeRequested(this, args);
                }
            }

            OnPropertyChanged(nameof(CurrentPage));
        }

        public void SelectValue(string id)
        {
            foreach (IRegion region in _selectedRegion)
            {
                region.IsSelected = false;
            }
            _selectedRegion.Clear();
            IRegion regionToSelect = null;
            foreach (PageViewModel page in _pages)
            {
                regionToSelect = page.RegionsViewModel.AllRegions.FirstOrDefault(x => x.Id == id);
                if (regionToSelect != null)
                {
                    GoToPage(page.Page.PageIndex);

                    regionToSelect.IsSelected = true;
                    _selectedRegion.Add(regionToSelect);
                    break;
                }
            }
            if (regionToSelect != null)
            {
                StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage("")); //TODO: Use/Implement StatusBarService.ClearMessage() method.
            }
            else
            {
                StatusBarService.ShowMessage<StringStatusBarMessage>(new StringStatusBarMessage(Automation.Cognitive.Validator.Properties.Resources.ValidatorNotAbleToSelectRegion));
            }
        }

        public void SetCurrentPage(double verticalOffset)
        {
            if (_zoomViewModel.ZoomPercentage != 0)
            {
                double effectiveOffset = verticalOffset / _zoomViewModel.ZoomPercentage;
                double spacebetweenTwoPages = 0;
                PageViewModel pvModel = _pages.Where(pgvm => ((pgvm.Page.Top + (spacebetweenTwoPages * pgvm.Page.PageIndex)) <= effectiveOffset)
                                                              && (((spacebetweenTwoPages * pgvm.Page.PageIndex) + pgvm.Page.Top + pgvm.Page.Height) > effectiveOffset)
                                                             ).FirstOrDefault();
                if (pvModel != null)
                {
                    var p = pvModel.Page;

                    Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                    {
                        if (!IsThisPageInViewPort(pvModel))
                            return;
                        //  Fields.Clear();

                        if (_uiImageProvide == null)
                            return;

                        _uiImageProvide?.PageProcessActionQueue.Clear();
                        foreach (var pageViewModel in _pages)
                        {
                            PageImageSource page = pageViewModel.Page;
                            if (page.PageIndex == (p.PageIndex - 2) ||
                                page.PageIndex == (p.PageIndex - 1)
                                    || (page.PageIndex == p.PageIndex)
                                    || (page.PageIndex == p.PageIndex + 1)
                                    || (page.PageIndex == p.PageIndex + 2)
                                    )
                            {
                                if (page.PageIndex == p.PageIndex && page.PageImage == null)
                                {
                                    proceessCurrentPageView(pageViewModel);
                                }

                                if (!IsThisPageInViewPort(pvModel))
                                    return;
                            }
                            else
                            {
                                if (page?.PageImage != null)
                                {
                                    page.PageImage.StreamSource.Dispose();// = null;
                                    page.PageImage = null;
                                    pageViewModel.RegionsViewModel.DetachRegionsFromUI();
                                }
                            }
                        }

                        CurrentPage = pvModel;
                    }), DispatcherPriority.Background);
                }
            }
        }

        private void proceessCurrentPageView(PageViewModel pageViewModel)
        {
            Dispatcher uiDispatche = Dispatcher.CurrentDispatcher;
            PageImageSource page = pageViewModel.Page;
            if (page.PageIndex > 1)
            {
                processPage(_pages[page.PageIndex - 2], uiDispatche);
            }
            if (page.PageIndex != 0)
            {
                processPage(_pages[page.PageIndex - 1], uiDispatche);
            }

            processPage(pageViewModel, uiDispatche);

            if (page.PageIndex != _pages.Count - 1)
            {
                processPage(_pages[page.PageIndex + 1], uiDispatche);
            }
            if (page.PageIndex < _pages.Count - 2)
            {
                processPage(_pages[page.PageIndex + 2], uiDispatche);
            }
            if (_uiImageProvide != null)
                _uiImageProvide.CurrentViewIndex = page.PageIndex == 0 ? 0 : page.PageIndex - 1;
        }

        private void processPage(PageViewModel pageVM, Dispatcher uiDispatche)
        {
            if (pageVM.Page.PageImage == null && _uiImageProvide != null)
            {
                _uiImageProvide.GetImage(pageVM.Page.PageIndex, (bitmapImage) =>
                {

                    uiDispatche.BeginInvoke(new Action(() =>
                    {
                        if (_isDisposedCalled == true)
                        {
                            VBotLogger.Trace(() => { return $"Disposed is already called so no need to paste image on UI for {pageVM?.Page?.PageImagePath}."; });
                            return;
                        }
                        try
                        {
                            if (_pages != null
                            && pageVM?.Page != null
                            && _pages.Count > (pageVM.Page.PageIndex))
                            {
                                if (!IsThisPageInViewPort(pageVM))
                                {
                                    return;
                                }

                                if (pageVM?.Page != null
                                && pageVM?.Page?.PageImage == null
                                && _uiImageProvide != null)
                                {

                                    pageVM.Page.PageImage = _uiImageProvide.GetImage(bitmapImage);
                                    pageVM.RegionsViewModel.AttachRegionsToUI();
                                    RefreshLoadingScreen();

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            VBotLogger.Warning(() => { return $"Unable to paste Image on UI for page {pageVM?.Page?.PageImagePath} ,Message :{ex.Message}."; });
                        }
                    }), DispatcherPriority.Background);
                });
            }
        }

        private bool IsThisPageInViewPort(PageViewModel pageVM)
        {
            if (pageVM?.Page == null)
            {
                return false;
            }
            return ViewRect.IntersectsWith(new Rect(0, pageVM.Page.Top, pageVM.Page.Width, pageVM.Page.Height));
        }

        internal void RefreshLoadingScreen()
        {
            List<PageImageSource> pages = _pages.ToList().Select(x => x.Page).ToList().FindAll(page => (ViewRect.IntersectsWith(new Rect(0, page.Top, page.Width, page.Height)) && page.PageImage == null));

            if (pages.Count > 0)
            {
                CurrentViewIsLoading = true;
            }
            else
            {
                CurrentViewIsLoading = false;
            }
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void ZoomViewModel_ZoomPercentageChanged(object sender, EventArgs e)
        {
            if (ZoomPercentageChanged != null)
            {
                ZoomPercentageChanged(this, EventArgs.Empty);
            }
        }

        private bool CanExecutePageUp(object param)
        {
            return CurrentPage?.Page != null && CurrentPage.Page.PageIndex > 0;
        }

        private void pageUPHandler(object param)
        {
            turnPage(-1);
        }

        private bool CanExecutePageDown(object param)
        {
            return CurrentPage?.Page != null && CurrentPage.Page.PageIndex < _pages.Count - 1;
        }

        private void pageDownHandler(object param)
        {
            turnPage(1);
        }

        private void turnPage(int offset)
        {
            if (_pages.Count > 0 && CurrentPage != null)
            {
                int currentIndex = _pages.IndexOf(CurrentPage);
                int newIndex = currentIndex + offset;
                GoToPage(newIndex);
            }
        }

        public void Dispose()
        {
            _isDisposedCalled = true;
            DocumentProperties = null;
            if (_uiImageProvide != null)
                _uiImageProvide.Dispose();
            _uiImageProvide = null;
            _settings = null;
            _document = null;

            _currentPage = null;
            _pageUP = null; ;
            _pageDown = null;
            foreach (PageViewModel pvm in _pages)
            {
                pvm.Dispose();
            }

            if (_zoomViewModel != null)
                _zoomViewModel.ZoomPercentageChanged -= ZoomViewModel_ZoomPercentageChanged;
            _zoomViewModel = null;
        }

        #endregion Private Method(s)
    }

    internal class PageChangeRequestedArgs : EventArgs
    {
        public int? PageIndex { get; private set; }

        public PageChangeRequestedArgs(int? pageIndex)
        {
            PageIndex = pageIndex;
        }
    }
}