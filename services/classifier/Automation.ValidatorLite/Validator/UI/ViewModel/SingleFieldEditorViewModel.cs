﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.VisionBotEngine.Model;
    using Cognitive.VisionBotEngine.Validation.Validation.Validators.Confidence;
    using Services.Client;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using ValidatorServices.MachineLearningServiceConsumer;

    internal class SingleFieldEditorViewModel : ValidatorViewModelBase, IDisposable
    {
        private FieldData _fieldData;
        private FieldLayout _fieldLayout;
        private ObservableCollection<RecommendationModel> _suggestion;

        #region Properties

        public string ValueId
        {
            get
            {
                return _fieldData.Id;
            }
        }

        public string KeyId
        {
            get
            {
                return _fieldLayout.Id;
            }
        }

        public string Name
        {
            get { return _fieldData.Field.Name; }
            set { _fieldData.Field.Name = value; }
        }

        public Rectangle ValueBound
        {
            get { return _fieldData.Value.Field.Bounds; }
            set
            {
                if (_fieldData.Value.Field.Bounds != value)
                {
                    _fieldData.Value.Field.Bounds = value;
                    OnPropertyChanged(nameof(ValueBound));
                }
            }
        }

        public Rectangle LabelBound
        {
            get { return _fieldLayout.Bounds; }
            set
            {
                if (_fieldLayout.Bounds != value)
                {
                    _fieldLayout.Bounds = value;
                    OnPropertyChanged(nameof(LabelBound));
                }
            }
        }

        //TODO: Require some abstraction here. We may have to handle Value of other types in addition to string value.
        public string Value
        {
            get { return _fieldData.Value.Field.Text; }
            set
            {
                if (_fieldData.Value.Field.Text != value)
                {
                    string oldValue = _fieldData.Value.Field.Text;
                    _fieldData.Value.Field.Text = value;
                    OnPropertyChanged(nameof(Value));
                    OnValueChanged(oldValue, value);
                }
            }
        }

        public ValidationIssue ValidationIssue
        {
            get { return _fieldData.ValidationIssue; }
            set
            {
                if (_fieldData.ValidationIssue != value)
                {
                    _fieldData.ValidationIssue = value;
                    OnPropertyChanged(nameof(ValidationIssue));
                }
            }
        }

        private bool _isSelected = false;

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }

            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                    OnSelectionChanged(value);
                }
            }
        }

        public bool IsRequired
        {
            get { return _fieldData.Field.IsRequired; }
        }

        public bool isRecommendedOnce
        {
            get; set;
        }

        public ObservableCollection<RecommendationModel> Suggestion
        {
            get { return this._suggestion; }
            set
            {
                if (value != null)
                {
                    _suggestion = value;
                    OnPropertyChanged(nameof(Suggestion));
                }
            }
        }

        public string FieldID
        {
            get { return _fieldData.Field.Id; }
        }

        #endregion Properties

        #region Events

        public event EventHandler<FieldEditorSelectionChangedArgs> SelectionChanged;

        public event EventHandler<FieldEditorValueChangedArgs> ValueChanged;

        #endregion Events

        #region ctor

        public SingleFieldEditorViewModel(FieldLayout fieldLayout, FieldData fieldData)
        {
            _fieldLayout = fieldLayout ?? new FieldLayout();
            _fieldData = fieldData;
            _suggestion = new ObservableCollection<RecommendationModel>();
        }

        ~SingleFieldEditorViewModel()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            _fieldLayout = null;
            _fieldData = null;
            _suggestion = null;
        }

        #endregion ctor

        #region Public Method(s)

        public void RefreshValidationIssue()
        {
            OnPropertyChanged(nameof(ValidationIssue));
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void OnValueChanged(string oldValue, string newValue)
        {
            if (ValueChanged != null)
            {
                _fieldData?.IgnoreLowConfidenceIssue();

                FieldEditorValueChangedArgs args = new FieldEditorValueChangedArgs(ValueId, oldValue, newValue);
                ValueChanged(this, args);
            }
        }

        private void OnSelectionChanged(bool value)
        {
            if (SelectionChanged != null)
            {
                FieldEditorSelectionChangedArgs args = new FieldEditorSelectionChangedArgs(ValueId, value);
                SelectionChanged(this, args);
            }
        }

        #endregion Private Method(s)
    }

    internal class FieldEditorSelectionChangedArgs : EventArgs
    {
        public string Id { get; private set; }

        public bool IsSelected { get; private set; }

        public FieldEditorSelectionChangedArgs(string id, bool isSelected)
        {
            Id = id;
            IsSelected = isSelected;
        }
    }

    internal class FieldEditorValueChangedArgs : EventArgs
    {
        public string Id { get; private set; }

        public string OldValue { get; private set; }

        public string NewValue { get; private set; }

        public FieldEditorValueChangedArgs(string id, string oldValue, string newValue)
        {
            Id = id;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}