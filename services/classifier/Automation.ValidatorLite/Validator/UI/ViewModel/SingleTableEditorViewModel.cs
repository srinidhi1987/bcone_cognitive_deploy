﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{

    using Automation.VisionBotEngine.Model;
    using Service;
    using System;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Windows.Input;
    using VisionBotEngine;

    internal class SingleTableEditorViewModel : ValidatorViewModelBase,IDisposable
    {
        private Layout _layout;
        private TableValue _table;
        private ObservableCollection<FieldDef> _headers;

        #region Properties

        private DataTable _dataTable;

        public DataTable DataTable
        {
            get
            {
                return _dataTable;
            }

            set
            {
                _dataTable = value;
            }
        }

        public ICommand AddRow { get; set; }

        public ICommand InsertRowAbove { get; set; }

        public ICommand InsertRowBelow { get; set; }

        public ICommand DeleteRow { get; set; }

        private int _optionalErrorCount;

        public int OptionalErrorCount
        {
            get { return _optionalErrorCount; }
            set { _optionalErrorCount = value; }
        }

        private int _errorCount;

        public int ErrorCount
        {
            get
            {
                return _errorCount;
            }

            set
            {
                if (_errorCount != value)
                {
                    _errorCount = value;
                    OnPropertyChanged(nameof(ErrorCount));
                }
            }
        }

        private string _tableName;

        public string TableName
        {
            get
            {
                return _tableName;
            }

            set
            {
                if (_tableName != value)
                {
                    _tableName = value;
                    OnPropertyChanged(nameof(TableName));
                }
            }
        }

        public int RowCount
        {
            get { return _dataTable.Rows.Count; }
        }

        #endregion Properties

        #region Event(s)

        public event EventHandler<FieldEditorSelectionChangedArgs> SelectionChanged;

        public event EventHandler<FieldEditorValueChangedArgs> CellValueChanged;

        public event EventHandler<RowInsertedArgs> RowInserted;

        public event EventHandler<RowDeletedArgs> RowDeleted;

        public event EventHandler<RowMovedArgs> RowMoved;

        #endregion Event(s)

        #region ctor

        public SingleTableEditorViewModel(Layout layout, TableValue table)
        {
            _layout = layout;
            _table = table;
            _tableName = table.TableDef.Name;
            _dataTable = new DataTable(table.TableDef.Id);

            //TODO: Following column is for showing DragHandle on UI. We should have proper DataType for it instead of string
            DataColumn column = new DataColumn();
            column.Caption = string.Empty;
            column.ColumnName = "draghandle";
            column.DataType = typeof(string);
            _dataTable.Columns.Add(column);

            foreach (FieldDef header in table.Headers)
            {
                column = new DataColumn();
                column.Caption = header.Name;
                column.ColumnName = header.Id;
                column.DataType = typeof(TableCellEditorViewModel);
                _dataTable.Columns.Add(column);
            }

            foreach (DataRecord dataRecord in table.Rows)
            {
                DataRow dataRow = DataRowFromDataRecord(dataRecord);
                _dataTable.Rows.Add(dataRow);
            }

            _headers = new ObservableCollection<FieldDef>();
            foreach (FieldDef headerField in table.Headers)
            {
                _headers.Add(headerField);
            }

            AddRow = new CognitiveData.VisionBot.UI.RelayCommand(OnAddRow);
            InsertRowAbove = new CognitiveData.VisionBot.UI.RelayCommand(OnInsertRowAbove);
            InsertRowBelow = new CognitiveData.VisionBot.UI.RelayCommand(OnInsertRowBelow);
            DeleteRow = new CognitiveData.VisionBot.UI.RelayCommand(OnDeleteRow);
        }
        ~SingleTableEditorViewModel()
        {
            this.Dispose();
        }
        public void Dispose()
        {
            _layout = null;
            _table = null;
            _tableName = null;
            _dataTable = null;
            _headers = null;
            AddRow = null;
            InsertRowAbove = null;
            InsertRowBelow = null;
            DeleteRow = null;
        }
        #endregion ctor

        #region Public Method(s)

        public void MoveItem(int fromIndex, int toIndex)
        {
            try
            {
                DataRow row = _dataTable.Rows[fromIndex];
                DataRow newRow = _dataTable.NewRow();
                newRow.ItemArray = row.ItemArray;
                _dataTable.Rows.Remove(row);
                _dataTable.Rows.InsertAt(newRow, toIndex);
                DataRecord recordToMove = _table.Rows[fromIndex];
                _table.Rows.Remove(recordToMove);
                _table.Rows.Insert(toIndex, recordToMove);
                if (RowMoved != null)
                {
                    RowMovedArgs args = new RowMovedArgs(recordToMove, fromIndex, toIndex);
                    RowMoved(this, args);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(SingleTableEditorViewModel), nameof(MoveItem));
                //throw;
            }
        }

        public void RefreshValidationIssue()
        {
            if (_dataTable?.Rows != null)
            {
                foreach (DataRow row in _dataTable.Rows)
                {
                    foreach (object cellVMTemp in row.ItemArray)
                    {
                        TableCellEditorViewModel cellVM = cellVMTemp as TableCellEditorViewModel;
                        if (cellVM != null)
                        {
                            cellVM.RefreshValidationIssue();
                        }
                    }
                }
            }
        }

        public void UpdateValidationIssueCount()
        {
            int errorCount = 0;
            int optionalErrorCount = 0;

            if (_table?.Rows != null)
            {
                foreach (DataRecord dataRecord in _table.Rows)
                {
                    for (int i = 0; i < dataRecord.Fields.Count; i++)
                    {
                        if (dataRecord.Fields[i].ValidationIssue != null)
                        {
                            errorCount++;

                            if (dataRecord.Fields[i].ValidationIssue.IssueCode.Equals(FieldDataValidationIssueCodes.ConfidenceIssue))
                            {
                                optionalErrorCount++;
                            }
                        }
                    }
                }
            }

            ErrorCount = errorCount;
            OptionalErrorCount = optionalErrorCount;
        }

        public FieldDef GetHeader(string fieldDefID)
        {
            FieldDef headerDef = (from q in _headers where q.Id == fieldDefID select q).FirstOrDefault();
            return headerDef;
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private DataRow DataRowFromDataRecord(DataRecord dataRecord)
        {
            DataRow dataRow = null;
            try
            {
                dataRow = _dataTable.NewRow();
                object[] cells = new object[dataRecord.Fields.Count + 1];
                cells[0] = "Image";
                for (int i = 0; i < dataRecord.Fields.Count; i++)
                {
                    int indexOfColumn = _dataTable.Columns.IndexOf(dataRecord.Fields[i].Field.Id);
                    if (indexOfColumn > -1)
                    {
                        var tableCell = new TableCellEditorViewModel(null, dataRecord.Fields[i]);
                        tableCell.ValueChanged += TableCell_ValueChanged;
                        tableCell.SelectionChanged += TableCell_SelectionChanged;
                        cells[indexOfColumn] = tableCell;
                    }
                    else
                    {
                        VBotLogger.Error(() => $"[{nameof(SingleTableEditorViewModel)}.{nameof(DataRowFromDataRecord)}] Could not find column with id {dataRecord.Fields[i]} in datatable.");
                    }
                }
                dataRow.ItemArray = cells;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(SingleTableEditorViewModel), nameof(DataRowFromDataRecord));
                //throw;
            }

            return dataRow;
        }

        private void OnInsertRowAbove(object obj)
        {
            DataRow selectedRow = obj as DataRow;

            int index = _dataTable.Rows.IndexOf(selectedRow);

            DataRow newRow = getNewRow();

            _dataTable.Rows.InsertAt(newRow, index);
            addDataRowInTableValue(index, newRow);
        }

        private void addDataRowInTableValue(int? index, DataRow newRow)
        {
            DataRecord dataRecord = DataRecordFromDataRow(newRow);
            if (index.HasValue)
            {
                _table.Rows.Insert(index.Value, dataRecord);
            }
            else
            {
                _table.Rows.Add(dataRecord);
            }
            OnPropertyChanged(nameof(RowCount));
            OnRowInserted(dataRecord, index.HasValue ? index.Value : _table.Rows.Count - 1);
        }

        private DataRecord DataRecordFromDataRow(DataRow dataRow)
        {
            DataRecord dataRecord = new DataRecord();

            for (int i = 0; i < _headers.Count; i++)
            {
                TableCellEditorViewModel cellEitorVM = (from q in dataRow.ItemArray where (q is TableCellEditorViewModel && ((TableCellEditorViewModel)q).FieldData?.Field?.Id == _headers[i].Id) select (q as TableCellEditorViewModel)).FirstOrDefault();
                if (cellEitorVM?.FieldData != null)
                {
                    dataRecord.Fields.Add(cellEitorVM.FieldData);
                }
                else
                {
                    dataRecord.Fields.Add(new FieldData() { Field = _headers[i] });
                    VBotLogger.Error(() => $"[{nameof(DataRecordFromDataRow)}] did not find TableCellEditorViewModel for id {_headers[i].Id}");
                }
            }

            return dataRecord;
        }

        private DataRow getNewRow()
        {
            DataRow newRow = _dataTable.NewRow();
            object[] cells = new object[_headers.Count + 1];
            cells[0] = "Image";
            for (int i = 0; i < _headers.Count; i++)
            {
                int columnIndex = _dataTable.Columns.IndexOf(_headers[i].Id);
                if (columnIndex > -1)
                {
                    FieldData fData = new FieldData();
                    fData.Field = _headers[i];
                    fData.Value = new TextValue();
                    fData.Value.Field.Confidence = VisionBotEngine.Configuration.Constants.CDC.MAX_CONFIDENCE_THRESHOLD;
                    var tableCell = new TableCellEditorViewModel(null, fData);
                    tableCell.ValueChanged += TableCell_ValueChanged;
                    tableCell.SelectionChanged += TableCell_SelectionChanged;
                    cells[columnIndex] = tableCell;
                }
                else
                {
                    VBotLogger.Error(() => $"[{nameof(getNewRow)}] Could not find column with id {_headers[i].Id} in datatable.");
                }
            }

            newRow.ItemArray = cells;
            return newRow;
        }

        private void TableCell_ValueChanged(object sender, FieldEditorValueChangedArgs e)
        {
            OnCellValueChanged(sender, e);
        }

        private void OnInsertRowBelow(object obj)
        {
            DataRow selectedRow = obj as DataRow;

            int index = _dataTable.Rows.IndexOf(selectedRow);

            DataRow newRow = getNewRow();

            _dataTable.Rows.InsertAt(newRow, index + 1);
            addDataRowInTableValue(index + 1, newRow);
        }

        private void OnDeleteRow(object obj)
        {
            if (this.RowCount == 1)
            {
                foreach(FieldDef fieldDef in this._headers)
                {
                    if (fieldDef.IsRequired == true)
                    {
                        StatusBarService.ShowMessage<StringStatusBarMessage>(
                            new StringStatusBarMessage(
                                Automation.Cognitive.Validator.Properties.Resources.TableFieldMandatory));
                        return;
                    }
                }
            }
            DataRow selectedRow = obj as DataRow;
            int index = _dataTable.Rows.IndexOf(selectedRow);
            _dataTable.Rows.Remove(selectedRow);
            DataRecord dataRocordToDelete = _table.Rows[index];
            _table.Rows.RemoveAt(index);
            OnPropertyChanged(nameof(RowCount));
            OnRowDeleted(dataRocordToDelete, index);
        }

        private void TableCell_SelectionChanged(object sender, FieldEditorSelectionChangedArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(sender, e);
            }
        }

        private void OnAddRow(Object para)
        {
            DataRow newRow = getNewRow();
            _dataTable.Rows.Add(newRow);
            addDataRowInTableValue(null, newRow);
        }

        private void OnCellValueChanged(object sender, FieldEditorValueChangedArgs args)
        {
            if (CellValueChanged != null)
            {
                CellValueChanged(sender, args);
            }
        }

        private void OnRowDeleted(DataRecord dataRow, int index)
        {
            if (RowDeleted != null)
            {
                RowDeletedArgs args = new RowDeletedArgs(dataRow, index);
                RowDeleted(this, args);
            }
        }

        private void OnRowInserted(DataRecord dataRow, int index)
        {
            if (RowInserted != null)
            {
                RowInsertedArgs args = new RowInsertedArgs(dataRow, index);
                RowInserted(this, args);
            }
        }

        #endregion Private Method(s)
    }

    internal class RowInsertedArgs : EventArgs
    {
        public DataRecord InsertedRow { get; private set; }
        public int Index { get; private set; }

        public RowInsertedArgs(DataRecord insertedRow, int index)
        {
            InsertedRow = insertedRow;
            Index = index;
        }
    }

    internal class RowDeletedArgs : EventArgs
    {
        public DataRecord DeletedRow { get; private set; }
        public int Index { get; private set; }

        public RowDeletedArgs(DataRecord deletedRow, int index)
        {
            DeletedRow = deletedRow;
            Index = index;
        }
    }

    internal class RowMovedArgs : EventArgs
    {
        public DataRecord Row { get; private set; }
        public int FromIndex { get; private set; }

        public int ToIndex { get; private set; }

        public RowMovedArgs(DataRecord row, int fromIndex, int toIndex)
        {
            Row = row;
            FromIndex = fromIndex;
            ToIndex = toIndex;
        }
    }
}