﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Validator.Validator.UI.ViewModel
{
    internal class InvalidReasonModel
    {
        public string Id { get; set; }

        public string Text { get; set; }

        public bool IsSelected { get; set; }
    }
}
