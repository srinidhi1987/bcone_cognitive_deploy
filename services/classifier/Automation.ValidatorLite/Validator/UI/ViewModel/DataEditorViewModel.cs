﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.VisionBotEngine.Model;
    using System;

    internal class DataEditorViewModel : ValidatorViewModelBase,IDisposable
    {
        #region Propertie(s)

        private FieldsEditorViewModel _fieldEditorViewModel;

        public FieldsEditorViewModel FieldEditorViewModel
        {
            get
            {
                return _fieldEditorViewModel;
            }
            set
            {
                _fieldEditorViewModel = value;
            }
        }

        private TablesEditorViewModel _tableEditorViewModel;

        public TablesEditorViewModel TableEditorViewModel
        {
            get
            {
                return _tableEditorViewModel;
            }

            set
            {
                _tableEditorViewModel = value;
            }
        }

        private int _errorCount;

        public int ErrorCount
        {
            get
            {
                return _errorCount;
            }

            set
            {
                if (_errorCount != value)
                {
                    _errorCount = value;
                    OnPropertyChanged(nameof(ErrorCount));
                    StatusBarService.ShowErrorCount(_errorCount);
                }
            }
        }

        private int _optionalErrorCount;
        public int OptionalErrorCount
        {
            get { return _optionalErrorCount; }

            set
            {
                if (_optionalErrorCount != value)
                {
                    _optionalErrorCount = value;
                    OnPropertyChanged(nameof(OptionalErrorCount));
                    StatusBarService.ShowErrorCount(_errorCount);
                }
            }
        }

        private bool _isOnline;
        public bool IsOnline
        {
            get
            {
                return _isOnline;
            }
            set
            {
                if(_isOnline != value)
                {
                    _isOnline = value;
                    OnPropertyChanged(nameof(IsOnline));
                }
            }
        }

        #endregion Propertie(s)

        #region Event(s)

        public event EventHandler FieldValueChanged;

        public event EventHandler CellValueChanged;

        public event EventHandler<RowInsertedArgs> RowInserted;

        public event EventHandler<RowDeletedArgs> RowDeleted;

        public event EventHandler RowMoved;

        #endregion Event(s)

        #region ctor

        public DataEditorViewModel(VBotDocument vbotDocument)
        {
            IsOnline = true;
            _fieldEditorViewModel = new FieldsEditorViewModel(vbotDocument?.Layout, vbotDocument?.Data?.FieldDataRecord?.Fields);
            _fieldEditorViewModel.FieldValueChanged += _fieldEditorViewModel_FieldValueChanged;

            _tableEditorViewModel = new TablesEditorViewModel(vbotDocument?.Layout, vbotDocument?.Data?.TableDataRecord);
            _tableEditorViewModel.CellValueChanged += TableEditorViewModel_CellValueChanged;
            _tableEditorViewModel.RowDeleted += TableEditorViewModel_RowDeleted;
            _tableEditorViewModel.RowInserted += TableEditorViewModel_RowInserted;
            _tableEditorViewModel.RowMoved += TableEditorViewModel_RowMoved;
            UpdateValidationIssueCount();
        }
        public void Dispose()
        {
            if (_fieldEditorViewModel != null)
            {
                _fieldEditorViewModel.FieldValueChanged -= _fieldEditorViewModel_FieldValueChanged;
                _fieldEditorViewModel.Dispose();
                _fieldEditorViewModel = null;
            }
            
            if (_tableEditorViewModel != null)
            {
                _tableEditorViewModel.CellValueChanged -= TableEditorViewModel_CellValueChanged;
                _tableEditorViewModel.RowDeleted -= TableEditorViewModel_RowDeleted;
                _tableEditorViewModel.RowInserted -= TableEditorViewModel_RowInserted;
                _tableEditorViewModel.RowMoved -= TableEditorViewModel_RowMoved;
                _tableEditorViewModel.Dispose();
                _tableEditorViewModel = null;
                
            }
        }
        ~DataEditorViewModel()
        {
            this.Dispose();
        }
        #endregion ctor

        #region Public Method(s)

        public void RefreshValidationIssue()
        {
            _fieldEditorViewModel.RefreshValidationIssue();
            _tableEditorViewModel.RefreshValidationIssue();
        }

        public void UpdateValidationIssueCount()
        {
            _fieldEditorViewModel.UpdateValidationIssueCount();
            _tableEditorViewModel.UpdateValidationIssueCount();
            int errorCount = _fieldEditorViewModel.ErrorCount + _tableEditorViewModel.ErrorCount;
            ErrorCount = errorCount;
            OptionalErrorCount = _fieldEditorViewModel.OptionalErrorCount + _tableEditorViewModel.OptionalErrorCount;
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void TableEditorViewModel_RowInserted(object sender, RowInsertedArgs e)
        {
            OnRowInserted(sender, e);
        }

        private void TableEditorViewModel_RowDeleted(object sender, RowDeletedArgs e)
        {
            OnRowDeleted(sender, e);
            RefreshValidationIssue();
            UpdateValidationIssueCount();
        }

        private void TableEditorViewModel_RowMoved(object sender, EventArgs e)
        {
            OnRowMoved(sender, e);
        }

        private void TableEditorViewModel_CellValueChanged(object sender, EventArgs e)
        {
            OnCellValueChanged(sender, e);
        }

        private void _fieldEditorViewModel_FieldValueChanged(object sender, EventArgs e)
        {
            OnFieldValueChanged(sender, e);
        }

        private void OnFieldValueChanged(object sender, EventArgs e)
        {
            if (FieldValueChanged != null)
            {
                FieldValueChanged(sender, e);
            }
        }

        private void OnCellValueChanged(object sender, EventArgs e)
        {
            if (CellValueChanged != null)
            {
                CellValueChanged(sender, e);
            }
        }

        private void OnRowInserted(object sender, RowInsertedArgs e)
        {
            if (RowInserted != null)
            {
                RowInserted(sender, e);
            }
        }

        private void OnRowDeleted(object sender, RowDeletedArgs e)
        {
            if (RowDeleted != null)
            {
                RowDeleted(sender, e);
            }
        }

        private void OnRowMoved(object sender, EventArgs e)
        {
            if (RowMoved != null)
            {
                RowMoved(sender, e);
            }
        }

        
        #endregion Private Method(s)
    }
}