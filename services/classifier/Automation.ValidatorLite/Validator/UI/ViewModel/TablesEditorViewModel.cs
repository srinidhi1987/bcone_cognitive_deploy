﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    internal class TablesEditorViewModel : ValidatorViewModelBase,IDisposable
    {
        private Layout _layout;
        private List<TableValue> _tables;

        #region Properties

        private ObservableCollection<SingleTableEditorViewModel> _singleTableEditorViewModels;

        public ObservableCollection<SingleTableEditorViewModel> SingleTableEditorViewModels
        {
            get
            {
                return _singleTableEditorViewModels;
            }

            set
            {
                _singleTableEditorViewModels = value;
            }
        }

        private int _errorCount;

        public int ErrorCount
        {
            get
            {
                return _errorCount;
            }

            set
            {
                if (_errorCount != value)
                {
                    _errorCount = value;
                    OnPropertyChanged(nameof(ErrorCount));
                }
            }
        }

        private int _optionalErrorCount;

        public int OptionalErrorCount
        {
            get
            {
                return _optionalErrorCount;
            }

            set
            {
                if (_optionalErrorCount != value)
                {
                    _optionalErrorCount = value;
                    OnPropertyChanged(nameof(OptionalErrorCount));
                }
            }
        }


        #endregion Properties

        #region Event(s)

        public event EventHandler<FieldEditorSelectionChangedArgs> SelectionChanged;

        public event EventHandler<FieldEditorValueChangedArgs> CellValueChanged;

        public event EventHandler<RowInsertedArgs> RowInserted;

        public event EventHandler<RowDeletedArgs> RowDeleted;

        public event EventHandler<RowMovedArgs> RowMoved;

        #endregion Event(s)

        #region ctor

        public TablesEditorViewModel(Layout layout, List<TableValue> tables)
        {
            _layout = layout;
            _tables = tables;
            _singleTableEditorViewModels = new ObservableCollection<ViewModel.SingleTableEditorViewModel>();
            foreach (TableValue table in tables)
            {
                var tableViewModel = new SingleTableEditorViewModel(layout, table);
                tableViewModel.CellValueChanged += TableViewModel_CellValueChanged;
                tableViewModel.SelectionChanged += TableViewModel_SelectionChanged;
                tableViewModel.RowInserted += TableViewModel_RowInserted;
                tableViewModel.RowDeleted += TableViewModel_RowDeleted;
                tableViewModel.RowMoved += TableViewModel_RowMoved;
                _singleTableEditorViewModels.Add(tableViewModel);
            }
        }
        ~TablesEditorViewModel()
        {
            this.Dispose();
        }
        public void Dispose()
        {
            _layout = null;

            _singleTableEditorViewModels = null;

            _tables = null;
        }
        #endregion ctor

        #region Public Method(s)

        public void RefreshValidationIssue()
        {
            foreach (SingleTableEditorViewModel tableVM in _singleTableEditorViewModels)
            {
                tableVM.RefreshValidationIssue();
            }
        }

        public void UpdateValidationIssueCount()
        {
            int errorCount = 0;
            int optionalErrorCount = 0;

            foreach (SingleTableEditorViewModel singleTableVM in _singleTableEditorViewModels)
            {
                singleTableVM.UpdateValidationIssueCount();
                if (singleTableVM.RowCount == 0)
                {
                    singleTableVM.ErrorCount = 0;
                    singleTableVM.OptionalErrorCount = 0;
                }

                errorCount += singleTableVM.ErrorCount;
                optionalErrorCount += singleTableVM.OptionalErrorCount;
            }

            ErrorCount = errorCount;
            OptionalErrorCount = optionalErrorCount;
        }

        #endregion Public Method(s)

        #region Private Method(s)

        private void TableViewModel_RowMoved(object sender, RowMovedArgs e)
        {
            if (RowMoved != null)
            {
                RowMoved(sender, e);
            }
        }

        private void TableViewModel_RowDeleted(object sender, RowDeletedArgs e)
        {
            if (RowDeleted != null)
            {
                RowDeleted(sender, e);
            }
        }

        private void TableViewModel_RowInserted(object sender, RowInsertedArgs e)
        {
            if (RowInserted != null)
            {
                RowInserted(sender, e);
            }
        }

        private void TableViewModel_CellValueChanged(object sender, FieldEditorValueChangedArgs e)
        {
            OnCellValueChanged(sender, e);
        }

        private void TableViewModel_SelectionChanged(object sender, FieldEditorSelectionChangedArgs e)
        {
            OnSelectionChanged(sender, e);
        }

        private void OnSelectionChanged(object sender, FieldEditorSelectionChangedArgs args)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(sender, args);
            }
        }

        private void OnCellValueChanged(object sender, FieldEditorValueChangedArgs args)
        {
            if (CellValueChanged != null)
            {
                CellValueChanged(sender, args);
            }
        }

        

        #endregion Private Method(s)
    }
}