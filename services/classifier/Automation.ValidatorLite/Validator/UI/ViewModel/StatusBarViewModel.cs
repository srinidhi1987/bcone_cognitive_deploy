﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.UI.Contract;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    //using Automation.CognitiveData.Validator.Service;
    using System;

    internal class StatusBarViewModel : ViewModelBase
    {
        #region Properties

        private IStatusBarMessage _message;

        public IStatusBarMessage Message
        {
            get
            {
                return _message;
            }

            private set
            {
                if (_message != value)
                {
                    _message = value;
                    OnPropertyChanged(nameof(Message));
                }
            }
        }

        private int _errorCount;

        public int ErrorCount
        {
            get
            {
                return _errorCount;
            }
            private set
            {
                if(_errorCount != value)
                {
                    _errorCount = value;
                    OnPropertyChanged(nameof(ErrorCount));
                }
            }
        }
        private int _correctedCount;

        public int CorrectedCount
        {
            get
            {
                return _correctedCount;
            }
            private set
            {
                if (_correctedCount != value)
                {
                    _correctedCount = value;
                    OnPropertyChanged(nameof(CorrectedCount));
                }
            }
        }
        private string _userName;

        public string UserName
        {
            get
            {
                return _userName;
            }
            private set
            {
                if (_userName != value)
                {
                    _userName = value;                    
                }
            }
        }

        private bool _isOnline;

        public bool IsOnline
        {
            get
            {
                return _isOnline;
            }
            private set
            {
                if (_isOnline != value)
                {
                    _isOnline = value;
                    OnPropertyChanged(nameof(IsOnline));
                }
            }
        }
        public void Clear()
        {
            Message = null;
            ErrorCount = 0;
            CorrectedCount = 0;
            IsOnline = false;
            UserName = null;
        }

        public void ShowMessage<T>(T message) where T : IStatusBarMessage
        {
            Message = message;
        }
        public void ShowErrorCount(int errorCount)
        {
            ErrorCount = errorCount;
        }
        public void ShowCorrectedCount(int correctedCount)
        {
            CorrectedCount = correctedCount;
        }
        public void SetUserName(string userName)
        {
            _userName = userName;
        }
        public void SetIsOnline(bool isOnline)
        {
            IsOnline = isOnline;
        }
        #endregion
    }
}