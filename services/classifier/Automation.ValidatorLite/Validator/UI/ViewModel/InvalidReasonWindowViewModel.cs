﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System.Collections;
using System.Collections.Generic;

namespace Automation.Validator.Validator.UI.ViewModel
{
    internal class InvalidReasonWindowViewModel
    {
        public string OrganizationId { get; set; }

        public string ProjectId { get; set; }

        public string FileId { get;  set; }

        public IList<InvalidReasonModel> ReasonList { get; set; }

        public bool IsCancelled { get; set; }
    }
}
