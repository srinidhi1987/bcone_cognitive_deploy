﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Validator.UI.ViewModel
{
    using Automation.CognitiveData.Validator.Engine;
    //using Automation.Integrator.CognitiveDataAutomation.Validator.Model;
    using Automation.ValidatorLite.IntegratorCommon.Model;
    using Contract;
    using System;
    using System.ComponentModel;
    using System.Windows.Threading;

    internal class ValidatorWindowViewModel : ValidatorViewModelBase
    {
        private Dispatcher _uiDispatcher;

        private ValidatorWorkflowEngine _engine;

        public event EventHandler CloseRequested;

        #region Properties

        private ValidatorViewModel _validatorVM;

        public bool CloseWithoutMessage { get; set; } = false;

        public ValidatorViewModel ValidatorVM
        {
            get
            {
                return _validatorVM;
            }

            set
            {
                if (_validatorVM != value)
                {
                    _validatorVM = value;
                    OnPropertyChanged(nameof(ValidatorVM));
                }
            }
        }
        private BackgroundWorker _backgroundWorker_FileAvailability;
        #endregion Properties

        #region ctor

        public ValidatorWindowViewModel(QSetting qSetting, ValidatorWorkflowEngine engine, ValidatorSettings validatorSettings)
        {
            _engine = engine;
            _uiDispatcher = Dispatcher.CurrentDispatcher;
            _backgroundWorker_FileAvailability = new BackgroundWorker();
            _backgroundWorker_FileAvailability.WorkerSupportsCancellation = true;
            _backgroundWorker_FileAvailability.DoWork += new DoWorkEventHandler(backgroundWorker_FileAvailability_DoWork);
            _backgroundWorker_FileAvailability.RunWorkerAsync();
            _backgroundWorker_FileAvailability.RunWorkerCompleted += _backgroundWorker_FileAvailability_RunWorkerCompleted;

            _validatorVM = new ValidatorViewModel(engine, qSetting, validatorSettings);
            _validatorVM.CloseRequested += validatorVM_CloseRequested;
        }

        private void validatorVM_CloseRequested(object sender, EventArgs e)
        {
            if(this.CloseRequested != null)
            {
                CloseWithoutMessage = true;
                CloseRequested(sender, e);
            }
        }

        private void backgroundWorker_FileAvailability_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = (BackgroundWorker)sender;
            while (!bw.CancellationPending)
            {
                Worker();
            }
            e.Cancel = true;
            //_resetEvent.Set();
        }

        private void _backgroundWorker_FileAvailability_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _backgroundWorker_FileAvailability.Dispose();
        }
        bool allowedNext = true;
        private void Worker()
        {
            while (true)
            {
                try
                {
                    if (_validatorVM != null && allowedNext && _validatorVM.IsValidatorEmpty())
                    {
                        allowedNext = false;
                        _uiDispatcher.Invoke(new System.Action(() =>
                        {
                            _validatorVM.NextWhenEmpty();

                        }));
                        //_validatorVM.NextWhenEmpty();
                        allowedNext = true;
                    }
                }
                finally
                {
                    System.Threading.Thread.Sleep(60000);
                }
            }
        }

        #endregion ctor
    }
}