﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{
    using Automation.CognitiveData.Validator.Engine;
    using Services.Client;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Validator.Service;
    using Validator.UI.Contract;
    using Validator.UI.Service;
    using Validator.UI.ViewModel;
    using ValidatorLite.IntegratorCommon.Model;
    using VisionBotEngine;
    using VisionBotEngine.Model;

    public sealed class VisionBotValidatorExecutor// : IVisionBotValidatorExecutor
    {
        private static ValidatorQSettingEngine validatorQSettingEngine;

        public static ValidatorQSettingEngine GetValidatorQSettingEngine
        {
            get
            {
                return validatorQSettingEngine;
            }
            private set { }
        }

        //public VisionBotValidatorExecutor()
        //{
        //    ValidatorViewModel.QueueReconnected += QSettingService.ValidatorViewModel_QueueReconnected;
        //    validatorQSettingEngine = new ValidatorQSettingEngine();
        //}
        private static void initializingLoggingMechanism()
        {
            string logDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\Automation Anywhere IQBot Platform\\Logs\\";
            string servicConfigurationPath = "CognitiveServiceConfiguration.json";
            string path = Path.Combine(
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), servicConfigurationPath);
            string json = File.ReadAllText(path);

            CognitiveServicesConfiguration _cognitiveServicesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeObject<CognitiveServicesConfiguration>(json);
            //if (Directory.Exists("D:\\"))
            //{
            //    logDir = "D:\\" + logFolderName;

            //}
            //else
            //{
            //    logDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), logFolderName);

            //}

            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }
            logDir = Path.Combine(logDir, "Validator.log");
            VBotLogger.Initlization(logDir, _cognitiveServicesConfiguration.LoggerSettings);
        }

        public VisionBotValidatorExecutor(string orgid, string projectid, string projectname, string userid, string tocken)
        {
            initializingLoggingMechanism();
            QSetting qsetting = new QSetting
            {
                ProjectId = projectid,
                ProjectName = projectname,
                UserName = userid,
                UserToken = tocken,
                OrganizationId = orgid
            };

            ValidatorViewModel.QueueReconnected += QSettingService.ValidatorViewModel_QueueReconnected;
            validatorQSettingEngine = new ValidatorQSettingEngine(qsetting);
        }

        ~VisionBotValidatorExecutor()
        {
            ValidatorViewModel.QueueReconnected -= QSettingService.ValidatorViewModel_QueueReconnected;
        }

        //public Task<QSettingInitResponse> GetValidatorQueues(QSettingInitRequest initRequest)
        //{
        //    return validatorQSettingEngine.Init(initRequest);
        //}

        //public void AddObserversToEngine(IEnumerable<IObserver<QSetting>> listOfObservers)
        //{
        //    validatorQSettingEngine.SetupComponents(null, listOfObservers);
        //}

        //public Task<QSettingAddResponse> AddNewQueue(QSettingAddRequest addRequest)
        //{
        //    return validatorQSettingEngine.Add(addRequest);
        //}

        public bool OpenQueue(string name, string organizationid, string projectname, string projectid, string username, string tocken)
        {
            try
            {
                VBotLogger.Trace(() => string.Format("[VisionBotValidatorExecutor -> OpenQueue] orgId: {0} , projectName: {1} , projectId: {2} , userName:{3}", organizationid, projectname, projectid, username));

                IProjectServiceEndPointConsumer projectService = new ProjectServiceEndPointConsumer();
                ProjectDetails projectDetails = projectService.GetProjectDetails(organizationid, projectid);
                if (projectDetails == null)
                {
                    return false;
                }
                IValidatorUIService validatorUIService = new ValidatorUIService();
                QSetting qSetting = new QSetting();
                qSetting.QueuePath = new QPath();
                qSetting.QueuePermission = new QPermission();
                qSetting.QueuePath.ServerPath = name;
                qSetting.ProjectId = projectid;
                qSetting.ProjectName = projectDetails.name;
                qSetting.ConfidenceThreshold = projectDetails.ConfidenceThreshold;
                qSetting.UserName = username;
                qSetting.UserToken = tocken;
                qSetting.OrganizationId = organizationid;
                validatorUIService.OpenValidator(qSetting);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public Task<QSettingDeleteResponse> DeleteQueue(QSettingDeleteRequest deleteRequest)
        //{
        //    return validatorQSettingEngine.Delete(deleteRequest);
        //}

        //public System.Windows.Forms.Control GetValidatorStatusControl()
        //{
        //    IQBotStatusBoard board = new IQBotStatusBoard();

        //    ElementHost host = new ElementHost();
        //    host.Location = new System.Drawing.Point(18, 251);
        //    host.Size = new System.Drawing.Size(600, 200);
        //    host.Dock = System.Windows.Forms.DockStyle.Bottom;
        //    host.Child = board;
        //    host.BackColorTransparent = true;
        //    return host;
        //}
    }
}