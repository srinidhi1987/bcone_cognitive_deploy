namespace Automation.CognitiveData.Api
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Threading.Tasks;

    using Automation.CognitiveData.DataAccess.DomainModel;
    using Automation.CognitiveData.DataAccess.UnitOfWork;
    using Automation.CognitiveData.Shared;
    using Automation.Integrator.CognitiveDataAutomation;

    using MySql.Data.MySqlClient;

    /// <summary>
    /// Represents VisionBot API
    /// </summary>
    public class VisionBotManager : IVisionBotManager
    {
        private readonly IVisionBotUnitOfWork visionBotUnitOfWork;

        private Action<Exception> exceptionHandler; 

        #region Constructor
        internal VisionBotManager(IVisionBotUnitOfWork visionBotUnitOfWork)
        {
            if (visionBotUnitOfWork == null)
            {
                throw new ArgumentNullException("visionBotUnitOfWork");
            }
            this.visionBotUnitOfWork = visionBotUnitOfWork;
            this.exceptionHandler = this.HandleException;
        }

        private void HandleException(Exception exception)
        {
            if (exception is MySqlException )
            {
                
            }

            else if (exception is DbUpdateConcurrencyException)
            {
                
            }
        }

        public VisionBotManager()
        {
            IocConfig.RegisterDependencies();
            this.visionBotUnitOfWork = IocConfig.Container.GetInstance<IVisionBotUnitOfWork>();
            this.exceptionHandler = this.HandleException;
        } 
        #endregion

        #region IVisionBotManager Implementation
        /// <summary>
        /// Gets the list of form templates in async.
        /// </summary>
        /// <returns></returns>
        public Task<CognitiveDataResponse<IEnumerable<FormTemplate>>> GetFormTemplatesAsync()
        {
            return Task.Factory.StartNew(
                () =>
                {
                    var response = new CognitiveDataResponse<IEnumerable<FormTemplate>>();
                    try
                    {

                        var formTemplates = this.visionBotUnitOfWork.FormTemplateRepository.RetrieveAllRecordsAsync().Result;

                        response.Response = formTemplates.ToList();
                    }

                    catch (MySqlException mySqlException)
                    {
                        response.ApplicationException = new ApplicationException(mySqlException.Message, mySqlException);
                    }

                    catch (DbException dbException)
                    {
                        response.ApplicationException = new ApplicationException(dbException.Message, dbException);
                    }
                    catch (AggregateException aggregateException)
                    {
                        response.ApplicationException = new ApplicationException(aggregateException.Flatten().Message, aggregateException.Flatten());
                    }
                    catch (Exception exception)
                    {

                        response.ApplicationException = new ApplicationException(exception.Message, exception);
                        this.exceptionHandler(exception);

                    }

                    return response;
                });
        }

        /// <summary>
        /// Saves the form template async.
        /// </summary>
        /// <param name="formTemplate">The form template.</param>
        /// <returns></returns>
        public Task<CognitiveDataResponse<bool>> SaveFormTemplateAsync(FormTemplate formTemplate)
        {
            return Task.Factory.StartNew(
                  () =>
                  {
                      var response = new CognitiveDataResponse<bool>
                      {
                          ApplicationException = null,
                          Response = false
                      }; 
                      try
                      {
                          var templateToBeEdited = this.visionBotUnitOfWork.FormTemplateRepository.GetByIdAsync(formTemplate.Id).Result;
                          var isFormTemplateNew = templateToBeEdited == null;

                          if (isFormTemplateNew)
                          {
                              this.visionBotUnitOfWork.FormTemplateRepository.Insert(formTemplate);


                          }
                          else   // existing template must be updated and form fields history must be inserted.
                          {

                              if (this.ShouldWriteInHistory(templateToBeEdited.FormFields, formTemplate.FormFields))
                              {
                                  formTemplate.CurrentVersion = templateToBeEdited.CurrentVersion + 1;
                                  //TODO Write in History table
                              }
                              this.visionBotUnitOfWork.FormTemplateRepository.Update(formTemplate);
                          }
                          response.Response = this.visionBotUnitOfWork.SaveChanges() > 0;
                      }

                      catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
                      {
                          response.ApplicationException = new ApplicationException(dbUpdateConcurrencyException.Message, dbUpdateConcurrencyException);
                      }
                      catch (Exception exception)
                      {

                          response.ApplicationException = new ApplicationException(exception.Message, exception);
                      }
                      return response;
                  });

        }

        /// <summary>
        /// Deletes the form template from table.
        /// </summary>
        /// <param name="formTemplateId">The form template id.</param>
        /// <returns></returns>
        public Task<CognitiveDataResponse<bool>> DeleteFormTemplateAsync(string formTemplateId)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var response = new CognitiveDataResponse<bool> { ApplicationException = null, Response = false };
                        try
                        {
                            this.visionBotUnitOfWork.FormTemplateRepository.Delete(formTemplateId);
                            response.Response = this.visionBotUnitOfWork.SaveChanges() > 0;
                        }

                        catch (DbException)
                        {
                        }
                        catch (Exception exception)
                        {

                            response.ApplicationException = new ApplicationException(exception.Message, exception);
                            this.exceptionHandler(exception);
                        }
                        return response;
                    });
        }

        /// <summary>
        /// Gets the field formats in Dictionary.
        /// </summary>
        /// <returns></returns>
        public Task<CognitiveDataResponse<Dictionary<int, string>>> GetFieldFormatsAsync()
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var response = new CognitiveDataResponse<Dictionary<int, string>>();
                        try
                        {
                            response.Response = Helper.GetEnumsDescription<FieldFormat>();
                        }
                        catch (Exception exception)
                        {

                            response.ApplicationException = new ApplicationException(exception.Message, exception);
                            this.exceptionHandler(exception);
                        }
                        return response;
                    });
        }

        #endregion

        internal bool ShouldWriteInHistory(IEnumerable<FormField> originalFormFields, IEnumerable<FormField> updatedFormFields)
        {
            if (originalFormFields == null)
            {
                throw new ArgumentNullException("originalFormFields");
            }
            if (updatedFormFields == null)
            {
                throw new ArgumentNullException("updatedFormFields");
            }

            var original = originalFormFields as IList<FormField> ?? originalFormFields.ToList();
            var updated = updatedFormFields as IList<FormField> ?? updatedFormFields.ToList();
            if (original.Count() != updated.Count())
            {
                return true;
            }

            var history = false;
            foreach (var originalFormField in original)
            {
                var updatedFormField = updated.FirstOrDefault(x => x.Id == originalFormField.Id);
                history = originalFormField.Equals(updatedFormField);
                break;
            }
            return !history;


        }
    }
}