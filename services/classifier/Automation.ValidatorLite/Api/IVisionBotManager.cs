﻿namespace Automation.CognitiveData.Api
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Automation.CognitiveData.DataAccess.DomainModel;
    using Automation.Integrator.CognitiveDataAutomation;

    public interface IVisionBotManager
    {
        

        /// <summary>
        /// Gets the form templates async.
        /// </summary>
        /// <returns></returns>
        Task<CognitiveDataResponse<IEnumerable<FormTemplate>>> GetFormTemplatesAsync();

        /// <summary>
        /// Saves the form template in database.
        /// </summary>
        /// <param name="formTemplate">The form template.</param>
        /// <returns></returns>
        Task<CognitiveDataResponse<bool>> SaveFormTemplateAsync(FormTemplate formTemplate);

        /// <summary>
        /// Deletes the form template async.
        /// </summary>
        /// <param name="formTemplateId">The form template id.</param>
        /// <returns></returns>
        Task<CognitiveDataResponse<bool>> DeleteFormTemplateAsync(string formTemplateId);

        /// <summary>
        /// Gets the field formats.
        /// </summary>
        /// <returns></returns>
        Task<CognitiveDataResponse<Dictionary<int,string>>> GetFieldFormatsAsync();

    }
}