﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;

    /// <summary>
    /// A C# wrapper for all native functions in Automation.FormOCR dll
    /// </summary>
    [ExcludeFromCodeCoverage]
    static class CognitiveDataWrapper
    {
        private const string DllName = "Automation.FormOCR.dll";

        /// <summary>
        /// Gets comma seperated template name listing string.
        /// </summary>
        /// <returns></returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern string GetTemplates(string dataStoragePath);

        /// <summary>
        /// Starts the smart soft invoice executable.
        /// </summary>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void StartSmartSoftInvoice(string dataStoragePath);
       
        /// <summary>
        /// Executes the data processing.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="csvFilePath">The CSV file path.</param>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int ExecuteDataProcessing(string templateName, string filePath, string csvFilePath, string dataStoragePath);

        /// <summary>
        /// Creates new template with specified templateName.
        /// </summary>
        /// <param name="templateName">name of new template</param>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreateNewTemplate(string templateName, string dataStoragePath);

        /// <summary>
        /// Shows edit template UI for specified templateName.
        /// </summary>
        /// <param name="templateName">name of template to be edited</param>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int EditTemplate(string templateName, string dataStoragePath, bool isReadOnlyTemplate);

        /// <summary>
        /// Delete template with specified templateName.
        /// </summary>
        /// <param name="templateName">name of template to delete</param>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DeleteTemplate(string templateName, string dataStoragePath);

        /// <summary>
        /// Show template training UI for specified templateName.
        /// </summary>
        /// <param name="templateName">name of template to be trained</param>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int TrainTemplate(string templateName, string dataStoragePath);

        /// <summary>
        /// Show List-Configuration UI.
        /// </summary>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int ShowListConfiguration(string templateName, string dataStoragePath);

        /// <summary>
        /// Show List-to-Field-Mapping-Configuration UI.
        /// </summary>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int ShowListToFieldMappingConfiguration(string templateName, string dataStoragePath);

        /// <summary>
        /// Set passed templateName as default template.
        /// </summary>
        /// <param name="templateName">name of template to be set as default template</param>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetDefaultTemplate(string templateName, string dataStoragePath);

        /// <summary>
        /// Rename the template with specified name
        /// </summary>
        /// <param name="templateName">Template name</param>
        /// <param name="newTemplateName">New name of the template</param>
        /// <param name="dataStoragePath">Template path</param>
        /// <returns>1 = success; 0 = unsuccessful</returns>
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int RenameTemplate(string templateName, string newTemplateName, string dataStoragePath);
    }
}