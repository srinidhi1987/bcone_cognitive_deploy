﻿using Automation.Cognitive.Services.Client;
using Automation.Cognitive.Validator;
using Automation.VisionBotEngine;
using System;
using System.Linq;
using System.Web;
using System.Windows;

namespace Automation.CognitiveData.Validator
{
    class ValidatorInitEntity
    {
        public string projectid { get; set; }
        public string projectname { get; set; }
        public string username { get; set; }
        public string token { get; set; }
        public string organizationId { get; set; }
        public string baseUrl { get; set; }
    }
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// 

    public partial class App : Application
    {
        private const string Protocol_Handler_Text = "aacp.validator:";

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ValidatorInitEntity validatorInitEntity;

            try
            {
                validatorInitEntity = getValidatorInitEntity(e);
            }
            catch(Exception ex)
            {
                VBotLogger.Error(() => $"[{nameof(App)} -> {nameof(Application_Startup)}] Error:{ex.Message}");
                MessageBox.Show(Cognitive.Validator.Properties.Resources.ArgsFormattingException,
                    Cognitive.Validator.Properties.Resources.MessageBoxCaption, 
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            if (validatorInitEntity == null)
            {
                return;
            }

            RestConfigurationProvider.BaseUrl = validatorInitEntity.baseUrl;
            RestConfigurationProvider.Token = validatorInitEntity.token;
            RestConfigurationProvider.UserName = validatorInitEntity.username;
            RestConfigurationProvider.IsGatewayUrl = true;

            var ve = new Automation.CognitiveData.VisionBotValidatorExecutor(validatorInitEntity.organizationId, validatorInitEntity.projectid, validatorInitEntity.projectname, validatorInitEntity.username, "token");
            bool result = ve.OpenQueue(@"D:\stories_data\573\FailQ", validatorInitEntity.organizationId, validatorInitEntity.projectname, validatorInitEntity.projectid, validatorInitEntity.username, "token");
            if (!result)
            {
                MessageBox.Show(Cognitive.Validator.Properties.Resources.ProjectNotExist,
                    Cognitive.Validator.Properties.Resources.MessageBoxCaption,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private ValidatorInitEntity getValidatorInitEntity(StartupEventArgs e)
        {
            if (e.Args == null || e.Args.Count() != 1)
            {
                MessageBox.Show(Cognitive.Validator.Properties.Resources.InvalidArgs,
                    Cognitive.Validator.Properties.Resources.MessageBoxCaption,
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            string[] parameters = e.Args[0].Replace("%5C", "\\").Split('\\');

            if (parameters.Length != 3)
            {
                MessageBox.Show(Cognitive.Validator.Properties.Resources.InvalidArgs,
                   Cognitive.Validator.Properties.Resources.MessageBoxCaption,
                   MessageBoxButton.OK,
                   MessageBoxImage.Error);
                return null;
            }

            string decryptedString = DecryptionProvider.Decrypt(parameters[0].Substring(Protocol_Handler_Text.Length));
            string[] decryptedParameters = decryptedString.Split('\\');

            ValidatorInitEntity validatorInitEntity = new ValidatorInitEntity
            {
                baseUrl = decryptedParameters[0],
                token = HttpUtility.UrlDecode(parameters[1]),
                username = decryptedParameters[2],
                organizationId = decryptedParameters[1],
                projectid = parameters[2]
            };

            return validatorInitEntity;
        }
    }
}
