namespace Automation.CognitiveData.Migrations
{
    using System.Data.Entity.Migrations;

    using Automation.CognitiveData.DataAccess;

    internal sealed class Configuration : DbMigrationsConfiguration<VisionBotDataContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;      //TODO make it false
        }

        protected override void Seed(VisionBotDataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
