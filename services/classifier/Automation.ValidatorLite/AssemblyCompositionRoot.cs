﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

//using Automation.CognitiveData.VisionBot.UI.Model;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData
{
    //using Automation.CognitiveData.Analytics;
    using Automation.CognitiveData.Converters;
    using Automation.CognitiveData.Generic;
    using Automation.CognitiveData.VisionBot.UI.ViewModels.Validators;
    using Automation.Generic;
    //using Automation.Integrator.CognitiveDataAutomation;
    //using Automation.Integrator.CognitiveDataAutomation.Analytics;
    using LightInject;
    using System.Diagnostics.CodeAnalysis;

    public class AssemblyCompositionRoot : ICompositionRoot
    {
        [ExcludeFromCodeCoverage]
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<IFileHelper, FileHelper>();
            serviceRegistry.Register<IFolderHelper, FolderHelper>();
            //serviceRegistry.Register<IAnalyticsManager, ControlRoomAnalyticManager>();
            //serviceRegistry.Register<IAnalyticsManager, FileAnalyticsManager>();
            //serviceRegistry.Register<IDataPersister<T>, XmlDataPersister<T>>();
            //serviceRegistry.Register<ITemplateLocker, FolderBasedTemplateLocker>();
            //serviceRegistry.Register<IDataPathProvider, ControlRoomPathProvider>();
            //serviceRegistry.Register<IDataPathConfigurationProvider, DataPathConfigurationProvider>();
          //  serviceRegistry.Register<IDataTableSerializer, CsvDataTableSerializer>();
           // serviceRegistry.Register<ICsvReaderWriter, CsvHelperReaderWriter>();
            //serviceRegistry.Register<string, string, IFileHelper, ICsvDataReader>((factory, value1, value2) => new CsvHelperDataReader(value1, value2));
            //serviceRegistry.Register<IDocSetItemOperator, DocSetItemOperator>();
            serviceRegistry.Register<IInputValueValidator, BlankValueValidator>(ValidatorViewModelBase.BlankValueValidatorService);
            serviceRegistry.Register<IInputValueValidator, UniqueValueValidator>(ValidatorViewModelBase.UniqueValueValidatorService);
            serviceRegistry.Register<IInputValueValidator, InvalidCharacterValidator>(ValidatorViewModelBase.InvalidCharacterValidatorService);
        }
    }
}