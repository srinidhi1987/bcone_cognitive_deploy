﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VBotDataExporter
{
    using System.Collections.Generic;
    using System.Data;

    using Automation.VisionBotEngine.Model;

    public interface IVBotExporter
    {
        // TODO: delimiter should be take in as part of Settings object, which in future have many other setting properties
        void ExportToCsv(DataTable dataTableToExport, CSVExportSettings settings);

        DataTable ExportToDataTable(VBotDocument vBotDocument, IDictionary<string, string> postData);
    }

    public class CSVExportSettings
    {
        public string CSVExportFilePath { get; set; }
        public string Delimiter { get; set; }
    }
}
