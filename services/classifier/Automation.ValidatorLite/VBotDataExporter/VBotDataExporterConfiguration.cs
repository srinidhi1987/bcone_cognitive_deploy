﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VBotExecutor
{
    using Automation.CognitiveData.VBotDataExporter;
    using Automation.CognitiveData.VisionBot;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    internal class VisionBotExecutor
    {
        //public bool ProcessDocument(VisionBot visionBot, string docPath, string csvFilePath, string taskName, bool isValidatorEnabled, string visionbotFilePath)
        //{
        //    //VBotLogger.Trace(() => "-----------------------------------------Process Document-----------------------------------------------------");

        //    //VBotLogger.Trace(() => string.Format("[ProcessDocument] Start Process Document {0}", Path.GetFileName(docPath)));
        //    //DocumentProcessor documentProcessor = new DocumentProcessor();
        //    //VBotEngineSettings engineSettings = documentProcessor.GetEngineSettings(false, false);
        //    //VBotDocument vBotDocument = documentProcessor.GetVBotDocument(visionBot, docPath, engineSettings);

        //    //var processingResult = new ExportDataUtility().ExportData(vBotDocument,
        //    //    new ExportConfiguration
        //    //    {
        //    //        ExportFilePath = csvFilePath,
        //    //        ProcessedDocumnetPath = docPath,
        //    //        TaskName = taskName,
        //    //        IsValidatorEnabled = isValidatorEnabled,
        //    //        VisionbotPath = visionbotFilePath
        //    //    });

        //    //VBotLogger.Trace(() => string.Format("[ProcessDocument] End Process Document. Validation Status = {0}", processingResult));

        //    return true;
        //}
    }

    internal class VBotDataExporterConfiguration
    {
        public IVBotExporter VBotExporter { get; }
        public CSVExportSettings CsvExportSettings { get; }

        public VBotDataExporterConfiguration(IVBotExporter vBotExporter, CSVExportSettings csvExportSettings)
        {
            VBotExporter = vBotExporter;
            CsvExportSettings = csvExportSettings;
        }
    }
}