﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot
{
    using Analytics;
    using Automation.CognitiveData.VBotDataExporter;
    using Automation.CognitiveData.VBotExecutor;
    using Automation.Generic;
    using Automation.Generic.Compression;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using Generic;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Packaging;
    using System.Text.RegularExpressions;
    using VisionBotEngine;
    using VisionBotEngine.Generic;

    internal enum ErrorExportTypes
    {
        None = 0,
        SeparateErrorCsv = 1,
        CombinedDataAndErrorCsv = 2,
        EyeballForError = 3
    }

    internal class ExportDataUtility
    {
        #region Public Properties

        private static IFileHelper __fileHelper;

        public static IFileHelper FileHelper
        {
            get
            {
                return __fileHelper ?? (__fileHelper = new FileHelper());
            }
            set
            {
                __fileHelper = value;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public bool ExportData(VBotDocument vBotDocument, ExportConfiguration exportConfiguration)
        {
            var processingResult = ValidateVBotData(vBotDocument);

            if (processingResult == false && exportConfiguration.IsValidatorEnabled)
            {
                ExportEyeBallFile(vBotDocument, exportConfiguration);
            }
            else
            {
                if (File.Exists(exportConfiguration.ExportFilePath))
                {
                    try
                    {
                        File.Delete(exportConfiguration.ExportFilePath);
                    }
                    catch (Exception ex)
                    {
                        ex.Log(nameof(ExportDataUtility), nameof(ExportData));
                    }
                }

                var postData = new Dictionary<string, string>
                    {
                        { "Result", processingResult.ToString().ToLower() },
                        { "File Path", exportConfiguration.ProcessedDocumnetPath }
                    };

                ErrorExportTypes dataExportType = Configurations.Visionbot.ErrorExportType;

                IList<VBotDataExporterConfiguration> vBotExporterConfigurations = getVBotExporter(dataExportType, exportConfiguration.ExportFilePath, processingResult);

                foreach (var vBotExporterConfig in vBotExporterConfigurations)
                {
                    ExportVBotData(vBotExporterConfig.VBotExporter, vBotDocument, postData, vBotExporterConfig.CsvExportSettings);
                }
            }

            return processingResult;
        }

        #endregion Public Methods

        #region Private Methods

        private static bool ExportEyeBallFile(VBotDocument vBotDocument, ExportConfiguration exportConfiguration)
        {
            string tempPkgPath, pkgPath = null;
            using (var pkg = new Automation.Generic.Compression.ResourcePackage())
            {
                // create package file at "{csv folder}\FailQ\{document name}.eball"
                var rootDirPath = Path.GetDirectoryName(exportConfiguration.ExportFilePath);
                var failQDirPath = Path.Combine(rootDirPath, Eyeball.FailQueueSubFolderName);
                Directory.CreateDirectory(failQDirPath);
                var documentNameWithExtension = Path.GetFileName(exportConfiguration.ProcessedDocumnetPath);
                pkgPath = Path.Combine(failQDirPath, $"{documentNameWithExtension}.{Eyeball.FailedFileExtension}");
                var tempExtension = ".inter";
                tempPkgPath = $"{pkgPath}{tempExtension}";
                pkg.CreateNew(tempPkgPath);

                try
                {
                    // serialize vbotdocument >> add to package as "vbotdocument"
                    var vbotDocumentResource = new JsonResourceData<VBotDocument>(Eyeball.Parts.OriginalVBotDocument);
                    vbotDocumentResource.SetContent(vBotDocument);
                    pkg.AddResource(vbotDocumentResource);

                    // serialize task-name >> add to package as "taskname"
                    var metadata = new Validator.Model.FailedVBotDocumentMetaData
                    {
                        TaskName = exportConfiguration.TaskName,
                        CreatedDateTimeAsTicks = DateTime.Now.Ticks,
                        Status = Validator.Model.FailedVBotDocumentStatus.Created,
                        OutputFilePath = exportConfiguration.ExportFilePath
                    };
                    var metadataResource = new JsonResourceData<Validator.Model.FailedVBotDocumentMetaData>(Eyeball.Parts.Metadata);
                    metadataResource.SetContent(metadata);
                    pkg.AddResource(metadataResource);

                    // add actual document to package as "document"
                    pkg.AddFile(Path.GetFileName(exportConfiguration.ProcessedDocumnetPath).ToPackageResourceName(), exportConfiguration.ProcessedDocumnetPath);

                    // add validation data
                    using (var visionbotPkg = new ResourcePackage())
                    {
                        var visionbotFilePath = IQBotDataPathProvider.GetTemplatePath(exportConfiguration.VisionbotPath);
                        visionbotPkg.Open(visionbotFilePath);

                        VBotValidatorDataCopier.Copy(visionbotPkg, pkg);

                        visionbotPkg.Close();
                    }
                }
                finally
                {
                    pkg.Close();
                }
            }
            if (!string.IsNullOrEmpty(tempPkgPath) &&
                !string.IsNullOrEmpty(pkgPath))
            {
                bool safeToMove = true;
                if (File.Exists(pkgPath))
                {
                    try
                    {
                        File.Delete(pkgPath);
                    }
                    catch (Exception ex)
                    {
                        safeToMove = false;
                        VBotLogger.Warning(() => $"[ExportEyeBallFile] Could not delete {pkgPath} and so old data persists; check .inter file for new contents");
                    }
                }
                if (safeToMove)
                {
                    HandleExportingOfAlreadyCorrectedCase(pkgPath);

                    File.Move(tempPkgPath, pkgPath);
                }
            }
            return true;
        }

        private static void HandleExportingOfAlreadyCorrectedCase(string pkgPath)
        {
            var correctedFilePath = $"{pkgPath}{Eyeball.CorrectedFileExtension}";

            if (!FileHelper.DoesExists(correctedFilePath)) return;

            var newCorrectedFilePath = GenerateNextNameForFileAutoRename(correctedFilePath);

            if (newCorrectedFilePath != null) File.Move(correctedFilePath, newCorrectedFilePath.NextRenameFilePath);

            VBotLogger.Warning(() => $"[HandleExportingOfAlreadyCorrectedCase] Rename already existing corrected file \"{correctedFilePath}\" to \"{newCorrectedFilePath.NextRenameFilePath}\"");
        }

        public class NextAutoRenameResult
        {
            public NextAutoRenameResult(string inputFilePath, int number)
            {
                this.InputFilePath = inputFilePath;
                this.Number = number;
            }

            public string InputFilePath { get; private set; }

            public int Number { get; private set; }

            public string NextRenameFilePath { get { return $"{InputFilePath}.{Number.ToString("D3")}"; } }
        }

        public static NextAutoRenameResult GenerateNextNameForFileAutoRename(string filePath)
        {
            if (string.IsNullOrEmpty(filePath)) throw new ArgumentNullException(nameof(filePath));

            for (int i = 1; ; i++)
            {
                var result = new NextAutoRenameResult(filePath, i);
                if (!FileHelper.DoesExists(result.NextRenameFilePath))
                {
                    return result;
                }

                if (i == int.MaxValue)
                    return null;
            }
        }

        private static bool ValidateVBotData(VBotDocument vBotDocument)
        {
            if (vBotDocument.Data.FieldDataRecord.Fields.Count == 0 &&
                vBotDocument.Data.TableDataRecord.Count == 0)
            {
                //VBotLogger.Trace(() => "[ProcessDocument] There are no output data found...");
                return false;
            }

            var result = vBotDocument.Data.GetValidationResult();

            if (result == VBotDataValidationResultTypes.Failure)
            {
                // VBotLogger.Trace(() => "[ProcessDocument] Output data validation failed...");
                return false;
            }

            //VBotLogger.Trace(() => "[ProcessDocument] Output data validation succeeded...");
            return true;
        }

        private static string GetErrorCSVPath(string regularDataCSVFilePath)
        {
            var parentPath = Path.GetDirectoryName(regularDataCSVFilePath);
            var fileName = Path.GetFileNameWithoutExtension(regularDataCSVFilePath);
            fileName += "_errors";
            var extension = Path.GetExtension(regularDataCSVFilePath);

            return Path.Combine(parentPath, string.Format("{0}{1}", fileName, extension));
        }

        private static IList<VBotDataExporterConfiguration> getVBotExporter(ErrorExportTypes exportType, string csvFilePath, bool processingResult)
        {
            const string Delimiter = ",";
            var outputCsvExportSettings = new CSVExportSettings { CSVExportFilePath = csvFilePath, Delimiter = Delimiter };

            List<VBotDataExporterConfiguration> VBotExporters = new List<VBotDataExporterConfiguration>();

            switch (exportType)
            {
                case ErrorExportTypes.None:

                    VBotExporters.Add(new VBotDataExporterConfiguration(new VBotExporter(), outputCsvExportSettings));
                    break;

                case ErrorExportTypes.SeparateErrorCsv:

                    VBotExporters.Add(new VBotDataExporterConfiguration(new VBotExporter(), outputCsvExportSettings));

                    if (!processingResult)
                    {
                        var errorCsvExportSettings = new CSVExportSettings
                        {
                            CSVExportFilePath = GetErrorCSVPath(csvFilePath),
                            Delimiter = Delimiter
                        };

                        VBotExporters.Add(new VBotDataExporterConfiguration(new VBotErrorExporter(), errorCsvExportSettings));
                    }

                    break;

                case ErrorExportTypes.CombinedDataAndErrorCsv:
                default:

                    VBotExporters.Add(new VBotDataExporterConfiguration(new VBotCombinedErrorAndDataExporter(), outputCsvExportSettings));
                    break;
            }
            return VBotExporters;
        }

        private static void ExportVBotData(IVBotExporter vBotExporter, VBotDocument vBotDocument, IDictionary<string, string> postData, CSVExportSettings csvExportSettings)
        {
            // VBotLogger.Trace(() => "[ProcessDocument] Export to csv...");

            var dt = vBotExporter.ExportToDataTable(vBotDocument, postData);
            // VBotLogger.Trace(() => "[ProcessDocument] Datatable created...");
            vBotExporter.ExportToCsv(dt, csvExportSettings);

            // VBotLogger.Trace(() => "[ProcessDocument] Sucessfully export data to csv.");
        }

        #endregion Private Methods
    }

    internal class ExportConfiguration
    {
        public string ExportFilePath { get; set; }

        public string ProcessedDocumnetPath { get; set; }

        public string TaskName { get; set; }

        public bool IsValidatorEnabled { get; set; }

        public string VisionbotPath { get; set; }
    }

    public static class StringExtensionsForStream
    {
        public static MemoryStream GetMemoryStream(this string str)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }

    public class JsonResourceData<T> : Automation.Generic.Compression.ResourceData
    {
        public JsonResourceData(string uniqueId)
        {
            this.ContentType = @"text/json";
            this.FilePath = string.Empty;
            this.UniqueId = uniqueId;
        }

        public JsonResourceData(Automation.Generic.Compression.ResourceData resourceData)
        {
            if (!resourceData.ContentType.Equals(@"text/json", StringComparison.InvariantCultureIgnoreCase))
                throw new ArgumentException($"Passed resource data must be of json type (passed type {resourceData.ContentType})", nameof(resourceData));

            this.ContentType = resourceData.ContentType;
            this.FilePath = resourceData.FilePath;
            this.UniqueId = resourceData.UniqueId;
            this.Content = resourceData.Content;
        }

        public void SetContent(T content)
        {
            var serializedData = Newtonsoft.Json.JsonConvert.SerializeObject(content);
            this.Content = serializedData.GetMemoryStream();
        }

        public T GetContent()
        {
            var content = this.Content.GetUTF8String();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
        }
    }

    public static class JsonResourceDataExtensions
    {
        public static JsonResourceData<T> ConvertToJsonResourceData<T>(this Automation.Generic.Compression.ResourceData resourceData)
        {
            return new JsonResourceData<T>(resourceData);
        }

        public static bool TryConvertToJsonResourceData<T>(this Automation.Generic.Compression.ResourceData resourceData, out JsonResourceData<T> convertedJsonResourceData)
        {
            try
            {
                convertedJsonResourceData = resourceData.ConvertToJsonResourceData<T>();
                return true;
            }
            catch
            {
                convertedJsonResourceData = null;
                return false;
            }
        }
    }

    internal class Eyeball
    {
        public static string FailedFileExtension { get { return "vbotp"; } }
        public static string FailedFileExtensionWithDot { get { return $".{FailedFileExtension}"; } }

        public static string CorrectedFileExtension { get { return FailedFileExtension + ".corrected"; } }

        public static string FailQueueSubFolderName { get { return "FailQ"; } }

        public class Parts
        {
            public static string Metadata { get { return ".metadata"; } }
            public static string OriginalVBotDocument { get { return ".vbotdocument"; } }
            public static string HumanCorrectedVBotDocument { get { return ".hcorrected"; } }
            public static string MachineCorrectedVBotDocument { get { return ".mcorrected"; } }

            public static string CustomerDocument(string originalLocationPath)
            {
                return Path.GetFileName(originalLocationPath);
            }

            public static string AuditLog { get { return ".audit"; } }
        }
    }

    internal class ValidatorEngine
    {
        #region VbotDocument

        public VBotDocument GetVbotDocument(string eyeballFilePath)
        {
            return PackageHelper.GetFromPackage<VBotDocument>(eyeballFilePath, Eyeball.Parts.OriginalVBotDocument, (Stream contentStream, ResourcePackage pkg) =>
            {
                if (contentStream == null) throw new ContentStreamNullException(Eyeball.Parts.OriginalVBotDocument);
                return GetVbotDocument(contentStream);
            });
        }

        public VBotDocument TryGetVbotDocument(string eyeballFilePath)
        {
            try
            {
                return GetVbotDocument(eyeballFilePath);
            }
            catch
            {
                return null;
            }
        }

        private VBotDocument GetVbotDocument(Stream contentStream)
        {
            if (contentStream == null) throw new ContentStreamNullException(Eyeball.Parts.OriginalVBotDocument);
            var content = contentStream.GetUTF8String();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<VBotDocument>(content);
        }

        private VBotDocument TryGetVbotDocument(Stream contentStream)
        {
            try
            {
                return GetVbotDocument(contentStream);
            }
            catch
            {
                return null;
            }
        }

        #endregion VbotDocument

        #region Document

        public string GetDocument(string eyeballFilePath)
        {
            var resourceUniqueId = Eyeball.Parts.CustomerDocument(eyeballFilePath).ToPackageResourceName();
            return PackageHelper.GetFromPackage<string>(
                eyeballFilePath,
                resourceUniqueId,
                (Stream contentStream, ResourcePackage pkg) =>
                    {
                        if (contentStream == null) throw new ContentStreamNullException(resourceUniqueId);
                        var docTempFilePath = LocalMachineFileHelper.GetLocalFilePath(Eyeball.Parts.CustomerDocument(eyeballFilePath));
                        contentStream.SaveToFile(docTempFilePath);
                        return docTempFilePath;
                    });
        }

        public string TryGetDocument(string eyeballFilePath)
        {
            try
            {
                return GetDocument(eyeballFilePath);
            }
            catch
            {
                return null;
            }
        }

        public string GetDocument(Stream fileStream, string processedDocName)
        {
            var resourceUniqueId = processedDocName.ToPackageResourceName();
            return PackageHelper.GetFromPackage<string>(
                fileStream,
                resourceUniqueId,
                (Stream contentStream, VResourcePackage pkg) =>
                    {
                        if (contentStream == null) throw new ContentStreamNullException(resourceUniqueId);
                        var docTempFilePath = LocalMachineFileHelper.GetLocalFilePath(processedDocName);
                        contentStream.SaveToFile(docTempFilePath);
                        return docTempFilePath;
                    });
        }

        public string TryGetDocument(Stream fileStream, string processedDocName)
        {
            try
            {
                return GetDocument(fileStream, processedDocName);
            }
            catch
            {
                return null;
            }
        }

        #endregion Document

        #region Metadata

        public Validator.Model.FailedVBotDocumentMetaData GetMetadata(string eyeballFilePath)
        {
            return PackageHelper.GetFromPackage<Validator.Model.FailedVBotDocumentMetaData>(
                eyeballFilePath,
                Eyeball.Parts.Metadata,
                (Stream contentStream, ResourcePackage pkg) =>
                {
                    if (contentStream == null) throw new ContentStreamNullException(Eyeball.Parts.Metadata);
                    var content = contentStream.GetUTF8String();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<Validator.Model.FailedVBotDocumentMetaData>(content);
                });
        }

        public Validator.Model.FailedVBotDocumentMetaData TryGetMetadata(string eyeballFilePath)
        {
            try
            {
                return GetMetadata(eyeballFilePath);
            }
            catch
            {
                return null;
            }
        }

        #endregion Metadata
    }

    public class ContentStreamNullException : NullReferenceException
    {
        public ContentStreamNullException(string streamName)
            : base(CreateMessage(streamName))
        {
            this.StreamName = streamName;
        }

        public ContentStreamNullException(string streamName, Exception innerException)
            : base(CreateMessage(streamName), innerException)
        {
            this.StreamName = streamName;
        }

        private static string CreateMessage(string streamName)
        {
            return $"resource \"{streamName}\" not found in package";
        }

        public string StreamName { get; }
    }

    internal static class LocalMachineFileHelper
    {
        private const string TempFolderName = "ValidatorDocs";

        public static string GetLocalFilePath(string processedDocName)
        {
            var docTempFolder = Path.Combine(Path.GetTempPath(), TempFolderName);
            if (!Directory.Exists(docTempFolder))
                Directory.CreateDirectory(docTempFolder);

            return Path.Combine(docTempFolder, processedDocName);
        }

        public static void DeleteLocalFile(string processedDocName)
        {
            var docPath = GetLocalFilePath(processedDocName);
            if (File.Exists(docPath))
                File.Delete(docPath);

            var parentDirectory = Path.GetDirectoryName(docPath);
            if (Directory.GetFiles(parentDirectory).Length <= 0)
                Directory.Delete(parentDirectory);
        }
    }

    internal static class PackageHelper
    {
        public static T GetFromPackage<T>(string packageFilePath, string resourceUniqueId,
            Func<Stream, ResourcePackage, T> extractionLogic)
        {
            if (!File.Exists(packageFilePath)) throw new FileNotFoundException("file not found", packageFilePath);

            using (var pkg = new Automation.Generic.Compression.ResourcePackage())
            {
                //System.IO.Packaging.Package.Open();
                pkg.Open(packageFilePath);

                try
                {
                    var resource = pkg.GetResource(resourceUniqueId);
                    return extractionLogic(resource.Content, pkg);
                }
                finally
                {
                    pkg.Close();
                }
            }
        }

        public static T GetFromPackage<T>(Stream fileStream, string resourceUniqueId,
            Func<Stream, VResourcePackage, T> extractionLogic)
        {
            T result = default(T);
            WorkWithPackageResource(fileStream, resourceUniqueId, (resource, pkg) =>
            {
                result = extractionLogic(resource.Content, pkg);
            });
            return result;
        }

        public static void WorkWithPackage(Stream fileStream,
            Action<VResourcePackage> workLogic)
        {
            using (var pkg = new VResourcePackage())
            {
                pkg.Open(fileStream);

                try
                {
                    workLogic(pkg);
                }
                finally
                {
                    pkg.Close();
                }
            }
        }

        public static void WorkWithPackageResource(Stream fileStream, string resourceUniqueId,
            Action<ResourceData, VResourcePackage> workLogic)
        {
            WorkWithPackage(fileStream, (pkg) =>
            {
                var resource = pkg.GetResource(resourceUniqueId);
                workLogic(resource, pkg);
            });
        }

        public static string ToPackageResourceName(this string filePath)
        {
            return filePath?.Replace('#', '_');
        }
    }

    public class AAResourcePackage : Automation.Generic.Compression.ResourcePackage
    {
        public AAResourcePackage(string packagePath, bool overwriteIfAlreadyExists = false)
        {
            if (File.Exists(packagePath))
            {
                if (overwriteIfAlreadyExists)
                    this.CreateNew(packagePath);
                else
                    this.Open(packagePath);
            }
            else
                this.Create(packagePath);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            this.Close();
        }
    }

    public static class StreamExtensions
    {
        public static byte[] GetAllBytes(this Stream input)
        {
            /* source: http://stackoverflow.com/questions/221925/creating-a-byte-array-from-a-stream */

            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static string GetUTF8String(this Stream input)
        {
            var bytes = input.GetAllBytes();
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        public static void SaveToFile(this Stream input, string filePath)
        {
            string directoryPath = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            using (var fileStream = File.Create(filePath))
            {
                input.CopyTo(fileStream);
            }
        }

        public static T JsonConvert<T>(this Stream input)
        {
            var content = input.GetUTF8String();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
        }
    }
}