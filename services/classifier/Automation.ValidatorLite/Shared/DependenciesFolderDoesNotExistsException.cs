﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DependenciesFolderDoesNotExistsException.cs" company="Automation Anywhere">
//   All rights reserved 2015
// </copyright>
// <summary>
//   The dependencies folder does not exists exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Automation.CognitiveData.Shared
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The dependencies folder does not exists exception.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class DependenciesFolderDoesNotExistsException : ApplicationException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DependenciesFolderDoesNotExistsException"/> class.
        /// </summary>
        public DependenciesFolderDoesNotExistsException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependenciesFolderDoesNotExistsException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public DependenciesFolderDoesNotExistsException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependenciesFolderDoesNotExistsException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="inner">
        /// The inner.
        /// </param>
        public DependenciesFolderDoesNotExistsException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependenciesFolderDoesNotExistsException"/> class.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        protected DependenciesFolderDoesNotExistsException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        
        // Any additional custom properties, constructors and data members...
    }
}
