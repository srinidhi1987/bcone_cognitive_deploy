﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Shared
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    //using Automation.Integrator.CognitiveDataAutomation;

    using FluentValidation;

    /// <summary>
    /// Initializes the new instance of CognitiveDataValidator
    /// </summary>
    //public class CognitiveDataValidator : AbstractValidator<CognitiveData>
    //{
    //    private readonly ICognitiveExecutor _cognitiveExecutor;

    //    private readonly IFileService _fileService;

    //    public CognitiveDataValidator(ICognitiveExecutor cognitiveExecutor, IFileService fileService)
    //    {
    //        this._cognitiveExecutor = cognitiveExecutor;
    //        this._fileService = fileService;

    //        this.RuleFor(cd => cd.InputFile)
    //            .Cascade(CascadeMode.StopOnFirstFailure)
    //            .NotEmpty()
    //            .WithMessage(ErrorMessages.EmptyStringErrorMessage, "Input File")
    //            .Must(File.Exists)
    //            .WithMessage(ErrorMessages.FileDoesNotExistsErrorMessage, "Input File")
    //            .Must(this.IsExtensionValid)
    //            .WithMessage(ErrorMessages.InvalidFileExtensionErrorMessage, x => x.InputFile);

    //        this.RuleFor(cd => cd.Template)
    //            .Cascade(CascadeMode.StopOnFirstFailure)
    //            .NotEmpty()
    //            .WithMessage(ErrorMessages.EmptyStringErrorMessage, Common.Constants.CDC_TITLE_SINGULAR)
    //            .Must(this.DoesTemplateExists)
    //            .WithMessage(ErrorMessages.TemplateDoesNotExistsErrorMessage, Common.Constants.CDC_TITLE_SINGULAR);

    //        this.RuleFor(cd => cd.OutputFile)
    //            .Cascade(CascadeMode.StopOnFirstFailure)
    //            .NotEmpty()
    //            .WithMessage(ErrorMessages.EmptyStringErrorMessage, "Output File")
    //            .Must(this.DoesFolderExists)
    //            .WithMessage(ErrorMessages.OutputFolderDoesntExistsErrorMessage)
    //            .Must(this.DoesFolderHavePermission)
    //            .WithMessage(ErrorMessages.NoCreatePermission)
    //            .Must(this.IsFileAlreadyInUse)
    //            .WithMessage(ErrorMessages.FileinUseErrorMessage);
    //    }

    //    private bool IsExtensionValid(string filePath)
    //    {
    //        var validExtensions = new List<string> { ".pdf", ".jpg", ".jpeg", ".png", ".tif", ".tiff" };

    //        var fileExtension = Path.GetExtension(filePath);

    //        return validExtensions.Contains(fileExtension.ToLowerInvariant());
    //    }

    //    private bool IsFileAlreadyInUse(string filePath)
    //    {
    //        return this._fileService.IsFileAlreadyInUse(filePath);
    //    }

    //    private bool DoesFolderExists(string filePath)
    //    {
    //        return this._fileService.DoesFolderExists(filePath);
    //    }

    //    private bool DoesTemplateExists(string templateName)
    //    {
    //        if (templateName.Contains(Path.DirectorySeparatorChar))
    //        {
    //            return templateName.EndsWith(Util.IQBots.Constants.VisionBot.FILE_EXTENSION_WITH_DOT)
    //                   && File.Exists(templateName);
    //        }

    //        var availableTemplates = this._cognitiveExecutor.GetTemplates();
    //        return availableTemplates.Contains(templateName);
    //    }

    //    private bool DoesFolderHavePermission(string filePath)
    //    {
    //        return _fileService.DoesFolderHavePermission(filePath);
    //    }
    //}
}