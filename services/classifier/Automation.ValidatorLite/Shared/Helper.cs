﻿namespace Automation.CognitiveData.Shared
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal static class Helper
    {
        internal static Dictionary<int,string> GetEnumsDescription<T>() where T:struct 
        {
          var t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<int>().ToDictionary(d => d, d => Enum.GetName(t, d));
        }
        
    }
}