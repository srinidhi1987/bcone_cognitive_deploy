﻿namespace Automation.CognitiveData.DataAccess
{
    using System.Data.Entity;

    using Automation.CognitiveData.Migrations;

    internal class DbInitializer : MigrateDatabaseToLatestVersion<VisionBotDataContext, Configuration>
    {
        
    }
}