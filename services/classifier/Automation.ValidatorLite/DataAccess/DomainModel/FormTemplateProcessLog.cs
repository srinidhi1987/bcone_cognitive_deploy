namespace Automation.CognitiveData.DataAccess.DomainModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class FormTemplateProcessLog : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get;
            set;
        }

        public virtual FormTemplate FormTemplate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the process status.
        /// </summary>
        /// <value>The process status.</value>
        public ProcessStatus ProcessStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the last processed on.
        /// </summary>
        /// <value>The last processed on.</value>
        public DateTime LastProcessedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the version on which this template was processed.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get;
            set;
        }
    }
}