﻿namespace Automation.CognitiveData.DataAccess.DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class FormFieldsHistory
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public string Version
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form template.
        /// </summary>
        /// <value>The form template.</value>
        public virtual FormTemplate FormTemplate
        {
            get;
            set;
        }

        public virtual ICollection<FormFieldHistoryDetail> FormFieldHistoryDetails
        {
            get;
            set;
        }
    }
}