﻿namespace Automation.CognitiveData.DataAccess.DomainModel
{
    using System;

    public abstract class EntityBase
    {
        /// <summary>
        /// Gets or sets the created at.
        /// </summary>
        /// <value>The created at.</value>
        public DateTime CreatedAt { get;set; }

        /// <summary>
        /// Gets or sets the modified at.
        /// </summary>
        /// <value>The modified at.</value>
        public DateTime? ModifiedAt
        {
            get;
            set;
        }

     
    }
}