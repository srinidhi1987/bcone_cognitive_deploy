﻿namespace Automation.CognitiveData.DataAccess.DomainModel
{
    using System;
    using System.ComponentModel;

    [Flags]
    public enum FieldFormat
    {
        /// <summary>
        /// Text field format
        /// </summary>
        [Description("Text")]
        Text = 1,
        /// <summary>
        /// Date field format
        /// </summary>
        [Description("Date")]
        Date = 2,
        /// <summary>
        /// Integer field format
        /// </summary>
        [Description("Integer")]
        Integer = 3,
        /// <summary>
        /// Money field format
        /// </summary>
        [Description("Money")]
        Money = 4,
        [Description("Boolean")]
        Boolean = 5,
        /// <summary>
        /// Invoice Number format
        /// </summary>
        [Description("Invoice Number")]
        InvoiceNumber = 6 ,
        [Description("Double")]
        Double = 7
    }
}