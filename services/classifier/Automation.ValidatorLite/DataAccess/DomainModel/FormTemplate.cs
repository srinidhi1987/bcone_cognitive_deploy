﻿namespace Automation.CognitiveData.DataAccess.DomainModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public class FormTemplate : EntityBase
    {

        public FormTemplate()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [Key]
        [ConcurrencyCheck]
        public string Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Required, MaxLength(50)]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the type of the bot.
        /// </summary>
        /// <value>The type of the bot.</value>
        [Required]
        public BotType BotType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the current version.
        /// </summary>
        /// <value>The current version.</value>
        [Required]
        public int CurrentVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form fields.
        /// </summary>
        /// <value>The form fields.</value>
        public virtual ICollection<FormField> FormFields
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form template history.
        /// </summary>
        /// <value>The form template history.</value>
        public ICollection<FormTemplateProcessLog> FormTemplateProcessLogs
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the success count.
        /// </summary>
        /// <value>The success count.</value>
        [NotMapped]
        public int SuccessCount
        {
            get
            {
                return this.FormTemplateProcessLogs.Count(x => x.ProcessStatus == ProcessStatus.Success);
            }
        }

        /// <summary>
        /// Gets the failure count.
        /// </summary>
        /// <value>The failure count.</value>
        [NotMapped]
        public int FailureCount
        {
            get
            {
                return this.FormTemplateProcessLogs.Count(x => x.ProcessStatus == ProcessStatus.Fail);
            }
        }

        /// <summary>
        /// Gets the process count.
        /// </summary>
        /// <value>The process count.</value>
        [NotMapped]
        public int ProcessCount
        {
            get
            {
                return this.SuccessCount + this.FailureCount;
            }
        }

        /// <summary>
        /// Gets or sets the form fields histories.
        /// </summary>
        /// <value>The form fields histories.</value>
        public virtual ICollection<FormFieldsHistory> FormFieldsHistories
        {
            get;
            set;
        }

    }
}