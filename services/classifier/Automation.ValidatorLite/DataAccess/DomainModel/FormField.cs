using System;

namespace Automation.CognitiveData.DataAccess.DomainModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Represents FormField Entity
    /// </summary>
    public class FormField : EntityBase, IEquatable<FormField>
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="FormField" /> class.
        /// </summary>
        public FormField()
        {
            this.Id = Guid.NewGuid().ToString();
        } 
        #endregion

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [Key]
        [ConcurrencyCheck]
        public string Id
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the field id.
        /// </summary>
        /// <value>The field id.</value>
        public string FieldId
        {
            get;
            set;
        }

        public virtual FormTemplate FormTemplate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the regular expression.
        /// </summary>
        /// <value>The expression.</value>
        public string Expression
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>The header.</value>
        public string Header
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the formula.
        /// </summary>
        /// <value>The formula.</value>
        public string Formula
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>The default value.</value>
        public string DefaultValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the options.
        /// </summary>
        /// <value>The options.</value>
        public string Options
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the field format.
        /// </summary>
        /// <value>The field format.</value>
        public FieldFormat FieldFormat
        {
            get;
            set;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int result = 17;
                result = result * 23 + ((this.FormTemplate != null) ? this.FormTemplate.GetHashCode() : 0);
                return result;
            }
        }

        public bool Equals(FormField other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return Equals(this.FormTemplate, other.FormTemplate) 
                   && Equals(this.Formula, other.Formula)
                   && Equals(this.Expression, other.Expression)
                   && Equals(this.Title, other.Title)
                   && Equals(this.Header, other.Header);
        }

        public override bool Equals(object obj)
        {
            var temp = obj as FormField;
            return temp != null && this.Equals(temp);
        }
    }
}