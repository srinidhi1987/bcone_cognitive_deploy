namespace Automation.CognitiveData.DataAccess.DomainModel
{
    /// <summary>
    /// Represents  Success and Failure Status When Document is being processed.
    /// </summary>
    public enum ProcessStatus
    {
        Success = 1,
        Fail = 0
    }
}