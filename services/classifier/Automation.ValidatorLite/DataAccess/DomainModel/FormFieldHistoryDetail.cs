﻿namespace Automation.CognitiveData.DataAccess.DomainModel
{
    public class FormFieldHistoryDetail
    {
        public long Id
        {
            get;
            set;
        }

        public virtual FormFieldsHistory FormFieldsHistory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form field title.
        /// </summary>
        /// <value>The form field title.</value>
        public string FormFieldTitle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the field id.
        /// </summary>
        /// <value>The field id.</value>
        public string FieldId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the field format.
        /// </summary>
        /// <value>The field format.</value>
        public FieldFormat FieldFormat
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public string Expression
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>The header.</value>
        public string Header
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the formula.
        /// </summary>
        /// <value>The formula.</value>
        public string Formula
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the default value.
        /// </summary>
        /// <value>The default value.</value>
        public string DefaultValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the options.
        /// </summary>
        /// <value>The options.</value>
        public string Options
        {
            get;
            set;
        }

    }
}