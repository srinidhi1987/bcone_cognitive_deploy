﻿namespace Automation.CognitiveData.DataAccess
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    using Automation.CognitiveData.DataAccess.DomainModel;

    using MySql.Data.Entity;

    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class VisionBotDataContext : DbContext
    {
        public VisionBotDataContext()
            : base("AppCtx")
        {
        }

        /// <summary>
        /// Gets or sets the form templates.
        /// </summary>
        /// <value>The form templates.</value>
        public virtual DbSet<FormTemplate> FormTemplates
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form fields.
        /// </summary>
        /// <value>The form fields.</value>
        public virtual DbSet<FormField> FormFields
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form template history.
        /// </summary>
        /// <value>The form template history.</value>
        public DbSet<FormTemplateProcessLog> FormTemplateHistory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the form fields history entity.
        /// </summary>
        /// <value>The form fields history.</value>
        public DbSet<FormFieldsHistory> FormFieldsHistory { get; set; }

        public DbSet<FormFieldHistoryDetail> FormFieldHistoryDetails { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<FormTemplate>()
                .HasOptional(a=>a.FormFields)
                .WithOptionalDependent()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<FormTemplate>()
               .HasOptional(a => a.FormFieldsHistories)
               .WithOptionalDependent()
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<FormTemplate>()
               .HasOptional(a => a.FormTemplateProcessLogs)
               .WithOptionalDependent()
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<FormFieldsHistory>()
                .HasOptional(a=>a.FormFieldHistoryDetails)
                .WithOptionalDependent()
                .WillCascadeOnDelete(true);
        }


    }
}