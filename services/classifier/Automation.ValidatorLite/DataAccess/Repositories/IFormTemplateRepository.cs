﻿namespace Automation.CognitiveData.DataAccess.Repositories
{
    using Automation.CognitiveData.DataAccess.DomainModel;

    public interface IFormTemplateRepository  :IDataRepository<FormTemplate,string>
    {
         
    }
}