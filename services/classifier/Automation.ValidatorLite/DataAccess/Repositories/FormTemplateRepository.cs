﻿namespace Automation.CognitiveData.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Automation.CognitiveData.DataAccess.DomainModel;

    public class FormTemplateRepository : IFormTemplateRepository
    {
        private readonly VisionBotDataContext visionBotDataContext;

        public FormTemplateRepository(VisionBotDataContext visionBotDataContext)
        {
            this.visionBotDataContext = visionBotDataContext;
        }

        /// <summary>
        /// The get by id async.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The <see cref="Task" />.</returns>
        public Task<FormTemplate> GetByIdAsync(string id)
        {
           return Task.Factory.StartNew(
                () =>
                    {
                        var formTemplate =  this.visionBotDataContext.FormTemplates.FirstOrDefault(x => x.Id.Equals(id));
                        return formTemplate ?? null;
                    });

            
        }

        /// <summary>
        /// Gets all records from the entity
        /// </summary>
        /// <returns>The <see cref="ICollection{T}" />.</returns>
        public virtual Task<IQueryable<FormTemplate>> RetrieveAllRecordsAsync()
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var formTemplates = this.visionBotDataContext.FormTemplates.AsQueryable();
                        return formTemplates;
                    });
        }

        public Task Insert(FormTemplate entity)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        this.visionBotDataContext.FormTemplates.Add(entity);
                    });
        }

        public Task Update(FormTemplate entity)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var original =
                            this.visionBotDataContext.FormTemplates.FirstOrDefault(e => e.Id.Equals(entity.Id));
                        if (original != null)
                        {
                            this.visionBotDataContext.Entry(original).CurrentValues.SetValues(entity);
                            
                        }
                    });
        }

        public Task Delete(string id)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var itemtobeDeleted =
                            this.visionBotDataContext.FormTemplates.FirstOrDefault(e => e.Id.Equals(id));
                        this.visionBotDataContext.FormTemplates.Remove(itemtobeDeleted);
                    });
        }
    }
}