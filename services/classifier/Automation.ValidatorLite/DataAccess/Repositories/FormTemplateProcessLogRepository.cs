﻿namespace Automation.CognitiveData.DataAccess.Repositories
{
    using System.Linq;
    using System.Threading.Tasks;

    using Automation.CognitiveData.DataAccess.DomainModel;

    public class FormTemplateProcessLogRepository : IFormTemplateProcessLogRepository
    {
        public Task<FormTemplateProcessLog> GetByIdAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IQueryable<FormTemplateProcessLog>> RetrieveAllRecordsAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task Insert(FormTemplateProcessLog entity)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(FormTemplateProcessLog entity)
        {
            throw new System.NotImplementedException();
        }

        public Task Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}