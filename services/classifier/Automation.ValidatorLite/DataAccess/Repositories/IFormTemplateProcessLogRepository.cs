﻿namespace Automation.CognitiveData.DataAccess.Repositories
{
    using Automation.CognitiveData.DataAccess.DomainModel;

    public interface IFormTemplateProcessLogRepository :IDataRepository<FormTemplateProcessLog,int>
    {
         
    }
}