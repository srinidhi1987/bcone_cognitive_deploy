﻿namespace Automation.CognitiveData.DataAccess.UnitOfWork
{
    using Automation.CognitiveData.DataAccess.Repositories;

    public interface IVisionBotUnitOfWork
    {
        IFormTemplateRepository FormTemplateRepository { get;  }

        int SaveChanges();


    }
}