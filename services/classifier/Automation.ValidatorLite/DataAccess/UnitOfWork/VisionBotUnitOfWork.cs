﻿namespace Automation.CognitiveData.DataAccess.UnitOfWork
{
    using Automation.CognitiveData.DataAccess.Repositories;

    public class VisionBotUnitOfWork : IVisionBotUnitOfWork
    {
        private readonly VisionBotDataContext visionBotDataContext;

        public VisionBotUnitOfWork(VisionBotDataContext visionBotDataContext)
        {
            this.visionBotDataContext = visionBotDataContext;

            this.FormTemplateRepository = new FormTemplateRepository(visionBotDataContext);
        }

        public virtual IFormTemplateRepository FormTemplateRepository
        {
            get;
            private set;
        }


        public int SaveChanges()
        {
            return this.visionBotDataContext.SaveChanges();
        }
    }
}