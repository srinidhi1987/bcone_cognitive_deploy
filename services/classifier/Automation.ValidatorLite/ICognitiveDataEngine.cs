﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData
{
    //using Automation.Integrator.CognitiveDataAutomation;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    //public interface ICognitiveDataEngine : IChecksCompatibility
    //{
    //    IList<string> GetTemplates(string dataStoragePath);

    //    void StartSmartSoftInvoice(string dataStoragePath);

    //    int ExecuteDataProcessing(string templateName, string filePath, string csvFilePath, string dataStoragePath, string taskName, bool isValidatorEnabled);

    //    int CreateNewTemplate(string templateName, string dataStoragePath);

    //    int EditTemplate(string templateName, string dataStoragePath, bool isReadOnlyTemplate);

    //    int TrainTemplate(string templateName, string dataStoragePath);

    //    int ShowListConfiguration(string templateName, string dataStoragePath);

    //    int ShowListToFieldMappingConfiguration(string templateName, string dataStoragePath);

    //    void SetDefaultTemplate(string templateName, string dataStoragePath);
    //}
}