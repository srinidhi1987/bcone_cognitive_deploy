﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Security.AccessControl;
using System.Security.Principal;

namespace Automation.CognitiveData.SmartOcrSdkFacade
{
    #region Usings

    using System;
    using System.IO;

    //using Automation.Integrator.CognitiveDataAutomation;
    using System.Diagnostics.CodeAnalysis;
    using VisionBotEngine;
    #endregion

    /// <summary>
    /// The file service.
    /// </summary>
    //public class FileService : IFileService
    //{
    //    #region IFileService Implementation
    //    /// <summary>
    //    /// Checks whether dependency folder exists.
    //    /// </summary>
    //    /// <param name="folderPath">
    //    /// The folder path.
    //    /// </param>
    //    /// <returns>
    //    /// The <see cref="bool"/>.
    //    /// </returns>
    //    [ExcludeFromCodeCoverage]
    //    public bool DoesDependenciesFolderExists(string folderPath)
    //    {
    //        return Directory.Exists(folderPath);
    //    }

    //    public bool IsFileAlreadyInUse(string filePath)
    //    {
    //        var fileInfo = new FileInfo(filePath);
    //        FileStream fileStream = null;

    //        try
    //        {
    //            if (File.Exists(filePath))
    //            {
    //                fileStream = fileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
    //            }
    //        }
    //        catch (IOException)
    //        {
    //             return false;
    //        }
    //        finally
    //        {
    //            if (fileStream != null)
    //            {
    //                fileStream.Close();
    //                fileStream.Dispose();
    //            }

    //        }
    //        return true;
    //    }

    //    /// <summary>
    //    /// Checks whether the folder exists in given path.
    //    /// </summary>
    //    /// <param name="filePath">The file path.</param>
    //    /// <returns></returns>
    //    public bool DoesFolderExists(string filePath)
    //    {
    //        var dirPath = Path.GetDirectoryName(filePath);
    //        return dirPath != null && Directory.Exists(dirPath);
    //    }

    //    /// <summary>
    //    /// Checks whether the file path have write permission
    //    /// </summary>
    //    /// <param name="filePath">The file path.</param>
    //    /// <returns></returns>
    //    /// 
    //    public bool DoesFolderHavePermission(string filePath)
    //    {
    //        FileStream fileStream = null;
    //        try
    //        {
    //            var dirPath = Path.GetDirectoryName(filePath);
    //            DirectorySecurity directorySecurity = Directory.GetAccessControl(dirPath);
    //            AuthorizationRuleCollection accessRules = directorySecurity.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));

    //            var currentUser = new WindowsPrincipal(WindowsIdentity.GetCurrent());

    //            foreach (FileSystemAccessRule rule in accessRules)
    //            {
    //                var securityIdentifier = new SecurityIdentifier(rule.IdentityReference.Value);

    //                if (currentUser.IsInRole(securityIdentifier))
    //                {
    //                    if ((rule.FileSystemRights == FileSystemRights.Write ||
    //                        rule.FileSystemRights == FileSystemRights.Modify ||
    //                        rule.FileSystemRights == FileSystemRights.FullControl) &&
    //                        rule.AccessControlType == AccessControlType.Deny)
    //                    {
    //                        return false;
    //                    }

    //                    if ((rule.FileSystemRights == FileSystemRights.Read ||
    //                        rule.FileSystemRights == FileSystemRights.ReadAndExecute) &&
    //                        rule.AccessControlType == AccessControlType.Deny)
    //                    {
    //                        var fileInfo = new FileInfo(filePath);
    //                        fileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.None);
    //                    }
    //                }
    //            }
    //        }
    //        catch (FileNotFoundException)
    //        {
    //            return true;
    //        }
    //        catch (UnauthorizedAccessException )
    //        {
    //            return false;
    //        }
    //        catch (IOException)
    //        {
    //            return false;
    //        }

    //        finally
    //        {
    //            if (fileStream != null)
    //            {
    //                fileStream.Close();
    //                fileStream.Dispose();
    //            }

    //        }
    //        return true;
    //    }

    //    #endregion
    //}
}