﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;

namespace Automation.Common
{
    /// <summary>
    /// Defines the string value conversion functionality for enums
    /// </summary>
    public class StringValueAttribute : System.Attribute
    {
        private string value;

        public StringValueAttribute(string value)
        {
            this.value = value;
        }

        public string Value
        {
            get { return this.value; }
        }

        public override string ToString()
        {
            return (this.value);
        }
    }

    /// <summary>
    /// Defines the string value implementaion for enums
    /// </summary>
    public static class StringEnum
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;

            Type type = value.GetType();
            System.Reflection.FieldInfo fi = type.GetField(value.ToString());

            StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];

            if (attrs != null)
                if (attrs.Length > 0)
                {
                    output = attrs[0].Value;
                }

            return output;
        }

        public static object GetEnumByStringvalue(string value, Type EnumType)
        {
            Type enumType = EnumType;

            foreach (Enum val in Enum.GetValues(enumType))
            {
                System.Reflection.FieldInfo fi = enumType.GetField(val.ToString());
                StringValueAttribute[] attributes = (StringValueAttribute[])fi.GetCustomAttributes(typeof(StringValueAttribute), false);
                StringValueAttribute attr = attributes[0];

                if (attr.Value.Equals(value))
                    return val;
            }

            return null;
        }
    }
}