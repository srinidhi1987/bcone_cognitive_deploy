var request = {
    "Fields": [
        { "Id": "Sub_Total", "Value": "260" },
        { "Id": "Sub_GST", "Value": "26" },
        { "Id": "Grand_Total", "Value": "286" }
    ],
    "Tables": [{
        "Id": "Table1",
        "Columns": ["QTY", "PRICE", "GST", "TOTAL"],
        "Rows": [["1", "2", "3", "4"],
            ["20", "30", "20", "30"],
            ["2", "6", "6", "12"],
            ["22", "66", "66", "132"]]
    }],
    "Rules": [
        {
            Id: 'Grand_Total',
            Rule: 'Grand_Total==Sub_Total+Sub_GST'
        },
        {
            Id: 'TOTAL',
            Rule: 'TOTAL == QTY * PRICE + GST'
        },
        {
            Id: "GST",
            Rule: 'GST == (QTY * PRICE) * 10 / 100'
        },
         {
             Id: 'Sub_Total',
             Rule: 'Sub_Total == COLSUM("Table1", "QTY*PRICE")'
         },
         {
             Id: 'Sub_GST',
             Rule: 'Sub_GST == COLSUM("Table1", "GST")'
         },
         {
             Id: 'Grand_Total',
             Rule: 'Grand_Total == COLSUM("Table1", "QTY*PRICE")+Sub_GST'
         },
         {
             Id: 'Grand_Total',
             //Rule: 'Grand_Total == ADD(COLSUM("QTY*PRICE"),Sub_GST)'
             Rule: 'Grand_Total == COLSUM("Table1", "QTY*PRICE") + Sub_GST'
         },
        {
            Id: "test 1",//ADD function with static values",
            Rule: "ADD = function() { return 0; }; ADD(1,2,3,4,5) == 0;"
        },
        { Id: "add-test 0.3", Rule: "ADD(0.1, 0.2) == 0.3" },
        { Id: "sub-test 0.1", Rule: "SUB(0.3, 0.2) == 0.1" },
        { Id: "mul-test 0.02", Rule: "MUL(0.2, 0.1) == 0.02" },
        { Id: "div-test 3", Rule: "DIV(0.3, 0.1) == 3" },
    ]

};
/*var request={
    "Fields":[{"Id":"Subtotal","Value":"586.08"},{"Id":"Tax","Value":"58.61"},{"Id":"Amount_Due","Value":"644.69"}],
    "Tables":[{"Id":"Table",
        "Columns":["LI","Name","QTY","UnitPrice","LineTotal"],
        "Rows":[["1","2","3","4","5","6","7","8","9","10"],
            ["Coleman Montana 8 Tent","LodgeLogic ComboCooker","Yaktrax Walker for Snow","40 Finger Flashlights","High Sierra Loop Backpack","Leatherman Bit Driver Extens.","Septor LED Headlamp","Oakley Adult Splice Goggle","Adult Snow Sports Helmet","ColumbiaGirls Snow SlopeBib"],
            ["1","2","1","2","1","3","1","1","2","1"],
            ["138.00","29.99","11.52","8.49","23.99","9.67","30.49","102.39","69.00","35.72"],
            ["138.00","59.98","11.52","16.98","23.99","29.01","30.49","102.39","138.00","35.72"]]}],
        "Rules":[
            {"Id":"Subtotal","Rule":"Subtotal==ROWSUM(\"Table\",\"LineTotal\")"},
            {"Id":"Amount_Due","Rule":"Amount_Due == Subtotal + Tax"},
            {"Id":"LineTotal","Rule":"LineTotal==MUL(QTY,UnitPrice)"}]}*/