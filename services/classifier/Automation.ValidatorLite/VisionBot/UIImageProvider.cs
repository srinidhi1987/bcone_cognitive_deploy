﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot
{
    using Automation.CognitiveData.VisionBot.UI;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Cognitive.Validator.Properties;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;
    using static Validator.UI.ViewModel.DocumentViewModel;

    #region SingleTon

    //internal sealed class UIImageProvider
    //{
    //    public static UIImageProvider Instance
    //    {
    //        get
    //        {
    //            if (_intsance == null)
    //            {
    //                _intsance = new UIImageProvider();
    //            }
    //            return _intsance;
    //        }
    //    }

    //    private static UIImageProvider _intsance = null;
    //    private VisionBotDesignerResourcesHelper _resourcesHelper = null;

    //    //    lock InProcessPath

    //    //private ConcurrentDictionary<int,string> InProcessPath = new ConcurrentDictionary<int,string>();
    //    private ConcurrentBag<string> InProcessPath = new ConcurrentBag< string>();

    //    private UIImageProvider()
    //    {
    //        _resourcesHelper = new VisionBotDesignerResourcesHelper();
    //    }

    //    internal BitmapImage GetBitmapImage(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings, bool doesRequireToCreateNewImage)
    //    {
    //        if (!InProcessPath.Contains(documentProperties.Path) || (System.IO.File.Exists(imagePath) && !doesRequireToCreateNewImage))
    //        {
    //            InProcessPath.Add(documentProperties.Path);

    //            return _resourcesHelper.GetBitmapImage(documentProperties, pageIndex, imagePath, settings, doesRequireToCreateNewImage);
    //            InProcessPath.R (documentProperties.Path);
    //        }
    //    }
    //}

    #endregion SingleTon

    public class UIImageProvider : IDisposable
    {
        public Thread ImageProcessor = null;
        private VisionBotDesignerResourcesHelper _resourcesHelper = null;
        private DocumentProperties _documentProperties = null;
        private ImageProcessingConfig _imageProcessingConfig = null;

        private ReaderWriterLock rwl = new ReaderWriterLock();
        private ManualResetEvent signal = new ManualResetEvent(true);

        private int _currentViewIndex = 0;
        private bool _isStarted = false;
        private bool _isDisposed = false;

        //TODO: Read about Interlocked class.
        //TODO: Read about Slim versions of ReaderWriter and ManualReset.
        internal int CurrentViewIndex
        {
            get
            {
                rwl.AcquireReaderLock(Timeout.Infinite);
                int tempIndex = _currentViewIndex;
                rwl.ReleaseReaderLock();
                return tempIndex;
            }
            set
            {
                if (_currentViewIndex != value)
                {
                    rwl.AcquireWriterLock(Timeout.Infinite);
                    _currentViewIndex = value;
                    rwl.ReleaseWriterLock();
                }
            }
        }

        public ConcurrentDictionary<int, Action<string>> PageProcessActionQueue = new ConcurrentDictionary<int, Action<string>>();

        internal Action QueueAction { get; set; }
        //internal delegate void DocumentPageImageRequestHandler(object sender, DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings);
        internal event DocumentPageImageRequestHandler DocumentPageImageRequested;

        public UIImageProvider(DocumentProperties documentProperties, ImageProcessingConfig settings)
        {
            _documentProperties = documentProperties;
            _imageProcessingConfig = settings;
            _resourcesHelper = new VisionBotDesignerResourcesHelper();

            ImageProcessor = new Thread(new ThreadStart(documentProcessor));
            ImageProcessor.Name = "Layout_" + documentProperties.Path;
            ImageProcessor.IsBackground = true;
        }

        #region Public Method

        internal void StartOrResumeProcessing()
        {
            if (signal != null)
            {
                signal.Set();
            }
            //TODO: _isStarted may have race condition here if at a time multiple threads call this method on same instance.
            if (!_isStarted)
            {
                _isStarted = true;
                ImageProcessor.Start();
            }
        }

        internal void PauseProcess()
        {
            if (signal != null)
            {
                try
                {
                    signal.Reset();
                }
                catch (Exception ex)
                {
                    //Need to handle case of delete layout and add new layout
                    VBotLogger.Warning(() => "[PauseProcess] ");
                }
            }
        }

        internal void GetImage(int pageIndex, Action<string> CallbackAction)
        {
            string pageImagePath = GetPageImagePath(pageIndex);
            if (File.Exists(pageImagePath))
            {
                //BitmapImage image = GetImage(pageImagePath);
                //image = null;
                CallbackAction(pageImagePath);
            }
            else
            {
                PageProcessActionQueue.AddOrUpdate(pageIndex, CallbackAction, (key, oldValue) => oldValue);
            }
        }

        #endregion Public Method

        //private void processDocument()
        //{
        //    int lastViewIndex = CurrentViewIndex;
        //    List<ProcessedPageDetails> ProcessedPageQueue = new List<ProcessedPageDetails>();
        //    for (int pageIndex = 0; pageIndex < _documentProperties.PageProperties.Count; pageIndex++)
        //    {
        //        ProcessedPageDetails proceessedPageDetails = new ProcessedPageDetails(pageIndex, false);
        //        ProcessedPageQueue.Add(proceessedPageDetails);
        //    }
        //    //   bool isAllDoumentProcessed = true;
        //    for (int pageIndex = 0; pageIndex < _documentProperties.PageProperties.Count; pageIndex++)
        //    {
        //        signal.WaitOne();
        //        if (lastViewIndex != CurrentViewIndex)
        //        {
        //            lastViewIndex = CurrentViewIndex;
        //            pageIndex = lastViewIndex;
        //        }

        //        string pageImagePath = _documentProperties.Path + "_Page" + pageIndex.ToString() + ".vimg";
        //        if (!ProcessedPageQueue.Any(x => x.PageIndex == pageIndex && x.IsProcessed))
        //        {
        //            // isAllDoumentProcessed = false;

        //            _resourcesHelper.GetBitmapImage(_documentProperties, pageIndex, pageImagePath, _imageProcessingConfig, true);

        //            ProcessedPageDetails proceessedPageDetails = new ProcessedPageDetails(pageIndex, true);
        //            if (ProcessedPageQueue.Any(x => x.PageIndex == pageIndex))
        //            {
        //                proceessedPageDetails = ProcessedPageQueue.FirstOrDefault(x => x.PageIndex == pageIndex);
        //                proceessedPageDetails.IsProcessed = true;
        //            }
        //            else
        //            {
        //                ProcessedPageQueue.Add(proceessedPageDetails);
        //            }
        //        }
        //        if (PageActionQueue.ContainsKey(pageIndex))
        //        {
        //            PageActionQueue[pageIndex].Invoke(GetImage(pageImagePath));
        //            //if (QueueAction != null)
        //            //{
        //            //    QueueAction.Invoke();
        //            //}
        //            //QueueAction = null;
        //            Action<BitmapImage> temp = null;
        //            if (PageActionQueue.TryRemove(pageIndex, out temp))
        //            {
        //            }
        //        }
        //        if (pageIndex == _documentProperties.PageProperties.Count - 1 && ProcessedPageQueue.Any(x => !x.IsProcessed))
        //        {
        //            pageIndex = 0;
        //        }
        //    }
        //}

        #region Private Method

        private void documentProcessor()
        {
            List<ProcessedPageDetails> ProcessedPageQueue = new List<ProcessedPageDetails>();
            for (int pageIndex = 0; pageIndex < _documentProperties.PageProperties.Count; pageIndex++)
            {
                ProcessedPageDetails proceessedPageDetails = new ProcessedPageDetails(pageIndex, false);
                ProcessedPageQueue.Add(proceessedPageDetails);
            }

            while (ProcessedPageQueue.Any(page => !page.IsProcessed))
            {
                signal.WaitOne();
                if (_isDisposed)
                    return;
                MakeSurePageActionQueueIsEmpty(ProcessedPageQueue);

                int pageIndex = ProcessedPageQueue.FindIndex(CurrentViewIndex, x => !x.IsProcessed);

                pageIndex = pageIndex > -1 ? pageIndex : ProcessedPageQueue.FindIndex(0, x => !x.IsProcessed);

                if (pageIndex > -1)
                {
                    SaveImageForSpecifiedPage(pageIndex);
                    ProcessedPageQueue[pageIndex].IsProcessed = true;
                }
                else
                {
                    break;
                }
            }
            if (_isDisposed)
                return;
            MakeSurePageActionQueueIsEmpty(ProcessedPageQueue);
        }

        private void MakeSurePageActionQueueIsEmpty(List<ProcessedPageDetails> ProcessedPageQueue)
        {
            while (PageProcessActionQueue.Count > 0)
            {
                if (_isDisposed)
                    return;
                KeyValuePair<int, Action<string>> pageActionDetails = PageProcessActionQueue.ElementAt(PageProcessActionQueue.Count - 1);

                BitmapImage image = GetImageOfSpecifiedPage(pageActionDetails.Key);
                image = null;
                string pageImagePath = GetPageImagePath(pageActionDetails.Key);
                ProcessedPageQueue[pageActionDetails.Key].IsProcessed = true;
                pageActionDetails.Value.Invoke(pageImagePath);

                Action<string> temp = null;
                PageProcessActionQueue.TryRemove(pageActionDetails.Key, out temp);

                CurrentViewIndex = pageActionDetails.Key;
            }
        }

        private BitmapImage GetImageOfSpecifiedPage(int pageIndex)
        {
            string pageImagePath = GetPageImagePath(pageIndex);

            return GetBitmapImage(_documentProperties, pageIndex, pageImagePath, _imageProcessingConfig, true);
        }

        private string GetPageImagePath(int pageIndex)
        {
            string[] path = _documentProperties.Path.Split(new[] { Resources.ValidatorTempDirectory + "\\" }, StringSplitOptions.None);

            string imagePath = Path.Combine(Path.GetTempPath(), Resources.ValidatorTempDirectory, path[1] + "_Page" + pageIndex.ToString() + ".vimg");
            string directory = Path.GetDirectoryName(imagePath);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            return imagePath; //_documentProperties.Path + "_Page" + pageIndex.ToString() + ".vimg";
        }

        internal BitmapImage GetBitmapImage(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings, bool doesRequireToCreateNewImage)
        {
            BitmapImage bitmap = null;
            try
            {
                if (!File.Exists(imagePath) || doesRequireToCreateNewImage)
                {
                    bitmap = GetByteImage(documentProperties, pageIndex, imagePath, settings);
                }
                else
                {
                    bitmap = GetImage(imagePath);
                }

                VBotLogger.Trace(() => string.Format("[GetBitmapImage] Sucessfully Image loaded. DocumentProperties path: {0} pageIndex: {1} imagePath: {2}", documentProperties.Path, pageIndex, imagePath));
            }
            catch (Exception ex)
            {
                ex.Log(nameof(UIImageProvider), nameof(GetBitmapImage));
            }
            return bitmap;
        }

        public BitmapImage GetByteImage(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            if (DocumentPageImageRequested != null)
            {
                DocumentPageImageRequested(this, documentProperties, pageIndex, imagePath, settings);
            }
            return GetImage(imagePath);
        }
        private void SaveImageForSpecifiedPage(int pageIndex)
        {
            string pageImagePath = GetPageImagePath(pageIndex);

            if (DocumentPageImageRequested != null)
            {
                DocumentPageImageRequested(this, _documentProperties, pageIndex, pageImagePath, _imageProcessingConfig);
            }
        }

        //private string GetPageImagePath(int pageIndex)
        //{
        //    return _documentProperties.Path + "_Page" + pageIndex.ToString() + ".vimg";
        //}

        public BitmapImage GetImage(string path)
        {
            try
            {
                if (string.IsNullOrEmpty(path))
                {
                    return null;
                }

                string imagePath = File.Exists(path)
                                       ? path
                                       : Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + path;

                imagePath = new Uri(imagePath).LocalPath;

                if (!File.Exists(imagePath))
                {
                    return null;
                }

                using (var stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
                {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = stream;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();

                    stream.Close();
                    stream.Dispose();
                    return bitmapImage;
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[GetImage] Exception occured -> {0}", ex.ToString()));
                return null;
            }
        }

        public void Dispose()
        {
            _isDisposed = true;
            if (signal != null)
            {
                try
                {
                    signal.Set();
                }
                catch (Exception ex)
                {

                }
            }
            if (ImageProcessor.ThreadState != ThreadState.Unstarted)
            {
                ImageProcessor.Join();
            }
            var directoryPath = Path.Combine(
                          Path.GetTempPath(),
                          Resources.ValidatorTempDirectory);
            try
            {
                if (Directory.Exists(directoryPath))
                {
                    DirectoryInfo directory = new DirectoryInfo(directoryPath);
                    string documentName = Path.GetFileName(_documentProperties.Path);
                    foreach (FileInfo file in directory.GetFiles())
                    {
                        try
                        {
                            if (file.Name.Contains(documentName))
                                file.Delete();
                        }
                        catch (Exception ex)
                        {
                            VBotLogger.Warning(() => string.Format("[UIImageProvider->Dispose] [Delete temp file] [Fail] [Document ->{0}] [Location ->{1}] [Exception ->{2}]", documentName, directoryPath, ex.Message));
                        }
                    }
                    VBotLogger.Trace(() => string.Format("[UIImageProvider->Dispose] [Delete temp file] [Success] [{Document ->{0}] [Location ->{1}]", documentName, directoryPath));
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Warning(() => string.Format("[UIImageProvider->Dispose] [Delete temp file] [Fail] [Location ->{1}] [Exception ->{2}]", directoryPath, ex.Message));
            }

        }
        public void DisposeNonBlocking()
        {
            _isDisposed = true;
            if (signal != null)
            {
                try
                {
                    signal.Set();
                }
                catch (Exception ex)
                {

                }
            }
        }
        #endregion Private Method
    }

    public class ProcessedPageDetails
    {
        public int PageIndex;
        public bool IsProcessed;

        public ProcessedPageDetails(int pageIndex = -1, bool isProcessed = false)
        {
            PageIndex = pageIndex;
            IsProcessed = isProcessed;
        }
    }
}