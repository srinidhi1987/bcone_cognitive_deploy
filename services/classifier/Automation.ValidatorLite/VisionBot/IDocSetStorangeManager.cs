﻿
namespace Automation.CognitiveData.VisionBot
{
    using System;

    using Automation.CognitiveData.VBotExecutor;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;

    interface IDocSetManager
    {
        void SaveDocSet(VisionBotManifest visionBotManifest, VBotDocSet docSet);
        void SaveDocSet(string VBotFilePath, VBotDocSet docSet);

        VBotDocSet LoadDocSet(VisionBotManifest visionBotManifest);
        VBotDocSet LoadDocSet(string VBotFilePath);
    }

    internal class DocSetManager : IDocSetManager
    {
        public void SaveDocSet(VisionBotManifest visionBotManifest, VBotDocSet docSet)
        {
            VBotLogger.Trace(() => "[DocSetManager::SaveDocSet] Saving DocSet....");
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
            docSetStorage.Save(docSet);
            VBotLogger.Trace(() => "[DocSetManager::SaveDocSet] DocSet saved successfully."); 
        }

        public void SaveDocSet(string VBotFilePath, VBotDocSet docSet)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
            docSetStorage.Save(docSet);
        }


        public VBotDocSet LoadDocSet(VisionBotManifest visionBotManifest)
        {
            VBotLogger.Trace(() => "[LoadDocSet] Loading DocSet...");

            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
            return docSetStorage.Load();
        }

        public VBotDocSet LoadDocSet(string VBotFilePath)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
            return docSetStorage.Load();
        }
    }

    interface ITestDocSetManager
    {
        void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName);
        void SaveTestDocSet(VBotDocSet docSet, string VBotFilePath, string layoutName);

        VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName);
        VBotDocSet LoadTestDocSet(string VBotFilePath, string layoutName);

        void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName);
    }

    internal class TestDocSetManager : ITestDocSetManager
    {
        public void SaveTestDocSet(VBotDocSet docSet, VisionBotManifest visionBotManifest, string layoutName)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
            docSetStorage.Save(docSet, layoutName);
        }

        public void SaveTestDocSet(VBotDocSet docSet, string VBotFilePath, string layoutName)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
            docSetStorage.Save(docSet, layoutName);
        }


        public VBotDocSet LoadTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
            return docSetStorage.Load(layoutName);
        }

        public VBotDocSet LoadTestDocSet(string VBotFilePath, string layoutName)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(VBotFilePath);
            return docSetStorage.Load(layoutName);
        }


        public void DeleteTestDocSet(VisionBotManifest visionBotManifest, string layoutName)
        {
            IVBotDocSetStorage docSetStorage = new VBotDocSetStorage(visionBotManifest.GetFilePath());
            docSetStorage.Delete(layoutName);
        }
    }
}
