﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

//using PDFLibNet;
//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Linq;
//using System.Text;

//namespace Automation.CognitiveData.VisionBot.pdf
//{
//    public class PdfWrapper123 : IPdfWrapper123
//    {
//        private bool exportInProgress = false;

//        public int DocWidth(string pdfFilePath)
//        {
//            PDFWrapper pdfLib = new PDFWrapper();

//            if (pdfLib.LoadPDF(pdfFilePath))
//            {
//                if (pdfLib.Pages.Count > 0)
//                {
//                    return pdfLib.PageWidth;
//                }
//            }
//            pdfLib.Dispose();
//            return 0;
//        }
//        public int DocHeight(string pdfFilePath)
//        {
//            PDFWrapper pdfLib = new PDFWrapper();

//            if (pdfLib.LoadPDF(pdfFilePath))
//            {
//                if (pdfLib.Pages.Count > 0)
//                {
//                    return pdfLib.PageHeight;
//                }
//            }
//            pdfLib.Dispose();
//            return 0;
//        }

//        public void ExportPdfToImage(string pdfFilePath, string imagePath, int renderDpi)
//        {
//            PDFWrapper pdfLib = new PDFWrapper();
//            int imageOption = 70;
//            if (pdfLib.LoadPDF(pdfFilePath))
//            {
//                if (pdfLib.Pages.Count > 0)
//                {
//                    exportInProgress = false;
//                    pdfLib.ExportJpgFinished += pdfLib_ExportJpgFinished;
//                    pdfLib.ExportJpg(imagePath, 1, 1, renderDpi, imageOption);

//                    while (!exportInProgress)
//                    {
//                        System.Threading.Thread.Sleep(100);
//                    }
//                }
//            }
//            pdfLib.Dispose();
//        }

//        void pdfLib_ExportJpgFinished()
//        {
//            System.Threading.Thread.Sleep(1000);
//            exportInProgress = true;
//        }

//        public int PageCount(string pdfFilePath)
//        {
//            PDFWrapper pdfLib = new PDFWrapper();

//            if (pdfLib.LoadPDF(pdfFilePath))
//            {
//                if (pdfLib.Pages.Count > 0)
//                {
//                    return pdfLib.Pages.Count;
//                }
//            }
//            pdfLib.Dispose();

//            return 0;
//        }

//    }
//}