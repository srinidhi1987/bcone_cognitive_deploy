﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;

using System.Collections.Generic;

using System.Linq;

using System.Text;

using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot
{
    public interface ITableDefinitionManager
    {
        TableDef GetTable(string tableId);

        IList<TableDef> GetAllTables();

        void AddTable(TableDef table);

        void UpdateTable(TableDef table);

        void RemoveTable(string tableId);

        FieldDef GetTableField(string tableId, string fieldId);

        IList<FieldDef> GetAllTableFields(string tableId);

        void AddTableField(string tableId, FieldDef field);

        void UpdateTableField(string tableId, FieldDef field);

        void RemoveTableField(string tableId, string fieldId);
    }

    public class TableDefinitionManager : ITableDefinitionManager
    {
        private VisionBotEngine.Model.VisionBot _visionBot;

        public TableDefinitionManager(VisionBotEngine.Model.VisionBot visionBot)
        {
            _visionBot = visionBot;
        }

        public TableDef GetTable(string tableId)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException("tableId");
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            return tableDefinition;
        }

        public IList<TableDef> GetAllTables()
        {
            return _visionBot.DataModel.Tables;
        }

        public void AddTable(TableDef table)
        {
            if (table == null)
            {
                throw new ArgumentNullException("table");
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(table.Id));
            if (tableDefinition != null)
            {
                throw new TableIdDuplicationException(table.Id);
            }

            _visionBot.DataModel.Tables.Add(table);
        }

        public void UpdateTable(TableDef table)
        {
            if (table == null)
            {
                throw new ArgumentNullException();
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(table.Id));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(table.Id);
            }

            _visionBot.DataModel.Tables.Remove(tableDefinition);
            _visionBot.DataModel.Tables.Add(table);
        }

        public void RemoveTable(string tableId)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException();
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            _visionBot.DataModel.Tables.Remove(tableDefinition);
        }

        public FieldDef GetTableField(string tableId, string fieldId)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException("tableId");
            }

            if (string.IsNullOrEmpty(fieldId))
            {
                throw new ArgumentNullException("fieldId");
            }

            VisionBotEngine.Model.TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            IList<FieldDef> fieldDefinitions = tableDefinition.Columns;

            if (fieldDefinitions == null)
            {
                throw new TableFieldNotFoundException(tableId, fieldId);
            }

            FieldDef fieldDefinition = fieldDefinitions.FirstOrDefault(p => p.Id.Equals(fieldId));
            if (fieldDefinition == null)
            {
                throw new TableFieldNotFoundException(tableId, fieldId);
            }

            return fieldDefinition;
        }

        public IList<FieldDef> GetAllTableFields(string tableId)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException("tableId");
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            return tableDefinition.Columns;
        }

        public void AddTableField(string tableId, FieldDef field)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException("tableId");
            }

            if (field == null)
            {
                throw new ArgumentNullException("field");
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            IList<FieldDef> fieldDefinitions = tableDefinition.Columns;
            FieldDef fieldDef = fieldDefinitions.FirstOrDefault(p => p.Id.Equals(field.Id));
            if (fieldDef != null)
            {
                throw new FieldIdDuplicationException(tableId, field.Id);
            }

            tableDefinition.Columns.Add(field);
        }

        public void UpdateTableField(string tableId, FieldDef field)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException("tableId");
            }

            if (field == null)
            {
                throw new ArgumentNullException("field");
            }

            TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            FieldDef fieldDefinition = tableDefinition.Columns.FirstOrDefault(p => p.Id.Equals(field.Id));

            if (fieldDefinition == null)
            {
                throw new TableFieldNotFoundException(tableId, field.Id);
            }

            tableDefinition.Columns.Remove(fieldDefinition);
            tableDefinition.Columns.Add(field);
        }

        public void RemoveTableField(string tableId, string fieldId)
        {
            if (string.IsNullOrEmpty(tableId))
            {
                throw new ArgumentNullException("tableId");
            }

            if (string.IsNullOrEmpty(fieldId))
            {
                throw new ArgumentNullException("field");
            }

            VisionBotEngine.Model.TableDef tableDefinition = _visionBot.DataModel.Tables.FirstOrDefault(p => p.Id.Equals(tableId));
            if (tableDefinition == null)
            {
                throw new TableNotFoundException(tableId);
            }

            FieldDef fieldDefinition = tableDefinition.Columns.FirstOrDefault(p => p.Id.Equals(fieldId));
            if (fieldDefinition == null)
            {
                throw new TableFieldNotFoundException(tableId, fieldId);
            }

            tableDefinition.Columns.Remove(fieldDefinition);
        }
    }

    public class TableException : Exception
    {
        public string TableId { get; private set; }

        public TableException(string tableId)
            : base()
        {
            this.TableId = tableId;
        }

        public TableException(string tableId, string message)
            : base(message)
        {
            this.TableId = tableId;
        }

        public TableException(string tableId, string message, Exception innerException)
            : base(message, innerException)
        {
            this.TableId = tableId;
        }
    }

    class TableNotFoundException : TableException
    {
        public TableNotFoundException(string tableId)
            : base(tableId)
        {
        }
    }

    class TableFieldNotFoundException : TableException
    {
        public string FieldId { get; private set; }

        public TableFieldNotFoundException(string tableId, string fieldId)
            : base(tableId)
        {
            this.FieldId = fieldId;
        }
    }

    class TableIdDuplicationException : TableException
    {
        public TableIdDuplicationException(string tableId)
            : base(tableId)
        {
        }
    }

    class FieldIdDuplicationException : TableException
    {
        public string FieldId { get; private set; }
        public FieldIdDuplicationException(string tableId, string fieldId)
            : base(tableId)
        {
            this.FieldId = fieldId;
        }
    }
}
