﻿using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    class TestResultSummaryTemplateSelector : DataTemplateSelector
    {
        private const string CorrectnessTemplate = "CorrectnessTemplate";
        //private const string LayoutErrorTemplate = "LayoutErrorTemplate";
       // private const string DataRecognitionErrorTemplate = "DataRecognitionErrorTemplate";


        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            var contentControl = container as ContentPresenter;
            if (contentControl != null)
            {
                if (item is SummaryViewModel)
                {
                    return contentControl.FindResource(CorrectnessTemplate) as DataTemplate;
                }
            }
            return null;
        }
    }
}
