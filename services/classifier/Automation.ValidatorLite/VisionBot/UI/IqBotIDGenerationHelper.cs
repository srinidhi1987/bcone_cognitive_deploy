﻿
namespace Automation.CognitiveData.VisionBot.UI
{
    using System;
    using System.Collections.Generic;
    public class IqBotIDGenerationHelper
    {
        private const string _fieldIDPrefix = "FIELD";
        private const string _columnIDPrefix = "COLUMN";
        private const string _tableIDPrefix = "TABLE";
        private const string _markerIDPrefix = "MARKER";
        private const string _layoutIDPrefix = "LAYOUT";

        public string GetNewFieldId(IqBot iqBot)
        {
            //IList<string> allIds = getAllDataModelIds(iqBot);
            //return generateNewID(_fieldIDPrefix, allIds);

            if (string.IsNullOrEmpty(iqBot.DataModelIdStructure.FieldId))
            {
                int lastDataModelFieldId = getDataModelLastFieldId(iqBot, _fieldIDPrefix);
                iqBot.DataModelIdStructure.FieldId = _fieldIDPrefix + lastDataModelFieldId.ToString("D3");
            }

            string newId = generateNewID(_fieldIDPrefix, iqBot.DataModelIdStructure.FieldId);
            //iqBot.DataModelIdStructure.FieldId = newId;
            return newId;
        }

        public string GetNewColumnId(IqBot iqBot)
        {
            //IList<string> allIds = getAllDataModelIds(iqBot);
            //return generateNewID(_columnIDPrefix, allIds);

            if (string.IsNullOrEmpty(iqBot.DataModelIdStructure.ColumnId))
            {
                int lastDataModelFieldId = getDataModelLastColumnId(iqBot, _columnIDPrefix);
                iqBot.DataModelIdStructure.ColumnId = _columnIDPrefix + lastDataModelFieldId.ToString("D3");
            }

            string newId = generateNewID(_columnIDPrefix, iqBot.DataModelIdStructure.ColumnId);
            //iqBot.DataModelIdStructure.ColumnId = newId;
            return newId;
        }

        public string GetNewTableId(IqBot iqBot)
        {
            //IList<string> allIds = getAllDataModelIds(iqBot);
            //return generateNewID(_tableIDPrefix, allIds);

            if (string.IsNullOrEmpty(iqBot.DataModelIdStructure.TableDefId))
            {
                int lastDataModelFieldId = getDataModelLastTableId(iqBot, _tableIDPrefix);
                iqBot.DataModelIdStructure.TableDefId = _tableIDPrefix + lastDataModelFieldId.ToString("D3");
            }

            string newId = generateNewID(_tableIDPrefix, iqBot.DataModelIdStructure.TableDefId);
            //iqBot.DataModelIdStructure.TableDefId = newId;
            return newId;
        }

        public string GetNewLayoutFieldID(IqBot iqBot, string layoutID)
        {
            IList<string> allIds = getAllLayoutIds(iqBot);
            return generateNewID(layoutID + "_" + _fieldIDPrefix, allIds);
        }

        public string GetNewLayoutColumnID(IqBot iqBot, string layoutID)
        {
            IList<string> allIds = getAllLayoutIds(iqBot);
            return generateNewID(layoutID + "_" + _columnIDPrefix, allIds);
        }

        public string GetNewLayoutTableID(IqBot iqBot, string layoutID)
        {
            IList<string> allIds = getAllLayoutIds(iqBot);
            return generateNewID(layoutID + "_" + _tableIDPrefix, allIds);
        }

        public string GetNewLayoutMarkerID(IqBot iqBot, string layoutID)
        {
            IList<string> allIds = getAllLayoutIds(iqBot);
            return generateNewID(layoutID + "_" + _markerIDPrefix, allIds);
        }

        public string GetNewLayoutID(IqBot iqBot)
        {
            IList<string> allIds = getAllLayoutIds(iqBot);
            return generateNewID(_layoutIDPrefix, allIds);
        }

        private int getDataModelLastFieldId(IqBot iqBot, string prefix)
        {
            int lastFieldId = 0;

            if (iqBot.DataModel != null)
            {
                if (iqBot.DataModel.Fields != null)
                {
                    for (int i = 0; i < iqBot.DataModel.Fields.Count; i++)
                    {
                        var id = Convert.ToInt32(iqBot.DataModel.Fields[i].Id.Replace(prefix, ""));
                        lastFieldId = (id > lastFieldId) ? id : lastFieldId;
                    }
                }
            }

            return lastFieldId;
        }

        private int getDataModelLastTableId(IqBot iqBot, string prefix)
        {
            int lastFieldId = 0;

            if (iqBot.DataModel != null)
            {
                if (iqBot.DataModel.Tables != null)
                {
                    for (int i = 0; i < iqBot.DataModel.Tables.Count; i++)
                    {
                        var id = Convert.ToInt32(iqBot.DataModel.Tables[i].Id.Replace(prefix, ""));
                        lastFieldId = (id > lastFieldId) ? id : lastFieldId;
                    }
                }
            }

            return lastFieldId;
        }

        private int getDataModelLastColumnId(IqBot iqBot, string prefix)
        {
            int lastFieldId = 0;
            if (iqBot.DataModel != null)
            {
                if (iqBot.DataModel.Tables != null)
                {
                    for (int i = 0; i < iqBot.DataModel.Tables.Count; i++)
                    {
                        if (iqBot.DataModel.Tables[i].Columns != null)
                        {
                            for (int j = 0; j < iqBot.DataModel.Tables[i].Columns.Count; j++)
                            {
                                var id = Convert.ToInt32(iqBot.DataModel.Tables[i].Columns[j].Id.Replace(prefix, ""));
                                lastFieldId = (id > lastFieldId) ? id : lastFieldId;
                            }
                        }
                    }
                }
            }

            return lastFieldId;
        }

        private IList<string> getAllLayoutMarkerIDs(Layout layout)
        {
            IList<string> allIds = new List<string>();
            if (layout != null)
            {
                if (layout.Markers != null)
                {
                    for (int j = 0; j < layout.Markers.Fields.Count; j++)
                    {
                        if (layout.Markers.Fields[j] != null)
                        {
                            allIds.Add(layout.Markers.Fields[j].Id);
                        }
                    }
                }
            }

            return allIds;
        }

        private IList<string> getAllLayoutFieldIDs(Layout layout)
        {
            IList<string> allIds = new List<string>();
            if (layout != null)
            {
                if (layout.LayoutFields != null)
                {
                    for (int j = 0; j < layout.LayoutFields.Fields.Count; j++)
                    {
                        if (layout.LayoutFields.Fields[j] != null)
                        {
                            allIds.Add(layout.LayoutFields.Fields[j].Id);
                        }
                    }
                }
            }

            return allIds;
        }

        private IList<string> getAllLayoutTableAndColumnIDs(Layout layout)
        {
            IList<string> allIds = new List<string>();
            if (layout.LayoutTables != null && layout.LayoutTables.Tables != null)
            {
                for (int j = 0; j < layout.LayoutTables.Tables.Count; j++)
                {
                    if (layout.LayoutTables.Tables[j] != null)
                    {
                        allIds.Add(layout.LayoutTables.Tables[j].Id);
                        if (layout.LayoutTables.Tables[j].Fields != null)
                        {
                            for (int k = 0; k < layout.LayoutTables.Tables[j].Fields.Count; k++)
                            {
                                allIds.Add(layout.LayoutTables.Tables[j].Fields[k].Id);
                            }
                        }
                    }
                }
            }

            return allIds;
        }

        private IList<string> getAllLayoutIds(IqBot iqBot)
        {
            List<string> allIds = new List<string>();
            if (iqBot.Layouts != null)
            {
                for (int i = 0; i < iqBot.Layouts.Count; i++)
                {
                    if (iqBot.Layouts[i] != null)
                    {
                        allIds.Add(iqBot.Layouts[i].Id);
                        allIds.AddRange(getAllLayoutMarkerIDs(iqBot.Layouts[i]));
                        allIds.AddRange(getAllLayoutFieldIDs(iqBot.Layouts[i]));
                        allIds.AddRange(getAllLayoutTableAndColumnIDs(iqBot.Layouts[i]));
                    }
                }
            }
            return allIds;
        }

        private string generateNewID(string prefix, IList<string> allIds)
        {
            int i = 1;
            string newFieldID = prefix + i.ToString("D3");
            while (allIds.Contains(newFieldID))
            {
                i++;
                newFieldID = prefix + i.ToString("D3");
            }
            return newFieldID;
        }

        private string generateNewID(string prefix, string Id)
        {
            int i = string.IsNullOrEmpty(Id) ? 1 : Convert.ToInt32(Id.Replace(prefix, ""));
            return prefix + (++i).ToString("D3");
        }
    }
}
