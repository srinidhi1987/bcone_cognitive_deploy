﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    class RegionBox : Border
    {
        private bool _isDefiningRegion;
        internal bool IsDefiningRegion
        {
            get { return _isDefiningRegion; }
            set
            {
                _isDefiningRegion = value;
                var thumbsGrid = Child as Grid;
                if (thumbsGrid != null)
                {
                    thumbsGrid.Visibility = (_isDefiningRegion) ? Visibility.Collapsed : Visibility.Visible;
                }
            }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(RegionBox), null);
        
        public bool EnableResize
        {
            get { return (bool)GetValue(EnableResizeProperty); }
            set { SetValue(EnableResizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableResize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableResizeProperty =
            DependencyProperty.Register("EnableResize", typeof(bool), typeof(RegionBox), new PropertyMetadata(false));
        
        protected virtual void OnClick()
        {
            RoutedEventArgs newEvent = new RoutedEventArgs(ButtonBase.ClickEvent, this);
            RaiseEvent(newEvent);

            if (null != Command && Command.CanExecute(null))
            {
                Command.Execute(null);
            }
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new RegionBoxAutomationPeer(this);
        }

        internal void AutomationRegionBoxClick()
        {
            this.OnClick();
        }

        internal string AutomationRegionBoxName()
        {
            RegionViewModel regionViewModel = base.DataContext as RegionViewModel;
            LayoutField layoutField = regionViewModel?.Region as LayoutField;
            if (layoutField != null)
            {
                if (!string.IsNullOrEmpty(layoutField.Label))
                {
                    return layoutField.Label;
                }

                return layoutField.FieldDef.Name;
            }

            return string.Empty;
        }
    }
}
