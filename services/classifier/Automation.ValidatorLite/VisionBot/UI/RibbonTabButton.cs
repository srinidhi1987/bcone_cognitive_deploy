﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System.Windows.Media.Imaging;

namespace Automation.CognitiveData.VisionBot.UI
{
   public class RibbonTabButton : ViewModelBase
    {
        private bool _isSelected;

        private bool _isEnabled;

        private bool _isSettingsVisible;

        public bool IsSettingsVisible
        {
            get
            {
                return _isSettingsVisible;
            }
            set
            {
                _isSettingsVisible = value;
                OnPropertyChanged(nameof(IsSettingsVisible));
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(ImageSource));
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                OnPropertyChanged(nameof(IsEnabled));
            }
        }

        public BitmapImage ImageSource
        {
            get
            {
                VisionBotDesignerResourcesHelper vbotResourceHelper = new VisionBotDesignerResourcesHelper();
               
                return vbotResourceHelper.GetImage(ActiveImageSource);
                
            }
        }

        public string ActiveImageSource { get; set; }

    }
}
