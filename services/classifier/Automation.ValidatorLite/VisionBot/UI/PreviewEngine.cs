﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using Automation.CognitiveData.VBotDataExporter;
using Automation.CognitiveData.VisionBot.UI.Model;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;
using Automation.CognitiveData.ConfidenceScore;

namespace Automation.CognitiveData.VisionBot.UI
{
    public interface IPreviewEngine
    {
        BenchmarkData GeneratePreview();
    }

    public class PreviewEngine : IPreviewEngine
    {
        private VBotDocument _vBotDocument;

        public PreviewEngine(VBotDocument vBotDocument)
        {
            _vBotDocument = vBotDocument;
        }

        public BenchmarkData GeneratePreview()
        {
            ITestDataEngine testDataEngine = new TestDataEngine() { VBotDocument = _vBotDocument };
            BenchmarkData benchmarkData = testDataEngine.GenerateValidatedBanchMarkData();

           return benchmarkData;
        }

        

    }
}
