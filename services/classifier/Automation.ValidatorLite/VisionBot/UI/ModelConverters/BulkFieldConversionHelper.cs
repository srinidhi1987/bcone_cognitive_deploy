/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot.UI
{
    class BulkFieldConversionHelper
    {
        public ObservableCollection<LayoutField> ConvertFromFieldsWithType(List<FieldLayout> fields, ElementType elementType, List<FieldDef> fieldDefs)
        {
            var layoutFields = new ObservableCollection<LayoutField>();
            ILayoutFieldConverter layoutFIeldConverter = new LayoutFieldConverter();
            foreach (var field in fields)
            {
                FieldDef fieldDef = getFieldDefForFieldLayout(field, fieldDefs);
                layoutFields.Add(layoutFIeldConverter.ConvertFrom(field, elementType, fieldDef));
            }
            return layoutFields;
        }

        public ObservableCollection<LayoutField> ConvertFromFieldsWithType(List<MarkerLayout> fields)
        {
            var layoutFields = new ObservableCollection<LayoutField>();
            ILayoutFieldConverter layoutFIeldConverter = new LayoutFieldConverter();
            foreach (var field in fields)
            {
                layoutFields.Add(layoutFIeldConverter.ConvertFrom(field));
            }
            return layoutFields;
        }

        public List<FieldLayout> ConvertFieldsTo(ObservableCollection<LayoutField> layoutFields)
        {
            var fields = new List<FieldLayout>();
            ILayoutFieldConverter layoutFIeldConverter = new LayoutFieldConverter();
            foreach (var layoutField in layoutFields)
            {
                fields.Add(layoutFIeldConverter.ConvertTo(layoutField));
            }
            return fields;
        }

        public List<MarkerLayout> ConvertFieldsToMarkers(ObservableCollection<LayoutField> layoutFields)
        {
            var fields = new List<MarkerLayout>();
            ILayoutFieldConverter layoutFIeldConverter = new LayoutFieldConverter();
            foreach (var layoutField in layoutFields)
            {
                fields.Add(layoutFIeldConverter.ConvertToMarkerLayout(layoutField));
            }
            return fields;
        }

        private FieldDef getFieldDefForFieldLayout(FieldLayout fieldLayout, List<FieldDef> fieldDefs)
        {
            FieldDef fieldDef = null;
            if (fieldLayout != null && fieldDefs != null)
            {
                string fieldDefID = fieldLayout.FieldId;
                fieldDef = (from q in fieldDefs where q.Id == fieldDefID select q).FirstOrDefault();
            }

            return fieldDef;
        }
    }
}