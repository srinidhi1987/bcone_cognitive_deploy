/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows;
using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot.UI
{
    class LayoutFieldConverter : ILayoutFieldConverter
    {
        public LayoutField ConvertFrom(FieldLayout fieldLayout, ElementType elementType, FieldDef fieldDef)
        {
            var layoutField =  new LayoutField
            {
                EndsWith = fieldLayout.EndsWith,
                FieldDef = fieldDef,
                FieldDirection = fieldLayout.FieldDirection,
                FormatExpression = fieldLayout.FormatExpression,
                Id = fieldLayout.Id,
                IsMultiLine = fieldLayout.IsMultiline,
                Label = fieldLayout.Label,
                MergeRatio = fieldLayout.MergeRatio,
                SimilarityFactor = fieldLayout.SimilarityFactor,
                StartsWith = fieldLayout.StartsWith,
                Type = elementType,
                Value = fieldLayout.DisplayValue,
                ValueBounds = fieldLayout.ValueBounds.ConvertToRect(),
                IsValueBoundAuto = fieldLayout.IsValueBoundAuto,
                UserDefinedRegionType = LayoutConverter.GetUDRRegionType(fieldLayout.Type),
                ValueSelectionType = fieldLayout.ValueType
            };
            layoutField.OriginalBounds = fieldLayout.Bounds.ConvertToRect();
            if (layoutField.Type == ElementType.Table)
            {
                layoutField.Bounds = fieldLayout.Bounds.Add(fieldLayout.ValueBounds).ConvertToRect();
            }
            else
            {
                layoutField.Bounds = fieldLayout.Bounds.ConvertToRect();
            }
            return layoutField;
        }

        public LayoutField ConvertFrom(MarkerLayout markerLayout)
        {
            return new LayoutField
            {
                Bounds = markerLayout.Bounds.ConvertToRect(),
                Label = markerLayout.Label,
                MergeRatio = markerLayout.MergeRatio,
                MarkerName = markerLayout.Name,
                Type = ElementType.Marker,
                Id = markerLayout.Id,
                SimilarityFactor = markerLayout.SimilarityFactor,
                UserDefinedRegionType = LayoutConverter.GetUDRRegionType(markerLayout.Type),
                IsValueBoundAuto = false
            };
        }

        public FieldLayout ConvertTo(LayoutField layoutField)
        {
            var fieldLayout =  new FieldLayout
            {
                FieldId = layoutField.FieldDef != null ? layoutField.FieldDef.Id : string.Empty,
                IsMultiline = layoutField.IsMultiLine,
                Label = layoutField.Label,
                MergeRatio = layoutField.MergeRatio,
                Id = layoutField.Id,
                FieldDirection = layoutField.FieldDirection,
                SimilarityFactor = layoutField.SimilarityFactor,
                StartsWith = layoutField.StartsWith,
                EndsWith = layoutField.EndsWith,
                FormatExpression = layoutField.FormatExpression,
                DisplayValue = layoutField.Value,
                Type = LayoutConverter.GetFieldType(layoutField.UserDefinedRegionType),
                IsValueBoundAuto = layoutField.IsValueBoundAuto,
                ValueType = layoutField.ValueSelectionType
            };
            if (ElementType.Table.Equals(layoutField.Type))
            {
                if (isRegionResized(layoutField))
                {
                    fieldLayout.ValueBounds =
                        layoutField.Bounds.Subtract(layoutField.OriginalBounds)
                            .ConvertToRectangle();
                }
                else
                {
                    fieldLayout.ValueBounds = layoutField.ValueBounds.ConvertToRectangle();
                }
                fieldLayout.Bounds = layoutField.OriginalBounds.ConvertToRectangle();
           }
            else
            {
                fieldLayout.ValueBounds = layoutField.ValueBounds.ConvertToRectangle();
                fieldLayout.Bounds = layoutField.Bounds.ConvertToRectangle();
            }
            return fieldLayout;
        }

        private static bool isRegionResized(LayoutField layoutField)
        {
            return !(layoutField.OriginalBounds.X.Equals(System.Convert.ToInt32(layoutField.Bounds.X) - System.Convert.ToInt32(layoutField.ValueBounds.X)) &&
                    layoutField.OriginalBounds.Y.Equals(System.Convert.ToInt32(layoutField.Bounds.Y) - System.Convert.ToInt32(layoutField.ValueBounds.Y)) &&
                    isWidthNotChanged(layoutField) &&
                    isHeightNotChanged(layoutField));
        }

        private static bool isWidthNotChanged(LayoutField layoutField)
        {
            if (layoutField.OriginalBounds.Width.Equals(getCurrentWidth(layoutField)))
            {
                return true;
            }
            if (layoutField.Bounds.Width.Equals(layoutField.ValueBounds.Width))
            {
                return true;
            }
            return false;
        }

        private static bool isHeightNotChanged(LayoutField layoutField)
        {
            if (layoutField.OriginalBounds.Height.Equals(getCurrentHeight(layoutField)))
            {
                return true;
            }
            if (layoutField.Bounds.Height.Equals(layoutField.ValueBounds.Height))
            {
                return true;
            }
            return false;
        }

        private static int getCurrentHeight(LayoutField layoutField)
        {
            if (isFieldResized(layoutField))
            {
                return System.Convert.ToInt32(layoutField.Bounds.Height);
            }

            return System.Convert.ToInt32(layoutField.Bounds.Height) + System.Convert.ToInt32(layoutField.ValueBounds.Y);
        }

        private static bool isFieldResized(LayoutField layoutField)
        {
            return System.Convert.ToInt32(layoutField.ValueBounds.Width) == 0 && System.Convert.ToInt32(layoutField.ValueBounds.Height) == 0;
        }

        private static int getCurrentWidth(LayoutField layoutField)
        {
            if (isFieldResized(layoutField))
            {
                return System.Convert.ToInt32(layoutField.Bounds.Width);
            }

            return System.Convert.ToInt32(layoutField.Bounds.Width) + System.Convert.ToInt32(layoutField.ValueBounds.X);
        }

        public MarkerLayout ConvertToMarkerLayout(LayoutField layoutField)
        {
            return new MarkerLayout
            {
                Label = layoutField.Label,
                MergeRatio = layoutField.MergeRatio,
                Id = layoutField.Id,
                Name = layoutField.MarkerName,
                SimilarityFactor = layoutField.SimilarityFactor,
                Bounds = layoutField.Bounds.ConvertToRectangle(),
                Type = LayoutConverter.GetFieldType(layoutField.UserDefinedRegionType)
            };
        }
    }
}