﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.IO;
using Automation.CognitiveData.Properties;
using Automation.CognitiveData.VisionBot.UI.Model;
using Automation.VisionBotEngine.Model;
using BenchmarkData = Automation.VisionBotEngine.Model.BenchmarkData;
using Automation.CognitiveData.ConfidenceScore;
using Automation.VisionBotEngine;

namespace Automation.CognitiveData.VisionBot.UI.ModelConverters
{
    interface IDocSetConverter
    {
        DocSetItem ConvertFrom(VBotDocItem vBotDocItem);
        VBotDocItem ConvertTo(DocSetItem vBotDocItem);
    }

    public class DocSetConverter : IDocSetConverter
    {
        public DocSetItem ConvertFrom(VBotDocItem vBotDocItem)
        {
            VBotLogger.Trace(() => "[ConvertFrom] Convert from DocItem to DocSetItem");
            DocSetItem docSetItem = new DocSetItem
            {
                Id = vBotDocItem.Id,
                Name = vBotDocItem.Name,
                DocumentName = vBotDocItem.DocumentName,
                IsBenchMarkDataSaved = vBotDocItem.IsBenchMarkDataValidated,
                IsBenchmarkStructureMismatchFound = vBotDocItem.IsBenchmarkStructureMismatchFound,
                OriginalIsBenchmarchStructureFound = vBotDocItem.IsBenchmarkStructureMismatchFound,
                Fields = vBotDocItem.BenchmarkData.Fields,
                TableSet = vBotDocItem.BenchmarkData.TableSet,
                DocumentProperties = vBotDocItem.DocumentProperties,
                SystemIdentifiedFields = new LayoutConverter().ConvertSIRFields(vBotDocItem.SirFields)
            };

            VBotLogger.Trace(() => "[ConvertFrom] Converted successfully DocItem to DocSetItem");
            return docSetItem;
        }

        public List<DocSetItem> ConvertFrom(VBotDocSet vBotDocSet)
        {
            List<DocSetItem> docItems = new List<DocSetItem>();

            foreach (var vBotDocItem in vBotDocSet.DocItems)
            {
                DocSetItem docSetItem = new DocSetItem()
                {
                    Id = vBotDocItem.Id,
                    Name = vBotDocItem.Name,
                    DocumentName = vBotDocItem.DocumentName,
                    DocumentProperties = vBotDocItem.DocumentProperties,
                    IsBenchMarkDataSaved = vBotDocItem.IsBenchMarkDataValidated,
                    IsBenchmarkStructureMismatchFound = vBotDocItem.IsBenchmarkStructureMismatchFound,
                    OriginalIsBenchmarchStructureFound = vBotDocItem.IsBenchmarkStructureMismatchFound,
                    Fields = vBotDocItem.BenchmarkData.Fields,
                    TableSet = vBotDocItem.BenchmarkData.TableSet,
                    SystemIdentifiedFields = new LayoutConverter().ConvertSIRFields(vBotDocItem.SirFields)
                };
                
                docItems.Add(docSetItem);

            }
            return docItems;
        }

        public VBotDocItem ConvertTo(DocSetItem docSetItem)
        {
            VBotLogger.Trace(() => "[ConvertFrom] Convert from DocSetItem to DocItem");

            var vbotDocItem = new VBotDocItem
            {
                DocumentName = docSetItem.DocumentName,
                DocumentProperties = docSetItem.DocumentProperties,
                Name = docSetItem.Name,
                IsBenchMarkDataValidated = docSetItem.IsBenchMarkDataSaved,
                IsBenchmarkStructureMismatchFound = docSetItem.IsBenchmarkStructureMismatchFound,
                Id = string.IsNullOrWhiteSpace(docSetItem.Id) ? Guid.NewGuid().ToString() : docSetItem.Id,
                BenchmarkData = new TestDataEngine().GenerateBanchMarkData(docSetItem.Fields, docSetItem.TableSet)
            };

            VBotLogger.Trace(() => "[ConvertFrom] Converted successfully DocSetItem to DocItem");
            return vbotDocItem;
        }

    }
}
