/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Drawing;
using System.Linq;
using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot.UI
{
    class LayoutTableConverter : ILayoutTableConverter
    {
        public LayoutTable ConvertFrom(TableLayout tableLayout, TableDef tableDef)
        {
            var bulkFieldConversionHelper = new BulkFieldConversionHelper();
            ILayoutFieldConverter layoutFIeldConverter = new LayoutFieldConverter();
            LayoutTable layoutTable = new LayoutTable
            {
                Id = tableLayout.Id,
                TableDef = tableDef,// !string.IsNullOrWhiteSpace(tableLayout.TableId) ? tableLayout.TableId : tableDef.Id, //TODO: Here tableLayout.TableId should not be empty or null unless it is old visionbot created with temporary ui. So we need to remove this preventive condition in future.
                Fields = bulkFieldConversionHelper.ConvertFromFieldsWithType(tableLayout.Columns, ElementType.Table, tableDef != null ? tableDef.Columns.ToList() : null),
                Height = (int)(tableLayout.Bounds.Height ),
                Left = (int)(tableLayout.Bounds.Left ),
                Top = (int)(tableLayout.Bounds.Top ),
                Type = ElementType.Table,
                Width = (int)(tableLayout.Bounds.Width ),
                Footer = tableLayout.Footer != null ? layoutFIeldConverter.ConvertFrom(tableLayout.Footer, ElementType.Field, null) : new LayoutField() { MergeRatio = 1f, SimilarityFactor = 0.7f }
            };
            LayoutField primaryField = null;
            if (layoutTable.Fields != null && tableLayout.PrimaryColumn != null)
            {
                primaryField = (from q in layoutTable.Fields where q.Id == tableLayout.PrimaryColumn.Id select q).FirstOrDefault();
            }

            layoutTable.PrimaryField = primaryField;

            return layoutTable;
        }

        public TableLayout ConvertTo(LayoutTable layoutTable)
        {
            var bulkFieldConversionHelper = new BulkFieldConversionHelper();
            ILayoutFieldConverter layoutFIeldConverter = new LayoutFieldConverter();
            TableLayout tableLayout = new TableLayout
            {
                TableId = layoutTable.TableDef.Id,
                Columns = bulkFieldConversionHelper.ConvertFieldsTo(layoutTable.Fields),
                Bounds = new Rectangle(layoutTable.Left, layoutTable.Top, layoutTable.Width, layoutTable.Height),
                Id = layoutTable.Id,
                Footer = layoutFIeldConverter.ConvertTo(layoutTable.Footer)
            };

            FieldLayout primaryColumn = null;
            if (tableLayout.Columns != null && layoutTable.PrimaryField != null)
            {
                primaryColumn = (from q in tableLayout.Columns where q.Id == layoutTable.PrimaryField.Id select q).FirstOrDefault();
            }

            tableLayout.PrimaryColumn = primaryColumn;

            return tableLayout;
        }
    }
}