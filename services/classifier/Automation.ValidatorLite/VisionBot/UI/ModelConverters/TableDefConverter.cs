﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Automation.CognitiveData.VisionBot.UI
{
    class TableDefConverter : ITableDefConverter
    {
        public TableDef ConvertFrom(VisionBotEngine.Model.TableDef tableDef)
        {
            return new TableDef
            {
                Id = tableDef.Id,
                Name = tableDef.Name,
                Columns = convertFromColumns(tableDef.Columns)
            };
        }

        public ObservableCollection<FieldDef> convertFromColumns(IList<VisionBotEngine.Model.FieldDef> fieldDefs)
        {
            FieldDefConverter fieldDefConverter = new FieldDefConverter();
            var columns = new ObservableCollection<FieldDef>();
            foreach (VisionBotEngine.Model.FieldDef fieldDef in fieldDefs)
            {
                if (!fieldDef.IsDeleted)
                columns.Add(fieldDefConverter.ConvertFrom(fieldDef));
            }

            return columns;
        }

        public VisionBotEngine.Model.TableDef ConvertTo(TableDef tableDef, VisionBotEngine.Model.TableDef savedTableDef)
        {
            return new VisionBotEngine.Model.TableDef
            {
                Id = tableDef.Id,
                Name = tableDef.Name,
                Columns = convertToColumns(tableDef.Columns, savedTableDef)
            };
        }

        public List<VisionBotEngine.Model.FieldDef> convertToColumns(IList<FieldDef> fieldDefs, VisionBotEngine.Model.TableDef savedTableDef)
        {
            FieldDefConverter fieldDefConverter = new FieldDefConverter();
            var columns = new List<VisionBotEngine.Model.FieldDef>();
            //foreach (FieldDef fieldDef in fieldDefs)
            //{
            //    columns.Add(fieldDefConverter.ConvertTo(fieldDef));
            //}

            foreach (FieldDef fieldDef in fieldDefs)
            {
                VisionBotEngine.Model.FieldDef modelFieldDef = fieldDefConverter.ConvertTo(fieldDef);

                //if (savedTableDef != null)
                //{
                //    if (savedTableDef.Columns.Find(f => f.Id.Equals(fieldDef.Id)) == null)
                //    {
                //        modelFieldDef.IsDeleted = true;
                //    }
                //}

                columns.Add(modelFieldDef);
            }

            if (savedTableDef != null)
            {
                foreach (VisionBotEngine.Model.FieldDef fieldDef in savedTableDef.Columns)
                {
                    if (columns.Find(f => f.Id.Equals(fieldDef.Id)) == null)
                    {
                        fieldDef.IsDeleted = true;
                        columns.Add(fieldDef);
                    }
                }
            }

            return columns;
        }
    }
}
