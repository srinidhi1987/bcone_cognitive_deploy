﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Automation.CognitiveData.VisionBot.UI
{
    class DataModelConverter : IDataModelConverter
    {
        public DataModel ConvertFrom(VisionBotEngine.Model.DataModel dataModel)
        {
            return new DataModel
            {
                Fields = convertFromFields(dataModel.Fields),
                Tables = convertFromTables(dataModel.Tables)
            };
        }

        public ObservableCollection<FieldDef> convertFromFields(IList<VisionBotEngine.Model.FieldDef> fieldDefs)
        {
            FieldDefConverter fieldDefConverter = new FieldDefConverter();
            var columns = new ObservableCollection<FieldDef>();
            foreach (VisionBotEngine.Model.FieldDef fieldDef in fieldDefs)
            {
                if (!fieldDef.IsDeleted)
                columns.Add(fieldDefConverter.ConvertFrom(fieldDef));
            }

            return columns;
        }

        public ObservableCollection<TableDef> convertFromTables(IList<VisionBotEngine.Model.TableDef> tableDefs)
        {
            TableDefConverter tableDefConverter = new TableDefConverter();
            var tables = new ObservableCollection<TableDef>();
            foreach (VisionBotEngine.Model.TableDef tableDef in tableDefs)
            {

                if (!tableDef.IsDeleted)
                {
                tables.Add(tableDefConverter.ConvertFrom(tableDef));
            }
            }

            return tables;
        }

        public VisionBotEngine.Model.DataModel ConvertTo(DataModel dataModel, VisionBotEngine.Model.DataModel savedDataModel)
        {
            return new VisionBotEngine.Model.DataModel
            {
                Fields = convertToFields(dataModel.Fields, savedDataModel.Fields),
                Tables = convertToTables(dataModel.Tables, savedDataModel.Tables)
            };
        }       


        public List<VisionBotEngine.Model.FieldDef> convertToFields(IList<FieldDef> fieldDefs, List<VisionBotEngine.Model.FieldDef> savedFields)
        {
            FieldDefConverter fieldDefConverter = new FieldDefConverter();
            var columns = new List<VisionBotEngine.Model.FieldDef>();
            foreach (FieldDef fieldDef in fieldDefs)
            {
                VisionBotEngine.Model.FieldDef modelFieldDef = fieldDefConverter.ConvertTo(fieldDef);
                //if (savedFields.Find(f => f.Id.Equals(fieldDef.Id)) == null)
                //{
                //    modelFieldDef.IsDeleted = true;
                //}

                columns.Add(modelFieldDef);
            }

            foreach (VisionBotEngine.Model.FieldDef fieldDef in savedFields)
            {
                if (columns.Find(f => f.Id.Equals(fieldDef.Id)) == null)
                {
                    fieldDef.IsDeleted = true;
                    columns.Add(fieldDef);
            }
            }

            return columns;
        }

        public List<VisionBotEngine.Model.TableDef> convertToTables(IList<TableDef> tableDefs, List<VisionBotEngine.Model.TableDef> savedTableDefs)
        {
            ITableDefConverter tableDefConverter = new TableDefConverter();
            var tables = new List<VisionBotEngine.Model.TableDef>();
            foreach (TableDef tableDef in tableDefs)
            {
                VisionBotEngine.Model.TableDef modelTableDef = tableDefConverter.ConvertTo(tableDef, savedTableDefs.Find(t => t.Id.Equals(tableDef.Id)));
                //if (savedTableDefs.Find(f => f.Id.Equals(tableDef.Id)) == null)
                //{
                //    modelTableDef.IsDeleted = true;
                //}

                tables.Add(modelTableDef);
            }

            foreach (VisionBotEngine.Model.TableDef tableDef in savedTableDefs)
            {
                if (tables.Find(f => f.Id.Equals(tableDef.Id)) == null)
                {
                    tableDef.IsDeleted = true;
                    tables.Add(tableDef);
                }

            }

            return tables;
        }
    }
}
