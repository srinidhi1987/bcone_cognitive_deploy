﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Microsoft.Win32;
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Interaction logic for FileSelectionDialog.xaml
    /// </summary>
    public partial class FileSelectionDialog : Window
    {
        internal const string Filter = "Documents(*.pdf;*.jpg;*.jpeg;*.png;*.tif;*.tiff)|*.pdf;*.jpg;*.jpeg;*.png;*.tif;*.tiff";

        public FileSelectionDialog()
        {
            InitializeComponent();
        }

        public FileSelectionDialog(Layout layout)
            : this()
        {
            this.Layout = layout;
        }

        public Layout Layout { get; private set; }
        public ObservableCollection<Layout> Layouts { get; set; }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var layoutName = this.textLayoutName.Text;

            string errorMessage = ValidateLayoutName(layoutName);

            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                //ErrorMessage = errorMessage;

                textLayoutError.Text = errorMessage;
                WarningImage.Visibility = textLayoutError.Visibility = Visibility.Visible;
                textLayoutName.Focus();
                textLayoutName.CaretIndex = textLayoutName.Text.Length;
            }
            else
            {

                textLayoutError.Text = string.Empty;
                WarningImage.Visibility = textLayoutError.Visibility = Visibility.Collapsed;

                var filePath = SelectFile();

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    return;
                }

                this.Layout = new Layout
                                  {
                                      Name = layoutName,
                                      DocumentPath = filePath,
                                      DocumentName = Path.GetFileName(filePath)
                                  };
                this.DialogResult = true;
                this.Close();
            }

        }

        private static string SelectFile()
        {
            var dlg = new OpenFileDialog { CheckFileExists = true, Multiselect = false, Filter = Filter };

            return (true == dlg.ShowDialog()) ? dlg.FileName : string.Empty;
        }

        private string ValidateLayoutName(string layoutName)
        {
            try
            {
                new NameValidatorHelper().IsValidLayoutName(layoutName);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return this.checkIfSameLayoutNameAlreadyExists(layoutName);
        }

        private string checkIfSameLayoutNameAlreadyExists(string layoutName)
        {
            if (this.Layouts != null)
            {
                if (this.Layouts.Any(layout => !string.IsNullOrWhiteSpace(layout.Name)
                                               && layout.Name.Equals(layoutName, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return string.Format(Properties.Resources.ErrorMessageForSameItemName,Properties.Resources.LayoutLabel);
                }
            }

            return string.Empty;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Left = Left - 10;

            if (this.Layout != null)
            {
                if (!string.IsNullOrWhiteSpace(this.Layout.Name))
                    textLayoutName.Text = this.Layout.Name;
            }

            textLayoutName.Focus();
            textLayoutName.CaretIndex = textLayoutName.Text.Length;
        }
    }
}
