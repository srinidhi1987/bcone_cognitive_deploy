﻿namespace Automation.CognitiveData.VisionBot.UI
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAddField = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlTemplate = new System.Windows.Forms.Panel();
            this.rbtTemplateFields = new System.Windows.Forms.RadioButton();
            this.rbtTemplateTable = new System.Windows.Forms.RadioButton();
            this.rbtLayout = new System.Windows.Forms.RadioButton();
            this.rbtTemplate = new System.Windows.Forms.RadioButton();
            this.pnlLayout = new System.Windows.Forms.Panel();
            this.rbtLayoutMarker = new System.Windows.Forms.RadioButton();
            this.rbtLayoutField = new System.Windows.Forms.RadioButton();
            this.rbtLayoutTable = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlTemplate.SuspendLayout();
            this.pnlLayout.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnCancel);
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 326);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(713, 57);
            this.panel3.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(567, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(64, 32);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnRemove);
            this.panel2.Controls.Add(this.btnAddField);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(631, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(82, 292);
            this.panel2.TabIndex = 4;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(9, 44);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(64, 32);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAddField
            // 
            this.btnAddField.Location = new System.Drawing.Point(9, 6);
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.Size = new System.Drawing.Size(64, 32);
            this.btnAddField.TabIndex = 0;
            this.btnAddField.Text = "Add";
            this.btnAddField.UseVisualStyleBackColor = true;
            this.btnAddField.Click += new System.EventHandler(this.btnAddField_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pnlTemplate);
            this.panel4.Controls.Add(this.rbtLayout);
            this.panel4.Controls.Add(this.rbtTemplate);
            this.panel4.Controls.Add(this.pnlLayout);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(713, 34);
            this.panel4.TabIndex = 2;
            // 
            // pnlTemplate
            // 
            this.pnlTemplate.Controls.Add(this.rbtTemplateFields);
            this.pnlTemplate.Controls.Add(this.rbtTemplateTable);
            this.pnlTemplate.Location = new System.Drawing.Point(-3, 10);
            this.pnlTemplate.Name = "pnlTemplate";
            this.pnlTemplate.Size = new System.Drawing.Size(96, 26);
            this.pnlTemplate.TabIndex = 4;
            // 
            // rbtTemplateFields
            // 
            this.rbtTemplateFields.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtTemplateFields.AutoSize = true;
            this.rbtTemplateFields.Checked = true;
            this.rbtTemplateFields.Location = new System.Drawing.Point(4, 2);
            this.rbtTemplateFields.Name = "rbtTemplateFields";
            this.rbtTemplateFields.Size = new System.Drawing.Size(39, 23);
            this.rbtTemplateFields.TabIndex = 2;
            this.rbtTemplateFields.TabStop = true;
            this.rbtTemplateFields.Text = "Field";
            this.rbtTemplateFields.UseVisualStyleBackColor = true;
            this.rbtTemplateFields.CheckedChanged += new System.EventHandler(this.rbtTemplateFields_CheckedChanged);
            // 
            // rbtTemplateTable
            // 
            this.rbtTemplateTable.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtTemplateTable.AutoSize = true;
            this.rbtTemplateTable.Location = new System.Drawing.Point(43, 2);
            this.rbtTemplateTable.Name = "rbtTemplateTable";
            this.rbtTemplateTable.Size = new System.Drawing.Size(44, 23);
            this.rbtTemplateTable.TabIndex = 3;
            this.rbtTemplateTable.TabStop = true;
            this.rbtTemplateTable.Text = "Table";
            this.rbtTemplateTable.UseVisualStyleBackColor = true;
            this.rbtTemplateTable.CheckedChanged += new System.EventHandler(this.rbtTemplateTable_CheckedChanged);
            // 
            // rbtLayout
            // 
            this.rbtLayout.AutoSize = true;
            this.rbtLayout.Location = new System.Drawing.Point(398, 8);
            this.rbtLayout.Name = "rbtLayout";
            this.rbtLayout.Size = new System.Drawing.Size(57, 17);
            this.rbtLayout.TabIndex = 1;
            this.rbtLayout.TabStop = true;
            this.rbtLayout.Text = "Layout";
            this.rbtLayout.UseVisualStyleBackColor = true;
            this.rbtLayout.CheckedChanged += new System.EventHandler(this.rbtLayout_CheckedChanged);
            // 
            // rbtTemplate
            // 
            this.rbtTemplate.AutoSize = true;
            this.rbtTemplate.Checked = true;
            this.rbtTemplate.Location = new System.Drawing.Point(323, 8);
            this.rbtTemplate.Name = "rbtTemplate";
            this.rbtTemplate.Size = new System.Drawing.Size(69, 17);
            this.rbtTemplate.TabIndex = 0;
            this.rbtTemplate.TabStop = true;
            this.rbtTemplate.Text = "Template";
            this.rbtTemplate.UseVisualStyleBackColor = true;
            this.rbtTemplate.CheckedChanged += new System.EventHandler(this.rbtTemplate_CheckedChanged);
            // 
            // pnlLayout
            // 
            this.pnlLayout.Controls.Add(this.rbtLayoutMarker);
            this.pnlLayout.Controls.Add(this.rbtLayoutField);
            this.pnlLayout.Controls.Add(this.rbtLayoutTable);
            this.pnlLayout.Location = new System.Drawing.Point(-3, 10);
            this.pnlLayout.Name = "pnlLayout";
            this.pnlLayout.Size = new System.Drawing.Size(147, 26);
            this.pnlLayout.TabIndex = 5;
            this.pnlLayout.Visible = false;
            // 
            // rbtLayoutMarker
            // 
            this.rbtLayoutMarker.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtLayoutMarker.AutoSize = true;
            this.rbtLayoutMarker.Location = new System.Drawing.Point(87, 2);
            this.rbtLayoutMarker.Name = "rbtLayoutMarker";
            this.rbtLayoutMarker.Size = new System.Drawing.Size(50, 23);
            this.rbtLayoutMarker.TabIndex = 4;
            this.rbtLayoutMarker.TabStop = true;
            this.rbtLayoutMarker.Text = "Marker";
            this.rbtLayoutMarker.UseVisualStyleBackColor = true;
            this.rbtLayoutMarker.CheckedChanged += new System.EventHandler(this.rbtLayoutMarker_CheckedChanged);
            // 
            // rbtLayoutField
            // 
            this.rbtLayoutField.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtLayoutField.AutoSize = true;
            this.rbtLayoutField.Checked = true;
            this.rbtLayoutField.Location = new System.Drawing.Point(4, 2);
            this.rbtLayoutField.Name = "rbtLayoutField";
            this.rbtLayoutField.Size = new System.Drawing.Size(39, 23);
            this.rbtLayoutField.TabIndex = 2;
            this.rbtLayoutField.TabStop = true;
            this.rbtLayoutField.Text = "Field";
            this.rbtLayoutField.UseVisualStyleBackColor = true;
            this.rbtLayoutField.CheckedChanged += new System.EventHandler(this.rbtLayoutField_CheckedChanged);
            // 
            // rbtLayoutTable
            // 
            this.rbtLayoutTable.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtLayoutTable.AutoSize = true;
            this.rbtLayoutTable.Location = new System.Drawing.Point(43, 2);
            this.rbtLayoutTable.Name = "rbtLayoutTable";
            this.rbtLayoutTable.Size = new System.Drawing.Size(44, 23);
            this.rbtLayoutTable.TabIndex = 3;
            this.rbtLayoutTable.TabStop = true;
            this.rbtLayoutTable.Text = "Table";
            this.rbtLayoutTable.UseVisualStyleBackColor = true;
            this.rbtLayoutTable.CheckedChanged += new System.EventHandler(this.rbtLayoutTable_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dataGridView1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 34);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(631, 292);
            this.panel5.TabIndex = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(631, 292);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(640, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(64, 32);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 383);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Name = "TestForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.TestForm_Load);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnlTemplate.ResumeLayout(false);
            this.pnlTemplate.PerformLayout();
            this.pnlLayout.ResumeLayout(false);
            this.pnlLayout.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAddField;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton rbtTemplate;
        private System.Windows.Forms.RadioButton rbtLayout;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel pnlTemplate;
        private System.Windows.Forms.RadioButton rbtTemplateFields;
        private System.Windows.Forms.RadioButton rbtTemplateTable;
        private System.Windows.Forms.Panel pnlLayout;
        private System.Windows.Forms.RadioButton rbtLayoutField;
        private System.Windows.Forms.RadioButton rbtLayoutTable;
        private System.Windows.Forms.RadioButton rbtLayoutMarker;
        private System.Windows.Forms.Button btnCancel;
    }
}