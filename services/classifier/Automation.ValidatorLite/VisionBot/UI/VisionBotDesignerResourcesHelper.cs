/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    //using Automation.CognitiveData.VisionBot.UI.Model;
    //using Automation.CognitiveData.VisionBot.UI.ViewModels;

    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Imaging;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.PdfEngine;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media.Imaging;

    public enum ElementType
    {
        [Automation.Common.StringValue("Field")]
        Field,

        [Automation.Common.StringValue("Table Column")]
        Table,

        [Automation.Common.StringValue("Marker")]
        Marker,

        [Automation.Common.StringValue("SystemIdentifiedRegion")]
        SystemIdentifiedRegion,

        [Automation.Common.StringValue("SystemIdentifiedField")]
        SystemIdentifiedField
    }

    public enum UserDefinedRegionType
    {
        None,
        UserDefinedField,
        UserDefinedRegion
    }

    internal class VisionBotDesignerResourcesHelper
    {
        //public VisionBotDesignerResources CreateVisionBotDesignerResources(IqBot iqBot, FrameworkElement franeworkElement)
        //{
        //    var imageViewInfo = CreateImageViewInfoWithoutImagesAndRegions((iqBot.Layouts.Count > 0 ? iqBot.Layouts[0] : null), iqBot.Layouts.Count);

        //    var canvasDetails = new CanvasImageViewModel(franeworkElement)
        //    {
        //        ImageViewInfo = imageViewInfo,
        //        HasItems = iqBot.Layouts.Count > 0
        //    };

        //    return new VisionBotDesignerResources(iqBot, canvasDetails)
        //    {
        //        Logo = GetImage(@"\Images\AA-Logo.png"),
        //        DesignerInfo = new IqBotDesignerInfo
        //        {
        //            Title = "IQBot Designer",
        //            FileName = iqBot.Name,
        //            RibbonTabItems = new ObservableCollection<RibbonTabItem>
        //            {
        //                new RibbonTabItem{Caption = Properties.Resources.BlueprintTabLabel, ActiveImageSource = @"/Images/Iq_Blueprint_Active.png", InactiveImageSource = @"/Images/Iq_Blueprint_Inactive.png", IsSelected = false,IsEnabled =false},
        //                new RibbonTabItem{Caption = Properties.Resources.LayoutTabLabel, ActiveImageSource = @"/Images/Iq_Layout_Active.png", InactiveImageSource = @"/Images/Iq_Layout_Inactive.png", IsSelected = false,IsEnabled = true},
        //                new RibbonTabItem{Caption = Properties.Resources.TestTabLabel, ActiveImageSource = @"/Images/Iq_Testing_Active.png", InactiveImageSource = @"/Images/Iq_Testing_InActive.png", IsSelected = false,IsEnabled =false}
        //            },
        //            RibbonTabButtons = new ObservableCollection<RibbonTabButton>
        //            {
        //               new RibbonTabButton{ActiveImageSource = @"/Images/Iq_Settings.png", IsSelected = false,IsEnabled =false ,IsSettingsVisible = false}
        //            }
        //        }
        //    };
        //}

        //internal ImageViewInfo CreateImageViewInfoWithoutImagesAndRegions(Layout layout, int layoutCount)
        //{
        //    VBotLogger.Trace(() => "[CreateImageViewInfoWithoutImagesAndRegions] Start....");

        //    var imageViewInfo = new ImageViewInfo(layout?.ImageProvider);
        //    try
        //    {
        //        if (layout != null && layoutCount != 0)
        //        {
        //            VBotLogger.Trace(() => string.Format("[CreateImageViewInfoWithoutImagesAndRegions] Assigning ImageViewInfo properties. Layout {0}....", layout.Name));
        //            imageViewInfo.ImageSource = createPageImageSourcesWithoutImage(layout.DocumentProperties);
        //            imageViewInfo.DocumentProperties = layout.DocumentProperties;
        //            //Do not fill fields here. Fields will be filled in chunks in following Fields collection so UI does not hang/freeze.
        //            imageViewInfo.Fields = new ObservableCollection<RegionViewModel>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.Log(nameof(VisionBotDesignerResourcesHelper), nameof(CreateImageViewInfoWithoutImagesAndRegions));
        //    }

        //    VBotLogger.Trace(() => "[CreateImageViewInfoWithoutImagesAndRegions] End... ");

        //    return imageViewInfo;
        //}

        //internal ImageViewInfo CreateImageViewInfoWithoutImagesAndRegions(DocSetItem docset)
        //{
        //    VBotLogger.Trace(() => "[CreateImageViewInfoWithoutImagesAndRegions for DocSet] Start....");

        //    var imageViewInfo = new ImageViewInfo(docset.ImageProvider);

        //    if (!string.IsNullOrWhiteSpace(docset.DocumentProperties.Path))
        //    {
        //        VBotLogger.Trace(() => string.Format("[CreateImageViewInfoWithoutImagesAndRegions for DocSet] Assigning ImageViewInfo properties. Path {0}....", docset.DocumentProperties.Path));

        //        imageViewInfo.ImageSource = createPageImageSourcesWithoutImage(docset.DocumentProperties);

        //        //Do not fill fields here. Fields will be filled in chunks in following Fields collection so UI does not hang/freeze.
        //        imageViewInfo.Fields = new ObservableCollection<RegionViewModel>();
        //    }

        //    VBotLogger.Trace(() => "[CreateImageViewInfoWithoutImagesAndRegions for DocSet] End... ");

        //    return imageViewInfo;
        //}

        //private List<PageImageSource> createPageImageSourcesWithoutImage(DocumentProperties documentProperties)
        //{
        //    VBotLogger.Trace(() => string.Format("[createPageImageSourcesWithoutImage] Image Path: {0}", documentProperties.Path));
        //    addPagePropertiesIfMissing(documentProperties);
        //    List<PageImageSource> pageImageSources = new List<PageImageSource>();
        //    for (int i = 0; i < documentProperties.PageProperties.Count; i++)
        //    {
        //        int pageIndex = documentProperties.PageProperties[i].PageIndex;
        //        if (i != pageIndex)
        //        {
        //            VBotLogger.Error(() => string.Format("[createPageImageSourcesWithoutImage] i: {0} and pageIndex: {1} are not same", i, pageIndex));
        //        }
        //        string pageImagePath = documentProperties.Path + "_Page" + pageIndex.ToString() + ".vimg";
        //        VBotLogger.Trace(() => string.Format("[createPageImageSourcesWithoutImage] Page Path: {0}", pageImagePath));
        //        PageImageSource pageImageSource = createPageImageSource(documentProperties.PageProperties[i], pageImagePath);
        //        pageImageSource.Top = documentProperties.PageProperties.GetRange(0, i).Sum(x => x.Height);
        //        VBotLogger.Trace(() => string.Format("[createPageImageSourcesWithoutImage] PageIndex: {0} PageImage Path: {1} Height: {2} Width: {3}", pageImageSource.PageIndex, pageImageSource.PageImagePath, pageImageSource.Height, pageImageSource.Width));
        //        pageImageSources.Add(pageImageSource);
        //    }

        //    VBotLogger.Trace(() => string.Format("[createPageImageSourcesWithoutImage] End for {0}....", documentProperties.Path));
        //    return pageImageSources;
        //}

        //private PageImageSource createPageImageSource(PageProperties pageProperties, string pageImagePath)
        //{
        //    PageImageSource pageImageSource = new PageImageSource();
        //    pageImageSource.PageIndex = pageProperties.PageIndex;
        //    pageImageSource.PageImagePath = pageImagePath;
        //    pageImageSource.PageImage = null;
        //    pageImageSource.Height = pageProperties.Height;
        //    pageImageSource.Width = pageProperties.Width;

        //    return pageImageSource;
        //}

        internal BitmapImage GetBitmapImage(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings, bool doesRequireToCreateNewImage)
        {
            BitmapImage bitmap = null;
            try
            {
                if (!File.Exists(imagePath) || doesRequireToCreateNewImage)
                {
                    bitmap = GetByteImage(documentProperties, pageIndex, imagePath, settings);
                }
                else
                {
                    bitmap = GetImage(imagePath);
                }

                VBotLogger.Trace(() => string.Format("[GetBitmapImage] Sucessfully Image loaded. DocumentProperties path: {0} pageIndex: {1} imagePath: {2}", documentProperties.Path, pageIndex, imagePath));
            }
            catch (Exception ex)
            {
                //ex.Log(nameof(VisionBotDesignerResources), nameof(GetBitmapImage));
            }
            return bitmap;
        }

        //private void addPagePropertiesIfMissing(DocumentProperties documentProperties)
        //{
        //    if (documentProperties != null && (documentProperties.PageProperties == null || documentProperties.PageProperties.Count == 0))
        //    {
        //        if (documentProperties.PageProperties == null)
        //        {
        //            documentProperties.PageProperties = new List<PageProperties>();
        //        }

        //        PageProperties pageProperties = new PageProperties();
        //        pageProperties.BorderRect = documentProperties.BorderRect;
        //        pageProperties.Height = documentProperties.Height;
        //        pageProperties.PageIndex = 0;
        //        pageProperties.Width = documentProperties.Width;
        //        documentProperties.PageProperties.Add(pageProperties);
        //    }
        //}

        public BitmapImage GetByteImage(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            BitmapImage pageBitmap = null;
            Bitmap[] pages = null;
            ImageOperations imageOperations = new ImageOperations();

            if (documentProperties.Type == DocType.Image)
            {
                pages = imageOperations.GetPagesFromImage(documentProperties.Path, pageIndex, 1);
            }
            else
            {
                IPdfEngine pdfWrapper = PdfEngineStrategy.GetPdfEngine();
                IList<Bitmap> pageWiseBitmap = pdfWrapper.GetPagewiseBitmaps(documentProperties.Path, 200, pageIndex, 1);
                pages = pageWiseBitmap.ToArray();
            }

            imageOperations.ConvertImageToGrayscale(pages, imagePath, Constants.CDC.OcrEnginePath, Configurations.Visionbot.OcrEngineType, settings);
            pageBitmap = GetImage(imagePath);
            return pageBitmap;
        }

        public void SaveByteImage(DocumentProperties documentProperties, int pageIndex, string imagePath, ImageProcessingConfig settings)
        {
            Bitmap[] pages = null;
            ImageOperations imageOperations = new ImageOperations();

            if (documentProperties.Type == DocType.Image)
            {
                pages = imageOperations.GetPagesFromImage(documentProperties.Path, pageIndex, 1);
            }
            else
            {
                IPdfEngine pdfWrapper = PdfEngineStrategy.GetPdfEngine();
                IList<Bitmap> pageWiseBitmap = pdfWrapper.GetPagewiseBitmaps(documentProperties.Path, 200, pageIndex, 1);
                pages = pageWiseBitmap.ToArray();
            }

            imageOperations.ConvertImageToGrayscale(pages, imagePath, Constants.CDC.OcrEnginePath, Configurations.Visionbot.OcrEngineType, settings);
            //TODO :Write Code for Dispose;
            if (pages != null)
            {
                foreach (var page in pages)
                {
                    if (page != null)
                    {
                        page.Dispose();
                    }
                }
                pages = null;
            }
        }

        //public BitmapImage ToImage(byte[] array)
        //{
        //    using (var ms = new System.IO.MemoryStream(array))
        //    {
        //        var image = new BitmapImage();
        //        image.BeginInit();
        //        image.CacheOption = BitmapCacheOption.OnLoad; // here
        //        image.StreamSource = ms;
        //        image.EndInit();
        //        return image;
        //    }
        //}

        //private bool isEmptyRect(Rect rect)
        //{
        //    const int zeroSize = 0;
        //    return (zeroSize.Equals(Convert.ToInt32(rect.Width)) || zeroSize.Equals(Convert.ToInt32(rect.Height)));
        //}

        //internal IList<RegionViewModel> GetAllRegions(DocSetItem docset)
        //{
        //    var regions = new List<RegionViewModel>();

        //    if (docset != null)
        //    {
        //        regions.AddRange(getSIRBounds(docset));

        //        if (docset.FieldValueRegions != null)
        //        {
        //            for (int i = 0; i < docset.FieldValueRegions.Count; i++)
        //            {
        //                if (isEmptyRect(docset.FieldValueRegions[i].Bounds))
        //                {
        //                    continue;
        //                }
        //                RegionViewModel regionVM = new RegionViewModel(docset.FieldValueRegions[i]);
        //                regionVM.Type = ElementType.Field;
        //                regions.Add(regionVM);
        //            }
        //        }

        //        if (docset.ColumnValueRegions != null)
        //        {
        //            for (int i = 0; i < docset.ColumnValueRegions.Count; i++)
        //            {
        //                if (isEmptyRect(docset.ColumnValueRegions[i].Bounds))
        //                {
        //                    continue;
        //                }
        //                RegionViewModel regionVM = new RegionViewModel(docset.ColumnValueRegions[i]);
        //                regionVM.Type = ElementType.Table;
        //                regions.Add(regionVM);
        //            }
        //        }
        //    }

        //    return regions;
        //}

        //private List<RegionViewModel> getSIRBounds(DocSetItem docset)
        //{
        //    var regions = new List<RegionViewModel>();
        //    if (docset != null && docset.SystemIdentifiedFields != null)
        //    {
        //        for (int i = 0; i < docset.SystemIdentifiedFields.Count; i++)
        //        {
        //            regions.Add(new RegionViewModel(docset.SystemIdentifiedFields[i]));
        //        }
        //    }

        //    return regions;
        //}

        //internal IList<RegionViewModel> GetAllFields(Layout layout)
        //{
        //    var regions = new List<RegionViewModel>(getCount(layout));
        //    regions.AddRange(getRegions(layout.SystemIdentifiedFields));
        //    regions.AddRange(getRegions(layout.Markers.Fields));
        //    regions.AddRange(getRegions(layout.LayoutFields.Fields));
        //    if (layout.LayoutTables.Tables.Count > 0)
        //    {
        //        foreach (var layoutTable in layout.LayoutTables.Tables)
        //        {
        //            regions.AddRange(getRegions(layoutTable.Fields));
        //        }
        //    }

        //    return regions;
        //}

        //private IList<RegionViewModel> getRegions(IList<LayoutField> fields)
        //{
        //    return fields.Select(field => new RegionViewModel(field)).ToList();
        //}

        //private int getCount(Layout layout)
        //{
        //    return layout.SystemIdentifiedFields.Count +
        //        layout.Markers.Fields.Count +
        //        layout.LayoutFields.Fields.Count + (layout.LayoutTables.Tables.Count > 0 ? layout.LayoutTables.Tables[0].Fields.Count : 0);
        //}

        internal BitmapImage GetImage(string path)
        {
            try
            {
                if (string.IsNullOrEmpty(path))
                {
                    return null;
                }

                string imagePath = File.Exists(path)
                                       ? path
                                       : Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) + path;

                imagePath = new Uri(imagePath).LocalPath;

                if (!File.Exists(imagePath))
                {
                    return null;
                }

                using (var stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
                {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = stream;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();

                    stream.Close();
                    stream.Dispose();
                    return bitmapImage;
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(VisionBotDesignerResourcesHelper), nameof(GetImage));
                return null;
            }
        }

        //internal TestSetViewModel GetTestSets(List<LayoutTestSet> TestDocSets)
        //{
        //    var testSetViewModel = new TestSetViewModel
        //    {
        //        Caption = "Test Set",
        //        HasTitle = true,
        //        IsSelected = false
        //    };

        //    testSetViewModel.AddLayoutTestSets(TestDocSets);

        //    return testSetViewModel;
        //}

        //internal LayoutViewModel GetLayotViewModel(IqBot iqBot)
        //{
        //    var layoutViewModel = new LayoutViewModel(iqBot)
        //    {
        //        Caption = Properties.Resources.LayoutTreeLabel,
        //        HasTitle = true
        //    };

        //    return layoutViewModel;
        //}

        //internal BluePrintViewModel GetBluePrintViewModel(IqBot IqBot)
        //{
        //    var bluePrintViewModel = new BluePrintViewModel(IqBot.DataModel)
        //    {
        //        Caption = string.Empty,
        //        HasTitle = false
        //    };

        //    return bluePrintViewModel;
        //}
    }
}