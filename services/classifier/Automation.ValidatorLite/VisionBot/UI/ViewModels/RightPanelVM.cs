﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using System;
    using System.Windows;
    using System.Windows.Media.Imaging;

    public class RightPanelVM : ViewModelBase
    {
        #region Fields

        private bool _isPreviewOn;

        private object _content;
        private object _parentItem;
        private object _selectedItem;

        private IqBot _iqBot;

        private VisionBotDesignerResources _parent;

        #endregion Fields

        #region Events

        public event EventHandler TurnValueSelectionOffRequested;

        public event LayoutFieldValueRequestedHandler AutoValueBoundRequested;

        public event SelectionStatusChangeHandler SelectionStatusChanged;

        public event Action<FieldDef> UpdateFieldDefRequested;

        public event Action<TableDef, FieldDef> UpdateTableFieldDefRequested;

        public event Action<TableDef> UpdateTableDefRequested;

        public event Action<LayoutField> UpdateLayoutData;

        public event Action<TableDef, LayoutField> UpdateTableLayoutField;

        public event Action<LayoutField> UpdateMarker;

        public event Action<LayoutTable> UpdateLayoutTableRequested;

        public event Action<FieldDef> CancelFieldDefRequested;

        public event Action<TableDef, FieldDef> CancelTableFieldDefRequested;

        public event Action<TableDef> CancelTableDefRequested;

        public event Action<LayoutSettingsViewModel> SettingsSaveRequested;

        public event Action<LayoutSettingsViewModel> SettingsRescanRequested;

        public event Action<LayoutSettingsViewModel> LayoutRescanRequested;

        #endregion Events

        #region Properties

        public object Content
        {
            get { return _content; }
            private set
            {
                if (_content != value)
                {
                    _content = value;
                    OnPropertyChanged(nameof(Content));
                }
            }
        }

        public IqBot IqBot
        {
            get { return _iqBot; }
            private set { _iqBot = value; }
        }

        public bool IsMandatoryFieldDefChanged
        {
            get
            {
                var fieldDef = Content as UpdateFieldDef;
                return fieldDef != null && fieldDef.IsFieldChangedToRequired;
            }
        }

        public bool IsPreviewOn
        {
            get { return _isPreviewOn; }
            set
            {
                _isPreviewOn = value;
                OnPropertyChanged(nameof(IsPreviewOn));
            }
        }

        public VisionBotDesignerResources Parent
        {
            get { return _parent; }
        }

        public object ParentItem
        {
            get { return _parentItem; }
            private set
            {
                if (_parentItem != value)
                {
                    _parentItem = value;
                    OnPropertyChanged(nameof(ParentItem));
                }
            }
        }

        public object SelectedItem
        {
            get { return _selectedItem; }
            private set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    OnPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        #endregion Properties

        #region Constructor

        public RightPanelVM(VisionBotDesignerResources parent)
        {
            _parent = parent;
            _iqBot = parent.IqBot;
        }

        #endregion Constructor

        #region Methods

        public void UpdateContext(UpdateMode updateMode, object parent, object child)
        {
            ParentItem = parent;
            SelectedItem = child;
            CreateContextContent(updateMode);
        }

        public void UpdateLayoutFieldContext(Layout layout, LayoutField field)
        {
            SelectedItem = field;
            CreateContextContentForLayoutField(layout, field);
        }

        public void UpdateLayoutTableContext(LayoutTable layoutTable)
        {
            SelectedItem = layoutTable;
            CreateContextContentForLayoutTable(layoutTable);
        }

        public void UpdateTableColumnBounds(Rect bounds)
        {
            var layoutField = Content as UpdateLayoutFieldViewModel;
            if (layoutField != null)
            {
                layoutField.LayoutField.Bounds = bounds;
            }
        }

        internal void ClearInspectionPanel()
        {
            Content = null;
        }

        internal void UpdateLayoutContext(Layout layout)
        {
            var settings = new LayoutSettingsViewModel(layout.Settings);

            settings.SaveRequested += settings_SaveRequested;
            settings.LayoutRescanRequested += Layout_RescanRequested;
            Content = settings;
        }

        internal void UpdateTestDocResultSummaryContext(TestDocSummaryViewModel testDocSummaryViewModel)
        {
            Content = testDocSummaryViewModel;
        }

        internal void UpdateTestSetContext(TestInspectionViewModel testInspectionViewModel)
        {
            Content = testInspectionViewModel;
        }

        private void CreateContextContent(UpdateMode updateMode)
        {
            if (SelectedItem != null)
            {
                if (SelectedItem is FieldDef)
                {
                    UpdateFieldDef updateFieldDef = new UpdateFieldDef(updateMode, ParentItem as TableDef, SelectedItem as FieldDef);
                    updateFieldDef.UpdateFieldDefRequested += updateFieldDef_UpdateFieldDefRequested;
                    updateFieldDef.UpdateTableFieldDefRequested += updateFieldDef_UpdateTableFieldDefRequested;
                    updateFieldDef.ValidateFieldRequested += updateFieldDef_ValidateFieldRequested;
                    updateFieldDef.ValidateTableCollumnRequested += updateFieldDef_ValidateTableCollumnRequested;
                    updateFieldDef.CancelFieldDefRequested += updateFieldDef_CancelFieldDefRequested;
                    updateFieldDef.CancelTableFieldDefRequested += updateFieldDefForTable_CancelTableFieldDefRequested;

                    Content = updateFieldDef;
                }
                else if (SelectedItem is TableDef)
                {
                    UpdateTableDef updateTableDef = new UpdateTableDef(updateMode, SelectedItem as TableDef);
                    updateTableDef.UpdateTableDefRequested += updateTableDef_UpdateTableDefRequested;
                    updateTableDef.ValidateTableRequested += updateTableDef_ValidateTableRequested;
                    updateTableDef.CancelTableDefRequested += updateTableDef_CancelTableDefRequested;
                    Content = updateTableDef;
                }
                else
                {
                    Content = null;
                }
            }
            else
            {
                Content = null;
            }
        }

        internal void UpdateFieldValueBounds(Rect obj)
        {
            var layoutField = Content as UpdateLayoutFieldViewModel;
            if (layoutField != null)
            {
                layoutField.LayoutField.ValueBounds = obj;
            };
        }

        private void CreateContextContentForLayoutField(Layout layout, LayoutField layoutField)
        {
            CroppedBitmap croppedBitmap = null;
            UpdateLayoutFieldViewModel updateLayoutField = null;
            if (layoutField != null)
            {
                croppedBitmap = GetLayoutFieldPreview(layoutField, croppedBitmap);
                updateLayoutField = new UpdateLayoutFieldViewModel(IqBot, layout, layoutField, croppedBitmap);
                updateLayoutField.SelectionStatusChanged += UpdateLayoutField_SelectionStatusChanged;
                updateLayoutField.UpdateLayoutData += updateLayoutField_UpdateLayoutData;
                updateLayoutField.UpdateMarker += updateLayoutField_UpdateMarker;
                updateLayoutField.UpdateTableLayoutField += updateLayoutField_UpdateTableLayoutField;
                updateLayoutField.AutoValueBoundRequested += updateLayoutField_AutoValueBoundRequested;
                updateLayoutField.TurnValueSelectionOffRequested += UpdateLayoutField_TurnValueSelectionOffRequested;
            }
            Content = updateLayoutField;
        }

        public void RefreshRightPanelPreivewImage()
        {
            if (Content is UpdateLayoutFieldViewModel)
            {
                var updateLayoutFieldVM = Content as UpdateLayoutFieldViewModel;
                if (updateLayoutFieldVM.CroppedBitmap == null)
                {
                    CroppedBitmap croppedBitmap = null;
                    croppedBitmap = GetLayoutFieldPreview(updateLayoutFieldVM.LayoutField, croppedBitmap);
                    updateLayoutFieldVM.RefreshPreviewImage(croppedBitmap);
                }
            }
        }

        private void CreateContextContentForLayoutTable(LayoutTable layoutTable)
        {
            UpdateLayoutTable updateLayoutTable = new UpdateLayoutTable(layoutTable);
            updateLayoutTable.UpdateLayoutTableRequested += updateLayoutTable_UpdateLayoutTableRequested;
            Content = updateLayoutTable;
        }

        private CroppedBitmap GetLayoutFieldPreview(LayoutField layoutField, CroppedBitmap croppedBitmap)
        {
            if (layoutField != null && layoutField.Bounds != null && isImageSourcePresent())
            {
                Int32Rect croppingRect = new Int32Rect((int)layoutField.Bounds.X - 4,
                                                        (int)layoutField.Bounds.Y - 4,
                                                        (int)layoutField.Bounds.Width + 8,
                                                        (int)layoutField.Bounds.Height + 8);
                croppedBitmap = _parent.CanvasImageViewModel.ImageViewInfo.GetCroppedImage(croppingRect);
            }
            return croppedBitmap;
        }

        private bool isImageSourcePresent()
        {
            return _parent != null
                && _parent.CanvasImageViewModel != null
                && _parent.CanvasImageViewModel.ImageViewInfo != null;
        }

        private void Layout_RescanRequested(LayoutSettingsViewModel settings)
        {
            if (LayoutRescanRequested != null)
            {
                LayoutRescanRequested(settings);
            }
        }

        private void settings_SaveRequested(LayoutSettingsViewModel settings)
        {
            if (SettingsSaveRequested != null)
            {
                SettingsSaveRequested(settings);
            }
        }

        private void settings_RescanRequested(LayoutSettingsViewModel settings)
        {
            if (SettingsRescanRequested != null)
            {
                SettingsRescanRequested(settings);
            }
        }

        private void updateFieldDef_CancelFieldDefRequested(FieldDef fieldDef)
        {
            if (CancelFieldDefRequested != null)
            {
                CancelFieldDefRequested(fieldDef);
            }
        }

        private void updateFieldDef_UpdateFieldDefRequested(FieldDef fieldDef)
        {
            if (UpdateFieldDefRequested != null)
            {
                UpdateFieldDefRequested(fieldDef);
            }
        }

        private void updateFieldDef_UpdateTableFieldDefRequested(TableDef tableDef, FieldDef fieldDef)
        {
            if (UpdateTableFieldDefRequested != null)
            {
                UpdateTableFieldDefRequested(tableDef, fieldDef);
            }
        }

        private void updateFieldDef_ValidateFieldRequested(FieldDef obj)
        {
            new BluePrintNameValidator().ValidateField(obj.Name);
            (new DuplicateValidatorHelper()).ValidateField(IqBot, obj);
        }

        private void updateFieldDef_ValidateTableCollumnRequested(TableDef arg1, FieldDef arg2)
        {
            new BluePrintNameValidator().ValidateCollumn(arg2.Name);
            (new DuplicateValidatorHelper()).ValidateColumnField(IqBot, arg2);
        }

        private void updateFieldDefForTable_CancelTableFieldDefRequested(TableDef arg1, FieldDef arg2)
        {
            if (CancelTableFieldDefRequested != null)
            {
                CancelTableFieldDefRequested(arg1, arg2);
            }
        }

        private void UpdateLayoutField_SelectionStatusChanged(object sender, SelectionEventArgs e)
        {
            if (SelectionStatusChanged != null)
            {
                SelectionStatusChanged(sender, e);
            }
        }

        public void MakeFieldValueBoundSelectionOn()
        {
            var field = Content as UpdateLayoutFieldViewModel;
            if (field != null && field.TurnSelectionOn.CanExecute(null))
            {
                field.TurnSelectionOn.Execute(null);
            }
        }

        public void MakeLayoutFieldValueAuto()
        {
            var field = Content as UpdateLayoutFieldViewModel;
            if (field != null)
            {
                field.RequestAutoValueBound();
            }
        }

        private void updateLayoutField_UpdateMarker(LayoutField obj)
        {
            if (UpdateMarker != null)
            {
                UpdateMarker(obj);
            }
        }

        private void updateLayoutField_UpdateLayoutData(LayoutField obj)
        {
            if (UpdateLayoutData != null)
            {
                UpdateLayoutData(obj);
            }
        }

        private void updateLayoutField_AutoValueBoundRequested(object sender, LayoutFieldValueRequestedEventArgs e)
        {
            if (AutoValueBoundRequested != null)
            {
                AutoValueBoundRequested(sender, e);
            }
        }

        private void UpdateLayoutField_TurnValueSelectionOffRequested(object sender, EventArgs e)
        {
            if (TurnValueSelectionOffRequested != null)
            {
                TurnValueSelectionOffRequested(sender, e);
            }
        }

        private void updateLayoutField_UpdateTableLayoutField(TableDef arg1, LayoutField arg2)
        {
            if (UpdateTableLayoutField != null)
            {
                UpdateTableLayoutField(arg1, arg2);
            }
        }

        private void updateLayoutTable_UpdateLayoutTableRequested(LayoutTable obj)
        {
            if (UpdateLayoutTableRequested != null)
            {
                UpdateLayoutTableRequested(obj);
            }
        }

        private void updateTableDef_CancelTableDefRequested(TableDef tableDef)
        {
            if (CancelTableDefRequested != null)
            {
                CancelTableDefRequested(tableDef);
            }
        }

        private void updateTableDef_UpdateTableDefRequested(TableDef tableDef)
        {
            if (UpdateTableDefRequested != null)
            {
                UpdateTableDefRequested(tableDef);
            }
        }

        private void updateTableDef_ValidateTableRequested(TableDef obj)
        {
            new BluePrintNameValidator().ValidateTable(obj.Name);
            (new DuplicateValidatorHelper()).ValidateTable(IqBot, obj);
        }

        #endregion Methods
    }
}