﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Automation.CognitiveData.Shared;
using Automation.CognitiveData.VisionBot.UI.ViewModels.Validators;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class ValidatorViewModelBase : ViewModelBase, IDataErrorInfo
    {
        internal const string BlankValueValidatorService = "BlankValueValidatorService";
        internal const string UniqueValueValidatorService = "UniqueValueValidator";
        internal const string InvalidCharacterValidatorService = "InvalidCharacterValidator";

        private IDictionary<string, IList<ValidationContext>> _validators;
        protected IDictionary<string, string> _validationResults;

        private string _error=string.Empty;

        public ValidatorViewModelBase(IDictionary<string, IList<ValidationContext>> validators)
        {
            _validators = validators;
            _validationResults = new Dictionary<string, string>();
        }

        public string Error
        {
            get { return _error; }
        }

        public string this[string columnName]
        {
            get
            {
                if (_validationResults.ContainsKey(columnName))
                {
                    return _validationResults[columnName];
                }
               
                return string.Empty;
            }
        }

        private string getPropertyValue(string columnName)
        {
            return Convert.ToString(this.GetType().GetProperty(columnName).GetValue(this, null));
        }

        protected virtual void Validate()
        {
            _validationResults = new Dictionary<string, string>();

            foreach (var validator in _validators)
            {
                var propValue = getPropertyValue(validator.Key);
                foreach (var context in validator.Value)
                {
                    IInputValueValidator inputValueValidator =
                        IocConfig.Container.GetInstance<IInputValueValidator>(context.ServiceName);
                    var validationResult = inputValueValidator.Validate(context, propValue);
                    if (!validationResult.IsSuccess)
                    {
                        _validationResults.Add(validator.Key, validationResult.ErrorMessage);
                        break;
                    }
                }
            }
        }
    }
}
