/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using Automation.CognitiveData.Shared;
    using Automation.CognitiveData.VisionBot.UI.Model;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    public class LayoutTestSetViewModel : ViewModelBase, ISelectableItem
    {
        private LayoutTestSet _layoutTestSet;
        private IDocSetItemOperator _docSetItemOperator;

        public event EventHandler ItemSelected;

        public event TestDocSetItemAddHandler TestDocSetItemAddRequested;

        public string Name
        {
            get { return _layoutTestSet.LayoutName; }
        }

        public string Id
        {
            get { return _layoutTestSet.LayoutId; }
        }

        public LayoutTestSet LayoutTestSet
        {
            get { return _layoutTestSet; }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                if (ItemSelected != null && value)
                {
                    ItemSelected(this, new EventArgs());
                }
                OnPropertyChanged("IsSelected");
            }
        }

        // public event EventHandler ExpandChanged;
        private bool _isExpanded = false;

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    OnPropertyChanged("IsExpanded");
                    //  onExpandChanged();
                }
            }
        }

        public ISelectableItem Parent { get; private set; }

        public ObservableCollection<LayoutTestSetItem> LayoutTestSetItems { get; set; }

        public RelayCommand AddCommand { get; private set; }

        protected LayoutTestSetViewModel()
        { }

        public LayoutTestSetViewModel(LayoutTestSet layoutTestSet, ISelectableItem parent)
        {
            Parent = parent;
            _layoutTestSet = layoutTestSet;
            LayoutTestSetItems = new ObservableCollection<LayoutTestSetItem>();

            this.initialize(layoutTestSet);
        }

        private void initialize(LayoutTestSet layoutTestSet)
        {
            foreach (var docSetItem in layoutTestSet.DocSetItems)
            {
                this.addLayoutSetItem(new LayoutTestSetItem(docSetItem, this));
            }

            this.AddCommand = new RelayCommand(this.onAddCommand);
            this._docSetItemOperator = IocConfig.Container.Create<IDocSetItemOperator>();
        }

        private void onAddCommand(object obj)
        {
            var docSetItem = _docSetItemOperator.CreateLayoutTestSetItem(this, LayoutTestSetItems.Select(d => d.Name).ToList());
            if (!docSetItem.IsNull())
            {
                if (TestDocSetItemAddRequested != null)
                {
                    TestDocSetEventArgs args = new TestDocSetEventArgs(_layoutTestSet.LayoutId, docSetItem.GetDocSetItem(), Operation.Add);
                    TestDocSetItemAddRequested(this, args);
                    docSetItem.SetDocSetItem(args.DocSetItem);
                    Layout layout = args.IqBot.Layouts.FirstOrDefault(d => d.Id.Equals(_layoutTestSet.LayoutId));
                    if (layout != null)
                    {
                        docSetItem.Warning = layout.Warning;
                    }
                    addLayoutSetItem(docSetItem);
                    IsSelected = false;
                    docSetItem.IsSelected = true;
                }
            }
        }

        private void addLayoutSetItem(LayoutTestSetItem layoutTestSetItem)
        {
            var oldIndex = LayoutTestSetItems.Count;
            LayoutTestSetItems.Add(layoutTestSetItem);
            var newIndex = LayoutTestSetItems.OrderBy(l => l.Name).ToList().IndexOf(layoutTestSetItem);
            LayoutTestSetItems.Move(oldIndex, newIndex);
            layoutTestSetItem.RemoveRequested += layoutTestSetItem_RemoveRequested;
            layoutTestSetItem.ItemSelected += (s, e) =>
            {
                if (ItemSelected != null)
                {
                    this.IsExpanded = true;
                    ItemSelected(s, e);
                }
            };
        }

        private void layoutTestSetItem_RemoveRequested(object sender, EventArgs e)
        {
            var layoutTestSetItem = sender as LayoutTestSetItem;
            if (layoutTestSetItem != null)
            {
                if (_docSetItemOperator.RemoveDocSetItem(layoutTestSetItem))
                {
                    if (TestDocSetItemAddRequested != null)
                    {
                        TestDocSetItemAddRequested(this, new TestDocSetEventArgs(_layoutTestSet.LayoutId, layoutTestSetItem.GetDocSetItem(), Operation.Remove));
                        LayoutTestSetItems.Remove(layoutTestSetItem);
                        if (layoutTestSetItem?.GetDocSetItem()?.ImageProvider != null)
                        {
                            layoutTestSetItem.GetDocSetItem().ImageProvider.Dispose();
                        }
                        IsSelected = true;
                    }
                }
            }
        }

        internal LayoutTestSetViewModel Clone(LayoutTestSetItem layoutTestSetItem)
        {
            var layoutTestsetViewModel = new LayoutTestSetViewModel();
            layoutTestsetViewModel._layoutTestSet = this._layoutTestSet;
            layoutTestsetViewModel.LayoutTestSetItems = new ObservableCollection<LayoutTestSetItem>() { layoutTestSetItem };
            return layoutTestsetViewModel;
        }
    }

    public delegate void TestDocSetItemAddHandler(object sender, TestDocSetEventArgs e);
}