﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using Automation.VisionBotEngine.Model;
    using System;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    //TODO: IDisposible
    public class PageImageSource : ViewModelBase
    {
        private double _height;
        private double _width;
        private double _dpiX;
        private double _dpiY;
        private double _zoomHeight;
        private double _zoomWidth;
        private BitmapImage _pageImage;
        private Stretch _stretch = Stretch.Fill;
        private string _pageImagePath; //TODO: think should this class hold pageimage path in field?
        private int _pageIndex;

        public double Top { get; set; }

        public double Height
        {
            get
            {
                return _height;
            }

            set
            {
                _height = value;
            }
        }

        public double Width
        {
            get
            {
                return _width;
            }

            set
            {
                _width = value;
            }
        }

        public BitmapImage PageImage
        {
            get
            {
                return _pageImage;
            }

            set
            {
                if (_pageImage != value)
                {
                    _pageImage = value;
                    OnPropertyChanged("PageImage");
                }
            }
        }

        public double DpiX
        {
            get
            {
                return _dpiX;
            }

            set
            {
                _dpiX = value;
                adjustStretch();
            }
        }

        public double DpiY
        {
            get
            {
                return _dpiY;
            }

            set
            {
                _dpiY = value;
                adjustStretch();
            }
        }

        public double ZoomHeight
        {
            get
            {
                return _zoomHeight;
            }

            set
            {
                if (_zoomHeight != value)
                {
                    _zoomHeight = value;
                    OnPropertyChanged("ZoomHeight");
                }
            }
        }

        public double ZoomWidth
        {
            get
            {
                return _zoomWidth;
            }

            set
            {
                if (_zoomWidth != value)
                {
                    _zoomWidth = value;
                    OnPropertyChanged("ZoomWidth");
                }
            }
        }

        public Stretch Stretch
        {
            get
            {
                return _stretch;
            }

            set
            {
                if (_stretch != value)
                {
                    _stretch = value;
                    OnPropertyChanged("Stretch");
                }
            }
        }

        public string PageImagePath
        {
            get
            {
                return _pageImagePath;
            }

            set
            {
                _pageImagePath = value;
            }
        }

        public int PageIndex
        {
            get
            {
                return _pageIndex;
            }

            set
            {
                _pageIndex = value;
            }
        }

        internal void SetZoomHeightWidth(double zoomPercentage)
        {
            //ZoomHeight = (Height * (DpiY / 96)) * zoomPercentage;
            //ZoomWidth = (Width * (DpiX / 96)) * zoomPercentage;
            ZoomHeight = (Height) * zoomPercentage;
            ZoomWidth = (Width) * zoomPercentage;
        }

        private void adjustStretch()
        {
            //When image is having different DPIX and DPIY it was not aligned with SIRs on UI.
            //So in that case Stretch should be Fill otherwise Stretch should be uniform.

            //TODO: Check whether Stretch.Fill can work for all scenarios. If so we can remove this method.
            Stretch stretchToApply = Stretch.Fill;
            if (Convert.ToInt32(_dpiX) == Convert.ToInt32(_dpiY))
            {
                stretchToApply = Stretch.Uniform;
            }

            Stretch = stretchToApply;
        }
    }
}