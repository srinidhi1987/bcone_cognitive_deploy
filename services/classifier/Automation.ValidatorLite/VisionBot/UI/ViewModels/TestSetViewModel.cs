/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Automation.CognitiveData.VisionBot.UI.Model;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{

    internal interface IMessageProvider
    {
        string InformationMessage { get; set; }
    }

    internal class TestSetViewModel : ViewModelBase, ISelectableItem, IMessageProvider
    {
      
        private TestItemTypes _selectedTestSetMode;
        public TestItemTypes SelectedTestSetMode
        {
            get
            {
                return _selectedTestSetMode;
            }
            private set
            {
                _selectedTestSetMode = value;
                OnPropertyChanged("SelectedTestSetMode");
            }
        }

        private string _informationMessage;
        public string InformationMessage
        {
            get
            {
                return _informationMessage;
            }
            set
            {
                _informationMessage = value;
                OnPropertyChanged("InformationMessage");
            }
        }

        private bool _isSelected;

        public event TestDocSetItemAddHandler TestDocSetItemAddRequested;
        public event EventHandler ItemSelected;

        private IList<LayoutTestSetViewModel> _selectedTestSets;
        public IList<LayoutTestSetViewModel> SelectedTestSets
        {
            get { return _selectedTestSets; }
            private set
            {
                _selectedTestSets = value;
                OnPropertyChanged("SelectedTestSets");
            }
        }

        private LayoutTestSetItem _selectedTestSetItem;

        public LayoutTestSetItem SelectedTestSetItem
        {
            get { return _selectedTestSetItem; }
        }

        public string Caption { get; set; }

        public bool HasTitle { get; set; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    if (value)
                    {
                        removeChildSelection();
                    }
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                    onItemSelected();
                }
            }
        }


        public bool IsAddAvailable
        { get { return false; } }

        public ISelectableItem Parent { get; private set; }

        public ObservableCollection<LayoutTestSetViewModel> Items { get; set; }

        public ICommand TestSetSelected { get; private set; }

        public TestSetViewModel()
        {
            Items = new ObservableCollection<LayoutTestSetViewModel>();
            TestSetSelected = new RelayCommand(onTestSetSelected);
        }

        public void AddLayoutTestSets(IList<LayoutTestSet> layoutTestSets)
        {
            if (layoutTestSets != null)
            {
                foreach (var layoutTestSet in layoutTestSets)
                {
                    var layoutTestSetViewModel = new LayoutTestSetViewModel(layoutTestSet, this);
                    layoutTestSetViewModel.TestDocSetItemAddRequested += layoutTestSetViewModel_TestDocSetItemAddRequested;
                    layoutTestSetViewModel.ItemSelected += layoutTestSetViewModel_ItemSelected;
                    Items.Add(layoutTestSetViewModel);
                }
            }
        }

        public void AddLayoutTestSet(LayoutTestSet layoutTestSet)
        {
            var layoutTestSetViewModel = new LayoutTestSetViewModel(layoutTestSet, this);
            layoutTestSetViewModel.TestDocSetItemAddRequested += layoutTestSetViewModel_TestDocSetItemAddRequested;
            layoutTestSetViewModel.ItemSelected += layoutTestSetViewModel_ItemSelected;
            var oldIndex = Items.Count;
            Items.Add(layoutTestSetViewModel);
            var newIndex = Items.OrderBy(l => l.Name).ToList().IndexOf(layoutTestSetViewModel);
            Items.Move(oldIndex, newIndex);
        }

        public void SelectTestSetItem(string id)
        {
            LayoutTestSetViewModel parentLayout = null;
            LayoutTestSetItem testSetItemToBeSelected = null;
            if (Items != null)
            {
                //IsSelected = false;
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].LayoutTestSetItems != null)
                    {
                        for (int j = 0; j < Items[i].LayoutTestSetItems.Count; j++)
                        {
                            Items[i].IsSelected = false;
                            if (Items[i].LayoutTestSetItems[j].Id.Equals(id))
                            {
                                parentLayout = Items[i];
                                testSetItemToBeSelected = Items[i].LayoutTestSetItems[j];
                            }
                            else
                            {
                                Items[i].LayoutTestSetItems[j].IsSelected = false;
                            }
                        }
                    }
                }

                if (testSetItemToBeSelected != null)
                {
                    if (parentLayout != null)
                    {
                        parentLayout.IsExpanded = true;
                    }
                    SelectedTestSetMode = TestItemTypes.TesSetItem;
                    testSetItemToBeSelected.IsSelected = true;
                }
            }
        }

        void layoutTestSetViewModel_ItemSelected(object sender, EventArgs e)
        {
            if (!IsSelected)
            {
                onItemSelected();
            }
            else
            {
                IsSelected = false;
            }
        }

        private void onTestSetSelected(object obj)
        {
            this.IsSelected = true;
        }
        
        public void UpdateTestSetSelection()
        {
            SelectedTestSets = new List<LayoutTestSetViewModel>();

            if (IsSelected)
            {
                SelectedTestSets = Items.ToList();
                setMessageForTestSet();
                SelectedTestSetMode = TestItemTypes.TestSet;
            }
            else
            {
                var selectedLayout = getSelectedLayout();
                if (null != selectedLayout)
                {
                    SelectedTestSets = new List<LayoutTestSetViewModel> { selectedLayout };
                    setMessageForLayout(selectedLayout);
                    SelectedTestSetMode = TestItemTypes.Layout;
                }
                else
                {
                    var layoutTestSetViewModel = getSelectedDocSetItem();
                    if (null != layoutTestSetViewModel)
                    {
                        SelectedTestSets = new List<LayoutTestSetViewModel> { layoutTestSetViewModel };
                        InformationMessage = string.Empty;
                        SelectedTestSetMode = TestItemTypes.TesSetItem;
                    }
                }
            }

        }

        private void setMessageForLayout(LayoutTestSetViewModel selectedLayout)
        {
            InformationMessage = selectedLayout.LayoutTestSetItems.Count > 0 
                                ? string.Format(Properties.Resources.ClickOnRunForTests, "this " + Properties.Resources.LayoutLabel)
                                : string.Format(Properties.Resources.NoTestExistsMessge, Properties.Resources.LayoutLabel);
        }

        private void setMessageForTestSet()
        {
            InformationMessage = isLayoutAdded()
                        ? isTestsAdded() ? string.Format(Properties.Resources.ClickOnRunForTests, "Test Set") : string.Format(Properties.Resources.NoTestExistsMessgeForTestSet, Properties.Resources.LayoutLabel)
                        : String.Format(Properties.Resources.CreateLayoutMessage,Properties.Resources.LayoutLabel,
                            Properties.Resources.LayoutTabLabel, Properties.Resources.BlueprintTabLabel);
        }

        private bool isLayoutAdded()
        {
            if (SelectedTestSets != null && SelectedTestSets.Count > 0)
            {
                return true;
            }

            return false;
        }

        private bool isTestsAdded()
        {
            if (SelectedTestSets != null && SelectedTestSets.Count > 0)
            {
                foreach (var testSet in SelectedTestSets)
                {
                    if (testSet.LayoutTestSetItems != null && testSet.LayoutTestSetItems.Count > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        private LayoutTestSetViewModel getSelectedDocSetItem()
        {
            foreach (var layoutTestSetViewModel in Items)
            {
                var selectedItem = layoutTestSetViewModel.LayoutTestSetItems.FirstOrDefault(d => d.IsSelected);
                if (selectedItem != null)
                {
                    SelectedTestSetMode = TestItemTypes.TesSetItem;
                    return ((LayoutTestSetViewModel)selectedItem.Parent).Clone(selectedItem);
                }
            }

            return null;
        }


        private LayoutTestSetItem getSelectedLayoutTestSetItem()
        {
            foreach (var layoutTestSetViewModel in Items)
            {
                var selectedItem = layoutTestSetViewModel.LayoutTestSetItems.FirstOrDefault(d => d.IsSelected);
                if (selectedItem != null)
                {
                    return selectedItem;
                }
            }

            return null;
        }

        private LayoutTestSetViewModel getSelectedLayout()
        {
            return Items.FirstOrDefault(l => l.IsSelected);
        }

        private void removeChildSelection()
        {
            var selectedLayout = getSelectedLayout();
            if (null != selectedLayout)
            {
                selectedLayout.IsSelected = false;
            }
            else
            {
                var layoutTestSetItem = getSelectedLayoutTestSetItem();
                if (null != layoutTestSetItem)
                {
                    layoutTestSetItem.IsSelected = false;
                }
            }
        }

        void layoutTestSetViewModel_TestDocSetItemAddRequested(object sender, TestDocSetEventArgs e)
        {
            if (TestDocSetItemAddRequested != null)
            {
                TestDocSetItemAddRequested(this, e);
            }
        }

        private void onItemSelected()
        {
            getSelectedItems();
            if (ItemSelected != null)
            {
                ItemSelected(this, new EventArgs());
            }
        }

        private void getSelectedItems()
        {
            this.UpdateTestSetSelection();
            _selectedTestSetItem = getSelectedTestSetItem();
        }

        private LayoutTestSetItem getSelectedTestSetItem()
        {
            LayoutTestSetItem selectedTestSetItem = new NullLayoutTestSetItem();
            if (isSingleTestSetItemSelected())
            {
                selectedTestSetItem = _selectedTestSets[0].LayoutTestSetItems[0];
            }

            return selectedTestSetItem;
        }


        private bool isSingleTestSetItemSelected()
        {
            return _selectedTestSets != null
                && _selectedTestSets.Count == 1
                && _selectedTestSets[0].LayoutTestSetItems != null
                && _selectedTestSets[0].LayoutTestSetItems.Count == 1
                && _selectedTestSets[0].LayoutTestSetItems[0] != null
                && _selectedTestSets[0].LayoutTestSetItems[0].IsSelected;
        }
    }
}