﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;
using Automation.CognitiveData.ConfidenceScore;
using System;
using System.Windows.Input;
using System.Linq;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    class LayoutClassificationErrorViewModel : ViewModelBase, ISummaryResult
    {
        public string Header { get; private set; }

        public int ErrorCount { get; private set; }

        public string ItemName { get; private set; }

        public IList<ITestItemResult> Children { get; private set; }

        private ITestItemResult _selectedChild;

        public ITestItemResult SelectedChild
        {
            get { return _selectedChild; }
            set
            {
                if (_selectedChild != value)
                {
                    _selectedChild = value;
                    OnPropertyChanged("SelectedChild");
                }
            }
        }

        private ITestItemResult _selectedSecondLevelChild;

        public ITestItemResult SelectedSecondLevelChild
        {
            get { return _selectedSecondLevelChild; }
            set
            {
                if (_selectedSecondLevelChild != value)
                {
                    _selectedSecondLevelChild = value;
                    OnPropertyChanged("SelectedSecondLevelChild");
                }
            }
        }

        public string Id { get; private set; }

        public event EventHandler ViewRequested;

        public LayoutClassificationErrorViewModel(IResultGenerator resultGenerator)
        {
            var result = resultGenerator.Generate(viewHandler);
            ErrorCount = result.ErrorCount;
            ItemName = result.Name;
            Children = result.Children != null ? result.Children.OrderByDescending(element => element.ErrorCount).ToList() : result.Children;
            Header = (result as TestItemResults).Header;
            Id = result.Id;
        }

        private void viewHandler(object sender, EventArgs e)
        {
            if (ViewRequested != null)
            {
                ViewRequested(sender, e);
            }
        }
    }

    class TestItemResult : ITestItemResult
    {
        public string Name { get; private set; }

        public int ErrorCount { get; private set; }

        public string Id { get; private set; }

        private ICommand _viewCommand;

        public ICommand ViewCommand
        {
            get { return _viewCommand; }
            private set { _viewCommand = value; }
        }

        public event EventHandler ViewRequested;

        public TestItemResult(string fieldId, string name, int errorCount)
        {
            Id = fieldId;
            Name = name;
            ErrorCount = errorCount;
            _viewCommand = new RelayCommand(viewCommandHandler);
        }

        private void viewCommandHandler(object param)
        {
            onViewRequested();
        }

        private void onViewRequested()
        {
            if (ViewRequested != null)
            {
                ViewRequested(this, EventArgs.Empty);
            }
        }


    }

    class TestItemResults : ITestItemResults
    {
        public string Header { get; protected set; } = "Test";

        public string Name { get; private set; }

        public int ErrorCount { get; private set; }

        public string Id { get; private set; }

        public TestItemResults(string fieldId, string name, IList<ITestItemResult> children)
        {
            Id = fieldId;
            Name = name;
            Children = children;
            ErrorCount = children.Count;
        }


        public TestItemResults(string fieldId, string name, IList<ITestItemResult> children, int errorCount)
        {
            Id = fieldId;
            Name = name;
            Children = children;
            ErrorCount = errorCount;
        }

        public IList<ITestItemResult> Children { get; private set; }
    }

    class LayoutItemResults : TestItemResults
    {
        public LayoutItemResults(string fieldId, string name, IList<ITestItemResult> children) : base(fieldId, name, children)
        {
            Header = Properties.Resources.LayoutLabel;
        }


        public LayoutItemResults(string fieldId, string name, IList<ITestItemResult> children, int errorCount) : base(fieldId, name, children, errorCount)
        {
            Header = Properties.Resources.LayoutLabel;
        }
    }

    class LayoutClassificationResultGenerator : IResultGenerator
    {
        internal const string ErrorCountKey = "ErrorCount";
        internal const string ItemNameKey = "ItemName";
        internal const string ChildrenKey = "Children";
        internal const string TestResultIdKey = "TestResultId";

        private const string ItemName = "Document Classification Error";

        private IList<LayoutTestResult> _layoutTestResults;
        private bool _isTestSet;
        private EventHandler _viewHandler;

        public LayoutClassificationResultGenerator(IList<LayoutTestResult> layoutTestResults, bool isTestSet)
        {
            _layoutTestResults = layoutTestResults;
            _isTestSet = isTestSet;
        }

        public ITestItemResults Generate(EventHandler viewHandler)
        {
            _viewHandler = viewHandler;
            return fillListOfFailedObjects();
        }

        private ITestItemResults fillListOfFailedObjects()
        {
            int totalErrorCount = 0;
            string fieldId = string.Empty;
            IList<ITestItemResult> children;
            if (_isTestSet)
            {
                children = new List<ITestItemResult>();
                foreach (var result in _layoutTestResults)
                {
                    var tests = getFailedTests(result);
                    if (tests.Count > 0)
                    {
                        children.Add(new TestItemResults(result.LayoutId, result.LayoutName, tests));
                        totalErrorCount += tests.Count;
                    }
                }
                return new LayoutItemResults(fieldId, ItemName, children, totalErrorCount);
            }
            else
            {
                if (_layoutTestResults.Count > 0)
                {
                    fieldId = _layoutTestResults[0].LayoutId;
                    children = getFailedTests(_layoutTestResults[0]);
                    totalErrorCount = children.Count;
                }
                else
                {
                    children = new List<ITestItemResult>();
                }
                return new TestItemResults(fieldId, ItemName, children, totalErrorCount);
            }


        }

        private IList<ITestItemResult> getFailedTests(LayoutTestResult result)
        {
            IList<ITestItemResult> tests = new List<ITestItemResult>();
            foreach (var item in result.TestDocItemResults)
            {
                if (!item.LayoutClassificationError.IsSameLayoutClassified)
                {
                    FailedObject failedObject = new FailedObject(item.TestDocItemId, item.TestName, 1)
                    {
                        ParentId = result.LayoutId,
                        Type = TestItemTypes.TesSetItem
                    };

                    failedObject.ViewRequested += _viewHandler;
                    tests.Add(failedObject);
                }
            }
            return tests;
        }
    }
}
