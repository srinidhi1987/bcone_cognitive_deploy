/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.ConfidenceScore;

using System.Collections.Generic;
using System.Windows.Input;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
using System;
using System.Text;

    public class SummaryViewModel : ViewModelBase
    {
        private TestSetSummaryViewModel _testSetSummaryViewModel;
        private LayoutClassificationErrorViewModel _layoutClassificationErrorResultViewModel;
        private DataRecognitionErrorViewModel _dataRecognitionErrorResultViewModel;

        public ICommand CorrectnessCommand { get; private set; }
        public ICommand LayoutErrorCommand { get; private set; }
        public ICommand DataRecognitionCommand { get; private set; }

        public float Percentages { get { return _testSetSummaryViewModel.Percentages; } }

        public string CorrectnessLabel
        {
            get
            {
                return string.Join(" ", "Overall", Properties.Resources.CorrectnessLabel);
            }
        }

        public string LayoutClassificationTitle { get { return _layoutClassificationErrorResultViewModel.ItemName; } }
        public string DataRecognitionTitle { get { return _dataRecognitionErrorResultViewModel.ItemName; } }

        public int LayoutClassificationErrorCount { get { return _layoutClassificationErrorResultViewModel.ErrorCount; } }
        public int DataRecognitionErrorCount { get { return _dataRecognitionErrorResultViewModel.ErrorCount; } }

        private object _content;

        public object Content
        {
            get { return _content; }
            private set
            {
                if (_content != value)
                {
                    _content = value;
                    OnPropertyChanged("Content");
                }
            }
        }

        private bool _correctnessRadio = true;

        public bool CorrectnessRadio
        {
            get { return _correctnessRadio; }
            set { _correctnessRadio = value; }
        }

        private bool _layoutErrorRadio;

        public bool LayoutErrorRadio
        {
            get { return _layoutErrorRadio; }
            set { _layoutErrorRadio = value; }
        }

        private bool _dataErrorRadio;

        public bool DataErrorRadio
        {
            get { return _dataErrorRadio; }
            set { _dataErrorRadio = value; }
        }

        public EventHandler ViewTestRequested;

        public SummaryViewModel(IList<LayoutTestResult> layoutTestResults, bool isTestSet)
        {
            CorrectnessCommand = new RelayCommand(correctnessViewSelected);
            LayoutErrorCommand = new RelayCommand(layoutErrorViewSelected);
            DataRecognitionCommand = new RelayCommand(dataRecognitionErrorViewSelected);

            _testSetSummaryViewModel = new TestSetSummaryViewModel(layoutTestResults);
            _layoutClassificationErrorResultViewModel = new LayoutClassificationErrorViewModel(
                new LayoutClassificationResultGenerator(layoutTestResults, isTestSet));
            _layoutClassificationErrorResultViewModel.ViewRequested += viewRequestedHandler;
            _dataRecognitionErrorResultViewModel = new DataRecognitionErrorViewModel(
                new DataRecognitionResultGenerator(layoutTestResults, isTestSet));
            _dataRecognitionErrorResultViewModel.ViewRequested += viewRequestedHandler;

            Content = _testSetSummaryViewModel;
        }

        void viewRequestedHandler(object sender, EventArgs e)
        {
            if(ViewTestRequested != null)
            {
                ViewTestRequested(sender, e);
            }
        }

        private void correctnessViewSelected(object obj)
        {
            Content = _testSetSummaryViewModel;
        }

        private void layoutErrorViewSelected(object obj)
        {
            //_layoutClassificationErrorResultViewModel.FailedLayouts = _summaryDataExtraction.GetListOfFailedLayoutClassificationObjects();

            Content = _layoutClassificationErrorResultViewModel;
        }

        private void dataRecognitionErrorViewSelected(object obj)
        {
           // _dataRecognitionErrorResultViewModel.FailedFields = _summaryDataExtraction.GetListOfFailedFields();
            Content = _dataRecognitionErrorResultViewModel;
        }
    }
}