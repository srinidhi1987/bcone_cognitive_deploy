/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using Automation.CognitiveData.VisionBot.UI.Model;
    using System;
    
    public class LayoutTestSetItem : ViewModelBase, INullable, ISelectableItem
    {
        private DocSetItem _docSetItem;

        public event EventHandler RemoveRequested;

        public event EventHandler ItemSelected;

        public string Name
        {
            get { return _docSetItem.Name; }
            set { _docSetItem.Name = value; }
        }

        public string Id
        {
            get { return _docSetItem.Id; }
            set { _docSetItem.Id = value; }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                   
                    if (ItemSelected != null && value)
                    {
                        ItemSelected(this, new EventArgs());
                    }
                    OnPropertyChanged("IsSelected");
                    if (!_isSelected &&
                        _docSetItem.IsBenchmarkStructureMismatchFound != _docSetItem.OriginalIsBenchmarchStructureFound)
                    {
                        _docSetItem.IsBenchmarkStructureMismatchFound = _docSetItem.OriginalIsBenchmarchStructureFound;
                        Warning = getWarning();
                    }
                }
            }
        }

        public ISelectableItem Parent { get; private set; }

        public RelayCommand RemoveCommand { get; private set; }

        private IWarning _warning = GenericWarning.NoWarning;

        public IWarning Warning
        {
            get { return _warning; }
            set
            {
                if (_warning != value)
                {
                    _warning = value;
                    _docSetItem.IsBenchmarkStructureMismatchFound = _warning.HasWarning;
                    OnPropertyChanged(nameof(Warning));
                }
            }
        }

        public LayoutTestSetItem(ISelectableItem parent) : this(new DocSetItem(), parent)
        {
        }

        public LayoutTestSetItem(DocSetItem docSetItem, ISelectableItem parent)
        {
            _docSetItem = docSetItem;
            Parent = parent;
            RemoveCommand = new RelayCommand(onRemoveCommand);
            //setWarning();
        }

        private void setWarning()
        {
            _warning = getWarning();
        }

        private IWarning getWarning()
        {
            if (_docSetItem.IsBenchmarkStructureMismatchFound)
            {
                return new GenericWarning(true, string.Format(Properties.Resources.BlueprintBenchmarkStructureMismatchWarningMessage, Properties.Resources.BlueprintTabLabel), WarningType.Blueprint);
            }

            return GenericWarning.NoWarning;
        }

        public virtual bool IsNull()
        {
            return false;
        }

        public DocSetItem GetDocSetItem()
        {
            return _docSetItem;
        }

        //TODO: Remove this
        public void SetDocSetItem(DocSetItem docsetItem)
        {
            _docSetItem = docsetItem;
            setWarning();
        }

        private void onRemoveCommand(object obj)
        {
            if (RemoveRequested != null)
            {
                RemoveRequested(this, EventArgs.Empty);
            }
        }
    }

    public interface ISelectableItem
    {
        event EventHandler ItemSelected;

        bool IsSelected { get; set; }

        ISelectableItem Parent { get; }
    }
}