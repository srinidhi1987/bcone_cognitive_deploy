/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class UpdateLayoutTable : ViewModelBase
    {
        #region Fields
        private LayoutTable _layoutTable;
        private LayoutField _originalFooter;
        private LayoutField _originalPrimaryField;
        private LayoutField _selectedPrimaryField;
        private LayoutField _footer;

        private RelayCommand _cancelRequested;
        private RelayCommand _updateRequested;
        #endregion

        #region Events
        public event Action<LayoutTable> UpdateLayoutTableRequested;
        #endregion

        #region Properties
        public RelayCommand CancelRequested
        {
            get { return _cancelRequested; }
            set { _cancelRequested = value; }
        }

        public LayoutField Footer
        {
            get { return _footer; }
            set
            {
                if (_footer != value)
                {
                    _footer = value;
                    OnPropertyChanged(nameof(Footer));
                }
            }
        }

        public LayoutTable LayoutTable
        {
            get { return _layoutTable; }
            set { _layoutTable = value; }
        }

        public LayoutField SelectedPrimaryField
        {
            get { return _selectedPrimaryField; }
            set
            {
                if (_selectedPrimaryField != value)
                {
                    _selectedPrimaryField = value;
                    OnPropertyChanged(nameof(SelectedPrimaryField));
                    IsDirty = true;
                }
            }
        }

        public RelayCommand UpdateRequested
        {
            get { return _updateRequested; }
            set { _updateRequested = value; }
        }

        private bool _isDirty;
        public bool IsDirty
        {
            get { return _isDirty; }
            private set
            {
                if (_isDirty != value)
                {
                    _isDirty = value;
                    OnPropertyChanged(nameof(IsDirty));
                }
            }
        }
        #endregion

        #region Constructor
        public UpdateLayoutTable(LayoutTable layoutTable)
        {
            _layoutTable = layoutTable;
            if (layoutTable.Footer == null)
            {
                layoutTable.Footer = new LayoutField();
                layoutTable.Footer.MergeRatio = 1f;
                layoutTable.Footer.SimilarityFactor = 0.7f;
            }
            _originalFooter = layoutTable.Footer;
            Footer = new LayoutField();
            UpdateLayoutFieldViewModel.copyLayoutField(_originalFooter, Footer);
            _originalPrimaryField = _layoutTable.PrimaryField;
            _selectedPrimaryField = _layoutTable.PrimaryField;
            _updateRequested = new RelayCommand(UpdateRequestedHandler);
            _cancelRequested = new RelayCommand(CancelRequestedHandler);
            resetDirtyFlags();

        }
        #endregion

        #region Methods
        public void CancelRequestedHandler(object param)
        {
            LayoutField newFooter = new LayoutField();
            UpdateLayoutFieldViewModel.copyLayoutField(_originalFooter, newFooter);
            Footer = newFooter;
            SelectedPrimaryField = _originalPrimaryField;
            resetDirtyFlags();
        }

        public void UpdateRequestedHandler(object param)
        {
            LayoutTable.PrimaryField = SelectedPrimaryField;
            UpdateLayoutFieldViewModel.copyLayoutField(Footer, _originalFooter);

            if (UpdateLayoutTableRequested != null)
            {
                UpdateLayoutTableRequested(LayoutTable);

                _originalPrimaryField = SelectedPrimaryField;

                resetDirtyFlags();
            }

        }

        private void resetDirtyFlags()
        {
            if (null != Footer)
            {
                Footer.ResetDirtyFlag();
            }

            IsDirty = false;
        }
        #endregion
    }
}