﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class TestDocSummaryViewModel : ViewModelBase
    {
        public ObservableCollection<FailedObject> TestDocFailedFieldsSummary { get; set; }

        public bool HasFailedFields { get; set; }
        public bool IsCorrectnessNotZero { get; set; }

        public string CorrectnessLabel => Properties.Resources.CorrectnessLabel;

        public string Correctness { get; set; }
        public string TotalFields { get; set; }
        public string FailedFields { get; set; }
        public string ExecutionTime { get; set; }

        public TestDocSummaryViewModel()
        {
            TestDocFailedFieldsSummary = new ObservableCollection<FailedObject>();
            HasFailedFields = TestDocFailedFieldsSummary.Count > 0;
            IsCorrectnessNotZero = true;
        }

        public bool HasLayoutClassificationError { get; set; }
        public string LayoutName { get; set; }
    }

    public class FailedObject : ITestItemResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int ErrorCount { get; set; }
        public TestItemTypes Type { get; set; }
        public string ParentId { get; set; }

        public event EventHandler ViewRequested;

        private ICommand _viewCommand;

        public ICommand ViewCommand
        {
            get { return _viewCommand; }
            private set { _viewCommand = value; }
        }

        public FailedObject()
        {
            _viewCommand = new RelayCommand(viewCommandHandler);
        }

        public FailedObject(string id, string name, int errorCount)
        {
            Id = id;
            Name = name;
            ErrorCount = errorCount;
            _viewCommand = new RelayCommand(viewCommandHandler);
        }

        private void viewCommandHandler(object param)
        {
            onViewRequested();
        }

        private void onViewRequested()
        {
            if (ViewRequested != null)
            {
                ViewRequested(this, EventArgs.Empty);
            }
        }
    }
}
