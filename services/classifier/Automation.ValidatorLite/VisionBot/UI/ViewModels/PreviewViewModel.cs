///**
// * Copyright (c) 2015 Automation Anywhere.
// * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
// * All rights reserved.
// *
// * This software is the confidential and proprietary information of
// * Automation Anywhere.("Confidential Information").  You shall not
// * disclose such Confidential Information and shall use it only in
// * accordance with the terms of the license agreement you entered into
// * with Automation Anywhere.
// */

//using System.Collections.Generic;

//namespace Automation.CognitiveData.VisionBot.UI.ViewModels
//{
//    class PreviewViewModel : ViewModelBase, ITestResult
//    {
//        //TODO: Set Id to refer individual Test run result
//        public string Id { get; private set; }

//        public IList<FieldViewModel> Fields { get; set; }

//        public IList<TableViewModel> Tables { get; set; }

//        public PreviewViewModel()
//        {
//            Id = string.Empty;
//        }
//    }
//}