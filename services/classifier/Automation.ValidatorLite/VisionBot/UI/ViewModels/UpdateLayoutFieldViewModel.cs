﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    using Automation.VisionBotEngine;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    public delegate void SelectionStatusChangeHandler(object sender, SelectionEventArgs e);

    public enum SelectionState
    {
        On,
        Cancel,
        Update
    }

    public class SelectionEventArgs : EventArgs
    {
        public SelectionState SelectionState { get; private set; }

        public bool IsValuePreviewOn { get; private set; }

        public SelectionEventArgs(SelectionState selectionState, bool isValuePreviewOn)
        {
            SelectionState = selectionState;
            IsValuePreviewOn = isValuePreviewOn;
        }
    }

    public class UpdateLayoutFieldViewModel : ViewModelBase
    {
        private IqBot _iqBot;

        private Nullable<ElementType> _selectedFieldType;

        public Nullable<ElementType> SelectedFieldType
        {
            get { return _selectedFieldType; }
            set
            {
                _selectedFieldType = value;
                SelectedTable = null;
                SelectedFieldDef = null;
                OnPropertyChanged("SelectedFieldType");
                fillMatchingFields();
                AssignSaveCaption();
                resetOriginalObject();
            }
        }

        private FieldDef _originalSelectedFieldDef;
        private FieldDef _selectedFieldDef;

        public FieldDef SelectedFieldDef
        {
            get { return _selectedFieldDef; }
            set
            {
                if (_selectedFieldDef != value)
                {
                    _selectedFieldDef = value;
                    OnPropertyChanged("SelectedFieldDef");
                    if (_selectedFieldDef != null)
                    {
                        LayoutField.FieldDef = SelectedFieldDef;
                        LayoutField.MarkerName = _selectedFieldDef.Name;
                        RequestAutoValueBound();
                    }
                }
            }
        }

        private CroppedBitmap _croppedBitmap;

        public CroppedBitmap CroppedBitmap
        {
            get { return _croppedBitmap; }
            set { _croppedBitmap = value; OnPropertyChanged(nameof(CroppedBitmap)); }
        }

        //private TableDef _originalSelectedTableDef;
        private TableDef _selectedTableDef;

        public TableDef SelectedTable
        {
            get { return _selectedTableDef; }
            set
            {
                if (_selectedTableDef != value)
                {
                    _selectedTableDef = value;
                    fillSelectedTabelFields();
                    OnPropertyChanged("SelectedTable");
                }
            }
        }

        private ElementType _givenElementType;

        public ElementType GivenElementType
        {
            get { return _givenElementType; }
            set { _givenElementType = value; }
        }

        private bool _isDefEditable;

        public bool IsDefEditable
        {
            get { return _isDefEditable; }
            set { _isDefEditable = value; }
        }

        public bool _isMappingExpnded = true;

        public bool IsMappingExpnded
        {
            get { return _isMappingExpnded; }
            set
            {
                _isMappingExpnded = value;
                OnPropertyChanged("IsMappingExpnded");
            }
        }

        private ObservableCollection<FieldDef> _matchingFields = new ObservableCollection<FieldDef>();

        public ObservableCollection<FieldDef> MatchingFields
        {
            get { return _matchingFields; }
            set { _matchingFields = value; }
        }

        private ObservableCollection<TableDef> _matchingTabels = new ObservableCollection<TableDef>();

        public ObservableCollection<TableDef> MatchingTabels
        {
            get { return _matchingTabels; }
            set { _matchingTabels = value; }
        }

        private ObservableCollection<ElementType> _eligibleElementTypes = new ObservableCollection<ElementType>();

        public ObservableCollection<ElementType> EligibleElementTypes
        {
            get { return _eligibleElementTypes; }
            set { _eligibleElementTypes = value; }
        }

        private RelayCommand _updateRequested;

        public RelayCommand UpdateRequested
        {
            get { return _updateRequested; }
            set { _updateRequested = value; }
        }

        private RelayCommand _cancelRequested;

        public RelayCommand CancelRequested
        {
            get { return _cancelRequested; }
            set { _cancelRequested = value; }
        }

        private RelayCommand _editModeEditMarkerRequested;

        public RelayCommand EditModeEditMarkerRequested
        {
            get { return _editModeEditMarkerRequested; }
            set { _editModeEditMarkerRequested = value; }
        }

        public ICommand TurnSelectionOn { get; set; }

        private LayoutField _originalLayoutField;

        private LayoutField _layoutField;

        public LayoutField LayoutField
        {
            get { return _layoutField; }
            set
            {
                if (_layoutField != value)
                {
                    _layoutField = value;
                    OnPropertyChanged("LayoutField");
                }
            }
        }

        private Layout _layout;

        public Layout CurrentLayout
        {
            get { return _layout; }
            set { _layout = value; }
        }

        private string _saveCaption;

        public string SaveCaption
        {
            get { return _saveCaption; }
            set
            {
                if (_saveCaption != value)
                {
                    _saveCaption = value;
                    OnPropertyChanged("SaveCaption");
                }
            }
        }

        private string errorMessage = string.Empty;

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        private bool _markerEditableInEditMode;

        public bool MarkerEditableInEditMode
        {
            get { return _markerEditableInEditMode; }
            set
            {
                if (_markerEditableInEditMode != value)
                {
                    _markerEditableInEditMode = value;
                    OnPropertyChanged("MarkerEditableInEditMode");
                }
            }
        }

        public event EventHandler TurnValueSelectionOffRequested;

        public event SelectionStatusChangeHandler SelectionStatusChanged;

        public event Action<LayoutField> UpdateLayoutData;

        public event Action<TableDef, LayoutField> UpdateTableLayoutField;

        public event Action<LayoutField> UpdateMarker;

        public event LayoutFieldValueRequestedHandler AutoValueBoundRequested;

        public UpdateLayoutFieldViewModel(IqBot iqBot, Layout layout, LayoutField layoutField, CroppedBitmap previewImageSource)
        {
            CroppedBitmap = previewImageSource;
            _saveCaption = "Save";
            fillEligibleElementTypes();
            _iqBot = iqBot;
            CurrentLayout = layout;
            _originalLayoutField = layoutField;
            LayoutField = new LayoutField();
            copyLayoutField(_originalLayoutField, LayoutField);
            UpdateRequested = new RelayCommand(UpdateRequestedHandler);
            CancelRequested = new RelayCommand(CancelRequestedHandler);
            EditModeEditMarkerRequested = new RelayCommand(EditModeEditMarkerRequestedHandler);

            TurnSelectionOn = new RelayCommand(selectionTurnedOn);

            GivenElementType = _originalLayoutField.Type;
            if (GivenElementType == ElementType.SystemIdentifiedField || GivenElementType == ElementType.SystemIdentifiedRegion)
            {
                IsDefEditable = true;
                _selectedFieldType = null;
            }
            else
            {
                IsDefEditable = false;
            }

            if (_originalLayoutField.Type == ElementType.Field)
            {
                _selectedFieldType = ElementType.Field;
                fillMatchingFields();
                //string fieldDefID = _originalLayoutField.FieldID;
                if (_matchingFields != null)
                {
                    //for (int i = 0; i < _matchingFields.Count; i++)
                    //{
                    //    if (_matchingFields[i].Id == fieldDefID)
                    //    {
                    //        _selectedFieldDef = _matchingFields[i];
                    //        _originalSelectedFieldDef = _selectedFieldDef;
                    //        break;
                    //    }
                    //}
                    _selectedFieldDef = LayoutField.FieldDef;
                }
            }
            else if (_originalLayoutField.Type == ElementType.Table)
            {
                _selectedFieldType = ElementType.Table;
                string fieldDefID = _originalLayoutField.FieldDef.Id;
                fillMatchingFields();
                TableDef tableDefTmp;
                FieldDef fieldDefTemp;
                getTableDef(fieldDefID, out tableDefTmp, out fieldDefTemp);
                _selectedTableDef = tableDefTmp;
                fillSelectedTabelFields();
                _selectedFieldDef = fieldDefTemp;
                _originalSelectedFieldDef = _selectedFieldDef;
            }
            else if (_originalLayoutField.Type == ElementType.Marker)
            {
                _selectedFieldType = ElementType.Marker;
                _selectedFieldDef = null;
                _selectedTableDef = null;
            }
            else if (_originalLayoutField.Type == ElementType.SystemIdentifiedField || _originalLayoutField.Type == ElementType.SystemIdentifiedRegion)
            {
                _selectedFieldDef = null;
                _selectedTableDef = null;
                _selectedFieldType = null;
                fillMatchingFields();
            }

            evaluateValueBoundAutoFlag();
            LayoutField.ResetDirtyFlag();
        }

        private void selectionTurnedOn(object obj)
        {
            if (SelectionStatusChanged != null)
            {
                LayoutField.IsValueSelectionOn = true;
                SelectionStatusChanged(this, new SelectionEventArgs(SelectionState.On, isValuePreviewOn()));
            }
        }

        private bool isValuePreviewOn()
        {
            if (_selectedFieldType != null &&
                    (_selectedFieldType.Value == ElementType.Field ||
                   _selectedFieldType.Value == ElementType.SystemIdentifiedField ||
                   _selectedFieldType.Value == ElementType.SystemIdentifiedRegion))
            {
                return true;
            }

            return false;
        }

        private void getTableDef(string fieldDefID, out TableDef tableDef, out FieldDef fieldDef)
        {
            tableDef = null;
            fieldDef = null;

            if (_iqBot != null && _iqBot.DataModel != null && _iqBot.DataModel.Tables != null)
            {
                for (int i = 0; i < _iqBot.DataModel.Tables.Count; i++)
                {
                    if (_iqBot.DataModel.Tables[i].Columns != null)
                    {
                        for (int j = 0; j < _iqBot.DataModel.Tables[i].Columns.Count; j++)
                        {
                            if (_iqBot.DataModel.Tables[i].Columns[j].Id == fieldDefID)
                            {
                                tableDef = _iqBot.DataModel.Tables[i];
                                fieldDef = _iqBot.DataModel.Tables[i].Columns[j];
                                break;
                            }
                        }
                    }
                }
            }
        }

        public static void copyLayoutField(LayoutField from, LayoutField to)
        {
            to.Bounds = from.Bounds;
            to.OriginalBounds = from.OriginalBounds;
            to.ValueBounds = from.ValueBounds;
            to.IsValueBoundAuto = from.IsValueBoundAuto;
            to.Value = from.Value;
            to.FieldDirection = from.FieldDirection;
            to.FieldDef = from.FieldDef;
            to.FieldValueType = from.FieldValueType;
            to.IsMultiLine = from.IsMultiLine;
            to.IsRequired = from.IsRequired;
            //to.ItemBackground = from.ItemBackground;
            to.Label = from.Label;
            to.SimilarityFactor = from.SimilarityFactor;
            to.MergeRatio = from.MergeRatio;
            to.MarkerName = from.MarkerName;
            to.Type = from.Type;
            to.Confidence = from.Confidence;
            to.Id = from.Id;
            to.SystemIdentifiedFieldParentId = from.SystemIdentifiedFieldParentId;

            to.StartsWith = from.StartsWith;
            to.EndsWith = from.EndsWith;
            to.FormatExpression = from.FormatExpression;
            to.UserDefinedRegionType = from.UserDefinedRegionType;
            to.ValueSelectionType = from.ValueSelectionType;
        }

        public void UpdateRequestedHandler(object param)
        {
            //if (GivenElementType == ElementType.SystemIdentifiedField || GivenElementType == ElementType.SystemIdentifiedRegion)
            //{
            //    if (SelectedFieldDef != null && SelectedFieldType != ElementType.Marker)
            //    {
            //        LayoutField.FieldID = SelectedFieldDef.Id;
            //        LayoutField.Name = SelectedFieldDef.Name;
            //    }
            //}
            if (SelectedFieldDef != null && SelectedFieldType != ElementType.Marker)
            {
                LayoutField.FieldDef = SelectedFieldDef;
                //LayoutField.MarkerName = SelectedFieldDef.Name;
            }

            if (!Validate())
                return;
            OnUpdateData();
            LayoutField.ResetDirtyFlag();
            MarkerEditableInEditMode = false;
        }

        public void EditModeEditMarkerRequestedHandler(object param)
        {
            MarkerEditableInEditMode = true;
        }

        public void CancelRequestedHandler(object param)
        {
            if (LayoutField != null)
            {
                if (SelectionStatusChanged != null)
                {
                    SelectionStatusChanged(this, new SelectionEventArgs(SelectionState.Cancel, false));
                }
            }
            resetOriginalObject();
        }

        public void RequestAutoValueBound()
        {
            try
            {
                if (AutoValueBoundRequested != null && LayoutField != null && _selectedFieldType == ElementType.Field)
                {
                    if (_originalLayoutField.Type == ElementType.SystemIdentifiedField
                        || _originalLayoutField.Type == ElementType.SystemIdentifiedRegion
                        || _originalLayoutField.Type == ElementType.Field)
                    {
                        LayoutFieldValueRequestedEventArgs args = new LayoutFieldValueRequestedEventArgs(LayoutField);
                        AutoValueBoundRequested(this, args);
                        if (args.ValueField != null)
                        {
                            LayoutField.Value = args.ValueField.Text;
                            Rect valueBound = args.ValueField.Bounds.ConvertToRect();
                            LayoutField.ValueBounds = valueBound.Subtract(LayoutField.Bounds);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(UpdateLayoutFieldViewModel), nameof(RequestAutoValueBound));
            }
        }

        private void OnUpdateData()
        {
            LayoutField fieldToUpdate = null;
            if (GivenElementType == ElementType.SystemIdentifiedField || GivenElementType == ElementType.SystemIdentifiedRegion)
            {
                fieldToUpdate = LayoutField;
            }
            else
            {
                copyLayoutField(LayoutField, _originalLayoutField);
                fieldToUpdate = _originalLayoutField;
            }

            if (SelectedFieldType == ElementType.Field)
            {
                if (UpdateLayoutData != null)
                {
                    if (LayoutField.IsValueSelectionOn)
                    {
                        OnTurnValueSelectionOffRequested();
                    }
                    if (!IsDefEditable && null != SelectionStatusChanged)
                    {
                        SelectionStatusChanged(this, new SelectionEventArgs(SelectionState.Update, isValuePreviewOn()));
                    }
                    UpdateLayoutData(fieldToUpdate);
                }
            }
            else if (SelectedFieldType == ElementType.Table)
            {
                if (UpdateTableLayoutField != null)
                {
                    UpdateTableLayoutField(SelectedTable, fieldToUpdate);
                }
            }
            else if (SelectedFieldType == ElementType.Marker)
            {
                if (UpdateMarker != null)
                {
                    UpdateMarker(fieldToUpdate);
                }
            }
        }

        private void OnTurnValueSelectionOffRequested()
        {
            if (TurnValueSelectionOffRequested != null)
            {
                TurnValueSelectionOffRequested(this, EventArgs.Empty);
            }
        }

        private bool Validate()
        {
            ErrorMessage = string.Empty;
            try
            {
                if (SelectedFieldType != null && SelectedFieldType.Value == ElementType.Marker)
                {
                    new NameValidatorHelper().ValidateMarker(LayoutField.MarkerName);
                    (new DuplicateValidatorHelper()).ValidateMarkerField(CurrentLayout, LayoutField);
                }

                if (ElementType.Field == _selectedFieldType && !LayoutField.IsValueBoundAuto &&
                    (0.Equals(Convert.ToInt32(LayoutField.ValueBounds.Width)) ||
                    0.Equals(Convert.ToInt32(LayoutField.ValueBounds.Height))))
                {
                    ErrorMessage = LayoutField.ValueFieldMappingErrorMessage;
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }

        private void AssignSaveCaption()
        {
            SaveCaption = "Save";
            if (_selectedFieldType != null && _selectedFieldType == ElementType.Marker)
            {
                SaveCaption = "Define";
            }
        }

        private void resetOriginalObject()
        {
            if (IsMappedLayoutField(_originalLayoutField))
            {
                _originalLayoutField.IsSelected = false;
                _originalLayoutField.IsSelected = true;
                return;
            }

            SelectedFieldDef = _originalSelectedFieldDef;
            LayoutField newLayoutField = new LayoutField();
            copyLayoutField(_originalLayoutField, newLayoutField);
            LayoutField = newLayoutField;
            evaluateValueBoundAutoFlag();
            ErrorMessage = string.Empty;
            if (null != LayoutField)
            {
                LayoutField.ResetDirtyFlag();
            }
            MarkerEditableInEditMode = false;
        }

        private static bool IsMappedLayoutField(LayoutField layoutField)
        {
            return !IsGuid(layoutField.Id);
        }

        private static bool IsGuid(string value)
        {
            Guid parsedGuid;
            return Guid.TryParse(value, out parsedGuid);
        }

        private void evaluateValueBoundAutoFlag()
        {
            if (_originalLayoutField.Type == ElementType.SystemIdentifiedField || _originalLayoutField.Type == ElementType.SystemIdentifiedRegion)
            {
                LayoutField.IsValueBoundAuto = _selectedFieldType == ElementType.Field ? true : false;
            }
            else if (_originalLayoutField.Type != ElementType.Field)
            {
                LayoutField.IsValueBoundAuto = false;
            }
        }

        private void fillSelectedTabelFields()
        {
            _matchingFields.Clear();

            if (SelectedTable == null)
                return;

            LayoutTable selectedLayoutTable = (from layoutTable in CurrentLayout.LayoutTables.Tables where layoutTable.TableDef.Id == SelectedTable.Id select layoutTable).FirstOrDefault();
            ObservableCollection<LayoutField> layoutTableFields = new ObservableCollection<LayoutField>();
            if (selectedLayoutTable != null && selectedLayoutTable.Fields != null)
            {
                layoutTableFields = selectedLayoutTable.Fields;
            }

            for (int i = 0; i < SelectedTable.Columns.Count; i++)
            {
                LayoutField layoutField = (from q in layoutTableFields where q.FieldDef.Id == SelectedTable.Columns[i].Id && q.Id != _originalLayoutField.Id select q).FirstOrDefault();
                if (layoutField != null)
                    continue;
                _matchingFields.Add(SelectedTable.Columns[i]);
            }
        }

        private void fillEligibleElementTypes()
        {
            _eligibleElementTypes.Add(ElementType.Marker);
            _eligibleElementTypes.Add(ElementType.Field);
            _eligibleElementTypes.Add(ElementType.Table);
        }

        private void fillMatchingFields()
        {
            _matchingFields.Clear();
            _matchingTabels.Clear();
            if (_iqBot != null && _selectedFieldType != null)
            {
                if (_selectedFieldType == ElementType.Field)
                {
                    for (int i = 0; i < _iqBot.DataModel.Fields.Count; i++)
                    {
                        LayoutField layoutField = (from q in CurrentLayout.LayoutFields.Fields where q.FieldDef.Id == _iqBot.DataModel.Fields[i].Id && q.Id != _originalLayoutField.Id select q).FirstOrDefault();
                        if (layoutField != null)
                            continue;
                        _matchingFields.Add(_iqBot.DataModel.Fields[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < _iqBot.DataModel.Tables.Count; i++)
                    {
                        _matchingTabels.Add(_iqBot.DataModel.Tables[i]);
                    }
                }
            }
        }

        internal void RefreshPreviewImage(CroppedBitmap bitmap)
        {
            if (bitmap != null)
            {
                CroppedBitmap = bitmap;
            }
        }
    }
}