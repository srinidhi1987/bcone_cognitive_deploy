﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.Generic;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class TableViewModel : ViewModelBase
    {
        private LayoutTable _layoutTable;

        public string Name { get { return "Table Name Not Available!!!"; } }

        public IList<FieldViewModel> Columns { get; set; }

        public IList<RowViewModel> Rows { get; set; }

        public TableViewModel(LayoutTable layoutTable)
        {
            _layoutTable = layoutTable;
            Columns = new List<FieldViewModel>();
            Rows = new List<RowViewModel>();
        }

        public RowViewModel NewRow()
        {
            var row = new RowViewModel(new FieldViewModel[Columns.Count]);
            row.SetTable(this);
            return row;
        }
    }
}