﻿/***
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Automation.VisionBotEngine;
using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot.UI.ViewModels
{
    public class LayoutSettingsViewModel : ViewModelBase
    {
        private ImageProcessingConfig _imageProcessingConfig;
        private bool _isLayoutSettingVisible = false;
        public LayoutSettingsViewModel(ImageProcessingConfig imageProcessingConfig)
        {
            _imageProcessingConfig = imageProcessingConfig == null ? new ImageProcessingConfig() : imageProcessingConfig;

            Save = new RelayCommand(onSaveRequeted);
            Cancel = new RelayCommand(onCancelRequested);
            LayoutRescan = new RelayCommand(onLayoutRescanRequested);
        }

        public bool IsAutoRotationOn
        {
            get { return _imageProcessingConfig.EnableAutoRotation; }
            set
            {
                _imageProcessingConfig.EnableAutoRotation = value;
                OnPropertyChanged("IsAutoRotationOn");
            }
        }

        public bool IsPageOrientationOn
        {
            get { return _imageProcessingConfig.EnablePageOrientation; }
            set
            {
                _imageProcessingConfig.EnablePageOrientation = value;
                OnPropertyChanged("IsPageOrientationOn");
            }
        }

        public bool IsBorderFilterOn
        {
            get { return _imageProcessingConfig.EnableBorderFilter; }
            set
            {
                _imageProcessingConfig.EnableBorderFilter = value;
                OnPropertyChanged("IsBorderFilterOn");
            }
        }

        public bool IsBinarizationOn
        {
            get { return _imageProcessingConfig.EnableBinarization; }
            set
            {
                _imageProcessingConfig.EnableBinarization = value;
                OnPropertyChanged("IsBinarizationOn");
            }
        }

        public bool IsGrayscaleConversionOn
        {
            get { return _imageProcessingConfig.EnableGrayscaleConversion; }
            set
            {
                _imageProcessingConfig.EnableGrayscaleConversion = value;
                OnPropertyChanged("IsGrayscaleConversionOn");
            }
        }

        public bool IsNoiseFilterOn
        {
            get { return _imageProcessingConfig.EnableNoiseFilter; }
            set
            {
                _imageProcessingConfig.EnableNoiseFilter = value;
                OnPropertyChanged("IsNoiseFilterOn");
            }
        }

        public bool IsContrastStretchOn
        {
            get { return _imageProcessingConfig.EnableContrastStretch; }
            set
            {
                _imageProcessingConfig.EnableContrastStretch = value;
                OnPropertyChanged("IsContrastStretchOn");
            }
        }

        public IList<BinarizationTypes> BinarizationTypes
        {
            get
            {
                return new List<BinarizationTypes>(Enum.GetValues(typeof(BinarizationTypes)).Cast<BinarizationTypes>());
            }
        }

        public BinarizationTypes BinarizationType
        {
            get { return _imageProcessingConfig.BinarizationType; }
            set
            {
                _imageProcessingConfig.BinarizationType = value;
                OnPropertyChanged("BinarizationType");
            }
        }

        public int NoiseThreshold
        {
            get { return _imageProcessingConfig.NoiseThreshold; }
            set
            {
                _imageProcessingConfig.NoiseThreshold = value;
                OnPropertyChanged("NoiseThreshold");
            }
        }

        public int BinarizationThreshold
        {
            get { return _imageProcessingConfig.BinarizationThreshold; }
            set
            {
                _imageProcessingConfig.BinarizationThreshold = value;
                OnPropertyChanged("BinarizationThreshold");
            }
        }

        public bool IsLayoutSettingVisible
        {
            get { return _isLayoutSettingVisible; }
            set
            {
                _isLayoutSettingVisible = value;
                OnPropertyChanged("IsLayoutSettingVisible");
            }
        }

        public bool _isLayoutSettingExpanded = false;
        public bool IsLayoutSettingExpanded
        {
            get { return _isLayoutSettingExpanded; }
            set
            {
                _isLayoutSettingExpanded = value;
                OnPropertyChanged("IsLayoutSettingExpanded");
            }
        }
        public ICommand Rescan { get; set; }

        public ICommand Save { get; set; }
        public ICommand Cancel { get; set; }

        public ICommand LayoutRescan { get; set; }

        public Action<LayoutSettingsViewModel> SaveRequested;
        public Action<LayoutSettingsViewModel> LayoutRescanRequested;
        public Action<LayoutSettingsViewModel> CancelRequested;


        private void onSaveRequeted(object obj)
        {
            if (SaveRequested != null)
            {
                SaveRequested(this);
            }
        }

        private void onCancelRequested(object obj)
        {
            //throw new NotImplementedException();
        }

        private void onLayoutRescanRequested(object obj)
        {
            if (LayoutRescanRequested != null)
            {
                LayoutRescanRequested(this);
            }
        }
    }
}
