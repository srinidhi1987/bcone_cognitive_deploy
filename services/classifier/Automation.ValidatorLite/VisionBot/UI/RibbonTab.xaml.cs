﻿/**
* Copyright (c) 2015 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
using System.Windows;
using System.Windows.Controls;

using System;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for RibbonTab.xaml
    /// </summary>
    public partial class RibbonTab : UserControl
    {
        public int SelectedTabIndex { get; private set; }
        public event OnTabChangeHandler TabChanged;
        public event EventHandler SettingsClick;

        public RibbonTab()
        {
            InitializeComponent();
        }
        
        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            SelectedTabIndex = -1;
            var temp = this.DataContext as IqBotDesignerInfo;
            if (temp != null)
            {
                
                for (int index = 0; index < temp.RibbonTabItems.Count; index++)
                {
                    if (temp.RibbonTabItems[index].IsSelected)
                        SelectedTabIndex = index;
                }
            }
            if (TabChanged != null)
            {
                TabChanged(this, new RoutedEventArgs());
            }
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            if(SettingsClick != null)
            {
                SettingsClick(this, EventArgs.Empty);
            }
        }
    }
}
