﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Automation.CognitiveData.VisionBot.UI.ViewModels;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for RightPanelView.xaml
    /// </summary>
    public partial class LayoutRightPanelView : UserControl
    {
        public LayoutRightPanelView()
        {
            InitializeComponent();
        }

        private void grdMarker_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(e.NewValue is bool && ((bool)e.NewValue))
            {
                this.Dispatcher.BeginInvoke(new Action(() => setFocusToMarkerNameTextBox()));
            }
        }

        private void setFocusToMarkerNameTextBox()
        {            
            txtMarkerNameValue.Focusable = true;
            Keyboard.Focus(txtMarkerNameValue);
        }

        private void textBoxLabel_LostFocus(object sender, RoutedEventArgs e)
        {
            var dataContext = DataContext as UpdateLayoutFieldViewModel;
            if(dataContext != null)
            {
                dataContext.RequestAutoValueBound();
            }
        }
    }
}
