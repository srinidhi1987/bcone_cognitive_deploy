﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Automation.CognitiveData.VisionBot.UI
{
   public class NameValidatorHelper
    {
        public string ValidateField(string name)
        {
            return isValidInputForParameter(name, "Field");
        }

        public string ValidateTable(string name)
        {
            return isValidInputForParameter(name, "Table");
        }

        public string ValidateCollumn(string name)
        {
            return isValidInputForParameter(name, "Column");
        }

        public string ValidateMarker(string name)
        {
            return isValidInputForParameter(name, "Marker");
        }

        public string IsValidLayoutName(string name)
        {
            return isValidInputForParameter(name, Properties.Resources.LayoutLabel);
        }

      virtual  protected string isValidInputForParameter(string name, string parameterName)
        {
            var invalidFileRegex = new Regex(string.Format("[{0}]", Regex.Escape(@"<>:""/\|?*")));

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForBlankName, parameterName));
            }
            else if (invalidFileRegex.IsMatch(name))
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForInvalideCharacters, parameterName.ToLower()));
            }

            return string.Empty;
        }
    }

    public class BluePrintNameValidator:NameValidatorHelper
    {
        protected override string isValidInputForParameter(string name, string parameterName)
        {
            var validCharRegex = new Regex(@"^[a-zA-Z_][_a-zA-Z0-9]*$");
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForBlankName, parameterName));
            }
            else if (!validCharRegex.IsMatch(name))
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForInvalideBluePrintNameCharacters, parameterName.ToLower()));
            }

            return string.Empty;
        }
    }

    class DuplicateValidatorHelper
    {
        public string ValidateField(IqBot iqBot, FieldDef field)
        {
            return ValidatewithAll(iqBot, field.Name, field.Id);
        }

        public string ValidateTable(IqBot iqBot, TableDef table)
        {
            return ValidatewithAll(iqBot, table.Name, table.Id);
        }

        public string ValidateColumnField(IqBot iqBot, FieldDef field)
        {
            return ValidatewithAll(iqBot, field.Name, field.Id);
        }

        public void ValidateMarkerField(Layout layout, LayoutField marker)
        {
            LayoutField searchedField = (from markerElement in layout.Markers.Fields where string.Compare(((LayoutField)markerElement).MarkerName, marker.MarkerName, true) == 0 && string.Compare(((LayoutField)markerElement).Id, marker.Id, true) != 0 select markerElement).FirstOrDefault();


            if (searchedField != null)
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForSameItemName, "marker"));
            }
        }

        public string ValidatewithAll(IqBot iqBot, string name, string id)
        {
            FieldDef searchedField = (from fieldElement in iqBot.DataModel.Fields where string.Compare(((FieldDef)fieldElement).Name, name, true) == 0 && string.Compare(((FieldDef)fieldElement).Id, id, true) != 0 select fieldElement).FirstOrDefault();


            if (searchedField != null)
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForSameItemName, "field"));
            }

            TableDef searchedTable = (from tableElement in iqBot.DataModel.Tables where string.Compare(((TableDef)tableElement).Name, name, true) == 0 && string.Compare(((TableDef)tableElement).Id, id, true) != 0 select tableElement).FirstOrDefault();

            if (searchedTable != null)
            {
                throw new Exception(string.Format(Properties.Resources.ErrorMessageForSameItemName, "table"));
            }

            foreach (TableDef table in iqBot.DataModel.Tables)
            {
                FieldDef searchedFieldInTable = (from fieldElement in table.Columns where string.Compare(((FieldDef)fieldElement).Name, name, true) == 0 && string.Compare(((FieldDef)fieldElement).Id, id, true) != 0 select fieldElement).FirstOrDefault();
                if (searchedFieldInTable != null)
                {
                    throw new Exception(string.Format(Properties.Resources.ErrorMessageForSameItemName, "column"));
                }
            }

            return string.Empty;
        }
    }




}
