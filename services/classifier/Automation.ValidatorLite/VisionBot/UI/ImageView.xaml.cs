﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : UserControl
    {
        public const double RegionLeftOffset = -4;
        public const double RegionTopOffset = -4;
        public const double RegionRightOffset = 8;
        public const double RegionBottomOffset = 8;

        public Visibility ResizeThumbVisibility
        {
            get { return (Visibility)GetValue(ResizeThumbVisibilityProperty); }
            set { SetValue(ResizeThumbVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ResizeThumbVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ResizeThumbVisibilityProperty =
            DependencyProperty.Register("ResizeThumbVisibility", typeof(Visibility), typeof(ImageView), new PropertyMetadata(System.Windows.Visibility.Collapsed));

        public ImageView()
        {
            InitializeComponent();
        }

        private Point _startPoint;
        private bool _isMouseDown, _dragged;
        private CanvasImageViewModel _canvasImageViewModel;
        private SelectionType _originalSelectionType;

        private void UIElement_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _dragged = false;
            if (!_isMouseDown)
            {
                _isMouseDown = true;
                Canvas canvas = sender as Canvas;
                _startPoint = e.GetPosition(canvas);
                if (IsRegionCreationEnabled)
                {
                    Canvas.SetLeft(NewRegion, _startPoint.X);
                    Canvas.SetTop(NewRegion, _startPoint.Y);
                }
            }
        }

        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (IsUDRDeletionTriggerInstance)
            {
                IsUDRDeletionTriggerInstance = false;
                _isMouseDown = false;
                return;
            }

            //System.Diagnostics.Debug.WriteLine("#### UIElement_OnPreviewMouseLeftButtonUp " + e.Handled);
            if (_dragged)
            {
                onRegionCreated();
            }
            else if (_isMouseDown)
            {
                var canvasImageViewModel = DataContext as CanvasImageViewModel;

                var regions = getRegions(canvasImageViewModel);
                if (canvasImageViewModel != null && regions.Count > 0)
                {
                    IRegionDetector regionDetector = new SmallestRegionDetector(regions);
                    var detectionPoint = new Point(_startPoint.X / canvasImageViewModel.ZoomPercentage,
                        _startPoint.Y / canvasImageViewModel.ZoomPercentage);
                    IRegion region = regionDetector.DetectRegionFromPoint(detectionPoint);
                    if (region != null && region.SetSelection != null && region.SetSelection.CanExecute(null))
                    {
                        region.SetSelection.Execute(null);
                    }
                }
            }
            resetCursor();
            NewRegion.Visibility = Visibility.Collapsed;
            _isMouseDown = false;
        }

        private IList<IRegion> getRegions(CanvasImageViewModel canvasImageViewModel)
        {
            if (canvasImageViewModel == null)
            {
                return new List<IRegion>();
            }
            return canvasImageViewModel.ImageViewInfo.Fields.Select(f => f.Region).ToList();
        }

        private void UIElement_OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown && IsRegionCreationEnabled && e.LeftButton == MouseButtonState.Pressed && !(Mouse.Captured is ResizeThumb))
            {
                var currentPoint = e.GetPosition(sender as Canvas);
                if (isDragOn(currentPoint))
                {
                    setDragMode();
                    _dragged = true;
                    resizeRegionOnMouseMove(currentPoint);
                }
            }
            else if (_isMouseDown && !IsRegionCreationEnabled && (Mouse.Captured is ResizeThumb))
            {
                _isMouseDown = false;
            }
        }

        private void resizeRegionOnMouseMove(Point currentPoint)
        {
            NewRegion.Visibility = Visibility.Visible;
            NewRegion.Width = getMinValue(currentPoint.X, _startPoint.X, ResizeThumb.MinRegionWidth);
            NewRegion.Height = getMinValue(currentPoint.Y, _startPoint.Y, ResizeThumb.MinRegionHeight);
        }

        private bool isDragOn(Point currentPoint)
        {
            if (Math.Abs(currentPoint.X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(currentPoint.Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)
            {
                return true;
            }

            return false;
        }

        private double getMinValue(double newPoint, double startingPoint, double minSize)
        {
            if (newPoint < startingPoint)
            {
                return minSize;
            }

            return newPoint - startingPoint;
        }

        private void onRegionCreated()
        {
            if (RegionCreatedCommand != null)
            {
                var rect = getBounds();
                if (RegionCreatedCommand.CanExecute(rect))
                {
                    RegionCreatedCommand.Execute(rect);
                }
            }
        }

        private Rect getBounds()
        {
            var rect = new Rect
            {
                X = getNormalValue(Canvas.GetLeft(NewRegion), _canvasImageViewModel.ZoomPercentage, RegionLeftOffset),
                Y = getNormalValue(Canvas.GetTop(NewRegion), _canvasImageViewModel.ZoomPercentage, RegionTopOffset),
                Width = getNormalValue(NewRegion.ActualWidth, _canvasImageViewModel.ZoomPercentage, RegionRightOffset),
                Height = getNormalValue(NewRegion.ActualHeight, _canvasImageViewModel.ZoomPercentage, RegionBottomOffset)
            };
            return rect;
        }

        public ICommand RegionCreatedCommand
        {
            get { return (ICommand)GetValue(RegionCreatedCommandProperty); }
            set { SetValue(RegionCreatedCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RegionCreatedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegionCreatedCommandProperty =
            DependencyProperty.Register("RegionCreatedCommand", typeof(ICommand), typeof(ImageView), null);

        public ICommand RegionUpdatedCommand
        {
            get { return (ICommand)GetValue(RegionUpdatedCommandProperty); }
            set { SetValue(RegionUpdatedCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RegionUpdatedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegionUpdatedCommandProperty =
            DependencyProperty.Register("RegionUpdatedCommand", typeof(ICommand), typeof(ImageView), null);

        public bool IsRegionCreationEnabled
        {
            get { return (bool)GetValue(IsRegionCreationEnabledProperty); }
            set { SetValue(IsRegionCreationEnabledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRegionCreationEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRegionCreationEnabledProperty =
            DependencyProperty.Register("IsRegionCreationEnabled", typeof(bool), typeof(ImageView), new PropertyMetadata(false));

        private void ResizeThumb_OnCommitted(object sender, ResizeEventArgs e)
        {
            if (RegionUpdatedCommand != null)
            {
                var rect = new Rect
                {
                    X = getNormalValue(e.NewRect.X + 1, _canvasImageViewModel.ZoomPercentage, RegionLeftOffset),
                    Y = getNormalValue(e.NewRect.Y + 1, _canvasImageViewModel.ZoomPercentage, RegionTopOffset),
                    Width = getNormalValue(e.NewRect.Width - 1, _canvasImageViewModel.ZoomPercentage, RegionRightOffset),
                    Height = getNormalValue(e.NewRect.Height - 1, _canvasImageViewModel.ZoomPercentage, RegionBottomOffset)
                };
                if (RegionUpdatedCommand.CanExecute(rect))
                {
                    RegionUpdatedCommand.Execute(rect);
                }
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var regionViewModel = button.DataContext as RegionViewModel;
                if (UserDefinedRegionDeleteCommand != null && UserDefinedRegionDeleteCommand.CanExecute(regionViewModel))
                {
                    UserDefinedRegionDeleteCommand.Execute(regionViewModel);
                }
            }
        }

        public ICommand UserDefinedRegionDeleteCommand
        {
            get { return (ICommand)GetValue(UserDefinedRegionDeleteCommandProperty); }
            set { SetValue(UserDefinedRegionDeleteCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserDefinedRegionDeleteCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserDefinedRegionDeleteCommandProperty =
            DependencyProperty.Register("UserDefinedRegionDeleteCommand", typeof(ICommand), typeof(ImageView), null);

        private void ImageView_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _canvasImageViewModel = e.NewValue as CanvasImageViewModel;
            if (_canvasImageViewModel != null)
            {
                _canvasImageViewModel.ZoomPercentageChanged += canvasImageViewModel_ZoomPercentageChanged;
                _originalSelectionType = _canvasImageViewModel.ImageViewInfo.SelectionType;
            }
        }

        private void canvasImageViewModel_ZoomPercentageChanged(object sender, double e)
        {
            if (HighlighterBox.Visibility == Visibility.Visible)
            {
                var left = getScaledValue(Canvas.GetLeft(HighlighterBox), e, RegionLeftOffset, 1);
                var top = getScaledValue(Canvas.GetTop(HighlighterBox), e, RegionTopOffset, 1);
                Canvas.SetLeft(HighlighterBox, left);
                Canvas.SetTop(HighlighterBox, top);
                HighlighterBox.Width = getScaledValue(HighlighterBox.Width, e, RegionRightOffset, -1);
                HighlighterBox.Height = getScaledValue(HighlighterBox.Height, e, RegionBottomOffset, -1);
            }
        }

        private double getScaledValue(double scaledValue, double oldZoomPercentage, double offset, double padding)
        {
            scaledValue += padding;
            scaledValue = getNormalValue(scaledValue, oldZoomPercentage, offset);
            return ((scaledValue + offset) * _canvasImageViewModel.ZoomPercentage) - padding;
        }

        private double getNormalValue(double scaledValue, double oldZoomPercentage, double offset)
        {
            scaledValue /= oldZoomPercentage;
            return scaledValue - offset;
        }

        private void setDragMode()
        {
            if (_canvasImageViewModel != null)
            {
                _canvasImageViewModel.ImageViewInfo.SelectionType = SelectionType.Drag;
            }
        }

        private void resetCursor()
        {
            if (_canvasImageViewModel != null)
            {
                _canvasImageViewModel.ImageViewInfo.SelectionType = _originalSelectionType;
            }
        }

        public SelectionType ImageViewCursor
        {
            get { return (SelectionType)GetValue(ImageViewCursorProperty); }
            set { SetValue(ImageViewCursorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageViewCursor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageViewCursorProperty =
            DependencyProperty.Register("ImageViewCursor", typeof(SelectionType), typeof(ImageView), new PropertyMetadata(SelectionType.Default, onPropertyChanged));

        private void UDRResizeThumb_OnCommitted(object sender, ResizeEventArgs e)
        {
            if (RegionCreatedCommand != null)
            {
                _canvasImageViewModel.InspectionViewModel.MakeFieldValueBoundSelectionOn();
                //var rect = e.NewRect;
                var rect = new Rect
                {
                    X = getNormalValue(e.NewRect.X + 1, _canvasImageViewModel.ZoomPercentage, RegionLeftOffset),
                    Y = getNormalValue(e.NewRect.Y + 1, _canvasImageViewModel.ZoomPercentage, RegionTopOffset),
                    Width = getNormalValue(e.NewRect.Width - 1, _canvasImageViewModel.ZoomPercentage, RegionRightOffset),
                    Height = getNormalValue(e.NewRect.Height - 1, _canvasImageViewModel.ZoomPercentage, RegionBottomOffset)
                };

                if (RegionCreatedCommand.CanExecute(rect))
                {
                    RegionCreatedCommand.Execute(rect);
                }
                resetCursor();
            }
        }

        private void btnValueUDRDelete_OnClick(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var layoutField = button.Tag as LayoutField;
                if (layoutField != null)
                {
                    var regionViewModel = new RegionViewModel(layoutField);
                    if (UserDefinedRegionDeleteCommand != null && UserDefinedRegionDeleteCommand.CanExecute(regionViewModel))
                    {
                        UserDefinedRegionDeleteCommand.Execute(regionViewModel);
                        resetCursor();
                    }
                }
            }
        }

        // TODO: IMPROVE SOLUTION:
        // there is an issue that if delete "X" is on some SIR region, then clicking on "X", apart from raising the delete UDR event,
        // it also triggers the click event of the underlying SIR.
        // currently it is solved through flagging (used in event "UIElement_OnPreviewMouseLeftButtonUp" above).
        // try to solve this using a better and direct method then through flag "IsUDRDeletionTriggerInstance".
        // also remove non required events which are just kept for reference for now as to what all didn't work :)

        private void btnValueUDRDelete_Click(object sender, RoutedEventArgs e)
        {
            //(this.DataContext as CanvasImageViewModel).ImageViewInfo.UserDefinedRegionDeleteCommand.Execute(null);
        }

        private void btnValueUDRDelete_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //(this.DataContext as CanvasImageViewModel).ImageViewInfo.UserDefinedRegionDeleteCommand.Execute(null);
            //e.Handled = true;
        }

        private bool IsUDRDeletionTriggerInstance = false;

        private void btnValueUDRDelete_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("#### btnValueUDRDelete_PreviewMouseLeftButtonDown");
            (this.DataContext as CanvasImageViewModel).ImageViewInfo.UserDefinedRegionDeleteCommand.Execute(null);
            e.Handled = true;
            IsUDRDeletionTriggerInstance = true;
        }

        private static void onPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var imageView = d as ImageView;
            if (imageView != null && e.NewValue is SelectionType)
            {
                var selectionType = ((SelectionType)e.NewValue);
                if (selectionType == SelectionType.Drag)
                {
                    imageView.Cursor = Cursors.Cross;
                }
                else if (selectionType == SelectionType.Hover)
                {
                    imageView.Cursor = Cursors.Cross;
                }
                else
                {
                    imageView.Cursor = Cursors.Arrow;
                }
            }
        }
    }
}