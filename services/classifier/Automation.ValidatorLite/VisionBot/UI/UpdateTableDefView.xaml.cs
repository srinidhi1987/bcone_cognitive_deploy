﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for UpdateTableDefView.xaml
    /// </summary>
    public partial class UpdateTableDefView : UserControl
    {
        public UpdateTableDefView()
        {
            InitializeComponent();
        }

        private void UserControl_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            UpdateTableDef dataContext = e.NewValue as UpdateTableDef;
            if (dataContext != null && dataContext.TableDef != null && string.IsNullOrWhiteSpace(dataContext.TableDef.Name))
            {
                txtTableNameValue.Focus();
            }
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtTableNameValue.Text))
            {
                txtTableNameValue.Focus();
            }
        }
    }
}
