/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using Automation.CognitiveData.VisionBot.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class RightPanelDataTemplateSelector : DataTemplateSelector
    {
        internal const string UpdateFieldDefinitionTemplateKey = "UpdateFieldDefTemplate";
        internal const string UpdateTableDefinitionTemplateKey = "UpdateTableDefTemplate";
        internal const string UpdateLayoutFieldTemplateKey = "UpdateLayoutFieldTemplate";
        internal const string UpdateLayoutTableTemplateKey = "UpdateLayoutTableTemplate";
        internal const string EditMarkerViewTemplateKey = "EditMarkerViewTemplate";
        internal const string DefaultTemplateKey = "DefaultTemplate";
        internal const string TestInspectionTemplateKey = "TestInspectionTemplate";

        internal const string TestDocSummaryTemplateKey = "TestDocSummaryTemplate";
        internal const string LayoutSettingsTemplateKey = "LayoutSettingsTemplate";

        private IDictionary<Type, string> _resourceDictionary;

        public RightPanelDataTemplateSelector()
        {
            _resourceDictionary = new Dictionary<Type, string>
            {
                {typeof (UpdateFieldDef), UpdateFieldDefinitionTemplateKey},
                {typeof (UpdateTableDef), UpdateTableDefinitionTemplateKey},                
                {typeof (UpdateLayoutTable), UpdateLayoutTableTemplateKey},  
                {typeof (TestInspectionViewModel), TestInspectionTemplateKey},
                {typeof (TestDocSummaryViewModel), TestDocSummaryTemplateKey},
                {typeof (LayoutSettingsViewModel), LayoutSettingsTemplateKey}
            };
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ContentPresenter contentPresenter = container as ContentPresenter;
            if (item != null && contentPresenter != null)
            {
                Type itemType = item.GetType();
                if (item is UpdateLayoutFieldViewModel)
                {
                    return selectTemplateFromViewModelState(item as UpdateLayoutFieldViewModel, contentPresenter);
                }
                
                if (_resourceDictionary.ContainsKey(itemType))
                {
                    return contentPresenter.FindResource(_resourceDictionary[itemType]) as DataTemplate;
                }

                return contentPresenter.FindResource(DefaultTemplateKey) as DataTemplate;
            }

            return null;
        }

        private DataTemplate selectTemplateFromViewModelState(UpdateLayoutFieldViewModel item, ContentPresenter contentPresenter)
        {
            if(item != null && item.GivenElementType == ElementType.Marker && !item.IsDefEditable)
            {
                return contentPresenter.FindResource(EditMarkerViewTemplateKey) as DataTemplate;
            }

            return contentPresenter.FindResource(UpdateLayoutFieldTemplateKey) as DataTemplate;
        }
    }
}