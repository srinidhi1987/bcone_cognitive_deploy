﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Threading;

namespace Automation.CognitiveData.VisionBot.UI
{
    internal class RegionBoxAutomationPeer : FrameworkElementAutomationPeer, IInvokeProvider
    {
        public RegionBoxAutomationPeer(RegionBox regionBox)
            : base(regionBox)
        {
        }

        public void Invoke()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() =>
            {
                ((RegionBox)Owner).AutomationRegionBoxClick();
            }), null);
        }

        protected override string GetClassNameCore()
        {
            return "RegionBox";
        }

        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            return AutomationControlType.Text;
        }

        protected override string GetNameCore()
        {
            return ((RegionBox)Owner).AutomationRegionBoxName();
        }
    }
}
