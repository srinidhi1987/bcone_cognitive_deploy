﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.Extensions;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    internal class StructureVerifier : IStructureVerifier
    {
        public IWarning Verify(IqBot iqBot, DataTable fields, DataSet tables)
        {
            try
            {
                IWarning warning = GenericWarning.NoWarning;

                warning = verifyDataTableStructure(fields, iqBot?.DataModel?.Fields);
                warning = warning == GenericWarning.NoWarning ? verifyTableOrderInDataSet(tables, iqBot?.DataModel?.Tables) : warning;

                return warning;
            }
            catch (Exception ex)
            {
                ex.Log(nameof(StructureVerifier), nameof(Verify));
                throw;
            }
        }

        private IWarning verifyDataTableStructure(DataTable dataTable, IList<FieldDef> expectedOrder)
        {
            IWarning warning = GenericWarning.NoWarning;
            IList<string> fieldList = dataTable.GetOrderedColumnNames();

            IList<string> idList = expectedOrder.Select(q => q.Id).ToList();
            int expectedCount = expectedOrder != null ? expectedOrder.Count : 0;
            if (fieldList.Count != expectedCount)
            {
                return new GenericWarning(true, string.Format(Properties.Resources.BlueprintBenchmarkStructureMismatchWarningMessage, Properties.Resources.BlueprintTabLabel), WarningType.Blueprint);
            }
            else
            {
                for (int i = 0; i < fieldList.Count; i++)
                {
                    if (!idList.Contains(fieldList[i]))
                    {
                        return new GenericWarning(true, string.Format(Properties.Resources.BlueprintBenchmarkStructureMismatchWarningMessage, Properties.Resources.BlueprintTabLabel), WarningType.Blueprint);
                    }
                }
            }

            return warning;
        }

        private IWarning verifyTableOrderInDataSet(DataSet dataSet, IList<TableDef> expectedOrder)
        {
            IWarning warning = GenericWarning.NoWarning;
            IList<string> tableList = dataSet.GetOrderedTableNames();

            IList<string> tableIdList = expectedOrder.Select(q => q.Id).ToList();

            int expectedCount = expectedOrder != null ? expectedOrder.Count : 0;
            if (tableList.Count != expectedCount)
            {
                return new GenericWarning(true, string.Format(Properties.Resources.BlueprintBenchmarkStructureMismatchWarningMessage, Properties.Resources.BlueprintTabLabel), WarningType.Blueprint);
            }
            else
            {
                for (int i = 0; i < tableList.Count; i++)
                {
                    if (!tableIdList.Contains(tableList[i]))
                    {
                        return new GenericWarning(true, string.Format(Properties.Resources.BlueprintBenchmarkStructureMismatchWarningMessage, Properties.Resources.BlueprintTabLabel), WarningType.Blueprint);
                    }

                    DataTable dataTable = dataSet.Tables[tableList[i]];
                    warning = verifyDataTableStructure(dataTable, expectedOrder.FirstOrDefault(table => table.Id == dataTable.TableName)?.Columns);
                    if (warning != GenericWarning.NoWarning)
                    {
                        return warning;
                    }
                }
            }

            return warning;
        }
    }
}