﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.Behaviors;
    using Automation.CognitiveData.VisionBot.UI.EventArguments;
    using Automation.CognitiveData.VisionBot.UI.Extensions;
    using Automation.VisionBotEngine;
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for DataModelView.xaml
    /// </summary>
    //public partial class DataModelView : UserControl
    //{
    //    private DataGridDragDropBehavior<FieldDef> _fieldGridDragDropBehavior = null;
    //    private DataGridDragDropBehavior<TableDef> _tableGridDragDropBehavior = null;
    //    private Point? _startPoint;
    //    private bool _userConfirmedReOrderWarning = false;

    //    public DataModelView()
    //    {
    //        InitializeComponent();
    //        attachDragDropBehaviors();
    //    }

    //    private void attachDragDropBehaviors()
    //    {
    //        _fieldGridDragDropBehavior = new DataGridDragDropBehavior<FieldDef>();
    //        _fieldGridDragDropBehavior.Attach(fieldsListView);
    //        _fieldGridDragDropBehavior.Drop += fieldGridDragDropBehavior_Drop;

    //        _tableGridDragDropBehavior = new DataGridDragDropBehavior<TableDef>();
    //        _tableGridDragDropBehavior.Attach(tablesListView);
    //        _tableGridDragDropBehavior.Drop += tableGridDragDropBehavior_Drop;
    //    }

    //    private void tableGridDragDropBehavior_Drop(object sender, DropArgs dropArgs)
    //    {
    //        if (this.DataContext != null && this.DataContext is DataModelVM)
    //        {
    //            if (canDrop(dropArgs))
    //            {
    //                ((DataModelVM)this.DataContext).MoveTable(tablesListView.SelectedItem as TableDef, dropArgs.TargetIndex);
    //            }
    //        }
    //    }

    //    private void fieldGridDragDropBehavior_Drop(object sender, DropArgs dropArgs)
    //    {
    //        if (this.DataContext != null && this.DataContext is DataModelVM)
    //        {
    //            if (canDrop(dropArgs))
    //            {
    //                ((DataModelVM)this.DataContext).MoveField(fieldsListView.SelectedItem as FieldDef, dropArgs.TargetIndex);
    //            }
    //        }
    //    }

    //    private bool canDrop(DropArgs dropArgs)
    //    {
    //        return confirmReOrder() && dropArgs?.DragEventArgs?.Data != null && dropArgs.DragEventArgs.Data.GetDataPresent(typeof(DragData));
    //    }

    //    private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
    //    {
    //        try
    //        {
    //            if (this.DataContext != null && this.DataContext is DataModelVM)
    //            {
    //                TableDef table = (sender as FrameworkElement)?.DataContext as TableDef;
    //                ((DataModelVM)this.DataContext).SelectTableInLeftPane(table);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            ex.Log(nameof(DataModelView), nameof(TextBlock_MouseUp));
    //        }
    //    }

    //    private void Image_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    //    {
    //        _startPoint = e.GetPosition(null);
    //        setCursorForImage(sender as Image, Cursors.SizeNS);
    //    }

    //    private void Image_PreviewMouseMove(object sender, MouseEventArgs e)
    //    {
    //        if (_startPoint.HasValue && e.LeftButton == MouseButtonState.Pressed)
    //        {
    //            Point position = e.GetPosition(null);

    //            if (Math.Abs(position.X - _startPoint.Value.X) > 2 ||
    //                Math.Abs(position.Y - _startPoint.Value.Y) > 2)
    //            {
    //                _startPoint = null;
    //                DataGrid parentDataGrid = (sender as DependencyObject) != null ? (sender as DependencyObject).GetParentOfType<DataGrid>() : null;
    //                if (parentDataGrid == fieldsListView)
    //                {
    //                    FieldDef fieldToDrag = fieldsListView.SelectedItem as FieldDef;
    //                    if (fieldToDrag != null)
    //                    {
    //                        DragData dragData = new DragData { Id = fieldToDrag.Id, Name = fieldToDrag.Name };
    //                        _fieldGridDragDropBehavior.StartDrag(dragData);
    //                    }
    //                }
    //                else if (parentDataGrid == tablesListView)
    //                {
    //                    TableDef tableToDrag = tablesListView.SelectedItem as TableDef;
    //                    if (tableToDrag != null)
    //                    {
    //                        DragData dragData = new DragData { Id = tableToDrag.Id, Name = tableToDrag.Name };
    //                        _tableGridDragDropBehavior.StartDrag(dragData);
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    private bool confirmReOrder()
    //    {
    //        if (!_userConfirmedReOrderWarning)
    //        {
    //            var errorMessageDialog = new ErrorMessageDialog
    //            {
    //                WindowStartupLocation = WindowStartupLocation.CenterScreen,
    //                DataContext = new ErrorModel
    //                {
    //                    ErrorMessage = Properties.Resources.ReorderConfirmationWarning,
    //                    Buttons = new System.Collections.ObjectModel.ObservableCollection<ButtonModel>
    //                {
    //                    new ButtonModel
    //                    {
    //                        Caption = "Yes",
    //                        IsDefault = true,
    //                        Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 174, 223))
    //                    },
    //                    new ButtonModel
    //                    {
    //                        Caption = "No",
    //                        IsCancel = true,
    //                        Foreground = new SolidColorBrush(Color.FromArgb(255, 163, 163, 163))
    //                    }
    //                },
    //                }
    //            };

    //            errorMessageDialog.Owner = Window.GetWindow(this);
    //            _userConfirmedReOrderWarning = Convert.ToBoolean(errorMessageDialog.ShowDialog());
    //        }

    //        return _userConfirmedReOrderWarning;
    //    }

    //    private void Image_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    //    {
    //        setCursorForImage(sender as Image, Cursors.Hand);
    //    }

    //    private void Image_MouseLeave(object sender, MouseEventArgs e)
    //    {
    //        setCursorForImage(sender as Image, Cursors.Hand);
    //    }

    //    private void setCursorForImage(Image image, Cursor cursor)
    //    {
    //        if (image != null)
    //        {
    //            image.Cursor = cursor;
    //        }
    //    }
    //}

    public class DragData
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}