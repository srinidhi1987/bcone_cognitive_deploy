﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.Model;
    using System;
    using System.Collections.Generic;
    using VisionBotEngine;

    internal class TabHelper
    {
        public int GetDecidedTabViewIndex(IqBot iqbotViewModel, List<LayoutTestSet> testSets)
        {
            int initialTabIndex = VisionBotDesigner.BlueprintTabIndex;
            try
            {
                if (doesBluePrintContainAnyField(iqbotViewModel))
                {
                    if (doesTestSetsCaontainAnyDocument(testSets))
                    {
                        initialTabIndex = VisionBotDesigner.TestTabIndex;
                    }
                    else
                    {
                        if (!doesLayoutsContainAnyMappedField(iqbotViewModel))
                        {
                            initialTabIndex = VisionBotDesigner.LayoutTabIndex;
                        }
                        else
                        {
                            initialTabIndex = VisionBotDesigner.TestTabIndex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(TabHelper), nameof(GetDecidedTabViewIndex));
            }

            return initialTabIndex;
        }

        private bool doesBluePrintContainAnyField(IqBot iqbotViewModel)
        {
            if (doesBluePrintFieldsModelContainsAnyField(iqbotViewModel))
            {
                return true;
            }

            return doesBluePrintTablesModelContainsAnyColumn(iqbotViewModel);
        }

        private bool doesBluePrintFieldsModelContainsAnyField(IqBot iqbotViewModel)
        {
            return iqbotViewModel?.DataModel?.Fields?.Count > 0;
        }

        private bool doesBluePrintTablesModelContainsAnyColumn(IqBot iqbotViewModel)
        {
            if (iqbotViewModel?.DataModel?.Tables != null)
            {
                foreach (TableDef table in iqbotViewModel.DataModel.Tables)
                {
                    if (table.Columns.Count > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool doesLayoutsContainAnyMappedField(IqBot iqbotViewModel)
        {
            foreach (Layout layout in iqbotViewModel.Layouts)
            {
                if (doesLayoutContainAnyMappedField(layout))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool doesLayoutContainAnyMappedField(Layout layout)
        {
            if (layout.LayoutFields?.Fields?.Count > 0)
            {
                return true;
            }

            if (layout?.LayoutTables?.Tables != null)
            {
                foreach (LayoutTable layoutTable in layout.LayoutTables.Tables)
                {
                    if (layoutTable.Fields.Count > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool doesTestSetsCaontainAnyDocument(List<LayoutTestSet> testSets)
        {
            for (int i = 0; i < testSets.Count; i++)
            {
                if (testSets[i]?.DocSetItems?.Count > 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}