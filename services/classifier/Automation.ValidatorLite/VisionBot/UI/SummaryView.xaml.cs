﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for SummaryView.xaml
    /// </summary>
    public partial class SummaryView : UserControl
    {
        public SummaryView()
        {
            InitializeComponent();
        }

        void SummaryView_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            //Here we have list boxes inside scroll viewer. We want to display scroll bars only when width is less than 670.
            //If width is greater than 670 we do not want to display scroll bars.
            if(e.WidthChanged)
            {
                if (e.NewSize.Width <= (summaryGridPanel.MinWidth + summaryGridPanel.Margin.Left + summaryGridPanel.Margin.Right))
                {
                    summaryAndDetailScrollControl.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                    detailContentPresenterControl.Width = 600 - detailContentPresenterControl.Margin.Left - detailContentPresenterControl.Margin.Right;
                }
                else
                {
                    summaryAndDetailScrollControl.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    detailContentPresenterControl.Width = double.NaN;
                }             
            }
        }
    }
}
