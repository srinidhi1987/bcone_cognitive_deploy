﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

using Automation.CognitiveData.VisionBot.UI.Model;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for PreviewDataTable.xaml
    /// </summary>
    public partial class ReadOnlyTableBox : UserControl
    {
        public ReadOnlyTableBox()
        {
            InitializeComponent();
            dataGridManualData.DataContextChanged += DataGridManualData_DataContextChanged;
        }

        private void DataGridManualData_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            DataTable oldDataTable = (e.OldValue as PreviewTableModel)?.DataTable;
            if (oldDataTable != null)
            {
                oldDataTable.Columns.CollectionChanged -= Columns_CollectionChanged;
            }

            DataTable newDataTable = (e.NewValue as PreviewTableModel)?.DataTable;
            if (newDataTable != null)
            {
                newDataTable.Columns.CollectionChanged -= Columns_CollectionChanged;
                newDataTable.Columns.CollectionChanged += Columns_CollectionChanged;
            }
        }

        private void Columns_CollectionChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e)
        {
            DataTable dataTable = (dataGridManualData.DataContext as PreviewTableModel)?.DataTable;

            if (dataTable != null && dataGridManualData?.Columns != null)
            {
                int index = 0;
                foreach (DataColumn column in dataTable.Columns)
                {
                    DataGridColumn dgColumn = dataGridManualData.Columns.SingleOrDefault(element => element.Header.ToString() == column.Caption);

                    if (dgColumn != null)
                    {
                        dgColumn.DisplayIndex = index;
                    }

                    index++;
                }
            }
        }

        private void dataGridManualData_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid != null && !string.IsNullOrWhiteSpace(e.PropertyName))
            {
                PreviewTableModel previewTableModel = dataGrid.DataContext as PreviewTableModel;
                e.Column = new DataGridTemplateColumn
                {
                    Header = getCaption(previewTableModel, e.PropertyName),
                    CellTemplate = CreateSimpleCellTemplate(e.PropertyName)
                };
            }
        }

        private DataTemplate CreateSimpleCellTemplate(string propertyName)
        {
            ParserContext context = new ParserContext();
            context.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            context.XmlnsDictionary.Add("self", "clr-namespace:Automation.CognitiveData.VisionBot.UI;assembly=Automation.CongnitiveData");

            string _dataTemplateString = @"<DataTemplate>
                                                <self:ReadOnlyTableCell DataContext=""{Binding " + propertyName + @"}""/>
                                           </DataTemplate>";


            DataTemplate template = null;
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(_dataTemplateString)))
            {
                template = (DataTemplate)XamlReader.Load(memoryStream, context);
            }

            return template;

        }

        private string getCaption(PreviewTableModel tableModel, string fieldDefID)
        {
            string caption = string.Empty;
            if (tableModel != null && tableModel.DataTable != null && tableModel.DataTable.Columns != null && !string.IsNullOrWhiteSpace(fieldDefID))
            {
                for (int i = 0; i < tableModel.DataTable.Columns.Count; i++)
                {
                    if (tableModel.DataTable.Columns[i] != null && fieldDefID.Equals(tableModel.DataTable.Columns[i].ColumnName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        caption = tableModel.DataTable.Columns[i].Caption;
                        break;
                    }
                }
            }

            return caption;
        }

        private void dataGridManualData_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = ((DependencyObject)sender).FindAncestor<ScrollViewer>("PreviewScrollViewer");

            if (scrollViewer != null)
            {
                scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta);
            }
        }
    }
}
