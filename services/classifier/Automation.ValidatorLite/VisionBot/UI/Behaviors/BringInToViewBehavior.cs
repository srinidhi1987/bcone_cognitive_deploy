﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI.Behaviors
{
    public class BringInToViewBehavior : DependencyObject
    {
        public static DependencyProperty BringToViewProperty =
            DependencyProperty.RegisterAttached("BringInToView",
                                                typeof(bool),
                                                typeof(BringInToViewBehavior),
                                                new PropertyMetadata(false, new PropertyChangedCallback(OnPropertyChangedCallback)));

        public static void SetBringInToView(DependencyObject obj, bool value)
        {
            obj.SetValue(BringToViewProperty, value);
        }

        public static bool GetBringInToView(DependencyObject obj)
        {
            return (bool)obj.GetValue(BringToViewProperty);
        }

        private static void OnPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is bool)
            {
                bool newValue = (bool)(e.NewValue);
                if (newValue)
                {
                    FrameworkElement element = d as FrameworkElement;
                    if (element != null)
                    {
                        if (element.IsLoaded)
                        {
                            bringRectangleToView(element);
                        }
                        else
                        {
                            element.Loaded += Rectangle_Loaded;
                        }
                    }
                }
            }
        }

        private static void Rectangle_Loaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                element.Loaded -= Rectangle_Loaded;
                if (BringInToViewBehavior.GetBringInToView(element))
                {
                    bringRectangleToView(element);
                }
            }
        }

        private static void bringRectangleToView(FrameworkElement element)
        {
            Rect r = new Rect(-100, -100, element.Width + 200, element.Height + 200);
            element?.BringIntoView(r);
        }
    }
}