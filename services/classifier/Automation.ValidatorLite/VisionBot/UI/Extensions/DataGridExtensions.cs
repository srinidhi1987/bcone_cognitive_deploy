﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.Extensions
{
    internal static class DataGridExtensions
    {
        public static DataGridRow GetDataGridRow(this DataGrid dataGrid, int index)
        {
            DataGridRow row = null;
            if (dataGrid != null && dataGrid.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                row = dataGrid.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
            }

            return row;
        }

        public static DataGridRow GetDataGridRow(this DataGrid dataGrid, object dataItem)
        {
            DataGridRow row = null;
            if (dataItem != null && dataGrid != null && dataGrid.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                row = dataGrid.ItemContainerGenerator.ContainerFromItem(dataItem) as DataGridRow;
            }
            return row;
        }
        
        public static DataGridRow GetDataGridRow(this DataGrid dataGrid, Point point)
        {
            DataGridRow row = null;
            HitTestResult hitTestResult = VisualTreeHelper.HitTest(dataGrid, point);
            if (hitTestResult?.VisualHit != null)
            {
                row = hitTestResult.VisualHit.GetParentOfType<DataGridRow>();
            }

            return row;
        }

        public static int? GetDataGridRowIndex(this DataGrid dataGrid, DataGridRow row)
        {
            int? index = null;
            if (row != null && dataGrid != null && dataGrid.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                index = dataGrid.ItemContainerGenerator.IndexFromContainer(row);
            }
            return index;
        }
    }
}
