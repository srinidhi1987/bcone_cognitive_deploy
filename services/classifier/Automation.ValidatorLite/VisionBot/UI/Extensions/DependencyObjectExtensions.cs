﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Windows;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.Extensions
{
    internal static class DependencyObjectExtensions
    {
        public static T GetParentOfType<T>(this DependencyObject element) where T : DependencyObject
        {
            DependencyObject parent = null;
            if (element != null)
            {
                parent = VisualTreeHelper.GetParent(element);
                if (parent != null && !(parent is T))
                {
                    parent = parent.GetParentOfType<T>();
                }

            }

            return parent as T;
        }
    }
}
