﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */
using System.Collections.Generic;
using System.Data;

using Automation.VisionBotEngine;

namespace Automation.CognitiveData.VisionBot.UI.Extensions
{
    internal static class DataTableExtensions
    {
        public static void SetColumnsOrder(this DataTable table, IList<string> columnNames)
        {
            if (table != null && columnNames != null && table?.Columns != null)
            {
                int columnIndex = 0;
                foreach (var columnName in columnNames)
                {
                    if (!string.IsNullOrWhiteSpace(columnName) && table.Columns.Contains(columnName) && columnIndex < table.Columns.Count)
                    {
                        table.Columns[columnName].SetOrdinal(columnIndex);
                        columnIndex++;
                    }
                    else
                    {
                        VBotLogger.Error(() => $"[SetColumnsOrder] ColumnName: {columnName} not found in data table.");
                    }
                }
            }
            else
            {
                VBotLogger.Error(() => $"[SetColumnsOrder] table or columnNames or table.Columns are null.");
            }
        }

        public static IList<string> GetOrderedColumnNames(this DataTable table)
        {
            IList<string> orderedColumnNames = new List<string>();
            if(table?.Columns != null)
            {
                for(int i=0; i < table.Columns.Count; i++)
                {
                    orderedColumnNames.Add(table.Columns[i].ColumnName);
                }
            }
            return orderedColumnNames;
        }
    }
}
