﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    public enum UpdateMode
    {
        Add,
        Edit
    }

    public enum RightPanelMode
    {
        Field,
        LayoutField,
        Table
    }

    public enum SelectionType
    {
        Default,
        Hover,
        Drag
    }

    public enum TestItemTypes
    {
        None,
        TestSet,
        Layout,
        TesSetItem,
        Field
    }

    public enum DialogResult
    {
        Yes,
        No,
        Cancel
    }
}
