﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/
using System;
using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI.EventArguments
{
    internal class DropArgs : EventArgs
    {
        private DragEventArgs _dragEventArgs;
        private int _targetIndex;

        public DragEventArgs DragEventArgs
        {
            get
            {
                return _dragEventArgs;
            }
        }

        public int TargetIndex
        {
            get
            {
                return _targetIndex;
            }
        }

        public DropArgs(DragEventArgs dragEventArgs, int targetIndex)
        {
            _dragEventArgs = dragEventArgs;
            _targetIndex = targetIndex;
        }
    }
}
