﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automation.CognitiveData.VisionBot.UI
{
    interface IDataModelConverter
    {
        DataModel ConvertFrom(VisionBotEngine.Model.DataModel dataModel);
        VisionBotEngine.Model.DataModel ConvertTo(DataModel dataModel, VisionBotEngine.Model.DataModel savedDataModel);
    }
}
