﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Collections.ObjectModel;

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.VisionBotEngine.Model;

    public class IqBot
    {
        public string Id { get; set; }
        public float Version { get; set; }
        public string Name { get; set; }
        public ObservableCollection<Layout> Layouts { get; set; }
        public DataModel DataModel { get; set; }
        public BlueprintIdSnap DataModelIdStructure { get; set; }

        public ImageProcessingConfig Settings;

        public IqBot()
        {
            Id = string.Empty;
            Name = string.Empty;
            Layouts = new ObservableCollection<Layout>();
            DataModel = new DataModel();
            DataModelIdStructure = new BlueprintIdSnap();
            Settings = new ImageProcessingConfig();
        }
    }
}