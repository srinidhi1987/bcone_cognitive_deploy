/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using VisionBotEngine.Model;

    public delegate void DeleteUserDefinedRegionHandler(object sender, DeleteUserDefinedRegionEventArgs e);

    public class DeleteUserDefinedRegionEventArgs : EventArgs
    {
        public RegionViewModel RegionViewModel { get; private set; }

        public DeleteUserDefinedRegionEventArgs(RegionViewModel regionViewModel)
        {
            RegionViewModel = regionViewModel;
        }
    }

    public class ImageViewInfo : ViewModelBase, IDisposable
    {
        private UIImageProvider _uiImageProvide = null;
        public DocumentProperties DocumentProperties { get; set; }

        #region Constants

        private const double IMAGE_DPI = 96;

        #endregion Constants

        #region Events

        public event LayoutFieldSelectionHandler LayoutFieldSelectionChanged;

        public event DeleteUserDefinedRegionHandler DeleteUserDefinedRegion;

        #endregion Events

        #region Fields

        private LayoutField _selectedField = new LayoutField();

        #endregion Fields

        #region Properties

        private List<PageImageSource> _imageSource;

        public List<PageImageSource> ImageSource
        {
            get { return _imageSource; }
            set
            {
                if (_imageSource != value)
                {
                    disposeImageViewSource();
                    _imageSource = value;
                    OnPropertyChanged("ImageSource");
                }
            }
        }

        internal List<PageRegions> PageRegions { get; set; } = new List<UI.PageRegions>();

        private ObservableCollection<RegionViewModel> _fields;

        public ObservableCollection<RegionViewModel> Fields
        {
            get { return _fields; }
            set
            {
                _fields = value;
                _fields.CollectionChanged += fields_CollectionChanged;
                //  notifyOnSelection();
                //  OnPropertyChanged("Fields");
            }
        }

        private ObservableCollection<RegionViewModel> _viewPortFields = new ObservableCollection<RegionViewModel>();

        public ObservableCollection<RegionViewModel> ViewPortFields
        {
            get { return _viewPortFields; }
            set
            {
                _viewPortFields = value;
                // _viewPortFields.CollectionChanged += ViewPort_CollectionChanged;
                notifyOnSelection();
                OnPropertyChanged("ViewPortFields");
            }
        }

        public LayoutField SelectedField
        {
            get { return _selectedField; }
            set
            {
                _selectedField = value;
                OnPropertyChanged("SelectedField");
                if (!ViewPortFields.Any(x => x.Region == _selectedField))
                {
                    RegionViewModel currentField = Fields.FirstOrDefault(x => x.Region == _selectedField);
                    if (currentField != null)
                    {
                        ViewPortFields.Add(currentField);
                    }
                }
                resetSelectedHeaderField();
            }
        }

        private LayoutField _selectedHeaderField;

        public LayoutField SelectedHeaderField
        {
            get { return _selectedHeaderField; }
            set
            {
                _selectedHeaderField = value;
                if (_selectedHeaderField == null)
                {
                    SelectedValueBounds = Rect.Empty;
                    SelectionType = SelectionType.Default;
                }
                else
                {
                    SelectionType = SelectionType.Hover;
                    IsRegionCreationEnabled = true;
                    IsThumbVisibile = (UserDefinedRegionType.None != _selectedHeaderField.UserDefinedRegionType);
                }
                OnPropertyChanged("SelectedHeaderField");
            }
        }

        private Rect _selectedValueBounds;

        public Rect SelectedValueBounds
        {
            get { return _selectedValueBounds; }
            set
            {
                if (_selectedValueBounds != value)
                {
                    _selectedValueBounds = value;
                    IsThumbVisibile = (_selectedValueBounds != Rect.Empty);
                    OnPropertyChanged("SelectedValueBounds");
                }
            }
        }

        private ElementType _selectedElementType;

        public ElementType SelectedElementType
        {
            get { return _selectedElementType; }
            set
            {
                if (_selectedElementType != value)
                {
                    _selectedElementType = value;
                    OnPropertyChanged("SelectedElementType");
                }
            }
        }

        private bool _isSelectedValueBoundAuto;

        public bool IsSelectedValueBoundAuto
        {
            get
            {
                return _isSelectedValueBoundAuto;
            }

            set
            {
                _isSelectedValueBoundAuto = value;
                OnPropertyChanged(nameof(IsSelectedValueBoundAuto));
            }
        }

        private bool _isRegionCreationEnabled;

        public bool IsRegionCreationEnabled
        {
            get { return _isRegionCreationEnabled; }
            private set
            {
                if (value != _isRegionCreationEnabled)
                {
                    _isRegionCreationEnabled = value;
                    OnPropertyChanged("IsRegionCreationEnabled");
                }
            }
        }

        private bool _isThumbVisibile;

        public bool IsThumbVisibile
        {
            get { return _isThumbVisibile; }
            set
            {
                if (value != _isThumbVisibile)
                {
                    _isThumbVisibile = value;
                    OnPropertyChanged("IsThumbVisibile");
                }
            }
        }

        internal CanvasImageViewModel Parent { get; set; }

        public ICommand UserDefinedRegionDeleteCommand { get; private set; }

        private SelectionType _selectionType;

        public SelectionType SelectionType
        {
            get { return _selectionType; }
            set
            {
                _selectionType = value;
                OnPropertyChanged("SelectionType");
            }
        }

        public bool IsValuePreviewOn { get; set; }
        public PageImageSource CurrentPage { get; private set; }

        private Rect _viewRect = new Rect();

        public Rect ViewRect
        {
            get { return _viewRect; }
            set
            {
                if (value != _viewRect)
                {
                    _viewRect = value;
                    OnPropertyChanged(nameof(ViewRect));
                    RefreshLoadingScreen();
                }
            }
        }

        private bool _currentViewIsLoading = true;

        public bool CurrentViewIsLoading
        {
            get { return _currentViewIsLoading && Parent.HasItems; }
            set
            {
                if (value != _currentViewIsLoading)
                {
                    _currentViewIsLoading = value;
                    OnPropertyChanged(nameof(CurrentViewIsLoading));
                }
            }
        }

        #endregion Properties

        #region Constructor

        //public ImageViewInfo()
        //{
        //    _imageSource = new List<PageImageSource>();
        //    _fields = new ObservableCollection<RegionViewModel>();
        //    UserDefinedRegionDeleteCommand = new RelayCommand(onUdrDelete);
        //    _selectedValueBounds = Rect.Empty;
        //}

        public ImageViewInfo(UIImageProvider uiImageProvide)
        {
            _uiImageProvide = uiImageProvide;
            _imageSource = new List<PageImageSource>();
            _fields = new ObservableCollection<RegionViewModel>();
            UserDefinedRegionDeleteCommand = new RelayCommand(onUdrDelete);
            _selectedValueBounds = Rect.Empty;
        }

        #endregion Constructor

        #region Methods

        public CroppedBitmap GetCroppedImage(Int32Rect bounds)
        {
            CroppedBitmap croppedImage = null;
            try
            {
                int relativeHeight = 0;
                PageImageSource page = getPageAndPageRelativeHeightFromBounds(bounds, out relativeHeight);
                if (page != null && page.PageImage != null && bounds.X > 0 && bounds.Y > 0 && bounds.Height > 0 && bounds.Width > 0)
                {
                    Int32Rect relativeBounds = new Int32Rect(bounds.X, relativeHeight, bounds.Width, bounds.Height);
                    croppedImage = new CroppedBitmap(page.PageImage, relativeBounds);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageViewInfo), nameof(GetCroppedImage), "This may not be the harmful exception");
            }

            return croppedImage;
        }

        public void MapUserDefinedRegion(Rect rect)
        {
            SelectedElementType = SelectedHeaderField.Type;
            IsSelectedValueBoundAuto = false;
            SelectedHeaderField.IsValueBoundAuto = false;
            SelectedHeaderField.ValueBounds = GetRelativeBounds(rect, SelectedHeaderField.Bounds); //GetRelativeBoundsForUdr(rect, SelectedHeaderField.Bounds);
            SelectedHeaderField.ValueSelectionType = FieldType.UserDefinedField;
            SelectedValueBounds = rect;
            SelectedHeaderField.UserDefinedRegionType = UserDefinedRegionType.UserDefinedField;
            IsThumbVisibile = true;
            assignLayoutFieldValue(SelectedHeaderField);
            resetSelectedHeaderField();
        }

        public LayoutField CreateUserDefinedField(Rect rect)
        {
            var layoutField = new LayoutField
            {
                Bounds = getBounds(rect),
                Type = ElementType.SystemIdentifiedField,
                UserDefinedRegionType = UserDefinedRegionType.UserDefinedField
            };
            _fields.Add(new RegionViewModel(layoutField));
            layoutField.IsSelected = true;
            if (null != LayoutFieldSelectionChanged)
            {
                LayoutFieldSelectionChanged(this, new LayoutFieldSelectionEventArgs(layoutField));
            }
            return layoutField;
        }

        internal void SetCurrentPage(double verticalOffset)
        {
            if (Parent.ZoomPercentage != 0)
            {
                double effectiveOffset = verticalOffset / Parent.ZoomPercentage;
                PageImageSource p = ImageSource.Where(pgvm => pgvm.Top <= effectiveOffset && (pgvm.Top + pgvm.Height) > effectiveOffset).FirstOrDefault();
                if (p != null)
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                    {
                        if (!IsThisPageInViewPort(p))
                            return;

                        if (_uiImageProvide == null)
                            return;

                        _uiImageProvide.PageProcessActionQueue.Clear();
                        foreach (PageImageSource page in ImageSource)
                        {
                            if (page.PageIndex == (p.PageIndex - 1)
                                    || (page.PageIndex == p.PageIndex)
                                    || (page.PageIndex == p.PageIndex + 1)
                                    )
                            {
                                if (page.PageIndex == p.PageIndex)
                                {
                                    proceessCurrentPageView(page);
                                }

                                if (!IsThisPageInViewPort(p))
                                    return;
                            }
                            else
                            {
                                if (page?.PageImage != null)
                                {
                                    page.PageImage = null;
                                }
                            }
                        }

                        List<RegionViewModel> removalList = new List<RegionViewModel>();
                        foreach (var item in ViewPortFields)
                        {
                            if (!ViewRect.Contains(item.DisplayBounds))
                            {
                                removalList.Add(item);
                            }
                        }
                        foreach (var item in removalList)
                        {
                            ViewPortFields.Remove(item);
                        }
                        CurrentPage = p;
                    }), DispatcherPriority.Background);
                }
            }
        }

        private bool IsThisPageInViewPort(PageImageSource p)
        {
            return ViewRect.IntersectsWith(new Rect(0, p.Top, p.Width, p.Height));
        }

        private void proceessCurrentPageView(PageImageSource page)
        {
            Dispatcher uiDispatche = Dispatcher.CurrentDispatcher;
            if (page.PageIndex != 0)
            {
                processPage(ImageSource[page.PageIndex - 1], uiDispatche);
            }

            processPage(page, uiDispatche);

            if (page.PageIndex != ImageSource.Count - 1)
            {
                processPage(ImageSource[page.PageIndex + 1], uiDispatche);
            }

            _uiImageProvide.CurrentViewIndex = page.PageIndex == 0 ? 0 : page.PageIndex - 1;
        }

        private void processPage(PageImageSource page, Dispatcher uiDispatche)
        {
            if (page.PageImage == null)
            {
                _uiImageProvide.GetImage(page.PageIndex, (bitmapImage) =>
                {
                    uiDispatche.BeginInvoke(new Action(() =>
                    {
                        if (ImageSource != null && ImageSource.Count > (page.PageIndex))
                        {
                            if (!IsThisPageInViewPort(page))
                                return;
                            ImageSource[page.PageIndex].PageImage = bitmapImage;
                            assignPageRegionsToImageViewInfo(Fields, ImageSource[page.PageIndex]);
                            if (Parent?.InspectionViewModel != null)
                                Parent.InspectionViewModel.RefreshRightPanelPreivewImage();
                            RefreshLoadingScreen();
                        }
                    }
                    ), DispatcherPriority.Background);
                });
            }
            else
            {
                assignPageRegionsToImageViewInfo(Fields, ImageSource[page.PageIndex]);
            }
        }

        internal void RefreshLoadingScreen()
        {
            List<PageImageSource> pages = ImageSource.FindAll(page => (ViewRect.IntersectsWith(new Rect(0, page.Top, page.Width, page.Height)) && page.PageImage == null));

            if (pages.Count > 0)
            {
                CurrentViewIsLoading = true;
            }
            else
            {
                CurrentViewIsLoading = false;
            }
        }

        public void Dispose()
        {
            disposeImageViewSource();
        }

        internal void TurnValueSelectionOff()
        {
            SelectionType = SelectionType.Default;
            resetSelectedHeaderField();
        }

        internal double GetOriginalImageHeight()
        {
            double height = 0;
            double dpiY = IMAGE_DPI;
            if (ImageSource != null && ImageSource.Count > 0)
            {
                if (ImageSource[0].DpiY > 50)
                {
                    dpiY = ImageSource[0].DpiY;
                }

                for (int i = 0; i < ImageSource.Count; i++)
                {
                    height += ImageSource[i].Height;
                }

                //height = height * (dpiY / IMAGE_DPI);
            }

            return height;
        }

        internal double GetOriginalImageWidth()
        {
            double width = 0;
            double dpiX = IMAGE_DPI;
            if (ImageSource != null && ImageSource.Count > 0)
            {
                if (ImageSource[0].DpiX > 50)
                {
                    dpiX = ImageSource[0].DpiX;
                }

                for (int i = 0; i < ImageSource.Count; i++)
                {
                    if (ImageSource[i].Width > width)
                    {
                        width = ImageSource[i].Width;
                    }
                }

                //width = width * (dpiX / IMAGE_DPI);
            }

            return width;
        }

        internal void SetZoomHeightWidth(double zoomPercentage)
        {
            if (ImageSource != null)
            {
                for (int i = 0; i < ImageSource.Count; i++)
                {
                    ImageSource[i].SetZoomHeightWidth(zoomPercentage);
                }
            }
        }

        internal Rect GetRelativeBounds(Rect valueRegion, Rect labelRegion)
        {
            return new Rect
            {
                X = valueRegion.X - labelRegion.X,
                Y = valueRegion.Y - labelRegion.Y,
                Width = valueRegion.Width,
                Height = valueRegion.Height
            };
        }

        internal Rect GetAbsoluteBounds(LayoutField layoutField)
        {
            if (0 == Convert.ToInt32(layoutField.ValueBounds.Height) || 0 == Convert.ToInt32(layoutField.ValueBounds.Width))
            {
                return Rect.Empty;
            }

            var valueRect = new Rect
            {
                X = (layoutField.Bounds.X + layoutField.ValueBounds.X),
                Y = (layoutField.Bounds.Y + layoutField.ValueBounds.Y),
                Width = (layoutField.ValueBounds.Width),
                Height = (layoutField.ValueBounds.Height)
            };

            return valueRect;
        }

        private void resetSelectedHeaderField()
        {
            if (_selectedHeaderField != null)
                _selectedHeaderField.IsValueSelectionOn = false;
            _selectedHeaderField = null;
            IsRegionCreationEnabled = false;
        }

        private void fields_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    var regionViewModel = (newItem as RegionViewModel);
                    if (regionViewModel != null)
                    {
                        var layoutField = regionViewModel.Region as LayoutField;
                        if (layoutField != null)
                        {
                            layoutField.LayoutFieldSelectionChanged += layoutField_LayoutFieldSelectionChanged;
                        }

                        if (ViewRect.Contains(regionViewModel.DisplayBounds))
                        {
                            PageImageSource p = ImageSource.Where(pgvm => new Rect(0, pgvm.Top, pgvm.Width, pgvm.Height).IntersectsWith(regionViewModel.DisplayBounds)).FirstOrDefault();
                            if (p?.PageImage != null)
                                ViewPortFields.Add(regionViewModel);
                        }
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var newItem in e.OldItems)
                {
                    var regionViewModel = (newItem as RegionViewModel);

                    ViewPortFields.Remove(regionViewModel);
                }
            }
        }

        private void notifyOnSelection()
        {
            foreach (var regionViewModel in _fields)
            {
                var layoutField = regionViewModel.Region as LayoutField;
                if (layoutField != null)
                {
                    layoutField.LayoutFieldSelectionChanged += layoutField_LayoutFieldSelectionChanged;
                }
            }
        }

        private void layoutField_LayoutFieldSelectionChanged(object sender, LayoutFieldSelectionEventArgs e)
        {
            if (null == _selectedHeaderField)
            {
                if (e.LayoutField.ValueBounds != _selectedValueBounds)
                {
                    if (e.LayoutField.Type == ElementType.Table)
                    {
                        SelectedValueBounds = Rect.Empty;
                        IsSelectedValueBoundAuto = false;
                        SelectedElementType = ElementType.Table;
                    }
                    else
                    {
                        SelectedValueBounds = GetAbsoluteBounds(e.LayoutField);
                        IsSelectedValueBoundAuto = e.LayoutField.IsValueBoundAuto;
                        SelectedElementType = ElementType.Field;
                        IsThumbVisibile = e.LayoutField.UserDefinedRegionType != UserDefinedRegionType.None;
                    }
                }
                if (null != LayoutFieldSelectionChanged)
                {
                    LayoutFieldSelectionChanged(this, e);
                }
            }
            else
            {
                if (_selectedHeaderField.IsValueSelectionOn)
                {
                    _selectedHeaderField.ValueBounds = GetRelativeBounds(e.LayoutField.Bounds, _selectedHeaderField.Bounds);
                    _selectedHeaderField.ValueSelectionType = getFieldTypeFromElementType(e.LayoutField.Type);

                    SelectedValueBounds = new Rect
                    {
                        X = e.LayoutField.Bounds.X,
                        Y = e.LayoutField.Bounds.Y,
                        Width = e.LayoutField.Bounds.Width,
                        Height = e.LayoutField.Bounds.Height
                    };
                    SelectedElementType = e.LayoutField.Type;
                    IsSelectedValueBoundAuto = false;
                    _selectedHeaderField.IsValueBoundAuto = false;
                    //_selectedHeaderField.Value = e.LayoutField.Label;
                    //_selectedHeaderField.Value = getLayoutFieldValue(_selectedHeaderField);
                    assignLayoutFieldValue(_selectedHeaderField);
                    IsThumbVisibile = e.LayoutField.UserDefinedRegionType != UserDefinedRegionType.None;
                    resetSelectedHeaderField();
                }
            }
        }

        private FieldType getFieldTypeFromElementType(ElementType elementType)
        {
            FieldType fieldType = FieldType.SystemField;
            switch (elementType)
            {
                case ElementType.Field:
                    fieldType = FieldType.SystemField;
                    break;

                case ElementType.Table:
                    fieldType = FieldType.SystemField;
                    break;

                case ElementType.Marker:
                    fieldType = FieldType.SystemField;
                    break;

                case ElementType.SystemIdentifiedRegion:
                    fieldType = FieldType.SystemRegion;
                    break;

                case ElementType.SystemIdentifiedField:
                    fieldType = FieldType.SystemField;
                    break;

                default:
                    fieldType = FieldType.SystemField;
                    break;
            }

            return fieldType;
        }

        private Rect getBounds(Rect rect)
        {
            var zoomValue = Math.Round(Parent.ZoomPercentage, 2);
            rect.X = (rect.X / zoomValue) + (2 * zoomValue);
            rect.Y = (rect.Y / zoomValue) + (2 * zoomValue);
            rect.Width = getSizeValue(rect.Width, zoomValue, ResizeThumb.MinRegionWidth);
            rect.Height = getSizeValue(rect.Height, zoomValue, ResizeThumb.MinRegionHeight);
            return rect;
        }

        private double getSizeValue(double value, double zoomValue, int minSize)
        {
            var size = (value / zoomValue) - (4 * zoomValue);
            if (size < (minSize / zoomValue))
            {
                return minSize / zoomValue;
            }

            return size;
        }

        private void onUdrDelete(object obj)
        {
            Parent.MakeFieldValueBoundSelectionOn();
            if (SelectedHeaderField != null)
            {
                SelectedHeaderField.IsValueBoundAuto = true;
                SelectedHeaderField.UserDefinedRegionType = UserDefinedRegionType.None;
                SelectedHeaderField.ValueSelectionType = FieldType.SystemField;

                Parent.InspectionViewModel.MakeLayoutFieldValueAuto();
            }
            resetSelectedHeaderField();
            SelectionType = SelectionType.Default;
        }

        private void assignLayoutFieldValue(LayoutField layoutField)
        {
            try
            {
                if (layoutField != null && IsValuePreviewOn)
                {
                    layoutField.Value = getLayoutFieldValue(layoutField);
                    removeLabelIfSelfField(layoutField);
                }
            }
            catch (Exception ex)
            {
                ex.Log(nameof(ImageViewInfo), nameof(assignLayoutFieldValue));
            }
        }

        private string getLayoutFieldValue(LayoutField layoutField)
        {
            VisionBotEngine.Model.Field field =
                Parent != null ? Parent.GetLayoutFieldValue(layoutField) : null;
            if (field != null && !string.IsNullOrWhiteSpace(field.Text))
            {
                return field.Text.Trim();
            }

            return string.Empty;
        }

        private void removeLabelIfSelfField(LayoutField layoutField)
        {
            if (isSelfField(layoutField))
            {
                layoutField.Label = string.Empty;
            }
        }

        private bool isSelfField(LayoutField layoutField)
        {
            return layoutField != null && !string.IsNullOrWhiteSpace(layoutField.Label) && layoutField.Label.Trim().Equals(layoutField.Value);
        }

        private PageImageSource getPageAndPageRelativeHeightFromBounds(Int32Rect bounds, out int relativeHeight)
        {
            int i = 0;
            PageImageSource page = null;
            double currentHeight = 0;
            double previousHeight = 0;

            relativeHeight = 0;

            if (_imageSource != null && _imageSource.Count > 0)
            {
                while (i < _imageSource.Count)
                {
                    currentHeight += _imageSource[i].Height;
                    if (bounds.Y < currentHeight)
                    {
                        break;
                    }

                    previousHeight = currentHeight;
                    i++;
                }

                page = _imageSource[i];
                relativeHeight = bounds.Y - Convert.ToInt32(previousHeight);
            }

            return page;
        }

        private void disposeImageViewSource()
        {
            if (_imageSource != null)
            {
                foreach (PageImageSource page in _imageSource)
                {
                    if (page != null)
                    {
                        page.PageImage = null;
                    }
                }

                _imageSource.Clear();
            }
        }

        private void assignPageRegionsToImageViewInfo(ObservableCollection<RegionViewModel> fields, PageImageSource pageIamgeSource)
        {
            try
            {
                if (fields != null)
                {
                    // for (int j = 0; j < fields.Count; j++)
                    {
                        IList<RegionViewModel> viewPortRegionsToConsider = fields.Where(x => (ViewRect.IntersectsWith(x.DisplayBounds) &&
                        (x.DisplayBounds.Y > pageIamgeSource.Top)
                          &&
                        (x.DisplayBounds.Bottom < (pageIamgeSource.Top + pageIamgeSource.Height))
                        )).ToList();
                        // VBotLogger.Trace(() => string.Format("[assignPageRegionsToImageViewInfo] Queuing call for assignRegionsToImageViewInfo. PageIndex: {0} ChunkIndex: {1} ChunkCount: {2}", pageRegionsForPageIndex.PageIndex, j, pagewiseRegionsToConsider.Count));

                        Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => { assignRegionsToImageViewInfo(viewPortRegionsToConsider); }), DispatcherPriority.Background);
                    }
                }
                else
                {
                    VBotLogger.Error(() => string.Format("[assignPageRegionsToImageViewInfo] Could not found PageRegions for PageIndex: "));
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[assignPageRegionsToImageViewInfo] - Exception occured {0}.", ex.Message));
            }
        }

        private void assignRegionsToImageViewInfo(IList<RegionViewModel> regions)
        {
            try
            {
                //Fields.AddRange(regions);
                for (int i = 0; i < regions.Count; i++)
                {
                    if (ViewRect.IntersectsWith(regions[i].DisplayBounds) && (!ViewPortFields.Contains(regions[i])))
                    {
                        ViewPortFields.Add(regions[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                VBotLogger.Error(() => string.Format("[ assignRegionsToImageViewInfo ] - Exception occured {0}.", ex.Message));
            }
        }

        #endregion Methods
    }
}