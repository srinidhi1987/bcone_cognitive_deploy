﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;

    internal class TableFooterVerifier : IVerifier
    {
        private LayoutTable _layoutTable;

        public TableFooterVerifier(LayoutTable layoutTable)
        {
            _layoutTable = layoutTable;
        }
        public Warning Verify()
        {
            if (_layoutTable != null && (_layoutTable.Footer == null || string.IsNullOrWhiteSpace(_layoutTable.Footer.Label)))
                return new Warning(WarningTypes.TableFooterMissing, new List<object>() { _layoutTable });

            return new Warning();
        }
    }
}
