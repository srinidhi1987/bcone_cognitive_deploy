﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;

    class TablesWarning : VBotWarning
    {
        private List<LayoutTable> _layoutTables;

        private List<TableDef> _dataModelTables;

        public TablesWarning(List<LayoutTable> layoutTables, List<TableDef> dataModelTables)
        {
            _layoutTables = layoutTables;
            _dataModelTables = dataModelTables;
        }

        public override List<Warning> GetWarnings()
        {
            List<Warning> warnings = new List<Warning>();

            foreach (var dataModelTable in _dataModelTables)
            {
                LayoutTable layoutTable = _layoutTables.Find(t => t.TableDef.Id.Equals(dataModelTable.Id));
                IVBotWarning warning = new TableWarning(layoutTable, dataModelTable);

                warnings.AddRange(warning.GetWarnings());
            }

            return mergeWarnings(warnings);
        }
    }

    class TableWarning : VBotWarning
    {
        private LayoutTable _layoutTable;

        private TableDef _dataModelTable;

        public TableWarning(LayoutTable layoutTable, TableDef dataModelTable)
        {
            _layoutTable = layoutTable;
            _dataModelTable = dataModelTable;
        }

        public override List<Warning> GetWarnings()
        {
            List<Warning> warnings = new List<Warning>();

            IVerifier verifier = new TableColumnMappingVerifier(_layoutTable, _dataModelTable);
            checkForWarning(verifier, warnings);

            verifier = new TableReferenceColumnVerifier(_layoutTable);
            checkForWarning(verifier, warnings);

            verifier = new TableFooterVerifier(_layoutTable);
            checkForWarning(verifier, warnings);

            return warnings;
        }

        private static void checkForWarning(IVerifier verifier, List<Warning> warnings)
        {
            Warning warning = verifier.Verify();

            if (warning.WarningType != null)
            {
                warnings.Add(warning);
            }
        }
    }
}
