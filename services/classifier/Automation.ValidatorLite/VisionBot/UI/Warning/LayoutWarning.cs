﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;
    using System.Linq;

    class LayoutWarning : VBotWarning
    {
        private Layout _layout;

        private DataModel _dataModel;

        public LayoutWarning(Layout layout, DataModel dataModel)
        {
            _layout = layout;
            _dataModel = dataModel;
        }
        public override List<Warning> GetWarnings()
        {
            List<Warning> warnings = new List<Warning>();

            IVBotWarning warning = new FieldsWarning(_layout.LayoutFields.Fields.ToList(), _dataModel.Fields.ToList());
            warnings.AddRange(warning.GetWarnings());

            warning = new TablesWarning(_layout.LayoutTables.Tables.ToList(), _dataModel.Tables.ToList());
            warnings.AddRange(warning.GetWarnings());

            return mergeWarnings(warnings);
        }

    }
}
