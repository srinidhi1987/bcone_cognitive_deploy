﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.Collections.Generic;

    class FieldsWarning : VBotWarning
    {
        private List<LayoutField> _layoutFields;

        private List<FieldDef> _dataModelFields;

        public FieldsWarning(List<LayoutField> layoutFields, List<FieldDef> dataModelFields)
        {
            _layoutFields = layoutFields;
            _dataModelFields = dataModelFields;
        }

        public override List<Warning> GetWarnings()
        {
            List<Warning> warnings = new List<Warning>();

            IVerifier verifier = new FieldsMappingVerifier(_layoutFields, _dataModelFields);
            Warning warning = verifier.Verify();

            if (warning.WarningType != null)
                warnings.Add(warning);

            return warnings;
        }
    }
}
