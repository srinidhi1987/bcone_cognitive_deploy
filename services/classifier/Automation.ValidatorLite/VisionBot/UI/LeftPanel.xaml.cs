﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for FieldTree.xaml
    /// </summary>
    public partial class LeftPanel : UserControl
    {
        // TODO: Move FieldTree.xaml View here. Intentionally created duplicate copy of FieldTree.xaml.
        public LeftPanel()
        {
            InitializeComponent();
        }

        private void MainTreeView_OnSelectedItemChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
        {
            //if (MainTreeView.SelectedItem != null)
            //{
            //    MainTreeView.BringIntoView(MainTreeView.SelectedItem);
            //}
        }

        private void TreeViewItemSelected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                item.BringIntoView();
                e.Handled = true;
            }
        }
    }
}
