/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;

    using Automation.Common;
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Media;
    using VisionBotEngine.Model;
    using Automation.Cognitive.Validator.Properties;

    public class ZoomConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double presentValue = (double)value;
            double zoomPercentage = (double)parameter;

            return presentValue * zoomPercentage;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    public class TypeColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ElementType elementType = (ElementType)value;
            return LayoutFieldsConverter.GetColor(elementType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility;
            if (bool.TryParse(value.ToString(), out visibility) && visibility)
                return Visibility.Visible;

            if (Visibility.Hidden.ToString().Equals(parameter))
                return Visibility.Hidden;

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HeaderTextVisibilityConverter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            var parameterValues = parameter as string[];

            if (parameterValues == null)
            {
                parameterValues = new string[] { System.Convert.ToString(parameter) };
            }

            foreach (string str in parameterValues)
            {
                if (values.Equals(str))
                    return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HeaderTextVisibilityConverterMulti : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var parameterValues = parameter as string[];

            if (parameterValues == null)
            {
                parameterValues = new string[] { System.Convert.ToString(parameter) };
            }

            if (values[0] is string && values[1] is bool)
            {
                string selectedTab = System.Convert.ToString(values[0]);
                bool hasItems = System.Convert.ToBoolean(values[1]);
                if (hasItems)
                {
                    foreach (string str in parameterValues)
                    {
                        if (selectedTab.Equals(str))
                        {
                            return Visibility.Visible;
                        }
                    }
                }
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InverseBoolVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility;
            if (bool.TryParse(value.ToString(), out visibility) && !visibility)
                return Visibility.Visible;

            if (Visibility.Hidden.ToString().Equals(parameter))
                return Visibility.Hidden;

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ComplementBoolVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility;
            if (bool.TryParse(value.ToString(), out visibility) && !visibility)
                return Visibility.Visible;

            if (Visibility.Hidden.ToString().Equals(parameter))
                return Visibility.Hidden;

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FieldDirectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }

    public class InverseBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                return !(bool)value;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                return !(bool)value;
            }
            return value;
        }
    }

    public class CheckedBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isFlagOn = System.Convert.ToBoolean(value);
            bool isTrue = System.Convert.ToBoolean(parameter);

            return isFlagOn == isTrue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isFlagOn = System.Convert.ToBoolean(value);
            bool isTrue = System.Convert.ToBoolean(parameter);

            return isFlagOn == isTrue;
        }
    }

    public class BoolToYesNoStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string stringBoolValue = "No";
            bool boolValue;
            if (bool.TryParse(value.ToString(), out boolValue))
            {
                if (boolValue)
                {
                    stringBoolValue = "Yes";
                }
            }

            return stringBoolValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeToggleValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType().Equals(parameter))
            {
                return true;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TextBlockWidthValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var width = System.Convert.ToDouble(value);
            return width - 28;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ThumbColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType().Equals(parameter))
                return true;

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TrimmedTextBlockVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;

            TextBlock textBlock = (TextBlock)value;

            var typeface = new Typeface(textBlock.FontFamily, textBlock.FontStyle, textBlock.FontWeight, textBlock.FontStretch);
            var formattedText = new FormattedText(textBlock.Text, CultureInfo.CurrentCulture, textBlock.FlowDirection, typeface, textBlock.FontSize, textBlock.Foreground);
            if (formattedText.Width > textBlock.ActualWidth)
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ElementTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            if (value != null && value is Nullable<ElementType> && parameter != null && parameter is ElementType)
            {
                Nullable<ElementType> elementType = (Nullable<ElementType>)value;
                if (elementType != null && elementType.Value == (ElementType)parameter)
                {
                    visibility = Visibility.Visible;
                }
            }

            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            if (value != null)
            {
                visibility = Visibility.Visible;
            }
            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InverseNullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Collapsed;
            if (value == null)
            {
                visibility = Visibility.Visible;
            }
            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SelectionColorValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isSelected = System.Convert.ToBoolean(value);

            if (isSelected)
            {
                return new SolidColorBrush(Color.FromArgb(255, 159, 216, 241));
            }

            return new SolidColorBrush(Color.FromArgb(255, 112, 112, 112));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class EnumToItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string returnVal = string.Empty;

            if (value != null && value is ElementType)
            {
                ElementType type = (ElementType)value;
                returnVal = StringEnum.GetStringValue(type);
            }

            return returnVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //return  StringEnum.GetEnumByStringvalue((string)value,typeof(ElementType));
            throw new NotImplementedException();
        }
    }

    public class LayoutTableSaveCaptionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool result = false;
            string caption = "Save";
            foreach (object value in values)
            {
                if (value is bool)
                {
                    result = result || (bool)value;
                    if (result)
                        break;
                }
            }

            if (result)
                caption = caption + "*";

            return caption;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ErrorVisibilityValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(System.Convert.ToString(value)))
            {
                return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValueVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isInverse = System.Convert.ToString(parameter) == "Inverse";
            string message = System.Convert.ToString(value);

            if (!string.IsNullOrEmpty(message))
            {
                return SetVisibilityVisible(isInverse);
            }

            return SetVisibilityCollapsed(isInverse);
        }

        private static Visibility SetVisibilityVisible(bool isInverse)
        {
            return isInverse ? Visibility.Collapsed : Visibility.Visible;
        }

        private static Visibility SetVisibilityCollapsed(bool isInverse)
        {
            return !isInverse ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class ViewLinkVisibilityConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        Visibility visibility = Visibility.Collapsed;
    //        if (!(value is ITestItemResults) && (value is ITestItemResult))
    //        {
    //            visibility = Visibility.Visible;
    //        }

    //        return visibility;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class BoolToThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isSelected = System.Convert.ToBoolean(value);
            if (isSelected)
            {
                return new Thickness(3);
            }

            return new Thickness(1);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeColorValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ElementType elementType = (ElementType)value;
            return LayoutFieldsConverter.GetColor(elementType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HeaderTextSelectionColorConverter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            string color = "#FF00AEDF";

            var value = System.Convert.ToBoolean(values);
            if (value)
            {
                // Unselection color
                color = "#FF979797";
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString(color));
            }
            else
            {
                //Selection Color
                color = "#FF00AEDF";
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString(color));
            }
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoundsToVisibilityValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Rect)
            {
                var rect = (Rect)value;
                if (Rect.Empty.Equals(rect))
                {
                    return Visibility.Collapsed;
                }
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class RegionTooltipConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        string tooltip = string.Empty;
    //        LayoutField layoutField = value as LayoutField;
    //        if (layoutField != null)
    //        {
    //            if (!string.IsNullOrWhiteSpace(layoutField.Label))
    //            {
    //                tooltip = layoutField.Label;
    //                return tooltip;
    //            }

    //            if (!string.IsNullOrWhiteSpace(layoutField.Value))
    //            {
    //                tooltip = layoutField.Value;
    //                return tooltip;
    //            }
    //        }

    //        DocSetItemValueRegion validatedValue = value as DocSetItemValueRegion;
    //        if (validatedValue != null && !string.IsNullOrWhiteSpace(validatedValue.Value))
    //        {
    //            tooltip = validatedValue.Value;
    //            return tooltip;
    //        }
    //        return tooltip;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class EmptyStringToSpacesStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string returnString = "   ";
            string valueString = value as String;
            if (!string.IsNullOrEmpty(valueString))
            {
                returnString = valueString;
            }

            return returnString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StringToColorConverter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            string colorStr = values as string;

            if (!string.IsNullOrWhiteSpace(colorStr))
            {
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString(colorStr));
            }

            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class RescanSubTextBlockVisibilityConverter : IValueConverter
    //{
    //    public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        GenericWarning warning = values as GenericWarning;

    //        if (warning != null
    //            && warning.HasWarning
    //            && warning.WarningType == WarningType.Blueprint)
    //        {
    //            return Visibility.Visible;
    //        }

    //        return Visibility.Collapsed;
    //    }

    //    public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class SelectedToStrokeThickNessConverter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            bool isSelected = (bool)values;

            return isSelected ? 4 : 2;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidationToBorderBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is ValidationIssue)
            {
                string property = parameter as string;
                var validationIsuue = (ValidationIssue)value;

                if (property == "BorderBrush")
                {
                    if (validationIsuue.IssueCode == VisionBotEngine.FieldDataValidationIssueCodes.MissingRequiredFieldValue)
                    {
                        return new SolidColorBrush(Color.FromArgb(255, 247, 58, 58));
                    }

                    if (validationIsuue.IssueCode.Equals(VisionBotEngine.FieldDataValidationIssueCodes.ConfidenceIssue))
                    {
                        return new SolidColorBrush(Color.FromArgb(255, 255, 165, 0));
                    }
                }
                else if (property == "Foreground")
                {
                    if (validationIsuue.IssueCode.Equals(VisionBotEngine.FieldDataValidationIssueCodes.ConfidenceIssue))
                    {
                        return new SolidColorBrush(Color.FromArgb(255, 255, 165, 0));
                    }

                    if (validationIsuue.IssueCode != VisionBotEngine.FieldDataValidationIssueCodes.MissingRequiredFieldValue)
                    {
                        return new SolidColorBrush(Color.FromArgb(255, 247, 58, 58));
                    }
                }
            }

            return new SolidColorBrush(Color.FromArgb(255, 113, 113, 113));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidationToErrorIndicatorVisibilityConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is ValidationIssue)
            {
                var validationIsuue = (ValidationIssue)value;

                if (validationIsuue != null)
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidationIssueToErrorIndicatorFillConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is ValidationIssue)
            {
                var validationIsuue = (ValidationIssue)value;

                if (validationIsuue != null)
                {
                    switch (validationIsuue.IssueCode)
                    {
                        case VisionBotEngine.FieldDataValidationIssueCodes.ConfidenceIssue:
                            return new SolidColorBrush(Color.FromArgb(255, 255, 165, 0));
                        default:
                            return new SolidColorBrush(Color.FromArgb(255, 246, 79, 148));
                    }
                }
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidationIssueToErrorMessageConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is ValidationIssue)
            {
                var validationIsuue = (ValidationIssue)value;

                if (validationIsuue != null)
                {
                    switch (validationIsuue.IssueCode)
                    {
                        default:
                            return validationIsuue.ToUserFriendlyString();
                    }
                }
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RelativeWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double baseWidth = 0;
            double delta = 0;
            double finalWidth = 0;
            if (value != null && value is double)
            {
                baseWidth = (double)value;
                delta = parameter != null && parameter is double ? (double)parameter : 0;
                finalWidth = baseWidth + delta;
            }

            return finalWidth;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NumberAdjustmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int adjustedNumber;
            int originalNumber;
            int adjustment;

            originalNumber = value != null && value is int ? (int)value : 0;
            adjustment = parameter != null && parameter is int ? (int)parameter : 0;
            adjustedNumber = originalNumber + adjustment;
            return adjustedNumber;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PercentageDimensionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double zoomPercentage = 1;
            double originalDimension = (double)parameter;
            double dimensionToReturn = originalDimension;
            if (value != null && value is double)
            {
                zoomPercentage = (double)value;
                dimensionToReturn = originalDimension * zoomPercentage;
            }

            return dimensionToReturn;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RegionHandlePercentageThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double zoomPercentage = 1;
            Thickness originalMargin = (Thickness)parameter;
            Thickness marginToReturn = originalMargin;
            if (value != null && value is double)
            {
                zoomPercentage = (double)value;
                //Please be aware of addition of 2 (extra) in Top margin in below line to coinside the handle's border with Region Rectangle's border.
                marginToReturn = new Thickness(originalMargin.Left * zoomPercentage, (originalMargin.Top * zoomPercentage) + 2, originalMargin.Right * zoomPercentage, originalMargin.Bottom * zoomPercentage);
            }

            return marginToReturn;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValidatorTableFootNoteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int rowCount = 0;
            if (value != null && value is int)
            {
                rowCount = (int)value;
            }

            return "* " + (rowCount > 0 ? Automation.Cognitive.Validator.Properties.Resources.ValidatorDeleteInsertRowFootNote : Automation.Cognitive.Validator.Properties.Resources.ValidatorAddRowFootNote);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ElementTypeToZIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ElementType)
            {
                var elementType = (ElementType)value;

                switch (elementType)
                {
                    case ElementType.SystemIdentifiedRegion:
                        return 10;

                    case ElementType.SystemIdentifiedField:
                        return 20;

                    case ElementType.Field:
                        return 30;

                    case ElementType.Table:
                        return 40;

                    case ElementType.Marker:
                        return 50;

                    default:
                        return 10;
                }
            }

            return 10;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DatagridRowCountToHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int rowcount = 0;
            int.TryParse(value.ToString(), out rowcount);
            if (rowcount > 200)
            {
                return "500";
            }

            return "Auto";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}