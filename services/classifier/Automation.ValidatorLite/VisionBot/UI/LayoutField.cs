﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;

    public class LayoutField : INotifyPropertyChanged, IRegion
    {
        internal const string ValueFieldMappingErrorMessage = "Please select a valid value field mapping.";

        public event EventHandler SelectionChanged;

        private bool _isSelected = false;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    onPropertyChanged("IsSelected");
                    if (IsSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }

        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        public event EventHandler ExpandChanged;

        private bool _isExpanded = false;

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    onPropertyChanged("IsExpanded");
                    onExpandChanged();
                }
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        private bool _isPrimary = false;

        public bool IsPrimary
        {
            get { return _isPrimary; }
            set
            {
                if (_isPrimary != value)
                {
                    _isPrimary = value;
                    onPropertyChanged("IsPrimary");
                }
            }
        }

        private string _markerName;

        public string MarkerName
        {
            get { return _markerName; }
            set
            {
                if (_markerName != value)
                {
                    _markerName = value;
                    onPropertyChanged("MarkerName");
                    IsDirty = true;
                }
            }
        }

        private Rect _bounds;

        public Rect Bounds
        {
            get { return _bounds; }
            set
            {
                if (_bounds != value)
                {
                    _bounds = value;
                    onPropertyChanged(nameof(Bounds));
                    IsDirty = true;
                }
            }
        }

        public Rect OriginalBounds { get; set; }

        private Rect _valueBounds;

        public Rect ValueBounds
        {
            get { return _valueBounds; }
            set
            {
                if (_valueBounds != value)
                {
                    _valueBounds = value;
                    onPropertyChanged("ValueBounds");
                    IsDirty = true;
                }
            }
        }

        private bool _isValueBoundAuto = false;

        public bool IsValueBoundAuto
        {
            get
            {
                return _isValueBoundAuto;
            }

            set
            {
                if (_isValueBoundAuto != value)
                {
                    _isValueBoundAuto = value;
                    onPropertyChanged(nameof(IsValueBoundAuto));
                }
            }
        }

        private ElementType _type;

        public ElementType Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    onPropertyChanged("Type");
                }
            }
        }

        private UserDefinedRegionType _userdefinedRegionType;

        public UserDefinedRegionType UserDefinedRegionType
        {
            get { return _userdefinedRegionType; }
            set
            {
                if (_userdefinedRegionType != value)
                {
                    _userdefinedRegionType = value;
                    onPropertyChanged("UserDefinedRegionType");
                }
            }
        }

        private bool _isDirty;

        public bool IsDirty
        {
            get { return _isDirty; }
            private set
            {
                if (_isDirty != value)
                {
                    _isDirty = value;
                    onPropertyChanged("IsDirty");
                }
            }
        }

        public bool IsRequired { get; set; }

        private FieldDef _fieldDef;

        public FieldDef FieldDef
        {
            get { return _fieldDef; }
            set
            {
                if (_fieldDef != value)
                {
                    _fieldDef = value;
                    IsDirty = true;
                    onPropertyChanged("FieldDef");
                }
            }
        }

        public FieldValueType FieldValueType { get; set; }

        private string _label = string.Empty;

        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                if (_label != value)
                {
                    _label = value ?? string.Empty;
                    IsDirty = true;
                    onPropertyChanged("Label");
                }
            }
        }

        public FieldType ValueSelectionType { get; set; }

        private bool isSelf()
        {
            return 0.Equals(Convert.ToInt32(ValueBounds.X)) && 0.Equals(Convert.ToInt32(ValueBounds.Y)) &&
                !(0.Equals(Convert.ToInt32(ValueBounds.Width)) && !(0.Equals(Convert.ToInt32(ValueBounds.Height))));
        }

        private bool isValueStartingWithLabel()
        {
            return !string.IsNullOrEmpty(_value) && !string.IsNullOrEmpty(_label) && _value != _label && _value.StartsWith(_label);
        }

        private bool isSelfValue()
        {
            return isSelf() && isValueStartingWithLabel();
        }

        public double MergeRatio { get; set; }

        private bool _isMultiLine;

        public bool IsMultiLine
        {
            get { return _isMultiLine; }
            set
            {
                if (_isMultiLine != value)
                {
                    _isMultiLine = value;
                    IsDirty = true;
                }
            }
        }

        ////TODO: Below Fields are not required in Layout but available in UI. Need to conclude.
        //public string FieldType { get; set; }
        //public string DefaultValue { get; set; }
        //public FieldDirection FieldDirection { get; set; }
        private FieldDirection _fieldDirection;

        public FieldDirection FieldDirection
        {
            get { return _fieldDirection; }
            set
            {
                if (_fieldDirection != value)
                {
                    _fieldDirection = value;
                    IsDirty = true;
                }
            }
        }

        public string Id { get; set; }
        public string SystemIdentifiedFieldParentId { get; set; }
        public int Confidence { get; set; }

        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    onPropertyChanged("Value");
                }
            }
        }

        public double SimilarityFactor { get; set; }

        public SolidColorBrush ItemBackground { get; set; }

        private string _formatExpression;

        public string FormatExpression
        {
            get { return _formatExpression; }
            set
            {
                if (_formatExpression != value)
                {
                    _formatExpression = value;
                    IsDirty = true;
                }
            }
        }

        private string _startsWith;

        public string StartsWith
        {
            get { return _startsWith; }
            set
            {
                if (_startsWith != value)
                {
                    _startsWith = value;
                    IsDirty = true;
                }
            }
        }

        private string _endsWith;

        public string EndsWith
        {
            get { return _endsWith; }
            set
            {
                if (_endsWith != value)
                {
                    _endsWith = value;
                    IsDirty = true;
                }
            }
        }

        public decimal Angle { get; set; }

        public bool IsValueSelectionOn { get; set; }

        public event LayoutFieldSelectionHandler LayoutFieldSelectionChanged;

        #region Constructor

        public event EventHandler DeleteRequested;

        public RelayCommand Delete { get; set; }
        public ICommand SetSelection { get; set; }

        public LayoutField()
        {
            _userdefinedRegionType = UserDefinedRegionType.None;
            ItemBackground = FieldTree.NormalColorBrush;
            Delete = new RelayCommand(onDeleteRequested);
            SetSelection = new RelayCommand(setSelection);
        }

        #endregion Constructor

        #region Public Functionality

        public event PropertyChangedEventHandler PropertyChanged;

        public void ResetDirtyFlag()
        {
            IsDirty = false;
        }

        #endregion Public Functionality

        #region Internal Functionality

        private void onPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void onDeleteRequested(object obj)
        {
            if (null != DeleteRequested)
            {
                DeleteRequested(this, EventArgs.Empty);
            }
        }

        private void setSelection(object obj)
        {
            if (!IsValueSelectionOn && null != LayoutFieldSelectionChanged)
            {
                LayoutFieldSelectionChanged(this, new LayoutFieldSelectionEventArgs(this));
            }
        }

        #endregion Internal Functionality
    }
}