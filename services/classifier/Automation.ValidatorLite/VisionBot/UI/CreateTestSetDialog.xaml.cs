﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

using System;
using System.IO;
using System.Windows;
using Automation.CognitiveData.VisionBot.UI.ViewModels;
using Microsoft.Win32;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for CreateTestSetDialog.xaml
    /// </summary>

    public partial class CreateTestSetDialog : Window
    {
        private string _testName;

        public CreateTestSetDialog(DocSetItemViewModel docSetItemViewModel)
        {
            _testName = docSetItemViewModel.Name;
            InitializeComponent();
            DataContext = docSetItemViewModel;
            docSetItemViewModel.Validated += docSetItemViewModel_Validated;
        }

        void docSetItemViewModel_Validated(object sender, EventArgs e)
        {
            var filePath = SelectFile();

            if (string.IsNullOrWhiteSpace(filePath))
            {
                return;
            }

            var docSetItemViewModel = DataContext as DocSetItemViewModel;
            if (docSetItemViewModel != null)
            {
                docSetItemViewModel.DocumentPath = filePath;
                docSetItemViewModel.DocumentName = Path.GetFileName(filePath);
                LayoutTestSetItem = new LayoutTestSetItem(docSetItemViewModel.GetDocSetItem(),
                    docSetItemViewModel.Parent);
                docSetItemViewModel.Validated -= docSetItemViewModel_Validated;
                DialogResult = true;
                Close();
            }
        }

        private static string SelectFile()
        {
            var dlg = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false,
                Filter = FileSelectionDialog.Filter
            };
            return (true == dlg.ShowDialog()) ? dlg.FileName : string.Empty;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            LayoutTestSetItem = new NullLayoutTestSetItem();
            DialogResult = false;
            Close();
        }

        public LayoutTestSetItem LayoutTestSetItem { get; private set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Left = Left - 10;

            if (!string.IsNullOrWhiteSpace(_testName))
                textLayoutName.Text = _testName;

            textLayoutName.Focus();
            textLayoutName.CaretIndex = textLayoutName.Text.Length;
        }
    }

}
