﻿using Automation.VisionBotEngine.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automation.CognitiveData.VisionBot.UI
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
            Text = FormText;
            _visionBotManifest = new VisionBotManifest();
        }

        void rbtLayout_CheckedChanged(object sender, EventArgs e)
        {
            pnlLayout.Visible = true;
            pnlTemplate.Visible = false;

            bindLayout();
        }

        private void bindLayout()
        {
            var binding = new BindingSource();
            if (rbtLayoutTable.Checked)
                binding.DataSource = _visionBot.Layouts[0].Tables[0].Columns;
            else if (rbtLayoutMarker.Checked)
                binding.DataSource = _visionBot.Layouts[0].Identifiers;
            else
                binding.DataSource = _visionBot.Layouts[0].Fields;
            dataGridView1.DataSource = binding;
        }

        void rbtTemplate_CheckedChanged(object sender, EventArgs e)
        {
            pnlLayout.Visible = false;
            pnlTemplate.Visible = true;

            bindTemplate();
        }

        private void bindTemplate()
        {
            var binding = new BindingSource();
            if (rbtTemplateTable.Checked)
                binding.DataSource = _visionBot.DataModel.Tables[0].Columns;
            else
                binding.DataSource = _visionBot.DataModel.Fields;
            dataGridView1.DataSource = binding;
        }


        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count >= 0)
            {
                int selectedItemIndex = dataGridView1.SelectedRows[0].Index;
                if (rbtLayout.Checked)
                {
                    if (rbtLayoutMarker.Checked)
                        _visionBot.Layouts[0].Identifiers.RemoveAt(selectedItemIndex);
                    if (rbtLayoutTable.Checked)
                        _visionBot.Layouts[0].Tables[0].Columns.RemoveAt(selectedItemIndex); 
                    else
                        _visionBot.Layouts[0].Fields.RemoveAt(selectedItemIndex); 
                    bindLayout();
                }
                else
                {
                    if (rbtTemplateTable.Checked)
                        _visionBot.DataModel.Tables[0].Columns.RemoveAt(selectedItemIndex);
                    else
                        _visionBot.DataModel.Fields.RemoveAt(selectedItemIndex);
                    bindTemplate();
                }
            }
        }

        private void btnAddField_Click(object sender, EventArgs e)
        {
            int rowCount = dataGridView1.RowCount;
            dataGridView1.DataSource = null;
            if (rbtTemplate.Checked)
            {
                dataGridView1.DataSource = addToTemplate(rowCount);
            }
            else
            {
                 dataGridView1.DataSource = addToLayout(rowCount);
            }
        }

        private BindingSource addToLayout(int rowCount)
        {
            FieldLayout field = new FieldLayout
            {
                Id = Guid.NewGuid().ToString(),
                Bounds = Rectangle.Empty, 
                FieldDirection = VisionBotEngine.FieldDirection.Right, 
                FormatExpression = string.Empty, 
                IsMultiline = false, 
                Label = "Sample", 
                MergeRatio = 1.0f, 
                FieldId = string.Empty
            };
            var binding = new BindingSource();
            
            if (rbtLayoutMarker.Checked)
            {
                MarkerLayout marker = new MarkerLayout();
                marker.Id = string.Format("{0}{1}{2}", "MARKER", "_ID_", getNewId(rowCount));
                marker.Bounds = Rectangle.Empty;
                marker.Label = "Sample";
                marker.MarkerType = VisionBotEngine.MarkerType.Text;
                marker.MergeRatio = 1.0f;
                marker.Name = string.Format("{0}{1}{2}", "MARKER", "_ID_", getNewId(rowCount));
                marker.SimilarityFactor = 0.7f;
                _visionBot.Layouts[0].Identifiers.Add(marker);
                binding.DataSource = _visionBot.Layouts[0].Identifiers;
            }
            else if (rbtLayoutTable.Checked)
            {
                field.Id = string.Format("{0}{1}{2}", "TABLE", "_ID_", getNewId(rowCount));
                _visionBot.Layouts[0].Tables[0].Columns.Add(field);
                binding.DataSource = _visionBot.Layouts[0].Tables[0].Columns;
            }
            else
            {
                field.Id = string.Format("{0}{1}{2}", "FIELD", "_ID_", getNewId(rowCount));
                _visionBot.Layouts[0].Fields.Add(field);
                binding.DataSource = _visionBot.Layouts[0].Fields;
            }
            return binding;
        }

        private BindingSource addToTemplate(int rowCount)
        {
            VisionBotEngine.Model.FieldDef field = new VisionBotEngine.Model.FieldDef();
            field.Name = "Test";
            field.Formula = "1+2";
            field.ValueType = VisionBotEngine.FieldValueType.Text;
            var binding = new BindingSource();
            if (rbtTemplateTable.Checked)
            {
                field.Id = string.Format("{0}{1}{2}", "TABLE", "_ID_", getNewId(rowCount));
                _visionBot.DataModel.Tables[0].Columns.Add(field);
                binding.DataSource = _visionBot.DataModel.Tables[0].Columns;
            }
            else
            {
                field.Id = string.Format("{0}{1}{2}", "FIELD", "_ID_", getNewId(rowCount));
                _visionBot.DataModel.Fields.Add(field);
                binding.DataSource = _visionBot.DataModel.Fields;
            }
            return binding;
        }

        private string getNewId(int rowCount)
        {
            return Convert.ToString(rowCount + 1);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_visionBotManifest != null && _visionBot != null)
            {
                IVisionBotManager visionBotManager = new VisionBotManager();
                visionBotManager.SaveVisionBot(_visionBotManifest, _visionBot);
                this.Close();
            }
           
        }

        private void openVisionBot(IVisionBotManager visionBotManager)
        {
            if (File.Exists(Path.Combine(_visionBotManifest.ParentPath, _visionBotManifest.Name)))
                _visionBot = visionBotManager.LoadVisionBot(_visionBotManifest);
            else
                _visionBot = visionBotManager.CreateVisionBot();
        }

        private void loadVisionBot()
        {
            IVisionBotManager visionBotManager = new VisionBotManager();
            openVisionBot(visionBotManager);
            _visionBotManifest.Id = _visionBot.Id;
            setTitle();
            var binding = new BindingSource();
            binding.DataSource = _visionBot.DataModel.Fields;
            dataGridView1.DataSource = binding;

            if (_visionBot.DataModel.Tables.Count == 0)
                _visionBot.DataModel.Tables.Add(new VisionBotEngine.Model.TableDef { Id = "Test", Name = "Test", Columns = new List<VisionBotEngine.Model.FieldDef>() });
            if (_visionBot.Layouts.Count == 0)
                _visionBot.Layouts.Add(new VisionBotEngine.Model.Layout());

            if (_visionBot.Layouts[0].Tables.Count == 0)
                _visionBot.Layouts[0].Tables.Add(new TableLayout { Columns = new List<FieldLayout>(), Id = "Test" });
            //fieldsView2.View.DataSource = _visionBot.Template.Tables[0].Columns;
        }
        const string FormText = "VisionBot Designer";
        const string FormTextFormat = "{0} - {1}";
        const string UnsavedVisionBotText = "Untitled VisionBot.vbot";
        private void setTitle()
        {
            if (string.IsNullOrEmpty(_visionBotManifest.Name))
                Text = string.Format(FormTextFormat, UnsavedVisionBotText, FormText);
            else
                Text = string.Format(FormTextFormat, _visionBotManifest.Name, FormText);
        }

        VisionBotEngine.Model.VisionBot _visionBot;
        VisionBotManifest _visionBotManifest;

        public string DataStoragePath
        {
            get { return _visionBotManifest.ParentPath; }
            set { _visionBotManifest.ParentPath = value; }
        }
       
        public string VisionBotName 
        {
            get { return _visionBotManifest.Name; }
            set {
                if (!Path.HasExtension(value))
                {
                    _visionBotManifest.Name = value + ".vbot";
                }
                else
                {
                    _visionBotManifest.Name = value;
                }
            }
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            loadVisionBot();
        }

        private BindingSource getBindingSource(object bindingSource)
        {
            var binding = new BindingSource();
            binding.DataSource = bindingSource;
            return binding;
        }

        private void rbtTemplateFields_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtTemplateFields.Checked)
                dataGridView1.DataSource = getBindingSource(_visionBot.DataModel.Fields);
        }

        private void rbtTemplateTable_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtTemplateTable.Checked)
                dataGridView1.DataSource = getBindingSource(_visionBot.DataModel.Tables[0].Columns);
        }

        private void rbtLayoutField_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtLayoutField.Checked)
                dataGridView1.DataSource = getBindingSource(_visionBot.Layouts[0].Fields);
        }

        private void rbtLayoutTable_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtLayoutTable.Checked)
                dataGridView1.DataSource = getBindingSource(_visionBot.Layouts[0].Tables[0].Columns);
        }

        private void rbtLayoutMarker_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtLayoutMarker.Checked)
                dataGridView1.DataSource = getBindingSource(_visionBot.Layouts[0].Identifiers);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
