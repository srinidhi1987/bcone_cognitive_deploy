﻿using System.Windows;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ProgressWindow : Window
    {
        public ProgressWindow()
        {
            InitializeComponent();
        }

        public delegate void FromClose();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left = this.Left - 10;
        }
    }
}
