/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using System.ComponentModel;
    using System.Windows.Media;

    public class ButtonModel : INotifyPropertyChanged
    {
        public string Caption { get; set; }
        public SolidColorBrush Foreground { get; set; }
        public bool IsDefault { get; set; }
        public bool IsCancel { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}