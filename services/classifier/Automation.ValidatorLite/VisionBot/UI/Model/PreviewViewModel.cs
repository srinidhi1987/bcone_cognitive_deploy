﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.ConfidenceScore;
    using Automation.CognitiveData.VisionBot.UI.Model;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using Extensions;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;

    public class PreviewViewModel : ViewModelBase
    {
        private bool _isEnabled = true;

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                OnPropertyChanged(nameof(IsEnabled));
            }
        }

        private bool _isEditable;

        public delegate void EditChangedHandler(object sender, EventArgs arg);

        public event EditChangedHandler EditChanged;

        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                _isEditable = value;
                OnPropertyChanged(nameof(IsEditable));
                if (EditChanged != null)
                {
                    EditChanged(this, EventArgs.Empty);
                }
            }
        }

        private ObservableCollection<PreviewFieldModel> _previewFields = new ObservableCollection<PreviewFieldModel>();

        public ObservableCollection<PreviewFieldModel> PreviewFields
        {
            get { return _previewFields; }
            set
            {
                if (_previewFields != value)
                {
                    _previewFields = value;
                    OnPropertyChanged("PreviewFields");
                }
            }
        }

        private ObservableCollection<PreviewTableModel> _previewTables;

        public ObservableCollection<PreviewTableModel> PreviewTables
        {
            get { return _previewTables; }
            set
            {
                if (_previewTables != value)
                {
                    _previewTables = value;
                    OnPropertyChanged("PreviewTables");
                }
            }
        }

        public bool IsDataExtractedSuccessfully
        {
            get
            {
                return checkForValidDataExtraction();
            }
        }

        private bool checkForValidDataExtraction()
        {
            bool isDataExtractedSuccessfully = true;

            if (_previewFields != null)
            {
                foreach (PreviewFieldModel fieldModel in _previewFields)
                {
                    if (fieldModel.Value is ValidatedFieldValue)
                    {
                        ValidatedFieldValue validatedFieldValue = (ValidatedFieldValue)fieldModel.Value;

                        if (isDataExtractedSuccessfully)
                        {
                            isDataExtractedSuccessfully = (validatedFieldValue.FieldDataValidationIssue == null
                                ? true
                                : !validatedFieldValue.FieldDataValidationIssue.IssueCode.Equals(FieldDataValidationIssueCodes.MissingRequiredFieldValue));
                        }
                    }
                }

                foreach (PreviewTableModel tableModel in _previewTables)
                {
                    DataTable table = tableModel.DataTable;
                    if (table.Columns != null)
                    {
                        if (table.Rows != null)
                        {
                            for (var i = 0; i < table.Rows.Count; i++)
                            {
                                for (var j = 0; j < table.Columns.Count; j++)
                                {
                                    if (table.Rows[i][j] != null)
                                    {
                                        ValidatedFieldValue validatedFieldValue = table.Rows[i][j] as ValidatedFieldValue;

                                        if (validatedFieldValue != null)
                                        {
                                            if (isDataExtractedSuccessfully)
                                                isDataExtractedSuccessfully = (validatedFieldValue.FieldDataValidationIssue == null
                                                    ? true
                                                    : !validatedFieldValue.FieldDataValidationIssue.IssueCode.Equals(FieldDataValidationIssueCodes.MissingRequiredFieldValue));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return isDataExtractedSuccessfully;
        }

        public void ArrangePreview(DataModel iqbotDataModel)
        {
            try
            {
                arrangeFields(iqbotDataModel.Fields);
                arrangeTables(iqbotDataModel.Tables);
            }
            catch (Exception ex)
            {
                ex.Log(nameof(PreviewViewModel), nameof(ArrangePreview));
            }
        }

        private void arrangeFields(ObservableCollection<FieldDef> blueprintFieldList)
        {
            int indexToMove = 0;
            for (int blueprintIndex = 0; blueprintIndex < blueprintFieldList.Count; blueprintIndex++)
            {
                for (int layoutIndex = 0; layoutIndex < PreviewFields.Count; layoutIndex++)
                {
                    if (blueprintFieldList[blueprintIndex].Id == PreviewFields[layoutIndex].FieldDef.Id)
                    {
                        if (layoutIndex != indexToMove)
                        {
                            PreviewFields.Move(layoutIndex, indexToMove);
                        }
                        indexToMove++;
                        break;
                    }
                }
            }
        }

        private void arrangeTables(ObservableCollection<TableDef> blueprintTableList)
        {
            int indexToMove = 0;
            for (int blueprintTableIndex = 0; blueprintTableIndex < blueprintTableList.Count; blueprintTableIndex++)
            {
                for (int layoutTableIndex = 0; layoutTableIndex < PreviewTables.Count; layoutTableIndex++)
                {
                    if (blueprintTableList[blueprintTableIndex].Id == PreviewTables[layoutTableIndex].DataTable.ExtendedProperties["TableId"].ToString())
                    {
                        if (layoutTableIndex != indexToMove)
                        {
                            PreviewTables.Move(layoutTableIndex, indexToMove);
                        }
                        indexToMove++;
                        break;
                    }
                }
            }

            for (int blueprintTableIndex = 0; blueprintTableIndex < blueprintTableList.Count; blueprintTableIndex++)
            {
                for (int layoutTableIndex = 0; layoutTableIndex < PreviewTables.Count; layoutTableIndex++)
                {
                    if (blueprintTableList[blueprintTableIndex].Id == PreviewTables[layoutTableIndex].DataTable.ExtendedProperties["TableId"].ToString())
                    {
                        IList<FieldDef> blueprintColList = blueprintTableList[blueprintTableIndex].Columns;
                        IList<string> blueprintColListId = blueprintColList.Select(d => d.Id).ToList();
                        DataColumnCollection col = PreviewTables[layoutTableIndex].DataTable.Columns;
                        for (int blueprintColIndex = 0; blueprintColIndex < blueprintColList.Count; blueprintColIndex++)
                        {
                            for (int layoutColIndex = 0; layoutColIndex < col.Count; layoutColIndex++)
                            {
                                if (blueprintColList[blueprintColIndex].Id == col[layoutColIndex].ColumnName)
                                {
                                    PreviewTables[layoutTableIndex].DataTable.SetColumnsOrder(blueprintColListId);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}