﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot.UI.Model
{
    using Automation.CognitiveData.ConfidenceScore;
    using Automation.CognitiveData.ConfidenceScore.Validation;
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Windows;
    using System.Windows.Input;

    public class DocSetItem
    {
        internal UIImageProvider ImageProvider { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string DocumentPath { get; set; }

        public string ImagePath { get; set; }

        public bool IsSelected { get; set; }

        public bool IsBenchMarkDataSaved { get; set; }

        public bool IsDataExtractedSuccessfully { get; set; } = true;

        public ObservableCollection<LayoutField> SystemIdentifiedFields { get; set; }

        public ObservableCollection<DocSetItemValueRegion> FieldValueRegions { get; set; }

        public ObservableCollection<DocSetItemValueRegion> ColumnValueRegions { get; set; }

        public string DocumentName { get; set; }

        public DocumentProperties DocumentProperties { get; set; }

        public DataTable Fields { get; set; }
        public DataSet TableSet { get; set; }

        private DataTable _previewFields;

        public DataTable PreviewFields
        {
            get
            {
                if (_previewFields == null)
                {
                    fillPreviewFields();
                }

                return _previewFields;
            }
            set { _previewFields = value; }
        }

        private DataSet _previewTableSet;

        public DataSet PreviewTableSet
        {
            get
            {
                if (_previewTableSet == null)
                {
                    fillPreviewTableSet();
                }
                return _previewTableSet;
            }
            set { _previewTableSet = value; }
        }

        public bool OriginalIsBenchmarchStructureFound { get; set; }
        public bool IsBenchmarkStructureMismatchFound { get; set; }

        public void ResetPreviewData()
        {
            PreviewFields = null;
            PreviewTableSet = null;
        }

        public void AssignModifiedPreviewDataToOriginalData()
        {
            Fields = PreviewFields;
            TableSet = PreviewTableSet;
            ResetPreviewData();
        }

        public void FillRegions()
        {
            FillRegions(Fields, TableSet);
        }

        private void fillPreviewFields()
        {
            if (Fields != null)
            {
                IsDataExtractedSuccessfully = true;
                _previewFields = copyDataTable(Fields);
            }
            else
            {
                _previewFields = Fields;
            }
        }

        private void fillPreviewTableSet()
        {
            if (TableSet != null)
            {
                _previewTableSet = copyTableSet(TableSet);
            }
            else
            {
                _previewTableSet = TableSet;
            }
        }

        private DataSet copyTableSet(DataSet source)
        {
            DataSet TableSet = new DataSet("Tables");
            if (source.Tables != null)
            {
                for (int i = 0; i < source.Tables.Count; i++)
                {
                    DataTable dataTable = copyDataTable(source.Tables[i]);
                    dataTable.TableName = source.Tables[i].TableName;
                    TableSet.Tables.Add(dataTable);
                }
                TableSet.AcceptChanges();
            }

            return TableSet;
        }

        private DataTable copyDataTable(DataTable source)
        {
            DataTable destination = new DataTable();
            if (source.Columns != null)
            {
                for (var i = 0; i < source.Columns.Count; i++)
                {
                    DataColumn column = new DataColumn(source.Columns[i].ColumnName, typeof(ValidatedFieldValue));
                    column.Caption = source.Columns[i].Caption;
                    destination.Columns.Add(column);
                }

                if (source.Rows != null)
                {
                    for (var i = 0; i < source.Rows.Count; i++)
                    {
                        DataRow dataRow = destination.NewRow();

                        for (var j = 0; j < source.Columns.Count; j++)
                        {
                            if (source.Rows[i][j] != null)
                            {
                                ValidatedFieldValue validatedFieldValue = source.Rows[i][j] as ValidatedFieldValue;

                                if (validatedFieldValue != null)
                                {
                                    dataRow[j] = validatedFieldValue.Clone();

                                    if (this.IsDataExtractedSuccessfully)
                                        this.IsDataExtractedSuccessfully = (validatedFieldValue.FieldDataValidationIssue == null
                                            ? true
                                            : !validatedFieldValue.FieldDataValidationIssue.IssueCode.Equals(FieldDataValidationIssueCodes.MissingRequiredFieldValue));
                                }
                                else
                                {
                                    dataRow[j] = new ValidatedFieldValue();
                                }
                            }
                            else
                            {
                                dataRow[j] = null;
                            }
                        }

                        destination.Rows.Add(dataRow);
                    }
                }
            }

            return destination;
        }

        private void FillRegions(DataTable fields, DataSet tables)
        {
            FieldValueRegions = new ObservableCollection<DocSetItemValueRegion>(fillFieldValueRegions(fields));
            ColumnValueRegions = new ObservableCollection<DocSetItemValueRegion>(fillColumnValueRegions(tables));
        }

        private List<DocSetItemValueRegion> fillColumnValueRegions(DataSet tableSet)
        {
            List<DocSetItemValueRegion> regions = new List<DocSetItemValueRegion>();
            if (tableSet != null && tableSet.Tables != null)
            {
                for (int i = 0; i < tableSet.Tables.Count; i++)
                {
                    regions.AddRange(fillFieldValueRegions(tableSet.Tables[i]));
                }
            }

            return regions;
        }

        private List<DocSetItemValueRegion> fillFieldValueRegions(DataTable dataTable)
        {
            List<DocSetItemValueRegion> regions = new List<DocSetItemValueRegion>();
            if (dataTable != null && dataTable.Rows != null && dataTable.Columns != null)
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        ValidatedFieldValue fieldValue = dataTable.Rows[i][j] as ValidatedFieldValue;
                        if (fieldValue != null)
                        {
                            DocSetItemValueRegion region = new DocSetItemValueRegion();
                            region.Bounds = fieldValue.Bounds;
                            region.FieldDefID = fieldValue.FieldDefId;
                            region.Type = fieldValue.Type;
                            region.Value = fieldValue.Value;
                            regions.Add(region);
                        }
                    }
                }
            }

            return regions;
        }
    }

    public class DocSetItemValueRegion : IRegion
    {
        private string _fieldDefID;

        public string FieldDefID
        {
            get { return _fieldDefID; }
            set { _fieldDefID = value; }
        }

        public Rect Bounds
        {
            get;
            set;
        }

        public ElementType Type
        {
            get;
            set;
        }

        public bool IsSelected
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public ICommand SetSelection { get; set; }
    }

    //public class BenchmarkData
    //{
    //    public DataTable Fields { get; set; }
    //    public DataSet TableSet { get; set; }

    //    public BenchmarkData()
    //    {
    //        Fields = new DataTable();
    //        TableSet = new DataSet();
    //    }
    //}

    public class LayoutTestSet
    {
        public string LayoutId { get; set; }

        public string LayoutName { get; set; }

        public List<DocSetItem> DocSetItems { get; set; }
    }

    //#region BluePrint

    //public class TestDataViewModel
    //{
    //    public ObservableCollection<PreviewFieldModel> PreviewFields { get; set; }

    //    public ObservableCollection<PreviewTableModel> PreviewTables { get; set; }
    //}

    //#endregion

    #region TestResult

    internal class DocSetResultModel
    {
        public int FailedFieldsCount { get; set; }

        public float ConfidenceScore { get; set; }

        public List<ComparisonError> comparisonErrors { get; set; }
    }

    internal class LayoutClassificationResultModel
    {
        public string LayoutId { get; set; }

        public string Error { get; set; }
    }

    internal class TestBotResultViewModel
    {
        public DocSetResultModel docSetResult { get; set; }

        public LayoutClassificationResultModel layoutClassificationResult { get; set; }
    }

    #endregion TestResult
}