﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Automation.CognitiveData.VisionBot.UI
{
    public class FieldValue : INotifyPropertyChanged
    {
        public string Value { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
