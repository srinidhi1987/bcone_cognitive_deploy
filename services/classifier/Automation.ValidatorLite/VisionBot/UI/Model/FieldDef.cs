﻿using Automation.VisionBotEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.VisionBotEngine.Validation;

    public class FieldDef : INotifyPropertyChanged
    {
        #region Properties

        private string _id;
        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
                IsDirty = true;
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
                IsDirty = true;
            }
        }

        private FieldValueType _valueType;
        public FieldValueType ValueType
        {
            get { return _valueType; }
            set
            {
                _valueType = value;
                NotifyPropertyChanged("ValueType");
                IsDirty = true;
            }
        }

        private string _formula;
        public string Formula
        {
            get { return _formula; }
            set
            {
                _formula = value;
                NotifyPropertyChanged("Formula");
                IsDirty = true;
            }
        }

        private StaticListProvider _staticListProvider;
        public StaticListProvider StaticListProvider
        {
            get
            {
                return _staticListProvider;
            }
            set
            {
                _staticListProvider = value;
                NotifyPropertyChanged("StaticListItems");
            }
        }

        private string _filePathForList;

        public string FilePathForList
        {
            get
            {
                return _filePathForList;
            }
            set
            {
                _filePathForList = value;
                FileNameForList = System.IO.Path.GetFileName(_filePathForList);
                NotifyPropertyChanged("FilePathForList");
                IsDirty = true;

                
            }
        }

        private string _fileNameForList;

        public string FileNameForList
        {
            get
            {
                return _fileNameForList;
            }
            private set
            {
                _fileNameForList = value;
                NotifyPropertyChanged("FileNameForList");
            }
        }

        public string StaticListItems
        {
            get
            {
                return this.StaticListProvider != null
                    ? this.StaticListProvider.GetSingleStringForItems()
                    : string.Empty;
            }
            set
            {
                if (this.StaticListProvider == null && string.IsNullOrWhiteSpace(value))
                {
                    return;
                }

                if (this.StaticListProvider == null)
                {
                    this.StaticListProvider = new StaticListProvider();
                }

                if (this.StaticListProvider.GetSingleStringForItems() != value)
                {
                    this.StaticListProvider.SetItemsFromSingleString(value);

                    NotifyPropertyChanged("StaticListItems");
                    IsDirty = true;
                }
            }
        }

        private string _validationID;
        public string ValidationID
        {
            get { return _validationID; }
            set
            {
                _validationID = value;
                NotifyPropertyChanged("ValidationID");
                IsDirty = true;
            }
        }

        private bool _isRequired;
        public bool IsRequired
        {
            get { return _isRequired; }
            set
            {
                _isRequired = value;
                NotifyPropertyChanged("IsRequired");
                IsDirty = true;
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                    if (_isSelected)
                    {
                        onSelectionChanged();
                        onExpandChanged();
                    }
                }
            }
        }

        private string _defaultValue;
        public string DefaultValue
        {
            get { return _defaultValue; }
            set
            {
                if (_defaultValue != value)
                {
                    _defaultValue = value;
                    NotifyPropertyChanged("DefaultValue");
                    IsDirty = true;
                }
            }
        }

        private bool _isDirty;
        public bool IsDirty
        {
            get { return _isDirty; }
            private set
            {
                if (_isDirty != value)
                {
                    _isDirty = value;
                    NotifyPropertyChanged("IsDirty");
                }
            }
        }

        private string _formatExpression;
        public string FormatExpression
        {
            get { return _formatExpression; }
            set
            {
                if (_formatExpression != value)
                {
                    _formatExpression = value;
                    NotifyPropertyChanged("FormatExpression");
                    IsDirty = true;
                }
            }
        }

        private string _startsWith;
        public string StartsWith
        {
            get { return _startsWith; }
            set
            {
                if (_startsWith != value)
                {
                    _startsWith = value;
                    NotifyPropertyChanged("StartsWith");
                    IsDirty = true;
                }
            }
        }

        private string _endsWith;
        public string EndsWith
        {
            get { return _endsWith; }
            set
            {
                if (_endsWith != value)
                {
                    _endsWith = value;
                    NotifyPropertyChanged("EndsWith");
                    IsDirty = true;
                }
            }
        }

        public RelayCommand Delete { get; set; }

        #endregion

        #region Constructor

        public FieldDef()
        {
            Id = string.Empty;
            Name = string.Empty;
            ValueType = FieldValueType.Text;
            IsRequired = true;
            Formula = string.Empty;
            ValidationID = string.Empty;
            DefaultValue = string.Empty;
            IsDirty = false;

            Delete = new RelayCommand(onDeleteRequested);
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler DeleteRequested;
        public event EventHandler SelectionChanged;
        public event EventHandler ExpandChanged;

        #endregion

        #region On Event

        private void onDeleteRequested(object obj)
        {
            if (null != DeleteRequested)
            {
                DeleteRequested(this, EventArgs.Empty);
            }
        }
        private void onSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void onExpandChanged()
        {
            if (ExpandChanged != null)
            {
                ExpandChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Public Functionality

        public void ResetDirtyFlag()
        {
            IsDirty = false;
        }

        #endregion

        #region Internal Functionality

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
