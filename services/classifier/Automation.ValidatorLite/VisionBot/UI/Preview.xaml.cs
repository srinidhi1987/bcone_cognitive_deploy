﻿using System.Windows;
using System.Windows.Controls;

namespace Automation.CognitiveData.VisionBot.UI
{
    /// <summary>
    /// Interaction logic for Preview.xaml
    /// </summary>
    public partial class Preview : UserControl
    {
        internal const string ReadOnlyFieldBoxKey = "ReadOnlyFieldBox";
        internal const string ReadOnlyTableBoxKey = "ReadOnlyTableBox";
        internal const string EditableFieldBoxKey = "EditableFieldBox";
        internal const string EditableTableBoxKey = "EditableTableBox";

        public static readonly RoutedEvent PreviewCloseEvent = EventManager.RegisterRoutedEvent(
            "PreviewClose", // Event name
            RoutingStrategy.Bubble, // Bubble means the event will bubble up through the tree
            typeof(RoutedEventHandler), // The event type
            typeof(Preview));

        public Preview()
        {
            InitializeComponent();
            this.FieldsBox.DataItemTemplate = this.FindResource(ReadOnlyFieldBoxKey) as DataTemplate;
            this.TablesBox.DataItemTemplate = this.FindResource(ReadOnlyTableBoxKey) as DataTemplate;
        }

        public event RoutedEventHandler PreviewClose
        {
            add { AddHandler(PreviewCloseEvent, value); }
            remove { RemoveHandler(PreviewCloseEvent, value); }
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e)
        {
            var newEventArgs = new RoutedEventArgs(PreviewCloseEvent);
            RaiseEvent(newEventArgs);
        }

        public bool IsEditable
        {
            get { return (bool)GetValue(IsEditableProperty); }
            set { SetValue(IsEditableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsEditable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEditableProperty =
            DependencyProperty.Register("IsEditable", typeof(bool), typeof(Preview), 
            new PropertyMetadata(false, new PropertyChangedCallback(onIsEditableChanged)));

        private static void onIsEditableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var preview = d as Preview;
            if (preview != null)
            {
                if (true.Equals(e.NewValue))
                {
                    preview.FieldsBox.DataItemTemplate = preview.FindResource(EditableFieldBoxKey) as DataTemplate;
                    preview.TablesBox.DataItemTemplate = preview.FindResource(EditableTableBoxKey) as DataTemplate;
                }
                else
                {
                    preview.FieldsBox.DataItemTemplate = preview.FindResource(ReadOnlyFieldBoxKey) as DataTemplate;
                    preview.TablesBox.DataItemTemplate = preview.FindResource(ReadOnlyTableBoxKey) as DataTemplate;
                }
            }
        }

        
    }
}
