﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Automation.CognitiveData.VisionBot.UI.Adorners
{
    class ControlHostAdorner : Adorner
    {
        private Control _child;

        public ControlHostAdorner(UIElement adornedElement, Control child)
            : base(adornedElement)
        {
            _child = child;
            AddVisualChild(_child);
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            if (index != 0) throw new ArgumentOutOfRangeException();
            return _child;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            _child.Measure(constraint);
            return _child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            _child.Arrange(new Rect(finalSize));
            //return new Size(_child.ActualWidth, _child.ActualHeight);
            return finalSize;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            System.Diagnostics.Debug.WriteLine("#############Render Called");
            _child.Margin = new Thickness((this.AdornedElement as UserControl).ActualWidth / 2, (this.AdornedElement as UserControl).ActualHeight / 2, 0, 0);            
            base.OnRender(drawingContext);
        }
    }
}
