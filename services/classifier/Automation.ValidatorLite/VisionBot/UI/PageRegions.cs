﻿/**
* Copyright (c) 2016 Automation Anywhere.
* 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
* All rights reserved.
*
* This software is the confidential and proprietary information of
* Automation Anywhere.("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Automation Anywhere.
*/

namespace Automation.CognitiveData.VisionBot.UI
{
    using Automation.CognitiveData.VisionBot.UI.ViewModels;
    using System.Collections.Generic;

    internal class PageRegions
    {
        private int _pageIndex;
        private int _pageBottomY;
        private int _chunkSize = 10;

        private IList<IList<RegionViewModel>> _regionsInChunks;
        private IList<RegionViewModel> _currentChunk;

        public int PageIndex
        {
            get
            {
                return this._pageIndex;
            }
        }

        public int PageBottomY
        {
            get
            {
                return this._pageBottomY;
            }
        }

        public int ChunkSize
        {
            get
            {
                return this._chunkSize;
            }
        }

        public IList<IList<RegionViewModel>> RegionsInChunks
        {
            get
            {
                return this._regionsInChunks;
            }
        }

        public PageRegions(int pageIndex, int pageBottomY, int chunkSize)
        {
            this._pageIndex = pageIndex;
            this._pageBottomY = pageBottomY;
            this._chunkSize = chunkSize;

            this._regionsInChunks = new List<IList<RegionViewModel>>(10);
            this._currentChunk = new List<RegionViewModel>(this._chunkSize);
            this._regionsInChunks.Add(this._currentChunk);
        }

        public void Add(RegionViewModel region)
        {
            if (this._currentChunk.Count == this._chunkSize)
            {
                this._currentChunk = new List<RegionViewModel>(this._chunkSize);
                this._regionsInChunks.Add(this._currentChunk);
            }

            this._currentChunk.Add(region);
        }
    }
}