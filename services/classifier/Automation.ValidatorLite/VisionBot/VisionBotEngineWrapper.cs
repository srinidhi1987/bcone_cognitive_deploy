﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.VisionBot
{
    using Automation.CognitiveData.Analytics;
    using Automation.CognitiveData.VBotExecutor;
    using Automation.CognitiveData.VisionBot.UI;
    //using Automation.Integrator.CognitiveDataAutomation;
    using Automation.VisionBotEngine;
    using Automation.VisionBotEngine.Model;
    using Automation.VisionBotEngine.Validation;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    //using Constants = Automation.CognitiveData.Constants;

    //internal class VisionBotEngineWrapper : ICognitiveDataEngine
    //{
    //    //TODO: Temporary diversion for WPF VisionBot UI.
    //    private string _viewMode;

    //    //TODO: Temporary diversion for WPF VisionBot UI.
    //    public VisionBotEngineWrapper(string viewMode)
    //    {
    //        _viewMode = viewMode;
    //    }

    //    public IList<string> GetTemplates(string dataStoragePath)
    //    {
    //        return Directory.GetFiles(dataStoragePath, Constants.CDC.ExtensionFilter)
    //             .Select(Path.GetFileNameWithoutExtension).ToList();
    //    }

    //    public void StartSmartSoftInvoice(string dataStoragePath)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int ExecuteDataProcessing(string templateName, string filePath, string csvFilePath, string dataStoragePath, string taskName, bool isValidatorEnabled)
    //    {
    //        try
    //        {
    //            VBotLogger.Trace(() => String.Format("[ExecuteDataProcessing] Execute {0}  vision bot ", templateName));
    //            IVisionBotManager visionBotManager = new VisionBotManager();
    //            string templateFullPath = IQBotDataPathProvider.GetTemplatePath(templateName);

    //            var manifest = new VisionBotManifest()
    //            {
    //                Name = Path.GetFileNameWithoutExtension(templateFullPath),
    //                ParentPath = Path.GetDirectoryName(templateFullPath)
    //            };

    //            VisionBot visionBot = visionBotManager.LoadVisionBot(manifest);

    //            IQBotDataPathProvider.SetSpecificDataPath(manifest.ParentPath);

    //            VisionBotExecutor visionBotExecutor = new VisionBotExecutor();
    //            return visionBotExecutor.ProcessDocument(visionBot, filePath, csvFilePath, taskName, isValidatorEnabled, templateFullPath) ? 1 : 0;
    //        }
    //        catch (Exception ex)
    //        {
    //            ex.Log(nameof(VisionBotEngineWrapper), nameof(ExecuteDataProcessing), "Execute " + templateName + "for vision bot for file " + filePath);
    //        }
    //        finally
    //        {
    //            IQBotDataPathProvider.ResetSpecificDataPath();
    //        }
    //        return 0;
    //    }

    //    public int CreateNewTemplate(string templateName, string dataStoragePath)
    //    {
    //        //TODO: Temporary diversion for WPF VisionBot UI.
    //        openDesignerUI(templateName, dataStoragePath);
    //        return 1;
    //    }

    //    //TODO: Temporary diversion for WPF VisionBot UI.
    //    private void openDesignerUI(string templateName, string dataStoragePath)
    //    {
    //        if ("1.1".Equals(_viewMode))
    //        {
    //            openWpfUI(templateName, dataStoragePath);
    //        }
    //        else
    //        {
    //            openGridUI(templateName, dataStoragePath);
    //        }
    //    }

    //    //TODO: Temporary diversion for WPF VisionBot UI.
    //    private void openWpfUI(string templateName, string dataStoragePath)
    //    {
    //        //var visionBotInfo = new VisionBotUIManager(templateName, dataStoragePath);
    //        //visionBotInfo.ShowModalDialog();
    //    }

    //    //TODO: Temporary diversion for WPF VisionBot UI.
    //    private void openGridUI(string templateName, string dataStoragePath)
    //    {
    //        //using (var testForm = new TestForm())
    //        //{
    //        //    testForm.DataStoragePath = dataStoragePath;
    //        //    testForm.VisionBotName = templateName;
    //        //    testForm.ShowDialog();
    //        //}
    //    }

    //    //TODO: Temporary diversion for WPF VisionBot UI.
    //    public int EditTemplate(string templateName, string dataStoragePath, bool isReadOnlyTemplate)
    //    {
    //        try
    //        {
    //            openDesignerUI(templateName, dataStoragePath);
    //        }
    //        catch (Exception ex)
    //        {
    //            ex.Log(nameof(VisionBotEngineWrapper), nameof(EditTemplate));
    //        }
    //        return 1;
    //    }

    //    public int TrainTemplate(string templateName, string dataStoragePath)
    //    {
    //        System.Windows.Forms.MessageBox.Show("Train VisionBot Using AAEngine : " + templateName);
    //        return 1;
    //    }

    //    public int ShowListConfiguration(string templateName, string dataStoragePath)
    //    {
    //        System.Windows.Forms.MessageBox.Show("ShowListConfiguration of VisionBot Using AAEngine : " + templateName);
    //        return 1;
    //    }

    //    public int ShowListToFieldMappingConfiguration(string templateName, string dataStoragePath)
    //    {
    //        System.Windows.Forms.MessageBox.Show("ShowListToFieldMappingConfiguration of VisionBot Using AAEngine : " + templateName);
    //        return 1;
    //    }

    //    public void SetDefaultTemplate(string templateName, string dataStoragePath)
    //    {
    //        System.Windows.Forms.MessageBox.Show("Train VisionBot Using AAEngine : " + templateName);
    //    }

    //    public bool IsCompatible(string key, Version callerVersion)
    //    {
    //        var selfKey = new IQBotToClientCompatibilityKeyProvider().GetKey();
    //        return selfKey.Equals(key);
    //    }
    //}

    //internal class IQBotToClientCompatibilityKeyProvider : ICompatibilityKeyProvider
    //{
    //    public string GetKey()
    //    {
    //        return "4.0.0-RC1-161007.06";
    //    }
    //}

    public static class ValidationIssueMessageProvider
    {
        public static string ToUserFriendlyString(this ValidationIssue validationIssue)
        {
            switch (validationIssue.IssueCode)
            {
                case FieldDataValidationIssueCodes.MissingRequiredFieldValue:
                    return "Value cannot be empty.";

                case FieldDataValidationIssueCodes.InvalidTextTypeValue:
                    return "Invalid text format.";

                case FieldDataValidationIssueCodes.InvalidDateTypeValue:
                    return "Invalid date format.";

                case FieldDataValidationIssueCodes.InvalidNumberTypeValue:
                    return "Invalid number format.";

                case FieldDataValidationIssueCodes.InvalidStartsWithFieldValue:
                    {
                        var specificIssue = validationIssue as InvalidStartsWithFieldValueIssue;
                        return specificIssue != null
                            ? string.Format("Pattern validation failed. Value doesn't start with '{0}'.", specificIssue.ApplicableStartWith)
                            : "Pattern validation failed. Invalid \"Starts with\" value.";
                    }

                case FieldDataValidationIssueCodes.InvalidEndsWithFieldValue:
                    {
                        var specificIssue = validationIssue as InvalidEndsWithFieldValueIssue;
                        return specificIssue != null
                            ? string.Format("Pattern validation failed. Value doesn't end with '{0}'.", specificIssue.ApplicableEndWith)
                            : "Pattern validation failed. Invalid \"Ends with\" value.";
                    }

                case FieldDataValidationIssueCodes.InvalidFormula:
                    {
                        var specificIssue = validationIssue as InvalidFormulaIssue;
                        return specificIssue != null
                            ? string.Format("Formula validation failed. Either the formula '{0}' or atleast one of its value is invalid.", specificIssue.Formula)
                            : "Formula validation failed. Invalid formula.";
                    }

                case FieldDataValidationIssueCodes.FormulaValueMismatch:
                    {
                        var specificIssue = validationIssue as FormulaValueMismatchIssue;
                        return specificIssue != null
                            ? (specificIssue.CalculatedValue.HasValue
                                ? string.Format("Formula validation failed. Value doesn't match the formula '{0}' which equates to '{1}'.", specificIssue.Formula, specificIssue.CalculatedValue)
                                : string.Format("Formula validation failed. Mismatch in formula '{0}'", specificIssue.Formula))
                            : "Formula validation failed. Formula value mismatch.";
                    }

                case FieldDataValidationIssueCodes.ValueNotInList:
                    return "List validation failed.";

                case FieldDataValidationIssueCodes.PatternMismatch:
                    {
                        var patternMismatchIssue = validationIssue as PatternMismatchIssue;
                        return patternMismatchIssue != null
                            ? string.Format("Pattern validation failed. Value doesn't match the pattern '{0}'.", patternMismatchIssue.ApplicablePattern)
                            : "Pattern validation failed.";
                    }

                case FieldDataValidationIssueCodes.ValidationEngineIssue:
                    return "Unable to validate given formula.";
            }
            return validationIssue.ToString();
        }
    }
}