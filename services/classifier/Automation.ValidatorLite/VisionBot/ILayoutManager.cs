﻿using System;
using System.Collections.Generic;

using Automation.VisionBotEngine.Model;

namespace Automation.CognitiveData.VisionBot
{
    interface ILayoutManager
    {
        IList<Layout> GetAllLayouts();
        Layout GetLayout(string id);
        void AddLayout(Layout layout);
        void UpdateLayout(Layout layout);
        void RemoveLayout(string id);

        event EventHandler LayoutCollectionModified;
    }

    public class LayoutManager : ILayoutManager
    {
        private Automation.VisionBotEngine.Model.VisionBot visionBot;
        public event EventHandler LayoutCollectionModified;

        public LayoutManager(Automation.VisionBotEngine.Model.VisionBot vBot)
        {
            visionBot = vBot;
        }

        public IList<Layout> GetAllLayouts()
        {
            return visionBot.Layouts;
        }

        public Layout GetLayout(string id)
        {
            var layout = visionBot.Layouts.Find(layoutElement => layoutElement.Id == id);
            if (layout == null)
            {
                throw new ArgumentException("Invalid layout identifier");
            }

            return layout;
        }

        public void AddLayout(Layout layout)
        {
            throwExceptionIfLayoutIdisEmpty(layout);

            if (visionBot.Layouts.Find(layoutElement => layoutElement.Name == layout.Name) != null)
            {
                throw new LayoutException(layout, string.Format("Layout '{0}' already exists.", layout.Name));
            }

            visionBot.Layouts.Add(layout);
            raiseLayoutCollectionModified(EventArgs.Empty);
        }

        private void raiseLayoutCollectionModified(EventArgs e)
        {
            if (LayoutCollectionModified != null)
            {
                LayoutCollectionModified(this, e);
            }
        }

        public void UpdateLayout(Layout updatedLayout)
        {
            throwExceptionIfLayoutIdisEmpty(updatedLayout);

            int layoutIndex = visionBot.Layouts.FindIndex(layoutElement => layoutElement.Id == updatedLayout.Id);

            if (layoutIndex > -1)
            {
                visionBot.Layouts[layoutIndex] = updatedLayout;
                raiseLayoutCollectionModified(EventArgs.Empty);
            }
        }

        public void RemoveLayout(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("Layout Id is not Empty. It should have some value");
            }

            int layoutIndex = visionBot.Layouts.FindIndex(layoutElement => layoutElement.Id == id);

            if (layoutIndex > -1)
            {
                visionBot.Layouts.RemoveAt(layoutIndex);
                raiseLayoutCollectionModified(EventArgs.Empty);
            }
        }

        private void throwExceptionIfLayoutIdisEmpty(Layout layout)
        {
            if (string.IsNullOrWhiteSpace(layout.Name))
            {
                throw new LayoutException(layout, string.Format("Layout name can't be blank.", layout.Id));
            }
        }
    }

    public class LayoutException : Exception
    {
        public Layout Layout { get; private set; }

        //public LayoutException(Layout layout)
        //    : base()
        //{
        //    Layout = layout;
        //}

        public LayoutException(Layout layout, string message)
            : base(message)
        {
            Layout = layout;
        }

        //public LayoutException(Layout layout, string message, Exception innerException)
        //    : base(message, innerException)
        //{
        //    Layout = layout;
        //}
    }
}
