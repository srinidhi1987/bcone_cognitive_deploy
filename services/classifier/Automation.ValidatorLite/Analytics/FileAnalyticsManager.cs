﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Analytics
{
    using Automation.Common;
    //using Automation.Integrator.CognitiveDataAutomation.Analytics;
    //using Automation.Integrator.CognitiveDataAutomation.Analytics.Model;
    //using Automation.Util.PropertyManagement;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The file analytics manager.
    /// </summary>
    //[ObfuscationAttribute(Exclude = true, ApplyToMembers = false)]
    //public class FileAnalyticsManager : IAnalyticsManager
    //{
    //    #region Constructor

    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="FileAnalyticsManager"/> class.
    //    /// </summary>
    //    public FileAnalyticsManager()
    //    {
    //    }

    //    #endregion Constructor

    //    #region Public Functionality

    //    /// <summary>
    //    /// Get perticular named Template analytics.
    //    /// </summary>
    //    /// <param name="templateName">
    //    /// IQbot Name
    //    /// </param>
    //    /// <returns>
    //    /// Give Analitics of IQBot
    //    /// </returns>
    //    public TemplateAnalytics GetTemplateAnalytics(string templateName)
    //    {
    //        return this.GetTemplateAnalitycsForFilePath(IQBotDataPathProvider.GetTemplatePath(templateName));
    //    }

    //    /// <summary>
    //    /// Update Template Analytics pass-fail counter.
    //    /// </summary>
    //    /// <param name="templateName">
    //    /// IQBot Name
    //    /// </param>
    //    /// <param name="counterType">
    //    /// Pass-Fail Parameter
    //    /// </param>
    //    public void UpdateTemplateRunCounter(string templateName, CounterType counterType)
    //    {
    //        var templateAnalytics = this.GetTemplateAnalytics(templateName) ?? CreateAnalytics(templateName);
    //        UpdateCounter(templateAnalytics, counterType);
    //    }

    //    /// <summary>
    //    /// Delete Perticuler Template analalitics from Source.
    //    /// </summary>
    //    /// <param name="templateName">
    //    /// IQBot Name
    //    /// </param>
    //    public void DeleteTemplateAnalytics(string templateName)
    //    {
    //        var visionBotAnalytics = this.GetTemplateAnalytics(templateName);
    //        if (visionBotAnalytics == null)
    //        {
    //            return;
    //        }

    //        PropertyManagerHelperFactory
    //            .GetDefaultPropertyManagerHelper()
    //            .DeleteEntity(IQBotDataPathProvider.GetTemplatePath(templateName));
    //    }

    //    /// <summary>
    //    /// Update Template Analytics name.
    //    /// </summary>
    //    /// <param name="oldTemplateName">
    //    /// IQBot old Name
    //    /// </param>
    //    /// <param name="newTemplateName">
    //    /// IQBot new Name
    //    /// </param>
    //    public void UpdateTemplateAnalyticsName(string oldTemplateName, string newTemplateName)
    //    {
    //        PropertyManagerHelperFactory
    //            .GetDefaultPropertyManagerHelper()
    //            .RenameEntity(
    //               IQBotDataPathProvider.GetTemplatePath(oldTemplateName),
    //               IQBotDataPathProvider.GetTemplatePath(newTemplateName));
    //    }

    //    /// <summary>
    //    /// The on analytics changed.
    //    /// </summary>
    //    public event EventHandler OnAnalyticsChanged;

    //    /// <summary>
    //    /// Update Visionbot Pass Fail Counter.
    //    /// </summary>
    //    /// <param name="templateAnalytics">
    //    /// The template Analytics.
    //    /// </param>
    //    public void UpdateRuntimePropertiesForVisionBot(TemplateAnalytics templateAnalytics)
    //    {
    //        try
    //        {
    //            IDictionary<string, object> properties = new Dictionary<string, object>();
    //            properties.Add(Constants.BOT_PASS_COUNTER, templateAnalytics.Pass);
    //            properties.Add(Constants.BOT_FAILED_COUNTER, templateAnalytics.Fail);
    //            PropertyManagerHelperFactory
    //                .GetDefaultPropertyManagerHelper()
    //                .UpdateEntityProperties(IQBotDataPathProvider.GetTemplatePath(templateAnalytics.Name), properties);
    //        }
    //        catch (Exception ex)
    //        {
    //            Log.Write(Log.Modules.Player, Log.LogTypes.ERROR, "UpdateRuntimePropertiesForVisionBot", ex.Message);
    //        }
    //    }

    //    #endregion Public Functionality

    //    #region Internal Functionality

    //    /// <summary>
    //    /// The create analytics.
    //    /// </summary>
    //    /// <param name="templateName">
    //    /// The template name.
    //    /// </param>
    //    /// <returns>
    //    /// The <see cref="TemplateAnalytics"/>.
    //    /// </returns>
    //    private static TemplateAnalytics CreateAnalytics(string templateName)
    //    {
    //        return new TemplateAnalytics { Name = templateName, ID = 0, Fail = 0, Pass = 0 };
    //    }

    //    /// <summary>
    //    /// The update counter.
    //    /// </summary>
    //    /// <param name="visionBotAnalytics">
    //    /// The vision bot analytics.
    //    /// </param>
    //    /// <param name="counterType">
    //    /// The counter type.
    //    /// </param>
    //    /// <exception cref="NotImplementedException">
    //    /// </exception>
    //    private void UpdateCounter(TemplateAnalytics visionBotAnalytics, CounterType counterType)
    //    {
    //        if (visionBotAnalytics == null)
    //        {
    //            return;
    //        }

    //        switch (counterType)
    //        {
    //            case CounterType.Pass:
    //                visionBotAnalytics.Pass++;
    //                break;

    //            case CounterType.Fail:
    //                visionBotAnalytics.Fail++;
    //                break;

    //            default:
    //                throw new NotImplementedException("UpdateCounter not implemented for counterType " + counterType);
    //        }

    //        this.UpdateRuntimePropertiesForVisionBot(visionBotAnalytics);
    //    }

    //    /// <summary>
    //    /// Give All Template analytics Of IQbot folder Path. Sub folder is Not included.
    //    /// </summary>
    //    /// <returns>List of Analytics.</returns>
    //    private List<TemplateAnalytics> GetAllTemplateAnalytics()
    //    {
    //        return Directory
    //            .GetFiles(
    //                CognitiveData.Constants.CDC.ApplicationDataPath,
    //                CognitiveData.Constants.CDC.ExtensionFilter,
    //                SearchOption.TopDirectoryOnly)
    //            .Select(this.GetTemplateAnalitycsForFilePath)
    //            .ToList();
    //    }

    //    /// <summary>
    //    /// Give Analytics of the template
    //    /// </summary>
    //    /// <param name="filePath">
    //    /// </param>
    //    /// <returns>
    //    /// return template analyics.if analytics not found in file then it return 0 pass,and 0 fail counter.
    //    /// </returns>
    //    private TemplateAnalytics GetTemplateAnalitycsForFilePath(string filePath)
    //    {
    //        TemplateAnalytics template = new TemplateAnalytics();
    //        long passCounter = 0;
    //        long failedCounter = 0;

    //        IResult<IDictionary<string, object>> result =
    //            PropertyManagerHelperFactory
    //                .GetDefaultPropertyManagerHelper()
    //                .GetEntityProperties(filePath);

    //        if (result.IsSuccess)
    //        {
    //            if (result.Output.ContainsKey(Constants.BOT_PASS_COUNTER))
    //            {
    //                long.TryParse(result.Output[Constants.BOT_PASS_COUNTER].ToString(), out passCounter);
    //            }

    //            if (result.Output.ContainsKey(Constants.BOT_FAILED_COUNTER))
    //            {
    //                long.TryParse(result.Output[Constants.BOT_FAILED_COUNTER].ToString(), out failedCounter);
    //            }
    //        }

    //        template.Pass = passCounter;
    //        template.Fail = failedCounter;
    //        template.Name = Path.GetFileNameWithoutExtension(filePath);

    //        return template;
    //    }

    //    #endregion Internal Functionality

    //    #region Constant class

    //    /// <summary>
    //    /// The constants.
    //    /// </summary>
    //    private class Constants
    //    {
    //        /// <summary>
    //        /// The bo t_ pas s_ counter.
    //        /// </summary>
    //        public const string BOT_PASS_COUNTER = "PassCounter";

    //        /// <summary>
    //        /// The bo t_ faile d_ counter.
    //        /// </summary>
    //        public const string BOT_FAILED_COUNTER = "FailedCounter";
    //    }

    //    #endregion Constant class
    //}
}