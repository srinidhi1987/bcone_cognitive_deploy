﻿/**
 * Copyright (c) 2016 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Analytics
{
    using Automation.Common;
    using System;
    using System.IO;
    using System.Linq;
    using Constants = Automation.Util.IQBots.Constants;

    internal class IQBotDataPathProvider : IDataPathProvider
    {
        private static string __iqBotDefaultDataPath;
        private static string _iqBotSpecifiedDataPath;

        private static string IQBotDataPath
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_iqBotSpecifiedDataPath)) return _iqBotSpecifiedDataPath;

                if (string.IsNullOrEmpty(__iqBotDefaultDataPath))
                {
                    __iqBotDefaultDataPath = Path.Combine(
                        RoutineProvider.Routine.GetApplicationPath(),
                        RoutineProvider.Routine.GetProductName(),
                        Constants.ROOT_FOLDER_NAME);
                }

                return __iqBotDefaultDataPath;
            }
        }

        internal static void SetSpecificDataPath(string specificDataPath)
        {
            _iqBotSpecifiedDataPath = specificDataPath;
        }

        internal static void ResetSpecificDataPath()
        {
            SetSpecificDataPath(null);
        }

        public string GetPath()
        {
            return IQBotDataPath;
        }

        public static string GetTemplatePath(string templateName)
        {
            if (IsTemplatePath(templateName))
            {
                return templateName;
            }

            return templateName.Contains(Path.DirectorySeparatorChar)
                ? templateName
                : Path.Combine(IQBotDataPath, templateName + Constants.VisionBot.FILE_EXTENSION_WITH_DOT);
        }

        private static bool IsTemplatePath(string value)
        {
            return value.Contains(Path.DirectorySeparatorChar)
                   && value.EndsWith(Util.IQBots.Constants.VisionBot.FILE_EXTENSION_WITH_DOT);
        }
    }
}

namespace Automation.CognitiveData
{
    using Automation.Common;

    internal static class RoutineProvider
    {
        public static IRoutines Routine { get; set; } = new AARoutines();
    }

    internal interface IRoutines
    {
        string GetApplicationPath();

        string GetProductName();
    }

    internal class AARoutines : IRoutines
    {
        public string GetApplicationPath()
        {
            return Routines.GetApplicationPath();
        }

        public string GetProductName()
        {
            return Routines.GetProductName();
        }
    }
}