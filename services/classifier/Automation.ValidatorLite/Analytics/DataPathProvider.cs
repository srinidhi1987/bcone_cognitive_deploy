﻿/**
 * Copyright (c) 2015 Automation Anywhere.
 * 2580 N. 1st Street, Ste. 460,San Jose, CA 95131 USA
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Automation Anywhere.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Automation Anywhere.
 */

namespace Automation.CognitiveData.Analytics
{
    using System;

    using Automation.CognitiveData.Generic;

    public class DataPathProvider : IDataPathProvider
    {
        #region Constructor

        public DataPathProvider(
            IDataPathConfigurationProvider configurationProvider,
            IFileHelper fileHelper,
            IFolderHelper folderHelper)
        {
            if (configurationProvider == null)
            {
                throw new ArgumentNullException("configurationProvider");
            }

            if (fileHelper == null)
            {
                throw new ArgumentNullException("fileHelper");
            }

            if (folderHelper == null)
            {
                throw new ArgumentNullException("folderHelper");
            }

            this.ConfigurationProvider = configurationProvider;
            this.FolderHelper = folderHelper;
            this.FileHelper = fileHelper;
        }

        #endregion

        #region Variables & Properties

        private readonly IFileHelper FileHelper;

        private readonly IFolderHelper FolderHelper;

        private readonly IDataPathConfigurationProvider ConfigurationProvider;

        #endregion

        #region Public Functionality

        public string GetPath()
        {
            return this.ConfigurationProvider.GetLocalPath();
        }

        #endregion
    }
}