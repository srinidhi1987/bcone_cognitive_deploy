#pragma once

#include "ocrlogs.h"
#include <functional>
#include <set>

bool SortRectByX(cv::Rect &r1, cv::Rect &r2)
{
	return (r1.x < r2.x);
};
bool SortRectByY(cv::Rect &r1, cv::Rect &r2)
{
	return (r1.y < r2.y);
};
class LetterToWordGroup
{
public:
	struct RegionRect { int x; int y; int width; int height; };

	std::vector<cv::Rect> GetWordsFromLetters(std::vector<cv::Rect>	inputBoxes, const std::string& inputImagePath)
	{
		LOGGER->Trace("[LetterToWordGroup::GetWordsFromLetters] started... ");
		int nSensitivity = 7;
		cv::Mat& image = cv::imread(inputImagePath, 0);
		if (image.data == NULL)
		{
			std::string msg = "LetterToWordGroup::GetWordsFromLetters() Image Data is not available, may image path is wrong " + inputImagePath;
			throw new std::exception(msg.c_str());
		}
		MergeOverlappedBoundingBoxes(inputBoxes, image, nSensitivity);
		RemoveRectsInsideRect(inputBoxes);
		LOGGER->Trace("[GetWordsFromLetters] End... ");
		return inputBoxes;
	}
	void RemoveBigBoxContainsSmallRects(std::vector<cv::Rect> &inputBoxes)
	{
		try
		{
			LOGGER->Trace("RemoveBigBoxContainsSmallRects() Started... ");
			for (int i = inputBoxes.size() - 1; i > 0; i--)
			{
				cv::Rect outerRect = inputBoxes[i];
				for (int j = inputBoxes.size() - 1; j >= 0; j--)
				{
					if (i == j)
						continue;
					cv::Rect innerRect = inputBoxes[j];
					cv::Rect rectsIntersecion = outerRect & innerRect;
					if (rectsIntersecion.area() == innerRect.area())
					{
						if (inputBoxes.size() > i)
						{
							inputBoxes.erase(inputBoxes.begin() + i);
							LOGGER->Trace("Rect Removed");
							break;
						}
					}
				}
			}
			LOGGER->Trace("RemoveBigBoxContainsSmallRects() End... ");
		}
		catch (std::exception exc)
		{
			std::string msg = "Exception Occured in LetterToWordGroup::RemoveRectsInsideRect()" + std::string(exc.what());
			throw new std::exception(msg.c_str());
		}
	}
	void RemoveRectsInsideRect(std::vector<cv::Rect> &inputBoxes)
	{
		try
		{
			LOGGER->Trace("RemoveRectsInsideRect() Started... ");
			for (int i = inputBoxes.size() - 1; i > 0; i--)
			{
				cv::Rect outerRect = inputBoxes[i];
				for (int j = i - 1; j >= 0; j--)
				{
					cv::Rect innerRect = inputBoxes[j];
					cv::Rect rectsIntersecion = outerRect & innerRect;
					if (rectsIntersecion.area() == innerRect.area())
					{
						if (inputBoxes.size() > j)
						{
							inputBoxes.erase(inputBoxes.begin() + j);
							i--;
						}
					}
				}
			}
			LOGGER->Trace("RemoveRectsInsideRect() End... ");
		}
		catch (std::exception exc)
		{
			std::string msg = "Exception Occured in LetterToWordGroup::RemoveRectsInsideRect()" + std::string(exc.what());
			throw new std::exception(msg.c_str());
		}
	}
	Pix* GetBinarizeImage(string filename)
	{
		try {
			cv::Mat srcImage = cv::imread(filename);
			if (srcImage.data == NULL)
			{
				std::string msg = "LetterToWordGroup::GetBinarizeImage() Image Data is not available, may image path is wrong " + filename;
				throw new std::exception(msg.c_str());
			}
			if (srcImage.channels() >= 3)
				cv::cvtColor(srcImage, srcImage, cv::COLOR_BGR2GRAY);

			cv::threshold(srcImage, srcImage, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);

			string tempFilName = GetTempFilePath();
			cv::imwrite(tempFilName, srcImage);
			Pix* binrizeImage = pixRead(tempFilName.c_str());
			std::remove(tempFilName.c_str());
			return binrizeImage;
		}
		catch (std::exception exc)
		{
			std::string msg = "Exception Occured in LetterToWordGroup::GetBinarizeImage()" + std::string(exc.what());
			throw new std::exception(msg.c_str());
		}
	}
	Boxa* GetConnectedBoundingBox(string filename)
	{
		LOGGER->Trace("[LetterToWordGroup::GetConnectedBoundingBox] Started... ");

		Pix* pixImage = GetBinarizeImage(filename); //pixRead(filename.c_str());
		if (pixImage == nullptr)
		{
			return NULL;
		}

		PIX * pixImageBinary = pixConvertTo1(pixImage, 128);
		if (pixImageBinary == nullptr)
		{
			return NULL;
		}
		Boxa* connComponentBoundingBox = pixConnCompBB(pixImageBinary, 8);

		if (connComponentBoundingBox == nullptr)
		{
			if (pixImage)
				pixDestroy(&pixImage);
			if (pixImageBinary)
				pixDestroy(&pixImageBinary);
			LOGGER->Error("[LetterToWordGroup::GetConnectedBoundingBox] Not found any bounding box... ");
			return NULL;
		}
		LOGGER->Trace("[LetterToWordGroup::GetConnectedBoundingBox] End... ");

		return connComponentBoundingBox;
	}
	vector<Rect> GetLetterSegmentation(string filename)
	{
		LOGGER->Trace("[LetterToWordGroup::GetLetterSegmentation] started... ");

		Boxa* connComponentBoundingBox = GetConnectedBoundingBox(filename);
		vector<Rect> nRegionCoordinates;
		if (connComponentBoundingBox == nullptr)
		{
			LOGGER->Error("[LetterToWordGroup::GetLetterSegmentation] Not found any bounding box... ");
			return nRegionCoordinates;
		}

		nRegionCoordinates = GetRegionsFromBoundingBox(connComponentBoundingBox);

		LOGGER->Trace("[LetterToWordGroup::GetLetterSegmentation] Destroying connected Bounding Boxes... ");
		boxaDestroy(&connComponentBoundingBox);

		LOGGER->Trace("[LetterToWordGroup::segmentAndReturnRect] End... ");

		return nRegionCoordinates;
	}
	std::vector<cv::Rect> GetRegionsFromBoundingBox(Boxa* boundingBoxes)
	{
		LOGGER->Trace("[LetterToWordGroup::GetRegionsFromBoundingBox] Started... ");
		vector<cv::Rect> nRegionCoordinates;
		for (int i = 0; i < boundingBoxes->n; i++) {
			BOX* box = boxaGetBox(boundingBoxes, i, L_CLONE);
			BOX* boxd = boxCreate(box->x, box->y, box->w, box->h);
			Rect r(box->x, box->y, box->w, box->h);//, box->w, box->h);
			if (box->w > 1 && box->h > 1)
				nRegionCoordinates.push_back(r);

			boxDestroy(&boxd);
			boxDestroy(&box);
		}
		LOGGER->Trace("[LetterToWordGroup::GetRegionsFromBoundingBox] End... ");
		//for (int i = 0; i < boundingBoxes->n; i++) {
		//	BOX* box = boxaGetBox(boundingBoxes, i, L_CLONE);
		//	BOXA *box1 = boxaContainedInBox(boundingBoxes, box);

		//	bool flag = true;
		//	if (box1->n > 1)
		//	{
		//		for (int j = 0; j < box1->n; j++) {
		//			BOX* boxa = boxaGetBox(box1, j, L_CLONE);
		//			if ((boxa->x - (boxa->w * 2) > box->x) && (boxa->x + (boxa->w * 2) < box->x + box->w))
		//			{
		//				if ((boxa->y - (boxa->h * 2) > box->y) && (boxa->y + (boxa->h * 2) < box->y + box->h))
		//				{
		//					flag = false;
		//					break;
		//				}
		//			}
		//			boxDestroy(&boxa);
		//		}
		//	}
		//	if (flag == true)
		//	{
		//		if (box->w > 1 && box->h > 1) {
		//			nRegionCoordinates.push_back({ box->x, box->y, box->w, box->h });
		//		}
		//	}
		//	boxDestroy(&box);
		//	boxaDestroy(&box1);
		//}

		return nRegionCoordinates;
	}

	std::map<int, int> GetSameAreaRectMap(std::vector<cv::Rect> &inputBoxes)
	{
		std::map<int, int> areaMap;

		for (int i = 0; i < inputBoxes.size(); i++)
		{
			int area = inputBoxes[i].area();
			if (areaMap.count(area) == 0)
				areaMap.emplace(area, 1);
			else
			{
				std::map<int, int>::iterator it = areaMap.find(area);
				it->second = it->second + 1;
			}
		}

		return areaMap;
	}
	void RemoveTableCornerRects(std::vector<cv::Rect> &inputBoxes, std::vector<cv::Rect> &removedBoxes)
	{
		bool isModified = false;
		int iCount = 0;
		do
		{
			isModified = false;
			for (int i = inputBoxes.size() - 1; i >= 0; i--)
			{
				cv::Rect inputRect = inputBoxes[i];
				if (inputRect.area() > 100)
					continue;
				int pix = 5;
				cv::Rect expandedRect = cv::Rect(inputRect.x - pix, inputRect.y - pix, inputRect.width + (2 * pix), inputRect.height + (2 * pix));
				for (int j = 0; j < removedBoxes.size(); j++)
				{
					cv::Rect rmRect = removedBoxes[j];
					bool intersects = ((expandedRect & rmRect).area() > 0);
					if (intersects)
					{
						removedBoxes.push_back(inputBoxes[i]);
						if (inputBoxes.size() > i)
							inputBoxes.erase(inputBoxes.begin() + i);
						isModified = true;
						break;
					}
				}
			}
			iCount++;
		} while (isModified && iCount < 10);
	}
	
	void RemoveSegmentsOfLine(std::vector<cv::Rect> &inputBoxes)
	{
		try
		{
			std::map<int, int> areaMap = GetSameAreaRectMap(inputBoxes);

			//=====>>>>>>> SORT MAP by Value(No. Of segments for the perticlar size(Key))
			// Declaring the type of Predicate that accepts 2 pairs and return a bool
			typedef std::function<bool(std::pair<int, int>, std::pair<int, int>)> Comparator;

			// Defining a lambda function to compare two pairs. It will compare two pairs using second field
			Comparator compFunctor =
				[](std::pair<int, int> elem1, std::pair<int, int> elem2)
			{
				return elem1.second > elem2.second;
			};

			// Declaring a set that will store the pairs using above comparision logic
			std::set<std::pair<int, int>, Comparator> setOfWords(
				areaMap.begin(), areaMap.end(), compFunctor);
			//<<<<<<<==========================

			int segmentCount = 0;
			int tempArea = 0;
			std::vector<cv::Rect> removedBoxes;

			for (std::pair<int, int> element : setOfWords)
			{
				tempArea = element.first;
				segmentCount = element.second;
				if (tempArea <= 25 && segmentCount > inputBoxes.size()*0.1)
				{
					for (int i = inputBoxes.size() - 1; i >= 0; i--)
					{
						if (abs(inputBoxes[i].area() - tempArea) == 0)
						{
							removedBoxes.push_back(inputBoxes[i]);
							if (inputBoxes.size() > i)
								inputBoxes.erase(inputBoxes.begin() + i);
						}
					}
				}
			}
			RemoveTableCornerRects(inputBoxes, removedBoxes);
		}
		catch (std::exception exc)
		{
			std::string msg = "Exception Occured in LetterToWordGroup::RemoveSegmentsOfLine" + std::string(exc.what());
			throw new std::exception(msg.c_str());
		}
	}
	void MergeOverlappedBoundingBoxes(std::vector<cv::Rect> &inputBoxes, cv::Mat image, int sensitivity)
	{
		try
		{
			LOGGER->Trace("[LetterToWordGroup::MergeOverlappedBoundingBoxes] started... ");
			cv::Mat mask = cv::Mat::zeros(image.size(), CV_8UC1); // Mask of original image

			//Below lines are for debugging purpose
			//cv::Mat lettermask = cv::Mat::zeros(image.size(), CV_8UC1); // Mask of original image

			int mergeDist = sensitivity;

			LOGGER->Trace("[LetterToWordGroup::MergeOverlappedBoundingBoxes] drawing and merging letters... ");
			long averageWidth = 0;
			long averageHeight = 0;
			if (inputBoxes.size() <= 0)
			{
				LOGGER->Error("[LetterToWordGroup::MergeOverlappedBoundingBoxes] Not Found Any Letter Segment, Check the image ");
				return;
			}
			long averageArea = GetAverageAreaOfChracters(inputBoxes, image, averageWidth, averageHeight);
			for (int i = 0; i < inputBoxes.size(); i++)
			{
				if (IsLineSegmentAndNotBigCharacter(inputBoxes[i], averageHeight))
					continue;
				if (IsBigBlockButNotWord(inputBoxes[i], averageWidth, averageHeight))
					continue;
				if (IsLineSegment(inputBoxes[i], averageHeight))
					continue;
				//Below lines are for debugging purpose
				//cv::rectangle(lettermask, inputBoxes[i], cv::Scalar(255), CV_FILLED, 34, 0);

				mergeDist = inputBoxes[i].width / 2;
				if (IsWordSegment(inputBoxes[i], averageWidth, image))
					mergeDist = 0;
				if (IsDotCommaPixel(inputBoxes[i], image.cols*0.01))
					mergeDist = averageWidth/2;

				cv::Rect box;
				if ((inputBoxes[i].x - (mergeDist))>0)
					box.x = inputBoxes[i].x - (mergeDist);
				else
					box.x = inputBoxes[i].x;
				box.width = inputBoxes[i].width + (mergeDist * 2);
				box.y = inputBoxes[i].y;
				box.height = inputBoxes[i].height;
				cv::rectangle(mask, box, cv::Scalar(255), CV_FILLED, 34, 0); // Draw filled bounding boxes on mask
			}
			std::vector<std::vector<cv::Point>> contours;
			//Below lines are for debugging purpose to save and see the files
			//imwrite("C:\\Temp\\mask.png", mask);
			//imwrite("lettermask.png", lettermask);
			// Find contours in mask
			// If bounding boxes overlap, they will be joined by this function call
			LOGGER->Trace("[LetterToWordGroup::MergeOverlappedBoundingBoxes] finding contours... ");
			cv::findContours(mask, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
			if (inputBoxes.empty() == false)
				inputBoxes.erase(inputBoxes.begin(), inputBoxes.begin() + inputBoxes.size());
			for (int j = 0; j < contours.size(); j++)
			{
				cv::Rect rect = cv::boundingRect(contours.at(j));
				cv::Mat clipped = image(rect);
				if (clipped.data != NULL)
					rect = ShrinkRect(clipped, rect);

				inputBoxes.push_back(rect);
			}
			LOGGER->Trace("[LetterToWordGroup::MergeOverlappedBoundingBoxes] End... ");
		}
		catch (std::exception exc)
		{
			std::string msg = "Exception Occured in LetterToWordGroup::MergeOverlappedBoundingBoxes" + std::string(exc.what());
			throw new std::exception(msg.c_str());
		}
	}
	bool IsLineSegmentAndNotBigCharacter(cv::Rect box, int averageHeight)
	{
		return (box.height >= averageHeight * 6);
	}
	bool IsBigBlockButNotWord(cv::Rect box, int averageWidth , int averageHeight)
	{
		return (box.width >= averageWidth * 6 && box.height >= averageHeight * 3);
	}
	bool IsLineSegment(cv::Rect box, int averageHeight)
	{
		return ((box.height > averageHeight*1.2f && (float(box.width) / float(box.height)) < 0.1));
	}
	bool IsWordSegment(cv::Rect box, int averageWidth, cv::Mat &image)
	{
		return ((box.width >= averageWidth * 4) || (box.width >= image.cols*0.02));
	}
	long GetAverageAreaOfChracters(std::vector<cv::Rect> &inputBoxes, cv::Mat image, long &averageWidth, long &averageHeight)
	{
		LOGGER->Trace("[LetterToWordGroup::GetAverageAreaOfChracters] Start... ");
		long averageArea = 0.0f;
		int byPassBigSegments = 0;
		int byPassChars = 0;
		averageWidth = 0;
		averageHeight = 0;
		long byPassAverageWidth = 0;
		long byPassAverageHeight = 0;
		long byPassAverageArea = 0;

		for (int i = 0; i < inputBoxes.size(); i++)
		{
			if (inputBoxes[i].width >= image.cols*0.1 || inputBoxes[i].height >= image.rows*0.1)
			{
				byPassBigSegments++;
				continue;
			}

			byPassAverageWidth += inputBoxes[i].width;
			byPassAverageHeight += inputBoxes[i].height;
			byPassAverageArea += inputBoxes[i].area();

			if (IsDotCommaPixel(inputBoxes[i], image.cols*0.01))
			{
				byPassChars++;
				continue;
			}

			averageWidth += inputBoxes[i].width;
			averageHeight += inputBoxes[i].height;
			averageArea += inputBoxes[i].area();
		}
		int divisor = inputBoxes.size() - byPassChars - byPassBigSegments;
		if (abs(divisor) <= inputBoxes.size()*0.1)
		{
			LOGGER->Trace("[LetterToWordGroup::GetAverageAreaOfChracters] Most of the characters having small size... ");
			averageWidth = byPassAverageWidth;
			averageHeight = byPassAverageHeight;
			averageArea = byPassAverageArea;
			divisor = inputBoxes.size();
		}
		averageWidth = averageWidth / divisor;
		averageHeight = averageHeight / divisor;
		averageArea = averageArea / divisor;

		LOGGER->Trace("[LetterToWordGroup::GetAverageAreaOfChracters] End... ");
		return averageArea;
	}
	bool IsDotCommaPixel(cv::Rect inputBox, int widthThreshold)
	{
		return (inputBox.width <= widthThreshold && inputBox.height <= widthThreshold);
	}
	bool IsOneOrL(cv::Rect inputBox, int widthThreshold, int averageHeight)
	{
		return (inputBox.width <= widthThreshold && inputBox.height >= averageHeight*0.8);
	}
	float GetAdjucentWordWidth(int currentWordIndex, std::vector<cv::Rect> &inputBoxes)
	{
		int leftWordIndex = currentWordIndex - 1;
		int rightWordIndex = currentWordIndex + 1;
		int leftWordWidth = 0;
		int rightWordWidth = 0;

		if (leftWordIndex >= 0 && leftWordIndex < inputBoxes.size())
		{
			leftWordWidth = inputBoxes[leftWordIndex].width;
		}
		if (rightWordWidth >= 0 && rightWordWidth < inputBoxes.size())
		{
			rightWordWidth = inputBoxes[rightWordIndex].width;
		}

		float devisor = (leftWordWidth == 0 || rightWordWidth == 0) ? 1.0f : 2.0f;
		float averageWordWidth = float(leftWordWidth + rightWordWidth) / devisor;
		return averageWordWidth;
	}
	cv::Rect ShrinkRect(cv::Mat image, cv::Rect &imageRect)
	{
		LOGGER->Trace("[LetterToWordGroup::ShrinkRect] Started... ");
		string tempFileName = GetTempFilePath();
		imwrite(tempFileName, image);
		vector<Rect> RectofRoiImage = GetLetterSegmentation(tempFileName);
		if (RectofRoiImage.size() > 0)
		{
			sort(RectofRoiImage.begin(), RectofRoiImage.end(), SortRectByX);
			int X, width;
			int size = RectofRoiImage.size();
			int initialRect = 0;
			int lastRect = size - 1;
			X = RectofRoiImage[initialRect].x;
			width = (RectofRoiImage[size - 1].x + RectofRoiImage[size - 1].width) - X;
			imageRect.x = imageRect.x + X;
			imageRect.width = width;
		}
		std::remove(tempFileName.c_str());
		LOGGER->Trace("[LetterToWordGroup::ShrinkRect] End... ");
		return imageRect;
	}
	string GetTempFilePath()
	{
		char *cTempFileName = NULL;
		cTempFileName = std::tmpnam(cTempFileName);
		strcat(cTempFileName, ".png");

		return cTempFileName;
	}
	void SaveImageToDebug(std::string fileanmeSrc, std::string fileNameToSave, vector<cv::Rect> &nRegionCoordinates)
	{
		cv::Mat srcImage = imread(fileanmeSrc.c_str());
		if (srcImage.channels() < 3)
			cv::cvtColor(srcImage, srcImage, cv::COLOR_GRAY2BGR);
		for (int i = 0; i < nRegionCoordinates.size(); i++)
		{
			cv::rectangle(srcImage, nRegionCoordinates[i], cv::Scalar(255, 0, 0), 1);
		}
		imwrite(fileNameToSave, srcImage);
	}
};