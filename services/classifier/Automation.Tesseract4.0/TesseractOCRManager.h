#pragma once
#include "SingletonDocumentProcess.h"

class __declspec(dllexport)  TesseractOCRManager
{
private:
	const char* m_trainingPath;
	const char* m_applicationPath;
	char *m_languageCode;
	int m_pageSegmentationMode;
	int m_ocrEngineMode;
	int m_cpuId;
	//TODO: Instead of char* use const string& as parameter. const char* is not safe, malloc & delete can create side effects.
public:
	~TesseractOCRManager();

	TesseractOCRManager(const char* trainingDataPath, const char* applicationPath, int pageSegmentationMode, int ocrEngineMode, int cpuId, char* languageCode);
	SingletonDocumentProcess* m_DocumentProcessor;
	char* Scan(Pix* originalImage, Box* pixConnCompBBImage, const int index, const char* searchString);
	char* Scan(const Pix* originalImage, const char* searchString);
	/*char* Scan(InputImage* originalImage, const char* searchString);*/
};
