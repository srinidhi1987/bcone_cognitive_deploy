#include "stdafx.h"
#include "TesseractOCRManager.h"

TesseractOCRManager::~TesseractOCRManager()
{
}
char* TesseractOCRManager::Scan(const Pix* originalImage, const char* searchString)
{
	char* HOCRText = m_DocumentProcessor->SingletonDocumentProcessing((Pix*)originalImage, searchString);
	return HOCRText;
}
/*
char* TesseractOCRManager::Scan(InputImage* originalImage, const char* searchString)
{
char* HOCRText = m_DocumentProcessor->SingletonDocumentProcessing(originalImage, searchString);
return HOCRText;
}
*/
TesseractOCRManager::TesseractOCRManager(const char* trainingDataPath, const char* applicationPath,
	int pageSegmentationMode, int ocrEngineMode, int cpuId, char* languageCode)
	: m_trainingPath(trainingDataPath), m_applicationPath(applicationPath)
	, m_pageSegmentationMode(pageSegmentationMode), m_ocrEngineMode(ocrEngineMode), m_languageCode(languageCode), m_cpuId(cpuId)
{
	//m_DocumentProcessor = new DocumentProcess(m_pageSegmentationMode, m_ocrEngineMode, m_applicationPath, m_trainingPath);
	//m_DocumentProcessor = SingletonDocumentProcess::getInstance(m_pageSegmentationMode, m_ocrEngineMode, m_applicationPath, m_trainingPath);
	m_DocumentProcessor = SingletonDocumentProcess::getInstanceForCpuId(m_pageSegmentationMode, m_ocrEngineMode, m_applicationPath, m_trainingPath, m_cpuId, m_languageCode);
}