#include <baseapi.h>
#include <allheaders.h>
#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include "ocrmanager.h"
#include "PageOrientation.h"
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "documentprocess.h"
#include <exception>
#include "LeptSegmentation.h"
#include "ocrlogs.h"
#include "../Automation.PatternRecognition/PatternRecognition.h"
#include <fstream>

#define _countof(X) (sizeof(X) / sizeof(X[0]))

using namespace std;

using namespace tesseract;

const wchar_t* globalHOCRText;
namespace OCRManager
{
	char* ScanByLanguage(char* imagePath
		, char* searchString
		, char* languageCode
		, char* trainingDataPath
		, int pageSegmentationMode
		, int ocrEngineMode
		, char* applicationPath
		, char* hOCROutputFile
		, int logLevel
		, char* transactionID)
	{
		try
		{
			DocumentProcess *documentProcess = new DocumentProcess(pageSegmentationMode, ocrEngineMode, applicationPath, logLevel, transactionID);
			LOGGER->Trace("Tesseract4::ScanByLanguage Start");
			char* HOCRText = documentProcess->DocumentProcessing(imagePath, searchString, languageCode, trainingDataPath, hOCROutputFile);
			documentProcess->~DocumentProcess();
			delete documentProcess;
			LOGGER->Trace("Tesseract4::ScanByLanguage End");
			LOGGER->ShutDown();
			return HOCRText;
		}
		catch (std::exception exc)
		{
			LOGGER->Error("Tesseract4::ScanByLanguage [Exception Occured, Returning NULL OCR] %s", exc.what());
		}
		LOGGER->ShutDown();
		return NULL;
	}
	char* Scan(char* imagePath
		, char* searchString
		, char* trainingDataPath
		, int pageSegmentationMode
		, int ocrEngineMode
		, char* applicationPath
		, int logLevel
		, char* transactionID)
	{
		try
		{
			char* languageCode = "eng";
			DocumentProcess *documentProcess = new DocumentProcess(pageSegmentationMode, ocrEngineMode, applicationPath, logLevel, transactionID);
			LOGGER->Trace("Tesseract4::Scan Start");
			char* HOCRText = documentProcess->DocumentProcessing(imagePath, searchString, languageCode, trainingDataPath);
			documentProcess->~DocumentProcess();
			delete documentProcess;
			LOGGER->ShutDown();

			return HOCRText;
		}
		catch (std::exception exc)
		{
			LOGGER->Error("Tesseract4::Scan [Exception Occured, Returning NULL OCR] %s", exc.what());
		}
		LOGGER->ShutDown();
		return NULL;
	}
	void copy_file(const char* srce_file, const char* dest_file)
	{
		std::ifstream srce(srce_file, std::ios::binary);
		std::ofstream dest(dest_file, std::ios::binary);
		dest << srce.rdbuf();
	}
	char* ScanBySegmetation(char* processedimagePath
		, char* orgImagePath
		, char* fillHolesImg
		, char* searchString
		, char *languageCode
		, char* trainingDataPath
		, int pageSegmentationMode
		, int ocrEngineMode
		, char* applicationPath
		, char* hOCROutputFile
		, int logLevel
		, char* transactionID)
	{
		try
		{
			DocumentProcess *documentProcess = new DocumentProcess(pageSegmentationMode, ocrEngineMode, applicationPath, logLevel, transactionID);

			LOGGER->Trace("Tesseract4::ScanBySegmetation Start");
			LeptSegmentation leptSegmetation;

			LOGGER->Trace("Tesseract4::ScanBySegmetation Removing Lines");

			if (leptSegmetation.IsLineDetected(fillHolesImg))
			{
				std::string lineImage = leptSegmetation.GetTempFilePath();
				std::string lineByLeptonica = leptSegmetation.GetTempFilePath();
				copy_file(processedimagePath, lineImage.c_str());
				RemoveLines((char*)lineImage.c_str(), (char*)lineByLeptonica.c_str());

				leptSegmetation.AddTwoImages(fillHolesImg, lineByLeptonica, fillHolesImg);
				
				RemoveLinesByFillHoles(processedimagePath, fillHolesImg);
				std::remove(lineImage.c_str());
				std::remove(lineByLeptonica.c_str());
			}
			else
				RemoveLinesByFillHoles(processedimagePath, fillHolesImg);

			LOGGER->Trace("Tesseract4::ScanBySegmetation Removing Lines End Successfully");

			std::vector<cv::Rect> segmetations = leptSegmetation.GetSegmentation(processedimagePath);
			if (segmetations.size() <= 0)
			{
				LOGGER->Error("Not Found any word segmentations, returning NULL OCR");
				return NULL;
			}
			char* HOCRText = documentProcess->ProcessSegments(orgImagePath, fillHolesImg, searchString, languageCode, trainingDataPath, segmetations, hOCROutputFile);
			documentProcess->~DocumentProcess();
			delete documentProcess;
			
			LOGGER->Trace("Tesseract4::ScanBySegmetation End");
			LOGGER->ShutDown();

			return HOCRText;
		}
		catch (std::exception exc)
		{
			LOGGER->Error("Tesseract4::ScanBySegmetation [Exception Occured, Returning NULL OCR] %s", exc.what());
		}
		LOGGER->ShutDown();
		return NULL;
	}

	int GetPageOrientation(char* imagePath
		, char* trainingDataPath
		, char* applicationPath
		, int logLevel
		, char* transactionID)
	{
		try
		{
			PageOrientation *pageOrientation = new PageOrientation();
			pageOrientation->CalculateOrientationAngle(imagePath, applicationPath, trainingDataPath, logLevel, transactionID);
			int angle = pageOrientation->GetPageOrientationAngle();
			pageOrientation->~PageOrientation();
			delete pageOrientation;
			LOGGER->ShutDown();
			return angle;
		}
		catch (std::exception exc)
		{
			LOGGER->Error("Tesseract4::GetPageOrientation [Exception Occured, Returning 0 degree] %s", exc.what());
		}
		LOGGER->ShutDown();
		return 0;
	}
	void SegmentImage(char* imagePath, char* segmentFilePath, char* logFilePath, int logLevel, char* transactionID)
	{
		CPPLoggerConfiguration loggerConfiguration;
		loggerConfiguration.transactionID = transactionID;
		loggerConfiguration.elogLevel = eLogLevel(logLevel);
		loggerConfiguration.logFilePath = logFilePath;
		loggerConfiguration.eLoggerType = eLoggerType::LOG4CPlus;

		OCRLogs::ConfigureCPPLogger(loggerConfiguration);

		LeptSegmentation leptSegmetation;
		std::vector<cv::Rect> segmetations = leptSegmetation.GetSegmentation(imagePath);
		ofstream segFile(segmentFilePath);
		for (int i = 0; i < segmetations.size(); i++)
		{
			segFile << segmetations[i].x << "," << segmetations[i].y << "," << segmetations[i].width << "," << segmetations[i].height << endl;
		}
		segFile.flush();
		segFile.close();
		LOGGER->ShutDown();
	}
};