namespace OCRManager
{
	extern "C" {__declspec(dllexport) char* ScanBySegmetation(char* processedimagePath, char* orgImagePath, char* fillHolesImg, char* searchString, char *languageCode, char* trainingDataPath, int pageSegmentationMode, int ocrEngineMode, char* applicationPath, char* hOCROutputFile, int logLevel, char* transactionID); }
	extern "C" {__declspec(dllexport) char* Scan(char* imagePath, char* searchString, char* trainingDataPath, int pageSegmentationMode, int ocrEngineMode, char* applicationPath, int logLevel, char* transactionID); }
	extern "C" { __declspec(dllexport) char* ScanByLanguage(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath, int pageSegmentationMode, int ocrEngineMode, char* applicationPath, char* hOCROutputFile, int logLevel, char* transactionID); }
	extern "C" { __declspec(dllexport) int GetPageOrientation(char* imagePath, char* trainingdataPath, char* applicationPath, int logLevel, char* transactionID); }
	extern "C" { __declspec(dllexport) void SegmentImage(char* imagePath, char* segmentFilePath, char* logFilePath, int logLevel, char* transactionID); }
}