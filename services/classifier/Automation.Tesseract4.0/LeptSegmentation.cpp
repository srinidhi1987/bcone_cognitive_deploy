#pragma once
#include "LeptSegmentation.h";
#include "lettertowordgroup.h"

using namespace std;
using namespace cv;

std::vector<cv::Rect> LeptSegmentation::GetSegmentation(string fileName)
{
	LOGGER->Trace("[LeptSegmentation::GetSegmentation] Started... ");

	LetterToWordGroup letterToWord;
	vector<cv::Rect> nRegionCoordinates;

	nRegionCoordinates = letterToWord.GetLetterSegmentation(fileName);
	if (nRegionCoordinates.size() <= 0)
	{
		LOGGER->Error("LeptSegmentation::GetSegmentation [Not Found any Letter Segments]");
		return nRegionCoordinates;
	}
	letterToWord.RemoveSegmentsOfLine(nRegionCoordinates);
	if (nRegionCoordinates.size() <= 0)
	{
		LOGGER->Error("LeptSegmentation::GetSegmentation [All Segments are removed by letterToWord::RemoveSegmentsOfLine]");
		return nRegionCoordinates;
	}

	//Below lines are for debugging purpose to save and see the files
	//letterToWord.SaveImageToDebug(fileName, "C:\\Temp\\letterSegment.png", nRegionCoordinates);

	nRegionCoordinates = letterToWord.GetWordsFromLetters(nRegionCoordinates, fileName);
	//Below lines are for debugging purpose to save and see the files
	//letterToWord.SaveImageToDebug(fileName, "C:\\Temp\\wordsegmentedImg.png", nRegionCoordinates);

	cout << "No Of regions " << nRegionCoordinates.size() << endl;
	LOGGER->Trace("[LeptSegmentation::GetSegmentation] End... ");
	return nRegionCoordinates;
}
bool LeptSegmentation::IsLineDetected(string fillHolesImagePath)
{
	LetterToWordGroup letterToWord;
	cv::Mat line1 = cv::imread(fillHolesImagePath);
	int borderPix = 20;
	cv::Rect ROI;
	ROI.x = borderPix;
	ROI.y = borderPix;
	ROI.width = line1.cols - (borderPix * 2);
	ROI.height = line1.rows - (borderPix * 2);
	cv::Mat croppedImage = line1(ROI);
	std::string tempfile = GetTempFilePath();
	cv::imwrite(tempfile, croppedImage);
	
	vector<cv::Rect> nRegionCoordinates = letterToWord.GetLetterSegmentation(tempfile);
	
	//letterToWord.SaveImageToDebug(tempfile, "C:\\Temp\\FillHoleSegment.png", nRegionCoordinates);
	
	std::remove(tempfile.c_str());
	
	return (nRegionCoordinates.size() > 2);
}
void LeptSegmentation::AddTwoImages(string linesByFillHoles, string lineByLeptonica, string mergedLines)
{
	cv::Mat line1 = cv::imread(linesByFillHoles);
	cv::Mat line2 = cv::imread(lineByLeptonica);
	cv::bitwise_not(line1, line1);
	cv::bitwise_not(line2, line2);
	cv::Mat image;
	//Below commented lines are for debugging
	//cv::imwrite("C:\\Temp\\Line1.png", line1);
	//cv::imwrite("C:\\Temp\\Line2.png", line2);
	cv::bitwise_or(line1, line2, image);
	cv::bitwise_not(image, image);
	cv::imwrite(mergedLines, image);
	//Below commented lines are for debugging
	//cv::imwrite("C:\\Temp\\MergedLine.png", image);
}
string LeptSegmentation::GetTempFilePath()
{
	char *cTempFileName = NULL;
	cTempFileName = std::tmpnam(cTempFileName);
	strcat(cTempFileName, ".png");

	return cTempFileName;
}