#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <ctime>
#include "CPPLogger.h"

using namespace std;
#define LOGGER OCRLogs::GetLogger()
class OCRLogs
{
public:
	void static ConfigureCPPLogger(CPPLoggerConfiguration loggerConfiguration);
	static CPPLogger *_cppLogger;
	static CPPLogger*  GetLogger();
	static string ApplicationPath;
	void static WriteLogToFile(string currentLog);
};