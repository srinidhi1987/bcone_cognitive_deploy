#include "documentprocess.h"
#include <baseapi.h>
#include <allheaders.h>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <ctime>
#include "ocrlogs.h"

#ifdef HAVE_CONFIG_H
#include "config_auto.h"
#endif

#include <iostream>
#include "baseapi.h"
#include "basedir.h"
#include "renderer.h"
#include "strngs.h"
#include "tprintf.h"
#include <locale>
#include <codecvt>
#include <direct.h>
#include <stdlib.h>
#include "LeptSegmentation.h"

using namespace std;
using namespace tesseract;
using namespace cv;
bool DocumentProcess::isTesseractInitialized = true;

DocumentProcess::DocumentProcess(int pageSegmentationMode
	, int ocrEngineMode
	, char* applicationPath
	, int logLevel
	, char* transactionID)
{
	CPPLoggerConfiguration loggerConfiguration;
	loggerConfiguration.transactionID = transactionID;
	loggerConfiguration.elogLevel = eLogLevel(logLevel);
	loggerConfiguration.logFilePath = applicationPath;
	loggerConfiguration.eLoggerType = eLoggerType::LOG4CPlus;

	OCRLogs::ConfigureCPPLogger(loggerConfiguration);
	OCRLogs::ApplicationPath = applicationPath;
	SegMode = (PageSegMode)pageSegmentationMode;
	EngineMode = (OcrEngineMode)ocrEngineMode;
}

DocumentProcess::~DocumentProcess()
{
	api.~TessBaseAPI();
}

bool DocumentProcess::InitTesseract(InitTessractParams initTesseractParam
	, TessBaseAPI &apiObj)
{
	try {
		LOGGER->Trace("[Scan/OCR] Start initializing the Tesseract");
		if (apiObj.Init(initTesseractParam.trainingDataPath, initTesseractParam.languageCode, (OcrEngineMode)initTesseractParam.engineMode) != NULL)
		{
			LOGGER->Error("[Scan/OCR] Tesseract Initializing Failed");
			apiObj.~TessBaseAPI();
			return false;
		}

		if (initTesseractParam.searchString != NULL && initTesseractParam.searchString != "\0" && initTesseractParam.searchString != "")
		{
			apiObj.SetVariable("tessedit_char_whitelist", initTesseractParam.searchString);
		}

		api.SetPageSegMode(initTesseractParam.segMode);
		LOGGER->Trace("[Scan/OCR] End initializing the Tesseract");

		return true;
	}
	catch (std::exception exc)
	{
		LOGGER->Error("DocumentProcess::InitTesseract [Exception Occured] [%s]", exc.what());
		return false;
	}
}
void DocumentProcess::SetImageAndRecognise(PIX* image, TessBaseAPI &apiObj)
{
	try {
		//LOGGER->Trace("[Scan/OCR] Start SetImageAndRecognise");
		apiObj.SetImage(image);
		apiObj.Recognize(0);
		//LOGGER->Trace("[Scan/OCR] End SetImageAndRecognise");
	}
	catch (std::exception exc)
	{
		std::string msg = "Exception Occured in DocumentProcess::SetImageAndRecognise " + std::string(exc.what());
		throw new std::exception(msg.c_str());
	}
}
char* DocumentProcess::DocumentProcessing(char* imagePath
	, char* searchString
	, char* languageCode
	, char* trainingDataPath
	, char* hOCROutputFile)
{
	try
	{
		LOGGER->Trace("[Scan/OCR] Start DocumentProcess::DocumentProcessing with Language and HOCR output File");
		const char* inputfile = imagePath;

		InitTessractParams initTesseractParams;
		initTesseractParams.searchString = searchString;
		initTesseractParams.engineMode = EngineMode;
		initTesseractParams.segMode = SegMode;
		initTesseractParams.trainingDataPath = trainingDataPath;
		initTesseractParams.languageCode = languageCode;
		if (!InitTesseract(initTesseractParams, api))
		{
			LOGGER->Error("Error Occured in InitTesseract, throwing Exception");
			throw new std::exception("Error Occured in InitTesseract, returning empty OCR");
		}
		PIX *image = pixRead(inputfile);
		SetImageAndRecognise(image, api);

		pixDestroy(&image);

		LOGGER->Trace("[Scan/OCR] returning data ");
		char *hocrData = api.GetHOCRText(0);
		std::ofstream out(hOCROutputFile);
		if (hocrData)
		{
			out << hocrData;
		}
		else
		{
			LOGGER->Trace("[Scan/OCR] OCR text is empty ");
			out << "";
		}
		out.flush();
		out.close();
		delete hocrData;
		LOGGER->Trace("[Scan/OCR] End DocumentProcess::DocumentProcessing with Language and HOCR output File");
		return api.GetHOCRText(0);
	}
	catch (std::exception exc)
	{
		std::string msg = "Exception Occured in DocumentProcess::DocumentProcessing " + std::string(exc.what());
		throw new std::exception(msg.c_str());
	}

	return NULL;
}
std::string DocumentProcess::GetRandomFolderName()
{
	std::stringstream folderName;
	try {
		srand(time(NULL));
		LeptSegmentation leptSegmentation;
		std::string tempFilePath = leptSegmentation.GetTempFilePath();
		std::size_t found = tempFilePath.find_last_of("/\\");
		int folderNo = rand() % 1000;
		folderName << tempFilePath.substr(0, found) << "\\IQBotSegments";
		mkdir(folderName.str().c_str());
		folderName << "\\" << folderNo;
		mkdir(folderName.str().c_str());
		folderName << "\\";
	}
	catch (std::exception ex)
	{
		LOGGER->Error("[DocumentProcess::GetRandomFolderName] Exception [%s]", ex.what());
	}
	return folderName.str();
}
void DocumentProcess::SaveSegment(string folderName, int segmentNo, PIX* image)
{
	try
	{
		std::stringstream tempfilename;
		tempfilename << folderName << segmentNo << ".tif";
		pixWrite(tempfilename.str().c_str(), image, 1);
	}
	catch (std::exception ex)
	{
		LOGGER->Error("[DocumentProcess::SaveSegment] Exception [%s]", ex.what());
		LOGGER->Error("[DocumentProcess::SaveSegment] Directory [%s] may not found", folderName);
	}
}
void DocumentProcess::SaveOCRText(string folderName, int segmentNo)
{
	try {
		std::stringstream ocrFileName;
		ocrFileName << folderName << segmentNo << "_OCR.txt";
		std::ofstream out(ocrFileName.str());
		out << api.GetUTF8Text() << " Mean Conf: " << api.MeanTextConf() << " , AllWordConf: " << api.AllWordConfidences();
		out.flush();
		out.close();
	}
	catch (std::exception ex)
	{
		LOGGER->Error("[DocumentProcess::SaveOCRText] Exception [%s]", ex.what());
		LOGGER->Error("[DocumentProcess::SaveOCRText] Directory [%s] may not found", folderName);
	}
}
void DocumentProcess::SaveResolution(std::string folderName, int fileNo, int xRes, int yRes)
{
	std::stringstream ocrFileName;
	ocrFileName << folderName << "Resolution" << ".txt";
	std::ofstream out(ocrFileName.str());
	out << fileNo << " Resolution: X: " << xRes << ", YRes: " << yRes << endl;
	out.flush();
	out.close();
}
PIX* DocumentProcess::GetBinarizeImage(std::string imagePath)
{
	LOGGER->Trace("Start DocumentProcess::Binarization");

	PIX *image = pixRead(imagePath.c_str());
	LeptSegmentation leptSegmentation;
	std::string tempFilePath = leptSegmentation.GetTempFilePath();

	pixWrite(tempFilePath.c_str(), image, IFF_PNG);

	cv::Mat imgOrg = cv::imread(tempFilePath);

	if (imgOrg.channels() >= 3)
		cv::cvtColor(imgOrg, imgOrg, cv::COLOR_BGR2GRAY);

	cv::threshold(imgOrg, imgOrg, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);

	cv::imwrite(tempFilePath, imgOrg);

	if (image)
		pixDestroy(&image);

	PIX* binImage = pixRead(tempFilePath.c_str());

	std::remove(tempFilePath.c_str());

	LOGGER->Trace("End DocumentProcess::Binarization");

	return binImage;
}

char* DocumentProcess::ProcessSegments(char* imagePath
	, char* onlyLineImagePath
	, char* searchString
	, char *languageCode
	, char* trainingDataPath
	, std::vector<cv::Rect> &segmentations
	, char* hOCROutputFile)
{
	try
	{
		LOGGER->Trace("[Scan/OCR] Start DocumentProcess::ProcessSegments");
		const char* inputfile = imagePath;

		cout << "Initializing Tesseract..." << endl;
		InitTessractParams initTesseractParams;
		initTesseractParams.searchString = searchString;
		initTesseractParams.engineMode = EngineMode;
		initTesseractParams.segMode = SegMode;
		initTesseractParams.trainingDataPath = trainingDataPath;
		initTesseractParams.languageCode = languageCode;
		if (!InitTesseract(initTesseractParams, api))
		{
			LOGGER->Error("Error Occured in InitTesseract, throwing Exception");
			throw new std::exception("Error Occured in InitTesseract, returning empty OCR");
		}
		LOGGER->Trace("Initializing Tesseract with LSTM mode end");

		cout << "Initializing Tesseract End..." << endl;
		std::stringstream hocrStream;
		string folderName = "";

		PIX *image = GetBinarizeImage(inputfile);
		PIX *imageOriginal = pixRead(inputfile);
		if (image == NULL)
		{
			LOGGER->Trace("DocumentProcess::ProcessSegments: [Error in Reading Image file, Returning blank OCR]");
			return{};
		}
		bool bSaveImageNText = (LOGGER->GetLogLevel() == eLogLevel::TRACE_LEVEL) ? true : false;
		if (bSaveImageNText == true)
		{
			folderName = GetRandomFolderName();

			SaveResolution(folderName, 0, image->xres, image->yres);
		}
		hocrStream << "<div>";
		for (int i = 0; i < segmentations.size(); i++)
		{
			if (segmentations[i].x < 0 || segmentations[i].y < 0 ||
				segmentations[i].width <= 2 || segmentations[i].height <= 2)
			{
				continue;
			}

			BOX* cropWindow = boxCreate(segmentations[i].x, segmentations[i].y, segmentations[i].width, segmentations[i].height);

			if (cropWindow == NULL)
			{
				LOGGER->Info("Cropped Window is NULL, crop size can be very small ");
				continue;
			}

			stSegmentOCRParam segmentOCRParam;

			std::string hocrText = GetSegmentHOCR(image, cropWindow, bSaveImageNText, folderName, i, segmentOCRParam);

			/* //Below is adaptive binarization done for COG-4848
			if (segmentOCRParam.confidence < 80)
			{
				LOGGER->Info("Segments are available into folder [%s]", folderName);
				LOGGER->Info("Binarized Segment[No: %d, conf: %d, OCR: %s] confidence from Binarized Image is less than 80, so doing OCR using original Image", i, segmentOCRParam.confidence, &segmentOCRParam.text);
				stSegmentOCRParam segmentOCRParamOrg;
				segmentOCRParamOrg.isOnlyTesseractRequired = true;
				std::string hocrTextOrg = GetSegmentHOCR(imageOriginal, cropWindow, bSaveImageNText, folderName, i + 2000, segmentOCRParamOrg);
				if (segmentOCRParamOrg.confidence > (segmentOCRParam.confidence + 10))
				{
					LOGGER->Info("Original Segment[No: %d, conf: %d, OCR: %s] confidence from Original Image is 10 point more than binarized segment so taking this as a final text", i, segmentOCRParamOrg.confidence, &segmentOCRParamOrg.text);
					hocrText = hocrTextOrg;
					segmentOCRParam = segmentOCRParamOrg;
				}
				else
					LOGGER->Info("Original Segment[No: %d, conf: %d, OCR: %s] confidence from Original Image is NOT 10 point more than binarized segment so taking text of binarized segment as a final text", i, segmentOCRParamOrg.confidence, &segmentOCRParamOrg.text);
			}
			*/

			hocrStream << "<segment id='" << i << "'>" << hocrText << "<index>" << i << "</index><box-x>"
				<< cropWindow->x - segmentOCRParam.leftRightborderPix << "</box-x><box-y>" << cropWindow->y - segmentOCRParam.topBottomborderPix << "</box-y><box-w>" << cropWindow->w
				<< "</box-w><box-h>" << cropWindow->h << "</box-h><scale-x>" << segmentOCRParam.scaleFactorX << "</scale-x>"
				<< "<scale-y>" << segmentOCRParam.scaleFactorY << "</scale-y></segment>";

			boxDestroy(&cropWindow);
		}

		vector<Vec4i> lines = GetLinesInfo(onlyLineImagePath);
		for (size_t i = 0; i < lines.size(); i++)
		{
			Vec4i l = lines[i];
			int x1 = l[0];
			int y1 = l[1];
			int x2 = l[2];
			int y2 = l[3];
			//This is to keep line width 1 pixel
			if (x1 == x2)
				x2 = x2 + 1;
			//This is to keep line width 1 pixel
			if (y1 == y2)
				y2 = y2 + 1;

			// To swap co-ordinate in case of it is in opposite direction
			if (x1 > x2)
			{
				int tempx = x1;
				x1 = x2;
				x2 = tempx;
			}
			// To swap co-ordinate in case of it is in opposite direction
			if (y1 > y2)
			{
				int tempy = y1;
				y1 = y2;
				y2 = tempy;
			}
			hocrStream << "<div><span class='ocr_line' id=\'line_" << i << "\' title=\"bbox "
				<< x1 << " " << y1 << " " << x2 << " " << y2 << "\">" << "</span>" << "<box-x>"
				<< 0 << "</box-x><box-y>" << 0 << "</box-y>" << "<scale-x>" << 1.0 << "</scale-x>"
				<< "<scale-y>" << 1.0 << "</scale-y>" << "</div>";
		}

		hocrStream << "</div>";

		pixDestroy(&image);
		pixDestroy(&imageOriginal);
		std::ofstream out(hOCROutputFile);
		out << hocrStream.str();
		out.flush();
		out.close();
		LOGGER->Trace("[Scan/OCR] End DocumentProcess::ProcessSegments");
		return api.GetHOCRText(0);
	}
	catch (std::exception exc)
	{
		LOGGER->Error("DocumentProcess::ProcessSegments [Exception Occured] [%s]", exc.what());
		LOGGER->Error("Returning Blank OCR Text");
		return{};
	}

	return NULL;
}
void DrawAndSaveLineImage(char* imagePath, vector<Vec4i> lines, Mat image)
{
	if (image.channels() < 3)
		cv::cvtColor(image, image, cv::COLOR_GRAY2BGR);
	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		//if (l[0] == l[2])
		line(image, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, CV_AA);
	}
	cv::imwrite(imagePath, image);
}

std::vector<cv::Vec4i>  DocumentProcess::GetLinesInfo(std::string filePath)
{
	vector<Vec4i> lines;
	try {
		LOGGER->Trace("[Scan/OCR] Start DocumentProcess::GetLinesInfo");
		Mat src = imread(filePath, 0);
		//Below commented Line is for debugging purpose
		//cv::imwrite("C:\\Temp\\OnlyLine.png", src);
		Mat dst, cdst;
		Canny(src, dst, 50, 200, 3);
		cvtColor(dst, cdst, CV_GRAY2BGR);

		HoughLinesP(dst, lines, 1, CV_PI / 180, 50, 50, 10);
		//Below commented Line is for debugging purpose
		//DrawAndSaveLineImage("C:\\Temp\\RedLine.png", lines, src);
		LOGGER->Trace("[Scan/OCR] End DocumentProcess::GetLinesInfo");
	}
	catch (std::exception exc)
	{
		LOGGER->Error("DocumentProcess::GetLinesInfo [Exception Occured] [%s]", exc.what());
		LOGGER->Error("Returning blank array of Lines");
	}

	return lines;
}
char* DocumentProcess::DocumentProcessing(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath)
{
	try
	{
		LOGGER->Trace("[Scan/OCR] start DocumentProcess::DocumentProcessing with Only language");
		const char* inputfile = imagePath;
		PIX *image = pixRead(inputfile);

		InitTessractParams initTesseractParams;
		initTesseractParams.searchString = searchString;
		initTesseractParams.engineMode = EngineMode;
		initTesseractParams.segMode = SegMode;
		initTesseractParams.trainingDataPath = trainingDataPath;
		initTesseractParams.languageCode = languageCode;
		if (!InitTesseract(initTesseractParams, api))
		{
			LOGGER->Error("Error Occured in InitTesseract, throwing Exception");
			throw new std::exception("Error Occured in InitTesseract, returning empty OCR");
		}
		SetImageAndRecognise(image, api);

		LOGGER->Trace("[Scan/OCR] start destroying image ");
		pixDestroy(&image);
		LOGGER->Trace("[Scan/OCR] End destroying image ");

		LOGGER->Trace("[Scan/OCR] returning data ");
		char *hocrData = api.GetHOCRText(0);
		LOGGER->Trace("[Scan/OCR] End DocumentProcess::DocumentProcessing with Only language");

		return hocrData;
	}
	catch (std::exception exc)
	{
		LOGGER->Error("DocumentProcess::DocumentProcessing [Exception Occured] [%s]", exc.what());
		LOGGER->Error("Returning blank OCR Text");
		throw new std::exception("DocumentProcess::DocumentProcessing [Exception Occured]");
	}

	return NULL;
}
std::string DocumentProcess::GetSegmentHOCR(PIX* image
	, BOX* segmentBox
	, bool bSaveImageNText
	, std::string folderName
	, int segmentNo
	, stSegmentOCRParam &segmentOCRParam)
{
	if (segmentBox == NULL)
	{
		LOGGER->Info("Cropped Window is NULL, crop size can be very small ");
		return{};
	}
	PIX* croppedImage = pixClipRectangle(image, segmentBox, NULL);
	if (croppedImage == NULL)
	{
		LOGGER->Info("[Scan/OCR] Cropped Image is NULL, crop size can be very small ");
		segmentOCRParam.confidence = 0;
		return{};
	}
	if (bSaveImageNText == true)
	{
		SaveSegment(folderName, segmentNo, croppedImage);
	}

	int borderColor = 0;

	if (croppedImage->d == 8)
		borderColor = (pow(2, croppedImage->d) - 1);
	else if (croppedImage->d > 8)
		borderColor = 0xffffff00;

	PIX* paddedPixImg = pixAddBorderGeneral(croppedImage, segmentOCRParam.leftRightborderPix, segmentOCRParam.leftRightborderPix, segmentOCRParam.topBottomborderPix, segmentOCRParam.topBottomborderPix, borderColor);

	if (paddedPixImg == NULL)
	{
		segmentOCRParam.leftRightborderPix = 0;
		segmentOCRParam.topBottomborderPix = 0;
		LOGGER->Info("[Scan/OCR] Padding Image is not set, Recognizing org Image ");
		SetImageAndRecognise(croppedImage, api);
	}
	else
	{
		if (bSaveImageNText == true)
		{
			SaveSegment(folderName, segmentNo + 1000, paddedPixImg);
			SaveResolution(folderName, segmentNo, paddedPixImg->xres, paddedPixImg->yres);
		}

		SetImageAndRecognise(paddedPixImg, api);
	}
	string hocrText = api.GetHOCRText(0);
	segmentOCRParam.confidence = api.MeanTextConf();
	if (bSaveImageNText == true)
		SaveOCRText(folderName, segmentNo);
	segmentOCRParam.scaleFactorX = 1.0;
	segmentOCRParam.scaleFactorY = 1.0;

	segmentOCRParam.text = api.GetUTF8Text();
	if (segmentOCRParam.confidence < 80)
	{
		segmentOCRParam.scaleFactorX = segmentOCRParam.scaleFactorY = 2.0;
		PIX* scaledImage = NULL;
		if (paddedPixImg != NULL)
			scaledImage = pixScale(paddedPixImg, segmentOCRParam.scaleFactorX, segmentOCRParam.scaleFactorY);
		else if (croppedImage != NULL)
		{
			scaledImage = pixScale(croppedImage, segmentOCRParam.scaleFactorX, segmentOCRParam.scaleFactorY);
			segmentOCRParam.leftRightborderPix = segmentOCRParam.topBottomborderPix = 0;
		}

		if (scaledImage)
		{
			SetImageAndRecognise(scaledImage, api);
			if (bSaveImageNText == true)
			{
				SaveOCRText(folderName, segmentNo + 10000);
				SaveSegment(folderName, segmentNo + 10000, scaledImage);
			}
			int scaledConf = api.MeanTextConf();
			if (scaledConf > (segmentOCRParam.confidence + 10))
			{
				segmentOCRParam.confidence = scaledConf;
				segmentOCRParam.text = api.GetUTF8Text();
				hocrText = api.GetHOCRText(0);
			}
			else
				segmentOCRParam.scaleFactorX = segmentOCRParam.scaleFactorY = 1.0;
			pixDestroy(&scaledImage);
		}
		else
			LOGGER->Info("Scalled Image is NULL ");
	}

	pixDestroy(&croppedImage);
	pixDestroy(&paddedPixImg);

	return hocrText;
}