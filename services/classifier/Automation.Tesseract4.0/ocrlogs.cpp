#include "ocrlogs.h"

string OCRLogs::ApplicationPath = "";

CPPLogger *OCRLogs::_cppLogger = NULL;
void OCRLogs::WriteLogToFile(string currentLog)
{
	return;
	try {
		if (!ApplicationPath.empty())
		{
			string LogFileName = ApplicationPath;// "\\OCRExtractionLog.txt";
			ofstream myfile;
			time_t rawtime;
			struct tm * timeinfo;

			time(&rawtime);
			timeinfo = localtime(&rawtime);
			string currtime = asctime(timeinfo);

			myfile.open(ApplicationPath + LogFileName, std::ios_base::app);
			myfile << "  Time :" << currtime << currentLog << endl;
			myfile.close();
		}
	}
	catch (...)
	{
	}
}
CPPLogger*  OCRLogs::GetLogger()
{
	if (_cppLogger)
		return _cppLogger;
	else
	{
		cout << "CPPLogger is NULL, possibly not initalizing properly" << endl;
		throw new std::exception("CPPLogger is NULL, possibly not initalizing properly");
	}
}
void OCRLogs::ConfigureCPPLogger(CPPLoggerConfiguration loggerConfiguration)
{
	_cppLogger = CPPLogger::GetCppLogger(loggerConfiguration);
}