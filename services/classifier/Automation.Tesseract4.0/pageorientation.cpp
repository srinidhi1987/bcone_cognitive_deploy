#include "pageorientation.h"
#include <osdetect.h>
#include <ctime>
#include "ocrlogs.h"

PageOrientation::PageOrientation()
{
}

void PageOrientation::CalculateOrientationAngle(char* imagePath, char* applicationPath, char* trainingDataPath, int logLevel, char* transactionID)
{
	try
	{
		CPPLoggerConfiguration loggerConfiguration;
		loggerConfiguration.transactionID = transactionID;
		loggerConfiguration.elogLevel = eLogLevel(logLevel);
		loggerConfiguration.logFilePath = applicationPath;
		loggerConfiguration.eLoggerType = eLoggerType::LOG4CPlus;

		OCRLogs::ConfigureCPPLogger(loggerConfiguration);
		api = new TessBaseAPI();
		if (api->Init(trainingDataPath, "eng")) {
			LOGGER->Error("PageOrientation::CalculateOrientationAngle [Tesseract Initializing Failed]");
			SetPageOrientationAngle(0);
			return;
		}

		LOGGER->Trace("[Orientation/OCR] Start loading image [%s] ", string(imagePath));
		Pix *image = pixRead(imagePath);
		if (image == NULL)
		{
			LOGGER->Error("PageOrientation::CalculateOrientationAngle [Image cannot be read]");
			SetPageOrientationAngle(0);
			return;
		}
		api->SetImage(image);

		OSResults os_results;
		LOGGER->Trace("[Orientation/OCR] Start calculating orientation");
		api->DetectOS(&os_results);
		LOGGER->Trace("[Orientation/OCR] End calculating orientation");
		SetPageOrientationAngle(os_results.best_result.orientation_id);

		pixDestroy(&image);
	}
	catch (std::exception exc)
	{
		LOGGER->Error("PageOrientation::CalculateOrientationAngle [Exception Occured] [%s]", exc.what());
		throw new std::exception("Error in PageOrientation::CalculateOrientationAngle");
	}
}

PageOrientation::~PageOrientation()
{
	api->~TessBaseAPI();
}
int PageOrientation::GetPageOrientationAngle()
{
	if (Angle == 1)
	{
		LOGGER->Trace("[Orientation/OCR] Orientation angle found 90 degree");
		return 90;
	}
	else if (Angle == 2)
	{
		LOGGER->Trace("[Orientation/OCR] Orientation angle found 180 degree");
		return 180;
	}
	else if (Angle == 3)
	{
		LOGGER->Trace("[Orientation/OCR] Orientation angle found 270 degree");
		return 270;
	}
	else
	{
		LOGGER->Trace("[Orientation/OCR] Orientation angle found 0 degree");
		return 0;
	}
}
void PageOrientation::SetPageOrientationAngle(int orientation)
{
	Angle = orientation;
}