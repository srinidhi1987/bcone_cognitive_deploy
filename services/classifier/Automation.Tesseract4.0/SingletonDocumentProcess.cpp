#include "stdafx.h"
#include "SingletonDocumentProcess.h"

using namespace tesseract;
/*
-----------------------------------------------
Singleton implementation of document processing.
Only one instance of document processor will work for threads.
Tesseract API will not be init for every thread.
-------------------------------------------------
*/
SingletonDocumentProcess::SingletonDocumentProcess()
{
}

SingletonDocumentProcess::~SingletonDocumentProcess() {
	api.~TessBaseAPI();
}

std::atomic<SingletonDocumentProcess*> m_instance;
std::atomic<mapOfSingleton*> m_mapOfinstance;
std::mutex m_mutex;
std::mutex m_cpu_id_mutex;

SingletonDocumentProcess* SingletonDocumentProcess::getInstance(int pageSegmentationMode, int ocrEngineMode,
	const char* applicationPath, const char* trainingDataPath, char* languageCode) {
	SingletonDocumentProcess* tmp = m_instance.load(std::memory_order_relaxed);
	std::atomic_thread_fence(std::memory_order_acquire);
	if (tmp == nullptr) {
		std::lock_guard<std::mutex> lock(m_mutex);
		tmp = m_instance.load(std::memory_order_relaxed);
		if (tmp == nullptr) {
			tmp = new SingletonDocumentProcess;	//TODO: take care of deleting the document processor singleton
			tmp->SegMode = (PageSegMode)pageSegmentationMode;
			tmp->EngineMode = (OcrEngineMode)ocrEngineMode;
			tmp->LanguageCode = languageCode;
			OCRLogs::ApplicationPath = applicationPath;
			tmp->isTesseractInitialized = true;
			cout << "Init Tesseract Called 1...." << endl;
			if (tmp->api.Init(trainingDataPath, tmp->LanguageCode.c_str(), tmp->EngineMode) != NULL)
			{
				tmp->isTesseractInitialized = false;
				tmp = nullptr;
			}
			//tmp->isTesseractInitialized = tmp->api.Init(trainingDataPath, "eng", tmp->EngineMode) > 0 ? true : false;
			OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing segmentation mode ");
			tmp->api.SetPageSegMode(tmp->SegMode);
			OCRLogs::WriteLogToFile("[Scan/OCR] End initializing segmentation mode ");
			std::atomic_thread_fence(std::memory_order_release);
			m_instance.store(tmp, std::memory_order_relaxed);
		}
	}
	return tmp;
}
//Create hash of all input parameters and see if the tesseract object is available for given hash value in map.
//If no available, lock mutex and create the object.
SingletonDocumentProcess* SingletonDocumentProcess::getInstanceForCpuId(int pageSegmentationMode, int ocrEngineMode,
	const char* applicationPath, const char* trainingDataPath, int cpuId, char* languageCode)
{
	mapOfSingleton::iterator it;
	mapOfSingleton* tmp = m_mapOfinstance.load(std::memory_order_relaxed);
	std::atomic_thread_fence(std::memory_order_acquire);
	if (tmp == nullptr)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		tmp = m_mapOfinstance.load(std::memory_order_relaxed);
		if (tmp == nullptr)
		{
			SingletonDocumentProcess* doc = new SingletonDocumentProcess;	//TODO: take care of deleting the document processor singleton
			doc->SegMode = (PageSegMode)pageSegmentationMode;
			doc->EngineMode = (OcrEngineMode)ocrEngineMode;
			doc->LanguageCode = languageCode;
			OCRLogs::ApplicationPath = applicationPath;
			doc->isTesseractInitialized = true;
			cout << "Init Tesseract Called 1...." << endl;
			if (doc->api.Init(trainingDataPath, doc->LanguageCode.c_str(), doc->EngineMode) != NULL)
			{
				doc->isTesseractInitialized = false;
				doc = nullptr;
			}
			//tmp->isTesseractInitialized = tmp->api.Init(trainingDataPath, "eng", tmp->EngineMode) > 0 ? true : false;
			//if (searchString != NULL && searchString != "\0" && searchString != "")
			//	api.SetVariable("tessedit_char_whitelist", searchString);

			OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing segmentation mode ");
			doc->api.SetPageSegMode(doc->SegMode);
			OCRLogs::WriteLogToFile("[Scan/OCR] End initializing segmentation mode ");

			tmp = new mapOfSingleton();
			tmp->insert({ cpuId,doc });
			std::atomic_thread_fence(std::memory_order_release);
			m_mapOfinstance.store(tmp, std::memory_order_relaxed);
		}
		it = tmp->find(cpuId);
		if (it == tmp->end())
		{
			std::lock_guard<std::mutex> lock(m_cpu_id_mutex);
			it = tmp->find(cpuId);
			if (it == tmp->end())
			{
				SingletonDocumentProcess* doc = new SingletonDocumentProcess;	//TODO: take care of deleting the document processor singleton
				doc->SegMode = (PageSegMode)pageSegmentationMode;
				doc->EngineMode = (OcrEngineMode)ocrEngineMode;
				doc->LanguageCode = languageCode;
				OCRLogs::ApplicationPath = applicationPath;
				doc->isTesseractInitialized = true;
				cout << "Init Tesseract Called 2...." << endl;
				if (doc->api.Init(trainingDataPath, doc->LanguageCode.c_str(), doc->EngineMode) != NULL)
				{
					doc->isTesseractInitialized = false;
					doc = nullptr;
				}
				//tmp->isTesseractInitialized = tmp->api.Init(trainingDataPath, "eng", tmp->EngineMode) > 0 ? true : false;
				OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing segmentation mode ");
				doc->api.SetPageSegMode(doc->SegMode);
				OCRLogs::WriteLogToFile("[Scan/OCR] End initializing segmentation mode ");
				tmp->insert({ cpuId,doc });
				std::atomic_thread_fence(std::memory_order_release);
				m_mapOfinstance.store(tmp, std::memory_order_relaxed);
			}
		}
	}
	else
	{
		it = tmp->find(cpuId);
		if (it == tmp->end())
		{
			std::lock_guard<std::mutex> lock(m_cpu_id_mutex);
			it = tmp->find(cpuId);
			if (it == tmp->end())
			{
				SingletonDocumentProcess* doc = new SingletonDocumentProcess;	//TODO: take care of deleting the document processor singleton
				doc->SegMode = (PageSegMode)pageSegmentationMode;
				doc->EngineMode = (OcrEngineMode)ocrEngineMode;
				doc->LanguageCode = languageCode;
				OCRLogs::ApplicationPath = applicationPath;
				doc->isTesseractInitialized = true;
				cout << "Init Tesseract Called 3...." << endl;
				if (doc->api.Init(trainingDataPath, doc->LanguageCode.c_str(), doc->EngineMode) != NULL)
				{
					doc->isTesseractInitialized = false;
					doc = nullptr;
				}
				OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing segmentation mode ");
				doc->api.SetPageSegMode(doc->SegMode);
				OCRLogs::WriteLogToFile("[Scan/OCR] End initializing segmentation mode ");
				//tmp->isTesseractInitialized = tmp->api.Init(trainingDataPath, "eng", tmp->EngineMode) > 0 ? true : false;
				tmp->insert({ cpuId,doc });
				std::atomic_thread_fence(std::memory_order_release);
				m_mapOfinstance.store(tmp, std::memory_order_relaxed);
			}
		}
	}
	return tmp->at(cpuId);
}

//char* SingletonDocumentProcess::SingletonDocumentProcessing(const Pix* imageData, const char* searchString)
//{
//	try
//	{
//		if (isTesseractInitialized)
//		{
//			if (searchString != NULL && searchString != "\0" && searchString != "")
//				api.SetVariable("tessedit_char_whitelist", searchString);
//
//			OCRLogs::WriteLogToFile("[Scan/OCR] Start initializing segmentation mode ");
//			api.SetPageSegMode(SegMode);
//			OCRLogs::WriteLogToFile("[Scan/OCR] End initializing segmentation mode ");
//
//			OCRLogs::WriteLogToFile("[Scan/OCR] Start setting image to ocr ");
//			api.SetImage((Pix*)imageData);
//			OCRLogs::WriteLogToFile("[Scan/OCR] End setting image to ocr ");
//
//			OCRLogs::WriteLogToFile("[Scan/OCR] Start extraction of data ");
//			api.Recognize(0);
//			OCRLogs::WriteLogToFile("[Scan/OCR] End extractiong of data ");
//
//			OCRLogs::WriteLogToFile("[Scan/OCR] returning data ");
//			char* hOCR = api.GetUTF8Text();
//			int iConf = api.MeanTextConf();
//			api.Clear();
//
//			return hOCR;
//		}
//	}
//	catch (...)
//	{
//	}
//
//	return NULL;
//}

char* SingletonDocumentProcess::SingletonDocumentProcessing(Pix* imageData, const char* searchString)
{
	try
	{
		if (isTesseractInitialized)
		{
			OCRLogs::WriteLogToFile("[Scan/OCR] Start setting image to ocr ");
			api.SetImage((Pix*)imageData);
			OCRLogs::WriteLogToFile("[Scan/OCR] End setting image to ocr ");

			OCRLogs::WriteLogToFile("[Scan/OCR] Start extraction of data ");
			api.Recognize(0);
			OCRLogs::WriteLogToFile("[Scan/OCR] End extractiong of data ");

			OCRLogs::WriteLogToFile("[Scan/OCR] returning data ");
			char* hOCR = api.GetHOCRText(0);

			api.Clear();

			return hOCR;
		}
	}
	catch (...)
	{
	}

	return NULL;
}