#pragma once

#include <baseapi.h>
#include <allheaders.h>
#include <algorithm>
#include <atomic>
#include <mutex>
#include <map>
#include "ocrlogs.h"
/*
Singleton implementation of Document Process.
One instance of document processor which will hold only one kind of Tesseract API object.
Tesseract API object can be initialized with different parameters.
*/
struct SingletonDocumentProcess;
typedef std::map<int, SingletonDocumentProcess*> mapOfSingleton;
class SingletonDocumentProcess
{
private:
	tesseract::TessBaseAPI api;
	tesseract::PageSegMode SegMode;
	tesseract::OcrEngineMode EngineMode;
	std::string LanguageCode;
	bool isTesseractInitialized;
	SingletonDocumentProcess();
public:
	static SingletonDocumentProcess* getInstance(int pageSegmentationMode, int ocrEngineMode,
		const char* applicationPath, const char* trainingDataPath, char* languageCode);

	static SingletonDocumentProcess* getInstanceForCpuId(int pageSegmentationMode, int ocrEngineMode,
		const char* applicationPath, const char* trainingDataPath, int cpuId, char* languageCode);

	/*char* SingletonDocumentProcessing(const Pix* imageData, const char* searchString);*/
	char* SingletonDocumentProcessing(Pix* imageData, const char* searchString);

	~SingletonDocumentProcess();
};
typedef std::map<int, SingletonDocumentProcess*> mapOfSingleton;
