#pragma once

#include <vector>
#include <opencv2\opencv.hpp>
#include <allheaders.h>
using namespace std;
using namespace cv;
struct PIXWrapper
{
	PIX* m_ptr;
	PIXWrapper(PIX* ptr) : m_ptr(ptr)
	{
	}
	~PIXWrapper()
	{
		pixDestroy(&m_ptr);
	}
	PIX* get() { return m_ptr; }
};
class __declspec(dllexport) LeptSegmentation
{
public:
	struct RegionRect { int x; int y; int width; int height; };
	RegionRect nRegionCoordinates;
	std::vector<cv::Rect> GetSegmentation(std::string fileName);
	string GetTempFilePath();
	bool IsLineDetected(string fillHolesImagePath);
	void AddTwoImages(string linesByFillHoles, string lineByLeptonica, string mergedLines);

private:
	void SaveImageToDebug(std::string fileanmeSrc, std::string fileNameToSave, vector<cv::Rect> &nRegionCoordinates);

	std::vector<cv::Rect> GetRegionsFromBoundingBox(Boxa* boundingBoxes);
	void MergeOverlappedRects(std::vector<cv::Rect> &inputBoxes, cv::Mat image, int sensitivity);
	cv::Rect AdjustRectHeightToMerge(cv::Rect inputBoxes, int imageRows, int imageCols, int sensitivity);
	cv::Mat GetMaskImageOfMergedRects(std::vector<cv::Rect> &inputBoxes, int imageRows, int imageCols);
	cv::Mat GetBlackImage(int imageRows, int imageCols);
	void DrawFilledRectangleOn(cv::Mat image, cv::Rect rect);
	vector<cv::Rect> GetBoundingRect(cv::Mat maskImage);

};
