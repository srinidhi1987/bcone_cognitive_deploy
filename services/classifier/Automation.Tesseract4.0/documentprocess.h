#include <baseapi.h>
#include <allheaders.h>
#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "opencv2\opencv.hpp"
#include <vector>

using namespace tesseract;
using namespace cv;

struct stSegmentOCRParam
{
	int confidence;
	double scaleFactorX;
	double scaleFactorY;
	int leftRightborderPix;
	int topBottomborderPix;
	bool isOnlyTesseractRequired;
	std::string text;
	stSegmentOCRParam()
	{
		leftRightborderPix = 4;
		topBottomborderPix = 4;
		scaleFactorX = 1.0;
		scaleFactorY = 1.0;
		isOnlyTesseractRequired = false;
		text = {};
	}
	void copyConstructor(stSegmentOCRParam &segmentParam)
	{
		confidence = segmentParam.confidence;
		scaleFactorX = segmentParam.scaleFactorX;
		scaleFactorY = segmentParam.scaleFactorY;
		leftRightborderPix = segmentParam.leftRightborderPix;
		topBottomborderPix = segmentParam.topBottomborderPix;
		isOnlyTesseractRequired = segmentParam.isOnlyTesseractRequired;
		text = segmentParam.text;
	}
	stSegmentOCRParam(stSegmentOCRParam &segmentParam)
	{
		copyConstructor(segmentParam);
	}
	void operator=(stSegmentOCRParam &segmentParam)
	{
		copyConstructor(segmentParam);
	}
};

struct InitTessractParams
{
	char* searchString;
	char* trainingDataPath;
	char * languageCode;
	PageSegMode segMode;
	OcrEngineMode engineMode;
};
class DocumentProcess
{
public:
	DocumentProcess(int pageSegmentationMode, int ocrEngineMode, char* applicationPath, int logLevel, char* transactionID);
	char* ProcessSegments(char* imagePath, char* onlyLineImagePath, char* searchString, char *languageCode, char* trainingDataPath, std::vector<cv::Rect> &segmentations, char* hOCROutputFile);
	char* DocumentProcessing(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath);
	char* DocumentProcessing(char* imagePath, char* searchString, char* languageCode, char* trainingDataPath, char* hOCROutputFile);
	~DocumentProcess();

private:
	std::string GetSegmentHOCR(PIX* imageOriginal, BOX* segmentBox, bool bSaveImageNText
		, std::string folderName
		, int segmentNo, stSegmentOCRParam &segmentOCRParam);
	std::string GetRandomFolderName();
	void SaveSegment(std::string folderName, int segmentNo, PIX* image);
	void SaveOCRText(std::string folderName, int segmentNo);
	static bool isTesseractInitialized;
	bool InitTesseract(InitTessractParams initTesseractParam, TessBaseAPI &apiObj);
	void SetImageAndRecognise(PIX* image, TessBaseAPI &apiObj);
	std::vector<cv::Vec4i>  GetLinesInfo(std::string filePath);
	void SaveResolution(std::string folderName, int filename, int xRes, int yRes);
	PIX* GetBinarizeImage(std::string imagePath);
	TessBaseAPI api;
	PageSegMode SegMode;
	OcrEngineMode EngineMode;
};