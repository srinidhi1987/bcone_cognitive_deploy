package com.automationanywhere.cognitive.iqbot.domain.service.extraction;

public interface ExtractionService {

    void extract();
}
