package com.automationanywhere.cognitive.iqbot.application.extraction;

import com.automationanywhere.cognitive.iqbot.domain.service.extraction.ExtractionService;

public class Extraction {

    private ExtractionService extractionService;

    public Extraction(ExtractionService extractionService) {
        this.extractionService = extractionService;
    }

    public void extractFields() {
        // TODO: Identify Domain Model, Dtos and Converters.
        extractionService.extract();
    }
}
