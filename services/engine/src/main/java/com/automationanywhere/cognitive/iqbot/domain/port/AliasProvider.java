package com.automationanywhere.cognitive.iqbot.domain.port;

public interface AliasProvider {

    void getFields();
}
