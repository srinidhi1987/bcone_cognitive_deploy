@echo off
cd Scripts
SET rootDir=%~dp0
:: delete the workspace folder
RMDIR "%rootDir%../../buildartifacts" /S /Q
RMDIR "%rootDir%/../installer/Setup/Resources" /S /Q
RMDIR "%rootDir%/../installer/Setup/InstallShield2015/setup-support-files" /S /Q
cd %rootDir%
call "%rootDir%client_projects_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%java_projects_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%machine_learning_buildscript.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%rootDir%worker_buildscript.bat" 1
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred ) 
call "%rootDir%configuration_veracode.bat"
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )

if %ERRORLEVEL% == 0 ( goto :COMPLETED )
:ErrorOccurred
echo "Errors generating and zipping artifacts for veracode scanning. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog "
exit /b %errorlevel%
:COMPLETED
exit /b 0