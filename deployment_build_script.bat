:: Help ::
:: MasterBuildScript.bat %databasehostname% %databaseport% %databaseusername% %databasepassword% %controlroomhost% %controlroomport% %outputdir% %mqpassword% %authorizationheader% %reposlug% %pullrequestid% %installationdirectory% %gatewayhostname%

@echo off
set sqlserverhost=%1
set sqlserverport=%2
set dbusername=%3
set databasepassword=%4
set controlroomhost=%5
set controlroomport=%6
set outputdir=%7
set mqpassword=%8
set installdir=%9
shift
shift
shift
shift
shift
shift
set gatewayhostname=%4
set testintegrationserverusername=%5
set testintegrationserverpassword=%6
set dbpasswordforautomation=%7
set privatekey=%8
set workspace=%9
set scriptfolder=%workspace%\scripts\
echo "Root Dir:" %workspace%
echo "installdir:" %installdir%
echo "gatewayhostname:" %gatewayhostname%
echo "testintegrationserverusername:" %testintegrationserverusername%
echo "testintegrationserverpassword:" *******************************
echo "dbusername:" %dbusername%
echo "databasepassword:" ******************
echo "mqpassword:" **************
echo "outputdir:" %outputdir%
echo "privatekey:" %privatekey%
echo "dbpasswordforautomation:" *********************
echo "scripts directory:" %scriptfolder%
cd %scriptfolder%
call "%scriptfolder%stop_and_uninstall_services_remotely.bat" %gatewayhostname% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
:: delete the workspace folder
RMDIR "%scriptfolder%../../buildartifacts" /S /Q

call "%scriptfolder%client_projects_buildscript.bat"
echo "installdir after client build run:" %scriptfolder%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%scriptfolder%java_projects_buildscript.bat"
echo "installdir after java project build run:" %scriptfolder%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%scriptfolder%machine_learning_buildscript.bat"
echo "installdir after ML build run:" %scriptfolder%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%scriptfolder%worker_buildscript.bat"
echo "installdir after Worker build run:" %scriptfolder%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred ) 
call "%scriptfolder%configuration_ci.bat" %sqlserverhost% %dbusername% %sqlserverport% %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%scriptfolder%setup_database_rabbitmq_and_configurations.bat" %sqlserverhost% %dbusername% %databasepassword% %sqlserverport% %mqpassword% %controlroomhost% %controlroomport% %outputdir% %gatewayhostname% %installdir% %dbpasswordforautomation%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call powershell "%scriptfolder%frontend_js_hostname_updatescript.ps1" %gatewayhostname%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call powershell "%scriptfolder%create_private_key_file.ps1" %privatekey%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%scriptfolder%copy_build_artifacts.bat" %gatewayhostname% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
call "%scriptfolder%install_and_start_services_remotely.bat" %gatewayhostname% %testintegrationserverusername% %testintegrationserverpassword% %installdir%
if %ERRORLEVEL% NEQ 0 ( goto :ErrorOccurred )
cd %workspace%
if %ERRORLEVEL% == 0 ( goto :COMPLETED )
:ErrorOccurred
echo "Errors encountered during installing services. Exited with status: %errorlevel%"
echo "Kindly refer builderrorlog "
exit /b %errorlevel%
:COMPLETED
exit /b 0