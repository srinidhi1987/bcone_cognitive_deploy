﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions;
using System.IO;
using Automation.Cognitive.Installer.Custom.Model.ControlRoom;
using Moq;
using Automation.Cognitive.Installer.Custom.Common;
using System.Threading.Tasks;
using System.Net.Http;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.CRValidateAndIntegrate.Tests
{
    [TestClass()]
    public class ControlRoomIntegrationHelperTests
    {
        private readonly string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMSIsImlzcyI6Imh0dHA6Ly9FTkdHTFQ0Ni5BQVNQTC1CUkQuQ09NOjgwODAvd2ViY3JzdmMvIiwiYXVkIjoiaHR0cDovL0VOR0dMVDQ2LkFBU1BMLUJSRC5DT006ODA4MC93ZWJjcnN2Yy8iLCJleHAiOjE1Mjk0MTI2NDYsIm5iZiI6MTUyOTQxMTc0Nn0.Z7oTSOPGM4B20YFkv2L1rBWrUAPTKIxbuNuxmVJlvpg";
        private string authResponseJson;

        [TestInitialize]
        public void Initialize()
        {
            AuthenticationResponse response = new AuthenticationResponse() { Token = token };
            authResponseJson = JsonSerializer.Default.Serialize(response);
        }

        [TestMethod()]
        public void ValidateControlRoomServiceAvailability_ValidControlRoomURL_ShouldReturnValidStatus()
        {
            //given
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);           
            mockedrestHelper.Setup(s =>
             s.ExecuteUnsecureGetAsync(ControlRoomIntegrationHelper.ROUTE_STATUS_CR)).
             Returns(Task.FromResult(responseMessage));
            
            //when
            helper.ValidateControlRoomServiceAvailability();

            //then
            Assert.IsTrue(true);
        }

        [TestMethod()]
        [ExpectedException(typeof(ControlRoomException))]
        public void ValidateControlRoomServiceAvailability_InvalidControlRoomURL_ShouldThrowControlRoomException()
        {
            //given
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.ServiceUnavailable);
            mockedrestHelper.Setup(s =>
             s.ExecuteUnsecureGetAsync(ControlRoomIntegrationHelper.ROUTE_STATUS_CR)).
             Returns(Task.FromResult(responseMessage));

            //when
            helper.ValidateControlRoomServiceAvailability();

            //then
        }

        [TestMethod()]
        public void ExecuteAuthentication_ValidCredentials_ShouldAuthentication()
        {
            // given            
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            HttpContent content = new StringContent(authResponseJson);
            responseMessage.Content = content;
            mockedrestHelper.Setup(s =>
             s.ExecuteAuthentication(ControlRoomIntegrationHelper.ROUTE_AUTHENTICATION, "Admin", "12345678")).
             Returns(Task.FromResult(responseMessage));

            //when
            AuthenticationResponse response = helper.Authenticate("Admin", "12345678");

            //then
            Assert.IsNotNull(response.Token);
            Assert.AreEqual(response.Token, token);
        }

        [TestMethod()]
        [ExpectedException(typeof(ControlRoomException))]
        public void ExecuteAuthentication_InvalidCredentials_ShouldThrowException()
        {
            // given            
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            HttpContent content = new StringContent(@"{""message"": ""Invalid credentials.""}");
            responseMessage.Content = content;
            mockedrestHelper.Setup(s =>
             s.ExecuteAuthentication(ControlRoomIntegrationHelper.ROUTE_AUTHENTICATION, "Admin", "1234")).
             Returns(Task.FromResult(responseMessage));

            //when
            helper.Authenticate("Admin", "1234");

            //then
        }

        [TestMethod()]
        public void ExecuteVersionCheck_ValidURL_ShouldReturnVersion()
        {
            // given            
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            HttpContent content = new StringContent(@"{""message"": ""10.5.9.0""}");
            responseMessage.Content = content;
            mockedrestHelper.Setup(s => s.ExecuteSecuredGetAsync(ControlRoomIntegrationHelper.ROUTE_VERSION, token)).
             Returns(Task.FromResult(responseMessage));

            //when
            Version version = helper.GetVersion(token);

            //then
            Assert.IsNotNull(version);
        }

        [TestMethod()]
        public void ExecuteGetLicense_ValidURL_ShouldReturnRolesAndLicense()
        {
            // given            
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            HttpContent content = new StringContent(@"{""roles"": [{""key"": 1,""value"": ""Admin""}],""licenseFeatures"": [] }""");
            responseMessage.Content = content;
            mockedrestHelper.Setup(s => s.ExecuteSecuredGetAsync(ControlRoomIntegrationHelper.ROUTE_ROLEANDLICENSE, token)).
             Returns(Task.FromResult(responseMessage));

            //when
            License license = helper.GetLicense(token);

            //then
            Assert.IsNotNull(license);
            Assert.IsNotNull(license.Roles);            
            Assert.IsNotNull(license.LicenseFeatures);
        }

        [TestMethod()]
        public void ExecuteHasIQBotConfigurations_ValidKey_ShouldReturnTrue()
        {
            // given            
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            HttpContent content = new StringContent(@"{""centralizedConfigurationList"": {""cognitivePlatformOutputPath"":""C:\\OutputFolder"",""cognitivePlatformHost"":""https://ENGGLT46.AASPL-BRD.COM:3000/""}}""");
            responseMessage.Content = content;
            mockedrestHelper.Setup(s => s.ExecuteSecuredGetAsync(ControlRoomIntegrationHelper.ROUTE_CENTRALIZEDCONFIGURATION, token)).
             Returns(Task.FromResult(responseMessage));
            
            //when
            bool hasURLKey = helper.HasIQBotConfigurations(token, CONFIGURATIONKEY.PLATFORMURL);            

            //then
            Assert.IsTrue(hasURLKey);
        }

        [TestMethod()]
        public void ExecuteGetIQBotConfiguration_ValidConfiguration_ShouldReturnConfiguration()
        {

            // given            
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            HttpContent content = new StringContent(@"{""centralizedConfigurationList"": {""cognitivePlatformOutputPath"":""C:\\OutputFolder"",""cognitivePlatformHost"":""https://ENGGLT46.AASPL-BRD.COM:3000/""}}""");
            responseMessage.Content = content;
            mockedrestHelper.Setup(s => s.ExecuteSecuredGetAsync(ControlRoomIntegrationHelper.ROUTE_CENTRALIZEDCONFIGURATION, token)).
             Returns(Task.FromResult(responseMessage));

            //when
            IQBotConfiguration configuration = helper.GetIQBotConfiguration(token);

            //then
            Assert.IsNotNull(configuration);
            Assert.IsNotNull(configuration.IQBotURL);
            Assert.IsNotNull(configuration.OutputPath);
        }

        [TestMethod()]
        [ExpectedException(typeof(ConfigurationException))]
        public void ExecuteAddConfiguration_ConfigurationAlreadyExist_ShouldThrowException()
        {
            // given            
            CentralizedConfiguration centralizedconfiguration = new CentralizedConfiguration()
            {
                Category = ControlRoomIntegrationHelper.CONFIGURATION_CATEGORY_IQBOT,
                Key = ControlRoomIntegrationHelper.CONFIGURATION_KEY_PLATFORMURL,
                Value = @"https://ENGGLT46.AASPL-BRD.COM:3000/",
            };
            var mockedrestHelper = new Mock<RestHelper>() { CallBase = true };
            ControlRoomIntegrationHelper helper = new ControlRoomIntegrationHelper(mockedrestHelper.Object);
            HttpResponseMessage responseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            HttpContent content = new StringContent(@"{""message"": ""Category: IQBot and key: CognitivePlatformHost combination already exists""}""");
            responseMessage.Content = content;
            mockedrestHelper.Setup(s => s.ExecuteConfiguration(ControlRoomIntegrationHelper.ROUTE_ADD_CONFIGURATION, It.IsAny<CentralizedConfiguration>(), token, false)).
             Returns(Task.FromResult(responseMessage));

            //when
            helper.AddPlatformURL(token, @"https://ENGGLT46.AASPL-BRD.COM:3000/",false);

            //then 
        }
    }
}