﻿using Automation.Cognitive.Installer.Custom.Model.ControlRoom;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Common
{
    public class RestHelper
    {
        private readonly string baseUrl;
        private const string HEADER_XAUTH = "X-Authorization";
        private const string HEADER_CONTENT_JSON = "application/json";
        private const int CONNECTION_TIMEOUT = 5;

        public RestHelper() { }

        public RestHelper(string baseUrl)
        {
            this.baseUrl = baseUrl;
            Helper.LogToFile("Base url: " + baseUrl);
        }

        public virtual async Task<HttpResponseMessage> ExecuteUnsecureGetAsync(string route)
        {
            var client = GetHttpClient();
            Helper.LogToFile("route: " + route);
            HttpResponseMessage response = await client.GetAsync(route);
            Helper.LogToFile("response: " + response);
            return response;
        }

        public virtual async Task<HttpResponseMessage> ExecuteSecuredGetAsync(string route, string token)
        {
            var client = GetHttpClient();
            client.DefaultRequestHeaders.Add(HEADER_XAUTH, token);
            Helper.LogToFile("route: " + route);
            HttpResponseMessage response = await client.GetAsync(route);
            Helper.LogToFile("response: " + response);
            return response;
        }

        public virtual async Task<HttpResponseMessage> ExecuteAuthentication(string route, string username, string password)
        {
            Credentials credentials = new Credentials();
            credentials.Username = username;
            credentials.Password = password;
            string payload = JsonSerializer.Default.Serialize(credentials);
            HttpClient client = GetHttpClient();
            HttpContent content = new StringContent(payload, Encoding.UTF8, HEADER_CONTENT_JSON);
            HttpResponseMessage response = await client.PostAsync(route, content);
            return response;
        }

        public virtual async Task<HttpResponseMessage> ExecuteConfiguration(string route, CentralizedConfiguration configuration, string token, bool isUpdate)
        {
            HttpResponseMessage response;
            string payload = JsonSerializer.Default.Serialize(configuration);
            HttpClient client = GetHttpClient();
            client.DefaultRequestHeaders.Add(HEADER_XAUTH, token);
            HttpContent content = new StringContent(payload, Encoding.UTF8, HEADER_CONTENT_JSON);
            if (isUpdate)
                response = await client.PutAsync(route, content);
            else
                response = await client.PostAsync(route, content);
            
            return response;
        }

        private HttpClient GetHttpClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue(HEADER_CONTENT_JSON));
            client.Timeout = new TimeSpan(0, CONNECTION_TIMEOUT, 0);
            return client;
        }
    }
}
