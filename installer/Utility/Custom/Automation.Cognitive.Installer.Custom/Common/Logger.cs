﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Common
{
    internal class Logger
    {
        private const string LOG_FILENAME = "IQBotSetupLog.log";
        private readonly string _logDirPath;
        private readonly string _logFilepath;

        public Logger()
        {
            try
            {
                _logDirPath = Path.GetTempPath();

                _logFilepath = Path.Combine(_logDirPath, LOG_FILENAME);
            }
            catch
            {
                return;
            }
        }

        public void LogToFile(string data)
        {
            try
            {
                data = DateTime.Now + ": " + data + Environment.NewLine;
                System.IO.File.AppendAllText(_logFilepath, data);
            }
            catch
            {
                return;
            }
        }
    }
}