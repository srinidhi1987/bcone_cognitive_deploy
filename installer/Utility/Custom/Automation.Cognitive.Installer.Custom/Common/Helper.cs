﻿using Automation.Cognitive.Installer.Custom.BusinessLogic;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Database;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Security;
using Automation.Cognitive.Installer.Custom.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom
{
    public class Helper
    {
        private static readonly Logger Logger = new Logger();
        private const string JCE_LOCAL_POLICY = "local_policy.jar";
        private const string JCE_BACKUP_LOCAL_POLICY = "backup_local_policy.jar";
        private const string JCE_US_EXPORT_POLICY = "US_export_policy.jar";
        private const string JCE_BACKUP_US_EXPORT_POLICY = "backup_US_export_policy.jar";
        private const string PRIVATE_KEY_FILENAME = "private.key";
        private static Random random = new Random();

        public Helper()
        {
        }

        public string FormatStringValue(string stringValue)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return string.Empty;
            }
            return stringValue.Trim();
        }

        public WindowWrapper GetWindowWrapper(string windowTitle)
        {
            IntPtr hwnd = IntPtr.Zero;
            WindowWrapper wrapper = null;

            try
            {
                Process process = Process.GetProcessesByName(Resource.SetupProcessName).FirstOrDefault(p => p != null && p.MainWindowTitle.Contains(windowTitle));

                if (null != process)
                {
                    hwnd = process.MainWindowHandle;

                    if (hwnd != IntPtr.Zero)
                    {
                        wrapper = new WindowWrapper(hwnd);
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                //
            }
            catch (Exception ex)
            {
                //
            }

            return wrapper;
        }

        public string GetIpAddress()
        {
            string hostName = Dns.GetHostName();
            string ipAddress = "";
            IPHostEntry ip = Dns.GetHostEntry(hostName);
            IPAddress[] address = ip.AddressList;
            for (int i = 0; i < address.Length; i++)
            {
                if (address[i].AddressFamily == AddressFamily.InterNetwork)
                    ipAddress = address[i].ToString();
            }

            return ipAddress;
        }

        public static string GetFullyQualifiedDomainName()
        {
            var ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            return string.IsNullOrWhiteSpace(ipProperties.DomainName) ? ipProperties.HostName : string.Format("{0}.{1}", ipProperties.HostName, ipProperties.DomainName);
        }

        public static void LogToFile(string data)
        {
            if (Logger != null)
            {
                Logger.LogToFile(data);
            }
        }

        public static string GetExpressValidationErrors(List<string> errorList)
        {
            StringBuilder error = new StringBuilder();
            error.Append(Environment.NewLine + " One or more of the prerequisites were not satisfied." + Environment.NewLine + Environment.NewLine);
            foreach (string err in errorList)
            {
                if (!string.IsNullOrWhiteSpace(err))
                    error.Append("   \u2022  " + err + Environment.NewLine);
            }

            error.Append(Environment.NewLine + " Please refer to the installation guide to ensure all prerequisite software is installed on this machine and configured properly.");
            return error.ToString();
        }

        public static bool IsPortAvailableToUse(string port)
        {
            bool isAvailable = true;
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpListeners();// GetActiveTcpConnections();
            int intPort = 0;
            int.TryParse(port, out intPort);
            Helper.LogToFile(string.Format("IsPortInUse Checking Port {0} availability.", intPort.ToString()));
            foreach (IPEndPoint tcpi in tcpConnInfoArray)
            {
                if (tcpi.Port == intPort)
                {
                    Helper.LogToFile(string.Format("IsPortInUse Check: Port {0} found in use.", intPort.ToString()));
                    isAvailable = false;
                    break;
                }
            }
            return isAvailable;
        }

        public static string PrepareInstallationSummary(string clientSecurityType,
                                                        string clientHost,
                                                        string clientPort,
                                                        string gatewaySecurityType,
                                                        string gatewayHost,
                                                        string gatewayPort,
                                                        DatabaseHelper databaseHelper,
                                                        string outputDirectory,
                                                        string logDirectory,
                                                        string installDirectory,
                                                        bool sslOffloading)
        {
            StringBuilder installationSummary = new StringBuilder();
            logDirectory = logDirectory.TrimEnd('\\');
            installDirectory = installDirectory.TrimEnd('\\');
            outputDirectory = outputDirectory.TrimEnd('\\');
            installationSummary.Append(Environment.NewLine);
            installationSummary.Append(string.Format(" \u2022 Database Configuration: {0}:{1} (user: {2}; Authentication: {3}){4}", databaseHelper.DatabaseConfiguration.DatabaseServerName, databaseHelper.DatabaseConfiguration.DatabasePort, databaseHelper.DatabaseConfiguration.DatabaseUserName, (databaseHelper.DatabaseConfiguration.IsWindowsAuthentication ? "Windows" : "SQL"), Environment.NewLine));
            installationSummary.Append(" \u2022 Security Type: " + clientSecurityType + Environment.NewLine);
            if (!clientHost.Equals(gatewayHost, StringComparison.InvariantCultureIgnoreCase) || !clientPort.Equals(gatewayPort, StringComparison.InvariantCultureIgnoreCase))
                installationSummary.Append(string.Format(" \u2022 Load Balancer Configuration: {0}:{1} (SSL Offloading: {2}){3}", gatewayHost, gatewayPort, sslOffloading ? "Yes" : "No", Environment.NewLine));
            installationSummary.Append(string.Format(" \u2022 IQ Bot Portal access URL: {0}:{1}{2}", clientHost, clientPort, Environment.NewLine));
            installationSummary.Append(" \u2022 Output Directory: " + outputDirectory + Environment.NewLine);
            // installationSummary.Append(" \u2022 Log Directory: " + logDirectory + Environment.NewLine + Environment.NewLine);
            installationSummary.Append(" \u2022 Installation Directory: " + installDirectory);

            return installationSummary.ToString();
        }

        public static string GetPortInUseErrorMessage(bool aliasPortValidated,
            bool authPortValidated,
            bool fileManagerPortValidated,
            bool gatewayPortValidated,
            bool mlPortValidated,
            bool reportPortValidated,
            bool validatorPortValidated,
            bool visionPortValidated,
            bool projectPortValidated,
            bool portalPortValidated)
        {
            StringBuilder portInUseMessage = new StringBuilder();
            portInUseMessage.Append("Could not be able to validate services ports. Please make sure following ports are available." + Environment.NewLine + Environment.NewLine);
            if (!portalPortValidated)
                portInUseMessage.Append(" \u2022 IQ Bot Portal: " + Resource.PORT_SERVICE_PORTAL + Environment.NewLine);
            if (!gatewayPortValidated)
                portInUseMessage.Append(" \u2022 API Gateway: " + Resource.PORT_SERVICE_GATEWAY + Environment.NewLine);
            if (!aliasPortValidated)
                portInUseMessage.Append(" \u2022 Alias: " + Resource.PORT_SERVICE_ALIAS + Environment.NewLine);
            if (!authPortValidated)
                portInUseMessage.Append(" \u2022 Authentication: " + Resource.PORT_SERVICE_AUTHENTICATION + Environment.NewLine);
            if (!fileManagerPortValidated)
                portInUseMessage.Append(" \u2022 FileManager: " + Resource.PORT_SERVICE_FILEMANAGER + Environment.NewLine);
            if (!mlPortValidated)
                portInUseMessage.Append(" \u2022 Machine Learning: " + Resource.PORT_SERVICE_ML + Environment.NewLine);
            if (!projectPortValidated)
                portInUseMessage.Append(" \u2022 Project: " + Resource.PORT_SERVICE_PROJECT + Environment.NewLine);
            if (!reportPortValidated)
                portInUseMessage.Append(" \u2022 Report: " + Resource.PORT_SERVICE_REPORT + Environment.NewLine);
            if (!validatorPortValidated)
                portInUseMessage.Append(" \u2022 Validator: " + Resource.PORT_SERVICE_VALIDATOR + Environment.NewLine);
            if (!visionPortValidated)
                portInUseMessage.Append(" \u2022 Visionbot: " + Resource.PORT_SERVICE_VISIOBOT + Environment.NewLine);
            return portInUseMessage.ToString();
        }

        public static void ClearPlatformInstallationDirectory(string installDir)
        {
            string mlfilePath = Path.Combine(installDir, "ML");
            string conffilePath = Path.Combine(installDir, "Configurations");
            string portalfilePath = Path.Combine(installDir, "Portal");
            string servicesfilePath = Path.Combine(installDir, "Services");
            string workersfilePath = Path.Combine(installDir, "Workers");

            Directory.Delete(mlfilePath, true);
            Directory.Delete(conffilePath, true);
            Directory.Delete(portalfilePath, true);
            Directory.Delete(servicesfilePath, true);
            Directory.Delete(workersfilePath, true);

        }

        public static void ClearDatabaseConfigurationDetails(string installDir)
        {
            string privatekeyFilePath = Path.Combine(installDir, "Configurations", PRIVATE_KEY_FILENAME);
            // TODO: The method implementation is in progress. 
            string configurationFilePath = Path.Combine(installDir, "Configurations", "cognitive.properties");
            string settingsFilePath = Path.Combine(installDir, "Configurations", "Settings.txt");
            if (!File.Exists(configurationFilePath))
            {
                Helper.LogToFile("Configuration file has been deleted, could not be able to rollback node details.");
                return;
            }
            Dictionary<string, string> settingsProperties = ReadPropertiesFile(settingsFilePath);
            string sqlInstanceName = settingsProperties["SQLServerAddress"];
            string sqlUserName = settingsProperties["UserName"];
            string sqlPort = settingsProperties["SQLPort"];
            string installtype = settingsProperties["InstallationType"];

            Dictionary<string, string> properties = ReadPropertiesFile(configurationFilePath);
            string databaseSecureData = properties["database.password"];

            ISecretKeyProvider keyProvider = new SecretKeyProvider(privatekeyFilePath);
            ICipherService ciperService = new CipherService(keyProvider);
            string dbSecureData = ciperService.decrypt(databaseSecureData);

            DatabaseHelper helper = new DatabaseHelper();
            helper.ClearConfigurationEntryForCurrentNode(sqlInstanceName, sqlUserName, sqlPort, dbSecureData, installtype);
        }



        private static Dictionary<string, string> ReadPropertiesFile(string propertiesFilepath)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            foreach (var row in File.ReadAllLines(propertiesFilepath))
            {
                string[] values = row.Split('=');
                if (values != null && values.Count() > 1)
                    properties.Add(values[0].Trim(), values[1].Trim());
            }
            return properties;
        }

        public static string RandomString(int length)
        {
            //const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            //return new string(Enumerable.Repeat(chars, length)
            //  .Select(s => s[random.Next(s.Length)]).ToArray());

            return "ZF4LCP3KPU0QXVZQKWDKPFFV5NVKR7FR";
        }

        public static string BuildURL(string schema, string host, string port)
        {
            string urlTemplate = "<schema>://<host>:<port>/";
            urlTemplate = urlTemplate.Replace("<schema>", schema);
            urlTemplate = urlTemplate.Replace("<host>", host);
            urlTemplate = urlTemplate.Replace("<port>", port);
            return urlTemplate;
        }

        public static string FQDNtoHostName(string fqdn)
        {
            string hostname = string.Empty;
            string[] hostnamecomponents = fqdn.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            if (hostnamecomponents.Length > 0)
                return hostnamecomponents[0];
            else
                return fqdn;
        }

        public static bool IsValidateBaseURL(string URL)
        {
            string pattern = @"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([0-9])*(\/?)$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(URL);
        }
    }
}