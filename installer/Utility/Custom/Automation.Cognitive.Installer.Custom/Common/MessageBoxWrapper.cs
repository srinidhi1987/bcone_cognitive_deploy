﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;

using System.Windows.Forms;
using InstallShield.Interop;

namespace Automation.Cognitive.Installer.Custom
{
    public class MessageBoxWrapper
    {
        private IWin32Window _windowWrapper;
        private Msi.Install _install;
        private string _caption;

        public MessageBoxWrapper(Msi.Install install)
        {
            _install = install;

            _caption = install.GetProperty(Resource.ProductName);

            Helper helper = new Helper();

            _windowWrapper = helper.GetWindowWrapper(_caption);
        }

        public bool ShowYesNoDialog(string message)
        {
            DialogResult dialogResult;
            if (_windowWrapper != null)
            {
                dialogResult = MessageBox.Show(_windowWrapper, message, _caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
            else
            {
                dialogResult = MessageBox.Show(_windowWrapper, message, _caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }

            if (dialogResult == DialogResult.Yes)
            {
                _install.SetProperty(Resource.CONFIRMATION_YES_CLICKED, Resource.One);

                return true;
            }

            return false;
        }

        public DialogResult ShowRetryAbortDialog(string message)
        {
            DialogResult dialogResult;
            if (_windowWrapper != null)
            {
                dialogResult = MessageBox.Show(_windowWrapper, message, _caption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
            else
            {
                dialogResult = MessageBox.Show(_windowWrapper, message, _caption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
            return dialogResult;
        }

        public void ShowErrorDialog(string message)
        {
            MessageBox.Show(_windowWrapper, message, _caption, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }
    }
}