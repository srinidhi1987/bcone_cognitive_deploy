﻿using Automation.Cognitive.Installer.Custom.BusinessLogic;
using Automation.Cognitive.Installer.Custom.BusinessLogic.ControlRoomIntegrator;
using Automation.Cognitive.Installer.Custom.BusinessLogic.CRValidateAndIntegrate;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Database;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Express;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Security;
using InstallShield.Interop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace Automation.Cognitive.Installer.Custom
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        private DatabaseHelper _databaseHelper;
        private Validations _validations;
        public const string AAE_NOT_INSTALLED = "Please make sure that AAE Client is installed on this machine and a user currently logged in to it. This is a pre-requisite for IQ Bot Installation.";
        private const string COGNITIVE_SECURE_FILE_NAME = "Cognitive.properties";
        private const string PRIVATE_KEY_FILENAME = "private.key";
        private const string HTTP_PROTOCOL = "http";
        private const string COGNITIVE_PLATFORM_PORT = "3000";
        private readonly string controlRoomBaseURLtemplate = "<schema>://<hostname>:<port>";

        public Installer()
        {
            InitializeComponent();           
        }

        public Installer(IContainer container)
        {
            InitializeComponent();
        }

        public void ValidateDatabaseConfigurationsDialog(int msiHandle)
        {
            _databaseHelper = new DatabaseHelper(msiHandle);
            _databaseHelper.ValidateDatabaseConfigurationsDialog(msiHandle);
                
        }

        public void ValidateControlRoomConfiguration(int msiHandle)
        {
            Helper.LogToFile("ValidateControlRoomConfiguration started");
            ControlRoomIntegrator crIntegrator = new ControlRoomIntegrator(msiHandle);
            crIntegrator.ValidateControlRoomConfiguration();
        }

        public void ValidateOutputLocation(int msiHandle)
        {
            _validations = new Validations();
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            install.SetProperty(Resource.IS_SHARED_LOCATION_VALID, "0");
            bool isValid = _validations.isValidLocation(install);
            if (isValid)
                install.SetProperty(Resource.IS_SHARED_LOCATION_VALID, "1");
        }

        public void ControlRoomHandshakingInternal(int msiHandle)
        {
            Helper.LogToFile("ControlRoomHandshakingInternal started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            _databaseHelper = new DatabaseHelper(msiHandle);
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            ControlRoomIntegrator crIntegrator = new ControlRoomIntegrator(msiHandle);

            try
            {
                crIntegrator.UpdateIQBotConfiguration();
                _databaseHelper.InsertSettingstoConfigurationDatabase();
            }
            catch (Exception ex)
            {
                Helper.LogToFile(ex.ToString());
                throw;
            }
            Helper.LogToFile("ControlRoomHandshakingInternal completed");
        }

        public void UpdateControlRoomExpressWithPlatformAndOutputPath(int msiHandle)
        {
            Helper.LogToFile("UpdateControlRoomExpressWithPlatformAndOutputPath started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            _databaseHelper = new DatabaseHelper(msiHandle);
            try
            {
                _databaseHelper.InsertSettingstoConfigurationDatabase();
                _databaseHelper.UpdateIQBotConfigurationForExpressInstallation();
            }
            catch (Exception ex)
            {
                messageBoxWrapper.ShowErrorDialog("Error updating controlroom configuration with IQBot details. Please check logs for more details");
                Helper.LogToFile(ex.ToString());
                throw;
            }
            Helper.LogToFile("UpdateControlRoomExpressWithPlatformAndOutputPath completed");
        }

        public void ConfigureComponents(int msiHandle)
        {
            ComponentConfigurations componentConfigurations = new ComponentConfigurations(msiHandle);
            componentConfigurations.ConfigureComonents();
        }

        public void WriteSecureInformationToSecureFile(int msiHandle)
        {
            try
            {
                Helper.LogToFile("WriteSecureInformationToSecureFile started");
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                Helper helper = new Helper();
                SecurityProvider securityProvider = new SecurityProvider();
                string databasePassword = helper.FormatStringValue(install.GetProperty(Resource.DATABASE_PASSWORD));
                string installdir = install.GetProperty("INSTALLDIR");
                string secureFilePath = Path.Combine(installdir, "Configurations", COGNITIVE_SECURE_FILE_NAME);
                string privatekeyFilePath = Path.Combine(installdir, "Configurations", PRIVATE_KEY_FILENAME);
                string sslOffloading = install.GetProperty(Resource.IS_SSL_OFFLOADING);
                string clientSecurityType = install.GetProperty(Resource.CLIENT_SECURITY_TYPE);
                securityProvider.GeneratePrivateKeyFile(privatekeyFilePath);
                securityProvider.EncryptSecureDataInFile(privatekeyFilePath, secureFilePath, databasePassword);
                Helper.LogToFile("WriteSecureInformationToSecureFile completed.");
            }
            catch (Exception ex)
            {
                Helper.LogToFile("WriteInformationToFile" + ex.Message);
                throw;
            }
        }

        public void DoProgress(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                install.ResetProgressBar(50, true, true);

                install.IncrementProgressBar(20);
                install.AddTicksToProgressBar(20);

                install.IncrementProgressBar(30);
                install.AddTicksToProgressBar(30);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("DoProgress" + ex.Message);
            }
        }

        public void InstallAndStartServices(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                bool isWinAuthEnabled = !install.GetProperty(Resource.IS_DATABASE_WINDOWS_AUTH).Equals("0", StringComparison.InvariantCultureIgnoreCase);
                string username = install.GetProperty(Resource.DATABASE_USERNAME);
                string password = install.GetProperty(Resource.DATABASE_PASSWORD);
                ProcessExecutor executor = new ProcessExecutor(install);
                executor.InstallAndStartServices(isWinAuthEnabled,username,password);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("InstallAndStartServices" + ex.Message);
            }
        }

        public void StopAndUninstallServices(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                ProcessExecutor executor = new ProcessExecutor(install);
                executor.StopAndUninstallServices();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("StopAndUninstallServices" + ex.Message);
            }
        }

        public void StartLiquibase(int msiHandle)
        {
            try
            {
                Helper.LogToFile("StartLiquibase started");
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                bool isExpress = install.GetProperty(Resource.INSTALLTYPE).Equals("EXPRESS", StringComparison.InvariantCultureIgnoreCase) ? true : false;
                ProcessExecutor executor = new ProcessExecutor(install);
                executor.StartLiquibase(isExpress);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("StartLiquibase" + ex.Message);
            }
            Helper.LogToFile("StartLiquibase completed");
        }

        public void CreateRabbitMQUser(int msiHandle)
        {
            try
            {
                Helper.LogToFile("CreateRabbitMQUser started");
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                ProcessExecutor executor = new ProcessExecutor(install);
                executor.CreateRabbitMQUser();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("CreateRabbitMQUser" + ex.Message);
            }
            Helper.LogToFile("CreateRabbitMQUser completed.");
        }

        public void InstallPrerequisites(int msiHandle)
        {
            /*Msi.Install install = Msi.CustomActionHandle(msiHandle);
            ProcessExecutor executor = new ProcessExecutor(install);
            executor.InstallPrerequisites();*/
        }

        public void CreateCognitivePlatformUrl(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                string clientHostname = install.GetProperty(Resource.CLIENT_HOSTNAME);
                string clientPort = install.GetProperty(Resource.CLIENT_PORT);
                string scheme = (install.GetProperty(Resource.CLIENT_SECURITY_TYPE).Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase)) ? "https" : "http";

                string cognitivePlatformUrl = string.Format("{0}://{1}:{2}/", scheme, clientHostname, clientPort);

                Uri gatewayHost = new Uri(cognitivePlatformUrl);
                install.SetProperty(Resource.COGNITIVE_PLATFORM_URL, cognitivePlatformUrl);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("CreateCognitivePlatformUrl" + ex.Message);
            }
        }

        public void ReadDatabasePropertiesBeforeUninstall(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                ComponentConfigurations componentConfiguration = new ComponentConfigurations(msiHandle);
                string installdir = install.GetProperty(Resource.INSTALLDIR);
                Helper.ClearDatabaseConfigurationDetails(installdir);
                //componentConfiguration.RemoveRabbitMQClusterNode();

            }
            catch (Exception ex)
            {
                Helper.LogToFile("ReadDatabasePropertiesBeforeUninstall:" + ex.Message);
            }
        }

        public void OnUninstall(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                string installdir = install.GetProperty(Resource.INSTALLDIR);
                Helper.ClearPlatformInstallationDirectory(installdir);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("OnUninstall:" + ex.Message);
            }
        }

        public void CreateDatabaseForExpressInstallation(int msiHandle)
        {
            try
            {
                Msi.Install install = Msi.CustomActionHandle(msiHandle);
                DatabaseHelper databaseHelper = new DatabaseHelper(msiHandle);
                databaseHelper.CreateDatabase();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("OnUninstall:" + ex.Message);
            }
        }

        public void ValidateExpressPrerequisite(int msiHandle)
        {
            Helper.LogToFile("ValidateExpressPrerequisite Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            string sqlValidationReport = ValidateSQLExpressConnection(msiHandle);
            if (!string.IsNullOrWhiteSpace(sqlValidationReport))
            {
                Helper.LogToFile("Error encountered during validation");
                List<string> errorList = new List<string>() { sqlValidationReport };
                install.SetProperty(Resource.EXPRESS_ERR_TEXT, Helper.GetExpressValidationErrors(errorList));
                install.SetProperty(Resource.EXPRESS_PREREQUISITE_VALIDATED, "0");
            }
            else
            {
                install.SetProperty(Resource.EXPRESS_ERR_TEXT, string.Empty);
                install.SetProperty(Resource.EXPRESS_PREREQUISITE_VALIDATED, "1");
            }
            Helper.LogToFile("ValidateExpressPrerequisite Completed");
        }

        public void SetDefaultHostname(int msiHandle)
        {
            Helper.LogToFile("SetDefaultHostname Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            try
            {
                string defaultHostname = System.Net.Dns.GetHostEntry(Environment.MachineName).HostName;
                install.SetProperty(Resource.CLIENT_HOSTNAME, defaultHostname);
                install.SetProperty(Resource.GATEWAY_HOSTNAME, defaultHostname);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("SetDefaultHostname:" + ex.Message);
            }
            Helper.LogToFile("SetDefaultHostname Completed");
        }

        public void IsPortInUse(int msiHandle, string port)
        {
            Helper.LogToFile("IsPortInUse Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            install.SetProperty(Resource.PORT_NOT_IN_USE, "0");
            try
            {
                Helper.LogToFile("Port: " + port);
                if (Helper.IsPortAvailableToUse(port))
                    install.SetProperty(Resource.PORT_NOT_IN_USE, "1");
                else
                {
                    MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
                    messageBoxWrapper.ShowErrorDialog(Resource.VALIDATION_PORT_ERR_TEXT);
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("IsPortInUse:" + ex.Message);
            }
            Helper.LogToFile("IsPortInUse Completed");
        }

        public void ValidatePFXCertificate(int msiHandle)
        {
            Helper.LogToFile("ValidatePFXCertificate Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            install.SetProperty(Resource.CERTIFICATE_VALIDATED, "0");

            SSLSecurityProvider securityProvider = new SSLSecurityProvider(install);
            securityProvider.ValidatePFXCertificate();

            Helper.LogToFile("ValidatePFXCertificate Completed");
        }

        public void PrepareInstallationSummary(int msiHandle)
        {
            Helper.LogToFile("SetInstallationSummary Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);

            string clientSecurityType = install.GetProperty(Resource.CLIENT_SECURITY_TYPE);
            string clientHost = install.GetProperty(Resource.CLIENT_HOSTNAME);
            string clientPort = install.GetProperty(Resource.CLIENT_PORT);
            string lbSecurityType = install.GetProperty(Resource.GATEWAY_SECURITY_TYPE);
            string gatewayHost = install.GetProperty(Resource.GATEWAY_HOSTNAME);
            string gatewayPort = install.GetProperty(Resource.GATEWAY_PORT);
            DatabaseHelper databaseHelper = new DatabaseHelper(msiHandle);
            string outputDirectory = install.GetProperty(Resource.OUTPUT_PATH);
            string logDirectory = install.GetProperty(Resource.LOGDIR);
            string installDir = install.GetProperty(Resource.INSTALLDIR);
            bool ssloffloading = (!install.GetProperty(Resource.IS_SSL_OFFLOADING).Equals("1") || clientSecurityType.Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase)) ? false : true;

            try
            {
                string installationSummary = Helper.PrepareInstallationSummary(
                    (clientSecurityType.Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase) ? "HTTPs" : "None (HTTP)"),
                    clientHost,
                    clientPort,
                    (lbSecurityType.Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase) ? "HTTPs" : "None (HTTP)"),
                    gatewayHost,
                    gatewayPort,
                    databaseHelper,
                    outputDirectory,
                    logDirectory,
                    installDir,
                    ssloffloading);
                Helper.LogToFile("Installation Summary: " + installationSummary);
                install.SetProperty(Resource.INSTALLATION_SUMMARY, installationSummary);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("SetInstallationSummary:" + ex.Message);
            }
            Helper.LogToFile("SetInstallationSummary Completed");
        }

        public void ConfigureRabbitMQClusterConfiguration(int msiHandle)
        {

            return; 

            //string primaryNodeName = string.Empty;
            //bool addNodeToCluster = false;
            //Helper.LogToFile("ConfigureRabbitMQClusterConfiguration Started");
            //try
            //{
            //    DatabaseHelper databaseHelper = new DatabaseHelper(msiHandle);
            //    CognitiveConfiguration cogConfiguration = databaseHelper.ReadNodeConfigration(string.Empty, true);
            //    Helper.LogToFile("Primary Node found: " + cogConfiguration.NodeName);
            //    primaryNodeName = "rabbit@" + Helper.FQDNtoHostName(cogConfiguration.NodeName);
            //    if (!string.IsNullOrEmpty(cogConfiguration.NodeName) && !cogConfiguration.NodeName.Equals(Helper.GetFullyQualifiedDomainName()))
            //    {                    
            //        addNodeToCluster = true;
            //    }
            //    ComponentConfigurations configureComponents = new ComponentConfigurations(msiHandle);
            //    configureComponents.ConfigureRabbitMQClusterNodes(primaryNodeName, addNodeToCluster);
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogToFile("ConfigureRabbitMQClusterConfiguration: Error: " + ex.ToString());
            //}
            //Helper.LogToFile("ConfigureRabbitMQClusterConfiguration Completed");
        }

        public void ValidateSSLOffloadingSettings(int msiHandle)
        {
            Helper.LogToFile("ValidateSSLOffloadingSettings Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);

            SSLSecurityProvider securityProvider = new SSLSecurityProvider(install);
            securityProvider.ValidateSSLOffloadingSettings();

            Helper.LogToFile("ValidateSSLOffloadingSettings Completed");
        }

        public void ValidateMicroServicePorts(int msiHandle)
        {
            Helper.LogToFile("ValidateSSLOffloadingSettings Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);

            Validations validator = new Validations();
            validator.ValidateMicroServicePorts(install);

            Helper.LogToFile("ValidateSSLOffloadingSettings Completed");
        }

        public void SetExpressInstallationDefaultValues(int msiHandle)
        {
            Helper.LogToFile("SetExpressInstallationDefaultValues Started");
            ExpressInstallationService expressInstallationService = new ExpressInstallationService(msiHandle);
            try
            {
                expressInstallationService.SetExpressDefaultValues();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("SetExpressInstallationDefaultValues:" + ex.Message);
            }
            Helper.LogToFile("SetExpressInstallationDefaultValues Completed");
        }

        public void ResetExpressInstallationDefaultValues(int msiHandle)
        {
            Helper.LogToFile("ResetExpressInstallationDefaultValues Started");
            ExpressInstallationService expressInstallationService = new ExpressInstallationService(msiHandle);
            try
            {
                expressInstallationService.ResetExpressDefaultValues();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("ResetExpressInstallationDefaultValues:" + ex.Message);
            }
            Helper.LogToFile("ResetExpressInstallationDefaultValues Completed");
        }

        public void FetchExistingOutputPath(int msiHandle)
        {
            Helper.LogToFile("FetchExistingOutputPath Started");

            try
            {
                _databaseHelper = new DatabaseHelper(msiHandle);

                var configurationList = _databaseHelper.ReadConfigurations();
                var configuration = configurationList.Find(p => p.Key == DatabaseHelper.CONFIGURATION_KEY_OUTPUTFOLDER);
                if (configuration != null && !string.IsNullOrEmpty(configuration.Value))
                {
                    Msi.Install install = Msi.CustomActionHandle(msiHandle);
                    install.SetProperty(Resource.OUTPUT_PATH, configuration.Value);
                }
            }
            catch(Exception ex)
            {
                Helper.LogToFile("Fatal error : " + ex.ToString());
            }

            Helper.LogToFile("FetchExistingOutputPath Ended");
        }

        public void SaveOutputPath(int msiHandle)
        {
            Helper.LogToFile("SaveOutputPath Started");

            try
            {
                _databaseHelper = new DatabaseHelper(msiHandle);
                _databaseHelper.InsertOutputPath();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Fatal error : " + ex.ToString());
            }

            Helper.LogToFile("SaveOutputPath Ended");
        }

        private string ValidateSQLExpressConnection(int msiHandle)
        {
            Helper.LogToFile("ValidateSQLExpressConnection Started");
            string retVal = string.Empty;
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            DatabaseHelper databaseHelper = new DatabaseHelper(msiHandle);
            try
            {
                databaseHelper.ValidateSQLExpressInstanceConnection();
                string dynamicPort = databaseHelper.GetDynamicPortForSQLExpress();
                if (!string.IsNullOrEmpty(dynamicPort))
                    install.SetProperty(Resource.DATABASE_PORT, dynamicPort);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("ValidateSQLExpressConnection:" + ex.Message);
                retVal = ex.Message;
            }
            Helper.LogToFile("ValidateSQLExpressConnection Completed");
            return retVal;
        }

        private string ValidateControlRoomExpressInstallation(int msiHandle)
        {
            string retVal = string.Empty;
            Helper.LogToFile("ValidateControlRoomExpressInstallation Started");
            Msi.Install install = Msi.CustomActionHandle(msiHandle);
            string crExpressHostname = System.Net.Dns.GetHostEntry(Environment.MachineName).HostName;
            string controlRoomExpressBaseURL = controlRoomBaseURLtemplate.Replace("<schema>", "http");
            controlRoomExpressBaseURL = controlRoomExpressBaseURL.Replace("<hostname>", crExpressHostname);
            controlRoomExpressBaseURL = controlRoomExpressBaseURL.Replace("<port>", "8080");
            ControlRoomIntegrationHelper crIntegrationHelper = new ControlRoomIntegrationHelper(controlRoomExpressBaseURL);
            try
            {
                string setupSuportDirPath = install.GetProperty("SUPPORTDIR");
                Helper.LogToFile("Support Directory location: " + setupSuportDirPath);
                crIntegrationHelper.ValidateControlRoomServiceAvailability();
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
                Helper.LogToFile("ValidateCRExpressInstallation:" + ex.Message);
            }
            Helper.LogToFile("ValidateControlRoomExpressInstallation Completed");
            return retVal;
        }

    }
}