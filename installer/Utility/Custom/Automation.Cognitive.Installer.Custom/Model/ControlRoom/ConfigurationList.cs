﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class ConfigurationList
    {
        [JsonProperty("centralizedConfigurationList")]
        public IQBotConfiguration IQBotConfiguation { get; set; }
    }
}
