﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class Role
    {
        [JsonProperty( "value")]
        public string Name { get; set; }
    }
}
