﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class CRMetadata
    {
        private Version _version = new Version("0.0.0.0");

        [JsonProperty( "message")]
        public Version Version {
            get { return this._version; }
            set { this._version = value; }
        }
    }
}
