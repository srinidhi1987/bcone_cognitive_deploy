﻿namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
