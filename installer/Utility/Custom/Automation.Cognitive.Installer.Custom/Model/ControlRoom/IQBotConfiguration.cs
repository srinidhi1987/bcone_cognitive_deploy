﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class IQBotConfiguration
    {
        [JsonProperty("cognitivePlatformOutputPath")]
        public string OutputPath { get; set; }
        [JsonProperty("cognitivePlatformHost")]
        public string IQBotURL { get; set; }
    }
}
