﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class CentralizedConfiguration
    {
        [JsonProperty( "category")]
        public string Category { get; set; }

        [JsonProperty( "key")]
        public string Key { get; set; }

        [JsonProperty( "value")]
        public string Value { get; set; }


    }
}
