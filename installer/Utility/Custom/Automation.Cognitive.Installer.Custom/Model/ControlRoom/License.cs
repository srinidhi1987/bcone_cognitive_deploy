﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class License
    {
        [JsonProperty("roles")]
        public List<Role> Roles { get; set; }
        [JsonProperty("licenseFeatures")]
        public List<string> LicenseFeatures { get; set; }
    }
}
