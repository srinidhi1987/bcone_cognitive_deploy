﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Model.ControlRoom
{
    public class AuthenticationResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
