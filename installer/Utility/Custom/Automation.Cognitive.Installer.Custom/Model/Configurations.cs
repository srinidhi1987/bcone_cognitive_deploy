﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Model
{
    public class Configurations
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public Configurations(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
