﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Model
{
    public class CognitiveConfiguration
    {
        public string NodeName { get; set; }
        public string LoadBalancerUrl { get; set; }
        public string ControlRoomUrl { get; set; }
        public string ClientPlatformUrl { get; set; }
        public bool IsExpressInstallation { get; set; }
        public string OutputPath { get; set; }
        public string InstallOn { get; set; }
        public bool IsPrimaryNode { get; set; }
    }
}
