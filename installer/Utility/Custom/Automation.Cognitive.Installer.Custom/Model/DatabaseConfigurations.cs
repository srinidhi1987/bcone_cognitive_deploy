﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Model
{
    public class DatabaseConfigurations
    {
        public string DatabaseServerName { get; set; }
        public int DatabasePort { get; set; }
        public string DatabaseUserName { get; set; }
        public string DatabasePassword { get; set; }
        public string DatabaseName { get; set; }
        public bool IsWindowsAuthentication { get; set; }

        
        public override string ToString()
        {
            return string.Format("Database Servername: {0}, Database Port: {1}, Database Username: {2}, SQL Server Windows Authentication: {3}",
                this.DatabaseServerName,
                this.DatabasePort,
                this.DatabaseUserName,
                this.IsWindowsAuthentication.ToString());
        }
    }
}