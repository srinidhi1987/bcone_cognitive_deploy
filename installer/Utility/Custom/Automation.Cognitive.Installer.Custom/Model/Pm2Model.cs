﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.Model
{
    public class Pm2Model
    {
        public string PM2_HOME { get; set; }
        public string PM2_SERVICE_PM2_DIR { get; set; }
        public string PM2_SERVICE_SCRIPTS { get; set; }
    }
}