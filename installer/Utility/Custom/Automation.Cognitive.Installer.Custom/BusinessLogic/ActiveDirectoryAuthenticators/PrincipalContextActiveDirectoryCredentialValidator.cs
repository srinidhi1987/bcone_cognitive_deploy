﻿using Automation.Cognitive.Installer.Custom;
using System;
using System.DirectoryServices.AccountManagement;

namespace Automation.Cognitive.Installer.BusinessLogic.Security.ActiveDirectoryAuthenticators
{
    public class PrincipalContextActiveDirectoryCredentialValidator : IActiveDirectoryCredentialValidator
    {


        public bool ValidateNoSSL(string domain, string userName, string password)
        {
            Helper.LogToFile(nameof(PrincipalContextActiveDirectoryCredentialValidator) + ':' + nameof(ValidateNoSSL) +
                    $": invoked for user '{userName}', domain '{domain}'");

            try
            {
                using (PrincipalContext pc = string.IsNullOrWhiteSpace(domain) ?
                    new PrincipalContext(ContextType.Domain) :
                    new PrincipalContext(ContextType.Domain, domain))
                {
                    return pc.ValidateCredentials(userName, password, ContextOptions.Negotiate | ContextOptions.Sealing | ContextOptions.Signing);
                }
            }
            catch (Exception ex1)
            {
                Helper.LogToFile(nameof(PrincipalContextActiveDirectoryCredentialValidator) + ':' + nameof(ValidateNoSSL) + $"Failed for '{userName}' " +  ex1);
            }

            return false;
        }

        public bool ValidateWithSSL(string domain, string userName, string password)
        {
            Helper.LogToFile(nameof(PrincipalContextActiveDirectoryCredentialValidator) + ':' + nameof(ValidateWithSSL) +
                    $": invoked for user '{userName}', domain '{domain}'");

            try
            {
                using (PrincipalContext pc = string.IsNullOrWhiteSpace(domain) ?
                    new PrincipalContext(ContextType.Domain) :
                    new PrincipalContext(ContextType.Domain, domain))
                {
                    return pc.ValidateCredentials(userName, password, ContextOptions.SecureSocketLayer | ContextOptions.SimpleBind);
                }
            }
            catch (Exception ex2)
            {
                Helper.LogToFile(nameof(PrincipalContextActiveDirectoryCredentialValidator) + ':' + nameof(ValidateWithSSL) + $"Failed for '{userName}' " +  ex2);
            }

            return false;
        }
    }
}
