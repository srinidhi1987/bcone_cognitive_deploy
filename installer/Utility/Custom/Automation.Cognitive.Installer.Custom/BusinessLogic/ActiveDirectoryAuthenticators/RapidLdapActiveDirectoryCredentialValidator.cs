﻿using Automation.Cognitive.Installer.Custom;
using System;
using System.DirectoryServices.Protocols;

namespace Automation.Cognitive.Installer.BusinessLogic.Security.ActiveDirectoryAuthenticators
{
    public class RapidLdapActiveDirectoryCredentialValidator : IActiveDirectoryCredentialValidator
    {

        public bool ValidateNoSSL(string domain, string userName, string password)
        {
            Helper.LogToFile(nameof(RapidLdapActiveDirectoryCredentialValidator) + ':' + nameof(ValidateNoSSL) +
                    $": invoked for user '{userName}', domain '{domain}'");

            Exception ex = null;
            LdapConnection ldapConnection;

            if (string.IsNullOrWhiteSpace(domain))
            {
                ldapConnection = tryCreateLdapConnectionForCurrentDomain(out ex);
            }
            else
            {
                ldapConnection = tryCreateLdapConnectionForDomain(domain, out ex);
            }

            if (ex == null && ldapConnection != null)
            {
                Helper.LogToFile(nameof(RapidLdapActiveDirectoryCredentialValidator) + ':' + nameof(ValidateNoSSL) +
                   $": ldap connection created successfully for user '{userName}', domain '{domain}'");

                return validate(ldapConnection, domain, userName, password);
            }
            else
            {
                Helper.LogToFile(nameof(RapidLdapActiveDirectoryCredentialValidator) + ':' + nameof(ValidateNoSSL) + $"Failed for '{userName}'" +  ex);

                return false;
            }
        }

        public bool ValidateWithSSL(string domain, string userName, string password)
        {
            return false;
        }

        private bool validate(LdapConnection connection, string domain, string userName, string password)
        {
            try
            {
                connection.Bind(string.IsNullOrWhiteSpace(domain) ? new System.Net.NetworkCredential(userName, password) :
                    new System.Net.NetworkCredential(userName, password, domain));

                return true;              
            }
            catch (Exception ex)
            {
                Helper.LogToFile(nameof(RapidLdapActiveDirectoryCredentialValidator) + ':' + nameof(validate) + $"Failed for '{userName}' " +  ex);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return false; //Return false because we don't know that the credentials are invalid or not if we got to this point
        }

        private LdapConnection tryCreateLdapConnectionForCurrentDomain(out Exception failureReason)
        {
            //Using GetDC to get the machine's domain
            try
            {
                return tryCreateLdapConnectionForDomain(NativeWrapped.GetDc(null, DsFlag.DS_DIRECTORY_SERVICE_PREFERRED).DomainName, out failureReason);
            }
            catch (Exception ex)
            {
                failureReason = ex;
            }
            return null;
        }

        //Not using SSL here since we want fastconcurrentbinds using UDP
        private LdapConnection tryCreateLdapConnectionForDomain(string domain, out Exception failureReason)
        {
            LdapConnection lc = null;
            try
            {
                lc = new LdapConnection(domain);
                lc.AutoBind = false;
                lc.SessionOptions.ProtocolVersion = 3;
                //Set things up for fast concurrent binds now
                lc.SessionOptions.FastConcurrentBind();
                lc.SessionOptions.Signing = false;
                lc.SessionOptions.Sealing = false;
                lc.AuthType = AuthType.Basic;
                lc.SessionOptions.StartTransportLayerSecurity(new DirectoryControlCollection());
                lc.Bind();
                //It worked, so now return our object
                failureReason = null;
                return lc;
            }
            catch (Exception ex)
            {
                failureReason = ex;
                if (lc != null)
                {
                    lc.Dispose();
                    lc = null;
                }
                return null;
            }
        }
    }
}
