﻿namespace Automation.Cognitive.Installer.BusinessLogic.Security.ActiveDirectoryAuthenticators
{
    public interface IActiveDirectoryCredentialValidator
    {
        bool ValidateNoSSL(string domain, string userName, string password);
        bool ValidateWithSSL(string domain, string userName, string password);
    }
}
