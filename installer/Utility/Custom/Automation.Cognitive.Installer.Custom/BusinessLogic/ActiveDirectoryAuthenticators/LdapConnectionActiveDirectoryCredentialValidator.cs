﻿using Automation.Cognitive.Installer.Custom;
using System;
using System.DirectoryServices.Protocols;

namespace Automation.Cognitive.Installer.BusinessLogic.Security.ActiveDirectoryAuthenticators
{
    public class LdapConnectionActiveDirectoryCredentialValidator : IActiveDirectoryCredentialValidator
    {


        public bool ValidateNoSSL(string domain, string userName, string password)
        {
            return validate(domain, userName, password, false);
        }

        public bool ValidateWithSSL(string domain, string userName, string password)
        {
            return validate(domain, userName, password, true);
        }

        private bool validate(string domain, string userName, string password, bool useSSL)
        {
            try
            {
                Helper.LogToFile(nameof(LdapConnectionActiveDirectoryCredentialValidator) + ':' + nameof(validate) + 
                    $": invoked for '{userName}', domain '{domain}', SSL '{useSSL}'");

                const int LDAP_SSL_PORT = 636; //Non-GC version...this function is designed for the scenario of contacting a specific domain about that domain
                const int LDAP_TCP_PORT = 389; //Non-GC version...this function is designed for the scenario of contacting a specific domain about that domain

                using (var conn = new LdapConnection(new LdapDirectoryIdentifier(string.IsNullOrWhiteSpace(domain) ?
                    NativeWrapped.GetDc(null, DsFlag.DS_DIRECTORY_SERVICE_PREFERRED).DomainName :
                    domain, useSSL ? LDAP_SSL_PORT : LDAP_TCP_PORT)))
                {
                    //Assumes you want to use the same typical kinds that validatecredentials would support
                    conn.AuthType = AuthType.Negotiate;
                    conn.SessionOptions.ProtocolVersion = 3;
                    if (useSSL)
                    {
                        //If using the SSL path here, and you want to ignore cert validation failures, 
                        //set the callback functions in conn.SessionOptions and you can perform your own cert validation
                        conn.SessionOptions.SecureSocketLayer = true;
                    }
                    else
                    {
                        conn.SessionOptions.Signing = true;
                        conn.SessionOptions.Sealing = true;
                        conn.SessionOptions.SecureSocketLayer = false;
                    }
                    try
                    {
                        conn.Bind(string.IsNullOrWhiteSpace(domain) ? new System.Net.NetworkCredential(userName, password) :
                            new System.Net.NetworkCredential(userName, password, domain));

                        return true;
                    }
                    catch (LdapException lex2)
                    {
                        Helper.LogToFile(nameof(LdapConnectionActiveDirectoryCredentialValidator) + ':' + nameof(validate) + $"Failed for '{userName}' " + lex2);

                        switch (lex2.ErrorCode)
                        {
                            case 0x31: //LDAP_INVALID_CREDENTIALS
                                return false;
                            default:
                                throw lex2; //Just rethrow at this point so the general handle catches things if you don't want to handle other types of ldap exceptions explicitly
                        }
                    }
                }
            }
            catch (LdapException lex)
            {
                Helper.LogToFile(nameof(LdapConnectionActiveDirectoryCredentialValidator) + ':' + nameof(validate) + $"Failed for '{userName}' " + lex);
            }
            catch (Exception ex)
            {
                Helper.LogToFile(nameof(LdapConnectionActiveDirectoryCredentialValidator) + ':' + nameof(validate) + $"Failed for '{userName}' " +  ex);
            }

            return false;
        }
    }
}

