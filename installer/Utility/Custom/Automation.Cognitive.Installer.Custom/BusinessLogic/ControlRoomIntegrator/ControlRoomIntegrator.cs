﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InstallShield.Interop;
using Automation.Cognitive.Installer.Custom.BusinessLogic.CRValidateAndIntegrate;
using Automation.Cognitive.Installer.Custom.Model.ControlRoom;
using System.Reflection;
using System.IO;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.ControlRoomIntegrator
{
    public class ControlRoomIntegrator
    {
        private readonly ComponentConfigurations componentConfigurations;
        private readonly Msi.Install install;

        public ControlRoomIntegrator(int msiHandle)
        {
            componentConfigurations = new ComponentConfigurations(msiHandle);
            install = Msi.CustomActionHandle(msiHandle);
        }

        public void ValidateControlRoomConfiguration()
        {
            AuthenticationResponse response;
            string crUrl = install.GetProperty(Resource.CONTROLROOM_URL);
            string crUsername = install.GetProperty(Resource.CONTROLROOM_USERNAME);
            string crSecureData = install.GetProperty(Resource.CONTROLROOM_PASSWORD);
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            install.SetProperty(Resource.CONTROLROOM_CONFIGURATION_VALIDATED, "0");
            if (!Helper.IsValidateBaseURL(crUrl))
            {
                messageBoxWrapper.ShowErrorDialog(Resource.CONTROLROOM_INVALID_URL);
                return;
            }
            ControlRoomIntegrationHelper crIntegrationHelper = new ControlRoomIntegrationHelper(crUrl);
            try
            {
                response = crIntegrationHelper.Authenticate(crUsername, crSecureData);

                License license = crIntegrationHelper.GetLicense(response.Token);
                if (license.Roles.Count == 0 || !license.Roles.Any(x => x.Name.Equals("Admin", StringComparison.InvariantCultureIgnoreCase)))
                {
                    messageBoxWrapper.ShowErrorDialog(Resource.CONTROLROOM_INVALID_CREDENTIALS);
                    return;
                }

                Version compatibleVersion = new Version(Resource.CONTROLROOM_COMPATIBLE_VERSION);
                Version controlRoomVersion = crIntegrationHelper.GetVersion(response.Token);
                if (controlRoomVersion < compatibleVersion)
                {
                    messageBoxWrapper.ShowErrorDialog(Resource.CONTROLROOM_INCOMPATIBLE_VERSION_ERROR);
                    return;
                }

                install.SetProperty(Resource.CONTROLROOM_CONFIGURATION_VALIDATED, "1");
                try
                {
                    IQBotConfiguration configuration = crIntegrationHelper.GetIQBotConfiguration(response.Token);
                    if (configuration != null && !string.IsNullOrEmpty(configuration.IQBotURL))
                    {
                        Uri loadbalancerUri = new Uri(configuration.IQBotURL);
                        install.SetProperty(Resource.GATEWAY_HOSTNAME, loadbalancerUri.Host);
                        install.SetProperty(Resource.GATEWAY_PORT, loadbalancerUri.Port.ToString());
                    }
                    if (configuration != null && !string.IsNullOrEmpty(configuration.OutputPath))
                        install.SetProperty(Resource.OUTPUT_PATH, configuration.OutputPath);
                }
                catch (ControlRoomException cex)
                {
                    // We dont need to bubble up this exception as this is a specific case where control room doesnt hold configuration details and returns InternalServerError when asked for IQBot Configuration. 
                    Helper.LogToFile($"Configuration not found: {cex.ToString()}");
                }
                install.SetProperty(Resource.CONTROLROOM_AUTH_TOKEN, response.Token);
            }
            catch (Exception ex)
            {
                messageBoxWrapper.ShowErrorDialog(ex.Message);
            }
        }

        public void UpdateIQBotConfiguration()
        {
            string iqBotPlatformURL = componentConfigurations.GetCognitiveBaseURL();
            string outputPath = install.GetProperty(Resource.OUTPUT_PATH);
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            string crUrl = install.GetProperty(Resource.CONTROLROOM_URL);
            string token = install.GetProperty(Resource.CONTROLROOM_AUTH_TOKEN);

            ControlRoomIntegrationHelper crIntegrationHelper = new ControlRoomIntegrationHelper(crUrl);
            try
            {
                bool hasPlatformURL = crIntegrationHelper.HasIQBotConfigurations(token, CONFIGURATIONKEY.PLATFORMURL);
                Helper.LogToFile($"iqbot platform url: {iqBotPlatformURL}, is url update: {hasPlatformURL}");
                crIntegrationHelper.AddPlatformURL(token, iqBotPlatformURL, hasPlatformURL);
                bool hasOutputPath = crIntegrationHelper.HasIQBotConfigurations(token, CONFIGURATIONKEY.OUTPUTPATH);
                Helper.LogToFile($"outputPath: {outputPath}, is path update: {hasOutputPath}");
                crIntegrationHelper.AddOutputPath(token, outputPath, hasOutputPath);
                SetControlRoomIpAndPort(crUrl);
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                messageBoxWrapper.ShowErrorDialog(ex.Message);
            }
        }

        public void SetControlRoomIpAndPort(string link)
        {
            Uri crUrl = new Uri(link);
            install.SetProperty(Resource.CONTROLROOM_IP, crUrl.Host);
            install.SetProperty(Resource.CONTROLROOM_PORT, crUrl.Port.ToString());
            install.SetProperty(Resource.CONTROLROOM_SECURITY_TYPE, crUrl.Scheme);
        }
    }
}
