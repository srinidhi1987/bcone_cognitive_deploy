﻿using Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions;
using Automation.Cognitive.Installer.Custom.Common;
using Automation.Cognitive.Installer.Custom.Model.ControlRoom;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.CRValidateAndIntegrate
{
    public class ControlRoomIntegrationHelper
    {
        private readonly string crUrl;
        internal const string ROUTE_VERSION = "controlroomapi/v1/controlroom/version";
        internal const string ROUTE_ROLEANDLICENSE = "controlroomapi/v1/user/rolesandlicensetype";
        internal const string ROUTE_CENTRALIZEDCONFIGURATION = "controlroomapi/v1/configuration/get?category=IQBot";
        internal const string ROUTE_AUTHENTICATION = "controlroomapi/v1/authenticate";
        internal const string ROUTE_ADD_CONFIGURATION = "controlroomapi/v1/configuration/add";
        internal const string ROUTE_UPDATE_CONFIGURATION = "controlroomapi/v1/configuration/update";
        internal const string CONFIGURATION_CATEGORY_IQBOT = "IQBot";
        internal const string CONFIGURATION_KEY_PLATFORMURL = "CognitivePlatformHost";
        internal const string CONFIGURATION_KEY_OUTPUTFOLDER = "CognitivePlatformOutputPath";
        internal const string ROUTE_STATUS_CR = "webcrsvc/config/getstatus";
        private readonly RestHelper restHelper;

        public ControlRoomIntegrationHelper()
        {
        }

        public ControlRoomIntegrationHelper(string crUrl)
        {
            this.crUrl = crUrl;
            restHelper = new RestHelper(crUrl);
        }

        public ControlRoomIntegrationHelper(RestHelper restHelper)
        {
            this.restHelper = restHelper;
        }


        public AuthenticationResponse Authenticate(string username, string securedata)
        {
            Helper.LogToFile("Authenticate Started");
            AuthenticationResponse authResponse = new AuthenticationResponse();
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteAuthentication(ROUTE_AUTHENTICATION, username, securedata);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    string tokenJson = responseMessage.Content.ReadAsStringAsync().Result;
                    authResponse = JsonSerializer.Default.Deserialize<AuthenticationResponse>(tokenJson);
                }
                else
                    HandleControlRoomGenericErrors(responseMessage);
            }
            catch (ControlRoomException ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw;
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
            Helper.LogToFile("Authenticate Completed");
            return authResponse;
        }

        public Version GetVersion(string token)
        {
            Helper.LogToFile("GetVersion Started");
            CRMetadata crMetadata = new CRMetadata();
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteSecuredGetAsync(ROUTE_VERSION, token);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    string versionJson = responseMessage.Content.ReadAsStringAsync().Result;
                    crMetadata = JsonSerializer.Default.Deserialize<CRMetadata>(versionJson);
                }
                else
                    HandleControlRoomGenericErrors(responseMessage);
            }
            catch (ControlRoomException ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw;
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
            Helper.LogToFile("GetVersion Completed");
            return crMetadata.Version;
        }

        public License GetLicense(string token)
        {
            Helper.LogToFile("GetLicense Started");
            License license = new License();
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteSecuredGetAsync(ROUTE_ROLEANDLICENSE, token);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    string licenseJson = responseMessage.Content.ReadAsStringAsync().Result;
                    license = JsonSerializer.Default.Deserialize<License>(licenseJson);
                }
                else
                    HandleControlRoomGenericErrors(responseMessage);
            }
            catch (ControlRoomException ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw;
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
            Helper.LogToFile("GetLicense Completed");
            return license;
        }

        public bool HasIQBotConfigurations(string token, CONFIGURATIONKEY key)
        {
            Helper.LogToFile("HasIQBotConfigurations Started");
            bool hasConfiguration = false;
            try
            {
                IQBotConfiguration configuration = GetIQBotConfiguration(token);
                switch (key)
                {
                    case CONFIGURATIONKEY.PLATFORMURL:
                        if (configuration != null && !string.IsNullOrWhiteSpace(configuration.IQBotURL))
                            hasConfiguration = true;
                        break;
                    case CONFIGURATIONKEY.OUTPUTPATH:
                        if (configuration != null && !string.IsNullOrWhiteSpace(configuration.OutputPath))
                            hasConfiguration = true;
                        break;
                }
            }
            catch (ControlRoomException)
            {
                Helper.LogToFile($"Could not be able to find key {key.ToString()}");
            }
            Helper.LogToFile("HasIQBotConfigurations Completed");
            return hasConfiguration;
        }

        public IQBotConfiguration GetIQBotConfiguration(string token)
        {
            Helper.LogToFile("GetIQBotConfiguration Started");
            ConfigurationList centralizedConfiguration = new ConfigurationList();
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteSecuredGetAsync(ROUTE_CENTRALIZEDCONFIGURATION, token);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    string centralizedConfigurationJson = responseMessage.Content.ReadAsStringAsync().Result;
                    centralizedConfiguration = JsonSerializer.Default.Deserialize<ConfigurationList>(centralizedConfigurationJson);
                }
                else
                    HandleControlRoomGenericErrors(responseMessage);
            }
            catch (ControlRoomException ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw;
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
            Helper.LogToFile("GetIQBotConfiguration Completed");
            return centralizedConfiguration.IQBotConfiguation;
        }

        public void AddPlatformURL(string token, string iqBotUrl, bool isUpdate)
        {
            Helper.LogToFile("AddPlatformURL Started");
            CentralizedConfiguration centralizedConfiguration = new CentralizedConfiguration();
            centralizedConfiguration.Category = CONFIGURATION_CATEGORY_IQBOT;
            centralizedConfiguration.Key = CONFIGURATION_KEY_PLATFORMURL;
            centralizedConfiguration.Value = iqBotUrl;
            if (isUpdate)
                UpdateConfiguration(token, centralizedConfiguration);
            else
                AddConfiguration(token, centralizedConfiguration);
            Helper.LogToFile("AddPlatformURL Completed");
        }

        public void AddOutputPath(string token, string outputPath, bool isUpdate)
        {
            Helper.LogToFile("AddOutputPath Started");
            CentralizedConfiguration centralizedConfiguration = new CentralizedConfiguration();
            centralizedConfiguration.Category = CONFIGURATION_CATEGORY_IQBOT;
            centralizedConfiguration.Key = CONFIGURATION_KEY_OUTPUTFOLDER;
            centralizedConfiguration.Value = outputPath;
            if (isUpdate)
                UpdateConfiguration(token, centralizedConfiguration);
            else
                AddConfiguration(token, centralizedConfiguration);
            Helper.LogToFile("AddOutputPath Completed");
        }

        private void AddConfiguration(string token, CentralizedConfiguration configuration)
        {
            Helper.LogToFile("AddConfiguration Started");
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteConfiguration(ROUTE_ADD_CONFIGURATION, configuration, token, false);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    string message = responseMessage.Content.ReadAsStringAsync().Result;
                    Helper.LogToFile($"Response: {message}");
                }
                else if (responseMessage.StatusCode == HttpStatusCode.InternalServerError)
                    throw new ConfigurationException("Configuration already exists");
                else if (responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    throw new ConfigurationException("Unauthorized user. Please make sure the user is Control Room Administrator");
            }
            catch (ConfigurationException ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw;
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
            Helper.LogToFile("AddConfiguration Completed");
        }

        private void UpdateConfiguration(string token, CentralizedConfiguration configuration)
        {
            Helper.LogToFile("UpdateConfiguration Started");
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteConfiguration(ROUTE_UPDATE_CONFIGURATION, configuration, token, true);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    string message = responseMessage.Content.ReadAsStringAsync().Result;
                    Helper.LogToFile($"Response: {message}");
                }
                else if (responseMessage.StatusCode == HttpStatusCode.InternalServerError)
                    throw new ConfigurationException("Configuration already exists");
                else if (responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    throw new ConfigurationException("Unauthorized user. Please make sure the user is Control Room Administrator");
            }
            catch (ControlRoomException ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw;
            }
            catch (Exception ex)
            {
                Helper.LogToFile($"Exception: {ex.ToString()}");
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
            Helper.LogToFile("UpdateConfiguration Completed");
        }

        public void ValidateControlRoomServiceAvailability()
        {
            Helper.LogToFile("ValidateControlRoomServiceAvailability Started");
            try
            {
                Task<HttpResponseMessage> responseTask = restHelper.ExecuteUnsecureGetAsync(ROUTE_STATUS_CR);
                HttpResponseMessage responseMessage = responseTask.Result;
                if (responseMessage.StatusCode != HttpStatusCode.OK)
                    throw new ControlRoomException(Resource.EXPRESS_CR_SERVICE_ERR_TEXT);
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error Communicating with Control Room: " + ex.ToString() + Environment.NewLine + ex.StackTrace.ToString());
                throw new ControlRoomException(Resource.EXPRESS_CR_SERVICE_ERR_TEXT);
            }
        }

        private void HandleControlRoomGenericErrors(HttpResponseMessage responseMessage)
        {
            if (responseMessage.StatusCode == HttpStatusCode.Unauthorized || responseMessage.StatusCode == HttpStatusCode.Forbidden)
            {
                throw new ControlRoomException(Resource.CONTROLROOM_INVALID_CREDENTIALS);
            }
            else if (responseMessage.StatusCode == HttpStatusCode.NotFound)
            {
                throw new ControlRoomException(Resource.CONTROLROOM_INCOMPATIBLE_VERSION_ERROR);
            }
            else
            {
                throw new ControlRoomException(Resource.CONTROLROOM_CONNECTION_ERROR);
            }
        }
    }
}