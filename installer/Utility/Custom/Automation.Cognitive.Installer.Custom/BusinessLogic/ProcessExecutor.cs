﻿using InstallShield.Interop;
using System;
using System.Diagnostics;
using System.IO;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic
{
    public class ProcessExecutor
    {
        private string _filePath;
        private const string START_SERVICES_FILENAME = "installandstartallservices.bat";
        private const string STOP_SERVICES_FILENAME = "stopanduninstallallservices.bat";
        private const string RABBITMQ_SBIN = @"C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin";
        private const string RABBITMQ_CTL = "rabbitmqctl.bat";
        private Msi.Install _install;

        public ProcessExecutor(Msi.Install install)
        {
            _install = install;
            _filePath = Path.Combine(install.GetProperty(Resource.INSTALLDIR), "Configurations");
        }

        public ProcessExecutor()
        { }

        public void InstallAndStartServices(bool isWinAuthEnabled, string username, string password)
        {
            string startFilePath = Path.Combine(_filePath, START_SERVICES_FILENAME);
            if (!File.Exists(startFilePath))
                return;
            if (isWinAuthEnabled)
                excuteProcess(startFilePath, string.Format("{0} {1}", username, password));
            else
                excuteProcess(startFilePath, string.Empty);
            System.Threading.Thread.Sleep(10000);
        }

        public void StopAndUninstallServices()
        {
            string stopFilePath = Path.Combine(_filePath, STOP_SERVICES_FILENAME);
            if (!File.Exists(stopFilePath))
                return;

            excuteProcess(stopFilePath, "");

            System.Threading.Thread.Sleep(3000);
        }

        public void GenerateClientCert(string openSSLFilepath, string pfxSourcefile, string securedata, string clientCertfilePath)
        {
            Helper.LogToFile("GenerateClientCert started");
            string commandlineArgs = string.Format("pkcs12 -in \"{0}\" -passin pass:{1} -clcerts -nokeys -out \"{2}\"", pfxSourcefile, securedata, clientCertfilePath);
            string commandToLog = string.Format("pkcs12 -in \"{0}\" -passin pass:******* -clcerts -nokeys -out \"{1}\"", pfxSourcefile, clientCertfilePath);
            Helper.LogToFile(string.Format("Process to execute: {0}, Arguments: {1}", openSSLFilepath, commandToLog));
            excuteProcess(openSSLFilepath, commandlineArgs);
            Helper.LogToFile("GenerateClientCert completed");
        }

        public void GenerateClientKey(string openSSLFilepath, string pfxSourcefile, string securedata, string clientKeyfilePath)
        {
            Helper.LogToFile("GenerateClientKey started");
            string commandlineArgs = string.Format("pkcs12 -in \"{0}\" -passin pass:{1} -nocerts -nodes -out \"{2}\"", pfxSourcefile, securedata, clientKeyfilePath);
            string commandlineArgsToLog = string.Format("pkcs12 -in \"{0}\" -passin pass:******* -nocerts -nodes -out \"{1}\"", pfxSourcefile, clientKeyfilePath);
            Helper.LogToFile(string.Format("Process to execute: {0}, Arguments: {1}", openSSLFilepath, commandlineArgsToLog));
            excuteProcess(openSSLFilepath, commandlineArgs);
            Helper.LogToFile("GenerateClientKey completed");
        }

        public void GenerateCACert(string openSSLFilepath, string pfxSourcefile, string securedata, string caCertfilePath)
        {
            Helper.LogToFile("GenerateCACert started");
            string commandlineArgs = string.Format("pkcs12 -in \"{0}\" -passin pass:{1} -cacerts -nokeys -chain -out \"{2}\"", pfxSourcefile, securedata, caCertfilePath);
            string commandlineArgsToLog = string.Format("pkcs12 -in \"{0}\" -passin pass:{1} -cacerts -nokeys -chain -out \"{2}\"", pfxSourcefile, "*******", caCertfilePath);
            Helper.LogToFile(string.Format("Process to execute: {0}, Arguments: {1}", openSSLFilepath, commandlineArgsToLog));
            excuteProcess(openSSLFilepath, commandlineArgs);
            Helper.LogToFile("GenerateCACert completed");
        }

        public void GeneratePlatformSelfSignedKeyAndCert(string openSSLFilepath, string openSSLConfigFilePath, string platformSSLkeyFilePath, string platformSSLCertFilePath, string securedata)
        {
            Helper.LogToFile("GeneratePlatformSelfSignedKeyAndCert started");
            string hostname = System.Net.Dns.GetHostEntry(Environment.MachineName).HostName;
            string commandlineArgs = string.Format("req -sha256 -newkey rsa:2048 -nodes -keyout \"{0}\" -x509 -days 1094 -nodes -out \"{1}\" -subj \"/CN={2}/C=US/ST=CF/L=SJ/O=AA/OU=IQB\" -passout pass:{3} -config \"{4}\"", platformSSLkeyFilePath, platformSSLCertFilePath, hostname, securedata, openSSLConfigFilePath);
            string commandlineArgsToLog = string.Format("req -sha256 -newkey rsa:2048 -nodes -keyout \"{0}\" -x509 -days 1094 -nodes -out \"{1}\" -subj \"/CN={2}/C=US/ST=CF/L=SJ/O=AA/OU=IQB\" -passout pass:{3} -config \"{4}\"", platformSSLkeyFilePath, platformSSLCertFilePath, hostname, "*******", openSSLConfigFilePath);
            Helper.LogToFile(string.Format("Process to execute: {0}, Arguments: {1}", openSSLFilepath, commandlineArgsToLog));
            excuteProcess(openSSLFilepath, commandlineArgs);
            Helper.LogToFile("GeneratePlatformSelfSignedKeyAndCert completed");
        }

        public void GeneratePlatformSelfSignedJKS(string keyToolFilepath, string platformSSLJKSFilePath, string securedata)
        {
            Helper.LogToFile("GeneratePlatformSelfSignedJKS started");
            string hostname = System.Net.Dns.GetHostEntry(Environment.MachineName).HostName;
            string commandlineArgs = string.Format("-genkey -noprompt -keyalg RSA -alias cognitiveplatform -keystore \"{0}\" -keypass {1} -storepass {2} -validity 1094 -keysize 2048 -dname \"CN={3},OU=AA,O=AA,L=AA,S=AA,C=AA\"", platformSSLJKSFilePath, securedata, securedata, hostname);
            string commandlineArgsToLog = string.Format("-genkey -noprompt -keyalg RSA -alias cognitiveplatform -keystore \"{0}\" -keypass {1} -storepass {2} -validity 1094 -keysize 2048 -dname \"CN={3},OU=AA,O=AA,L=AA,S=AA,C=AA\"", platformSSLJKSFilePath, "********", "********", hostname);
            Helper.LogToFile(string.Format("Process to execute: {0}, Arguments: {1}", keyToolFilepath, commandlineArgsToLog));
            excuteProcess(keyToolFilepath, commandlineArgs);
            Helper.LogToFile("GeneratePlatformSelfSignedCertJKS completed");
        }

        public void ModifyRabbitMQNodeErlangCookie(string manageCookieScriptPath, string nodeName)
        {
            Helper.LogToFile("ModifyRabbitMQNodeErlangCookie started");
            string cookieValue = Resource.RABBITMQ_COOKIE;

            string rabbitMQCTL = Path.Combine(RABBITMQ_SBIN, RABBITMQ_CTL);
            string commandlineArgs = string.Format("-n {0} eval \"erlang: set_cookie(node(), '{1}').\"", nodeName, cookieValue);
            excuteProcessWithWindow(rabbitMQCTL, commandlineArgs);
            Environment.SetEnvironmentVariable("RABBITMQ_CTL_ERL_ARGS", string.Format("-setcookie {0}", cookieValue), EnvironmentVariableTarget.User);

            Helper.LogToFile("ModifyRabbitMQNodeErlangCookie completed");
            System.Threading.Thread.Sleep(5000);
        }

        public void AddNodeToRabbitMQCluster(string addNodeScriptPath, string primaryNodeName)
        {
            Helper.LogToFile("AddNodeToRabbitMQCluster started");
            string rabbitMQCTL = Path.Combine(RABBITMQ_SBIN, RABBITMQ_CTL);

            // echo "stop broker" : rabbitmqctl stop_app
            string stopBrokerCommand = "stop_app";
            excuteProcessWithWindow(rabbitMQCTL, stopBrokerCommand);

            // "join node to cluster" : rabbitmqctl join_cluster %rabbit_mq_node_name%
            string joinClusterCommand = string.Format("join_cluster {0}", primaryNodeName);
            excuteProcessWithWindow(rabbitMQCTL, joinClusterCommand);

            // "start app" rabbitmqctl start_app
            string startBrokerCommand = "start_app";
            excuteProcessWithWindow(rabbitMQCTL, startBrokerCommand);

            // "mirror queues" : rabbitmqctl set_policy ha - all "." "{""ha-mode"":""all""}"
            string mirrorQueue = "set_policy ha-all \".\" \"{\"\"ha-mode\"\":\"\"all\"\"}\"";
            excuteProcessWithWindow(rabbitMQCTL, mirrorQueue);

            Helper.LogToFile("AddNodeToRabbitMQCluster completed");
        }

        public void RemoveNodeFromRabbitMQCluster(string removeNodeScriptPath, string nodeName)
        {
            Helper.LogToFile("RemoveNodeFromRabbitMQCluster started");
            string rabbitMQCTL = Path.Combine(RABBITMQ_SBIN, RABBITMQ_CTL);

            string stopBrokerCommand = "stop_app";
            excuteProcessWithWindow(rabbitMQCTL, stopBrokerCommand);

            // "join node to cluster" : rabbitmqctl join_cluster %rabbit_mq_node_name%
            string resetClusterCommand = "reset";
            excuteProcessWithWindow(rabbitMQCTL, resetClusterCommand);

            // "start app" rabbitmqctl start_app
            string startBrokerCommand = "start_app";
            excuteProcessWithWindow(rabbitMQCTL, startBrokerCommand);

            string addUserCommand = "add_user messagequeue passmessage";
            excuteProcessWithWindow(rabbitMQCTL, addUserCommand);

            string addHostCommand = "add_vhost test";
            excuteProcessWithWindow(rabbitMQCTL, addHostCommand);

            string setPermissionCommand = "set_permissions -p test messagequeue \".*\" \".*\" \".*\"";
            excuteProcessWithWindow(rabbitMQCTL, setPermissionCommand);

            string setUserTagsCommand = "set_user_tags messagequeue administrator";
            excuteProcessWithWindow(rabbitMQCTL, setUserTagsCommand);

            Helper.LogToFile("RemoveNodeFromRabbitMQCluster completed");
        }

        public void StartLiquibase(bool isExpress)
        {

            string liquibaseFilePath = string.Empty;
            if (!isExpress)
                liquibaseFilePath = Path.Combine(_install.GetProperty("SUPPORTDIR"), "liquibase.bat");
            else
                liquibaseFilePath = Path.Combine(_install.GetProperty("SUPPORTDIR"), "liquibase_Express.bat");
            if (!File.Exists(liquibaseFilePath))
                return;

            string arguments = string.Format("{0} {1} {2} {3} {4}",
                                             _install.GetProperty(Resource.DATABASE_SERVER),
                                             _install.GetProperty(Resource.DATABASE_USERNAME),
                                             string.Format("\"{0}\"", _install.GetProperty(Resource.DATABASE_PASSWORD).Trim()),
                                             _install.GetProperty(Resource.DATABASE_PORT),
                                             string.Format("\"{0}\"", _install.GetProperty("INSTALLDIR")));
            Helper.LogToFile("Info: Database structure creation start");
            excuteProcess(liquibaseFilePath, arguments);
            Helper.LogToFile("Info: Database structure creation End");
            System.Threading.Thread.Sleep(3000);
        }

        public void CreateRabbitMQUser()
        {
            string rabbitMqUserCreationFile = Path.Combine(_install.GetProperty("SUPPORTDIR"), "rabbitmqservice_user.bat");
            if (!File.Exists(rabbitMqUserCreationFile))
                return;
            excuteProcess(rabbitMqUserCreationFile, "");
            System.Threading.Thread.Sleep(3000);
        }

        public void InstallPrerequisites()
        {
            //System.Diagnostics.Debugger.Launch();
            /* string rabbitMq = Path.Combine(_install.GetProperty("SUPPORTDIR"), "rabbitmqservice.bat");
             if (_install.GetProperty(Resource.IS_RABBITMQ_INSTALLED) == "0")
             {
                 if (!File.Exists(rabbitMq))
                     return;
                 excuteProcess(rabbitMq, "");
                 System.Threading.Thread.Sleep(3000);
                 _install.SetProperty((Resource.IS_RABBITMQ_INSTALLED), "1");
             }*/

            /* string pm2FilePath = Path.Combine(_install.GetProperty("SUPPORTDIR"), "installpm2.bat");
             if (!File.Exists(pm2FilePath))
                 return;
             excuteProcess(pm2FilePath, "");
             System.Threading.Thread.Sleep(3000);
             MessageBoxWrapper w = new MessageBoxWrapper(_install);
             w.ShowErrorDialog("PM2 installed");*/
        }

        private void excuteProcess(string processFilePath, string arguments)
        {
            try
            {
                ProcessStartInfo procStartInfo = new ProcessStartInfo()
                {
                    RedirectStandardOutput = false,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    Verb = "runas",
                    FileName = processFilePath,
                    Arguments = arguments
                };

                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.WaitForExit(1000 * 60 * 5);
            }
            catch (Exception exception)
            {
                Helper.LogToFile("Error: Error executing process. " + exception.ToString());
            }
        }

        private void excuteProcessWithWindow(string processFilePath, string arguments)
        {
            Helper.LogToFile(string.Format("Process to execute: {0}, Arguments: {1}", processFilePath, arguments));
            try
            {
                ProcessStartInfo procStartInfo = new ProcessStartInfo()
                {
                    RedirectStandardOutput = false,
                    UseShellExecute = false,
                    CreateNoWindow = false,
                    Verb = "runas",
                    FileName = processFilePath,
                    Arguments = arguments,
                };

                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.WaitForExit(1000 * 60 * 5);
            }
            catch (Exception exception)
            {
                Helper.LogToFile("Error: Error executing process with window. " + exception.ToString());
            }
        }
    }
}