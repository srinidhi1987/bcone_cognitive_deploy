﻿using Automation.Cognitive.Installer.Custom.BusinessLogic.Database;
using InstallShield.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Express
{
    public class ExpressInstallationService
    {
        private Msi.Install install;
        private DatabaseHelper databaseHelper;
        public ExpressInstallationService(int msiHandle)
        {
            install = Msi.CustomActionHandle(msiHandle);
            databaseHelper = new DatabaseHelper(msiHandle);
        }

        public void SetExpressDefaultValues()
        {
            string localServerName = Helper.FQDNtoHostName(Helper.GetFullyQualifiedDomainName());

            // Database Values
            install.SetProperty(Resource.DATABASE_SERVER, Path.Combine(localServerName, Resource.EXPRESS_SQL_INSTANCENAME));
            install.SetProperty(Resource.DATABASE_PORT, Resource.EXPRESS_SQL_PORT);
            install.SetProperty(Resource.DATABASE_USERNAME, Resource.EXPRESS_SQL_USERNAME);
            install.SetProperty(Resource.DATABASE_PASSWORD, Resource.EXPRESS_SQL_SECUREDATA);
            install.SetProperty(Resource.IS_DATABASE_WINDOWS_AUTH, "0");

            //Client Configuration
            install.SetProperty(Resource.CLIENT_SECURITY_TYPE, "ISHTTP");
            install.SetProperty(Resource.CLIENT_HOSTNAME, "localhost");
            install.SetProperty(Resource.CLIENT_PORT, "3000");

            //Gateway Configuration 
            install.SetProperty(Resource.GATEWAY_HOSTNAME, "localhost");
            install.SetProperty(Resource.GATEWAY_PORT, "3000");
            install.SetProperty(Resource.IS_SSL_OFFLOADING, "0");
            install.SetProperty(Resource.USE_PORTAL_AS_LOADBALANCER,"1");

            //ControlRoom Configuration
            install.SetProperty(Resource.CONTROLROOM_SECURITY_TYPE, Resource.EXPRESS_CONTROLROOM_PROTOCOL);
            install.SetProperty(Resource.CONTROLROOM_IP, Helper.GetFullyQualifiedDomainName());
            install.SetProperty(Resource.CONTROLROOM_PORT, Resource.EXPRESS_CONTROLROOM_PORT);
            install.SetProperty(Resource.CONTROLROOM_URL, Helper.BuildURL("http", Helper.GetFullyQualifiedDomainName(), Resource.EXPRESS_CONTROLROOM_PORT));          
        }

        public void ResetExpressDefaultValues()
        {
            install.SetProperty(Resource.DATABASE_SERVER, string.Empty);
            install.SetProperty(Resource.DATABASE_PORT, string.Empty);
            install.SetProperty(Resource.DATABASE_USERNAME, string.Empty);
            install.SetProperty(Resource.DATABASE_PASSWORD, string.Empty);
            install.SetProperty(Resource.IS_DATABASE_WINDOWS_AUTH, "0");

            install.SetProperty(Resource.CLIENT_SECURITY_TYPE, "ISHTTPS");
            install.SetProperty(Resource.USE_PORTAL_AS_LOADBALANCER, "1");
        }
    }
}
