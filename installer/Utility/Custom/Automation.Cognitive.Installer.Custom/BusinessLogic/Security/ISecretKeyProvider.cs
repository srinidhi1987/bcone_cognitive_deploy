﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Security
{
    interface ISecretKeyProvider
    {
        byte[] GetKey();
    }
}
