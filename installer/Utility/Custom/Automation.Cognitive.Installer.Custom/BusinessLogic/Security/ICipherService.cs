﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Security
{
    public interface ICipherService
    {
        string Encrypt(string plainText);

        string decrypt(string cipherText);
    }
}
