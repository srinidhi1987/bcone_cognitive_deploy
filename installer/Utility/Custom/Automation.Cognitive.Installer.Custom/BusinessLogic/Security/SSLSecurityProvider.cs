﻿using InstallShield.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Security
{
    public class SSLSecurityProvider
    {
        protected Msi.Install install;
        private const string OPENSSL_EXE = "openssl.exe";
        private const string OPENSSL_CONFIG = "openssl.cfg";
        private const string KEYTOOL_EXE = "keytool.exe";
        private const string JAVA_KEYTOOL_ERRORMSG = "Unable to find Java Keytool on this machine. It is essential for enabling HTTPS/TLS for this IQ Bot Platform installation. Please make sure java runtime is installed and JAVA_HOME is set properly.";
        private const string CERT_INVALID_ERRORMSG = "Invalid certificate passphrase. Enter a valid passphrase.";
        private const string CERT_EXPIRED_ERRORMSG = "Certificate expired. Select a valid PFX format certificate.";
        private const string CERT_INVALIDEXTENSION_ERRORMSG = "Invalid certificate. Select a valid PFX format certificate.";
        private const string SSL_OFFLOADING_ERRMSG = "Load Balancer details to enable SSL offloading are incorrect.";

        public SSLSecurityProvider()
        {
        }

        public SSLSecurityProvider(Msi.Install msiHandle)
        {
            install = msiHandle;
        }

        public void ExctractPrivateKeyAndCopyCertificate()
        {
            Helper.LogToFile("ExctractPrivateKeyAndCopyCertificate started");
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);

            string installDir = install.GetProperty(Resource.INSTALLDIR);
            string supportDir = install.GetProperty(Resource.SUPPORTDIR);
            string openSSLFilepath = Path.Combine(install.GetProperty(Resource.SUPPORTDIR), OPENSSL_EXE);
            string openSSLConfigFilepath = Path.Combine(install.GetProperty(Resource.SUPPORTDIR), OPENSSL_CONFIG);
            string pfxCertificatePath = install.GetProperty(Resource.CERTIFICATE_PATH);
            string pfxClientCertOutputPath = Path.Combine(installDir, Resource.PORTAL_CERT_HOMEPATH, Resource.PORTAL_CERT_CLIENT);
            string pfxCACertOutputPath = Path.Combine(installDir, Resource.PORTAL_CERT_HOMEPATH, Resource.PORTAL_CERT_CA);
            string pfxKeyCertOutputPath = Path.Combine(installDir, Resource.PORTAL_CERT_HOMEPATH, Resource.PORTAL_CERT_KEY);
            string pfxCertificatePassphrase = install.GetProperty(Resource.CERTIFICATE_PASSPHRASE);


            ProcessExecutor executor = new ProcessExecutor();
            //executor.GenerateCACert(openSSLFilepath, pfxCertificatePath, pfxCertificatePassphrase, pfxCACertOutputPath);
            executor.GenerateClientKey(openSSLFilepath, pfxCertificatePath, pfxCertificatePassphrase, pfxKeyCertOutputPath);
            executor.GenerateClientCert(openSSLFilepath, pfxCertificatePath, pfxCertificatePassphrase, pfxClientCertOutputPath);


            if (File.Exists(pfxKeyCertOutputPath) && File.Exists(pfxClientCertOutputPath))
                Helper.LogToFile("GenerateSelfSignedCRTandKeyFiles: Certificate and Key files verified successfully.");
            else
                throw new ApplicationException("Error generating Certificate and Key files.Check logs for more details.");


            Helper.LogToFile("ExctractPrivateKeyAndCopyCertificate completed");
        }

        public void GenerateSelfSignedCRTandKeyFiles()
        {
            Helper.LogToFile("GenerateSelfSignedCRTandKeyFiles started");
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);

            string installDir = install.GetProperty(Resource.INSTALLDIR);
            string supportDir = install.GetProperty(Resource.SUPPORTDIR);
            string openSSLFilepath = Path.Combine(install.GetProperty(Resource.SUPPORTDIR), OPENSSL_EXE);
            string openSSLConfigFilepath = Path.Combine(install.GetProperty(Resource.SUPPORTDIR), OPENSSL_CONFIG);
            string platformSSLCertFilePath = Path.Combine(installDir, Resource.PLATFORM_CERT_HOMEPATH, Resource.PLATFORM_CRT_CRT);
            string platformSSLkeyFilePath = Path.Combine(installDir, Resource.PLATFORM_CERT_HOMEPATH, Resource.PLATFORM_CRT_KEY);

            ProcessExecutor executor = new ProcessExecutor();
            executor.GeneratePlatformSelfSignedKeyAndCert(openSSLFilepath, openSSLConfigFilepath, platformSSLkeyFilePath, platformSSLCertFilePath, Resource.PLATFORM_CERT_PASSPHRASE);

            if (File.Exists(platformSSLCertFilePath) && File.Exists(platformSSLkeyFilePath))
                Helper.LogToFile("GenerateSelfSignedCRTandKeyFiles: Certificate and Key files verified successfully.");
            else
                throw new ApplicationException("Error generating Certificate and Key files.Check logs for more details.");

            Helper.LogToFile("GenerateSelfSignedCRTandKeyFiles completed");
        }

        public void GenerateSelfSignedJSKFile()
        {
            Helper.LogToFile("GenerateSelfSignedJSKFile started");
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);

            string installDir = install.GetProperty(Resource.INSTALLDIR);
            string supportDir = install.GetProperty(Resource.SUPPORTDIR);
            string keytoolFilePath = Path.Combine(Environment.GetEnvironmentVariable("JAVA_HOME"), "bin", KEYTOOL_EXE);
            string platformSSLJKSFilePath = Path.Combine(installDir, Resource.PLATFORM_CERT_HOMEPATH, Resource.PLATFORM_CRT_JKS);

            if (!File.Exists(keytoolFilePath))
            {
                throw new ApplicationException(JAVA_KEYTOOL_ERRORMSG);
            }

            ProcessExecutor executor = new ProcessExecutor();
            executor.GeneratePlatformSelfSignedJKS(keytoolFilePath, platformSSLJKSFilePath, Resource.PLATFORM_CERT_PASSPHRASE);

            if (File.Exists(platformSSLJKSFilePath))
                Helper.LogToFile("GenerateSelfSignedJSKFile: Java KeyStore file verified successfully.");
            else
                throw new ApplicationException("Error generating Java KeyStore file. Check logs for more details.");

            Helper.LogToFile("GenerateSelfSignedJSKFile completed.");
        }

        public void ValidatePFXCertificate()
        {
            string certFilePath = install.GetProperty(Resource.CERTIFICATE_PATH);
            string certPassPhrase = install.GetProperty(Resource.CERTIFICATE_PASSPHRASE);
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            try
            {
                if (!IsValidPFXFile(certFilePath))
                {
                    messageBoxWrapper.ShowErrorDialog(CERT_INVALIDEXTENSION_ERRORMSG);
                    return;
                }

                X509Certificate cert = new X509Certificate(certFilePath, certPassPhrase);
                DateTime expirationDate;
                bool validDate = DateTime.TryParse(cert.GetExpirationDateString(), out expirationDate);
                if (!validDate || expirationDate < DateTime.Now)
                {
                    messageBoxWrapper.ShowErrorDialog(CERT_EXPIRED_ERRORMSG);
                    return;
                }
                else
                    Helper.LogToFile("certificate expiry date: " + expirationDate.ToString());

                install.SetProperty(Resource.CERTIFICATE_VALIDATED, "1");
            }
            catch (Exception ex)
            {
                Helper.LogToFile(ex.ToString());
                messageBoxWrapper.ShowErrorDialog(CERT_INVALID_ERRORMSG);
            }
        }

        public bool IsValidPFXFile(string filepath)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            if (fileInfo.Extension.Equals(".pfx", StringComparison.InvariantCultureIgnoreCase))
                return true;
            else
                return false;
        }

        public void ValidateSSLOffloadingSettings()
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            string sslOffloading = install.GetProperty(Resource.IS_SSL_OFFLOADING);
            string loadBalancerHost = install.GetProperty(Resource.GATEWAY_HOSTNAME);
            string loadBalancerPort = install.GetProperty(Resource.GATEWAY_PORT);
            string clientSecurityType = install.GetProperty(Resource.CLIENT_SECURITY_TYPE);
            string clientHostName = install.GetProperty(Resource.CLIENT_HOSTNAME);
            string clientPort = install.GetProperty(Resource.CLIENT_PORT);
            string usePortalAsLoadBalancer = install.GetProperty(Resource.USE_PORTAL_AS_LOADBALANCER);


            install.SetProperty(Resource.PLATFORM_SSLOFFLOAD_VALIDATED, "0");

            if (!usePortalAsLoadBalancer.Equals("1",StringComparison.InvariantCultureIgnoreCase) && loadBalancerHost.Equals(clientHostName, StringComparison.InvariantCultureIgnoreCase) && loadBalancerPort.Equals(clientPort, StringComparison.InvariantCultureIgnoreCase) && sslOffloading.Equals("1", StringComparison.InvariantCultureIgnoreCase))
            {
                messageBoxWrapper.ShowErrorDialog(SSL_OFFLOADING_ERRMSG);
            }
            else
                install.SetProperty(Resource.PLATFORM_SSLOFFLOAD_VALIDATED, "1");
        }

    }
}
