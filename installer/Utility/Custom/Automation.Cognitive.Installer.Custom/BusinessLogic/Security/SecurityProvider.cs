﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Security
{
    public class SecurityProvider
    {
        public void GeneratePrivateKeyFile(string privatekeyFilePath)
        {
            string privatekey = Helper.RandomString(32);
         
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(privatekeyFilePath, false))
            {
                file.Write(privatekey);
            }
        }

        public void EncryptSecureDataInFile(string privatekeyFilePath, string secureFilePath, string databaseSecureData)
        {
            ISecretKeyProvider keyProvider = new SecretKeyProvider(privatekeyFilePath);
            ICipherService ciperService = new CipherService(keyProvider);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(secureFilePath, false))
            {
                file.WriteLine("database.password = " + ciperService.Encrypt(databaseSecureData));
                file.WriteLine("rabbitmq.password = " + ciperService.Encrypt(Resource.PLATFORM_MQ_PASSPHRASE));
                file.WriteLine("abbyy.license.key = ");
            }
        }
    }
}
