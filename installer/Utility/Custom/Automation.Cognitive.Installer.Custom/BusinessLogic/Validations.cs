﻿using InstallShield.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic
{
    public class Validations
    {
        private const string NO_ACCESS_PERMISSION = "The folder specified in the output path doesn’t have sufficient read/write/delete permission(s).\n\n Please grant the necessary permission(s) to the folder and try this step again.";
        private const string FOLDER_NOT_EXIST = "The folder specified in the output path does not exist.\n\n Please create the folder and try this step again.";
        private const string TEMP_FILE_NAME = "IQBotTestFile.txt";


        public Validations()
        {
        }

        public bool isValidLocation(Msi.Install install)
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);

            string outputPath = install.GetProperty(Resource.OUTPUT_PATH);

            if (string.IsNullOrWhiteSpace(outputPath))
            {
                return false;
            }
            if (!Directory.Exists(outputPath))
            {
                try
                {
                    Directory.CreateDirectory(outputPath);
                }
                catch (Exception ex)
                {
                    Helper.LogToFile("Error creating output directory: " + ex.ToString());
                    messageBoxWrapper.ShowErrorDialog(NO_ACCESS_PERMISSION);
                    return false;
                }
            }
            bool hasAccess = false;

            string tempFilePath = Path.Combine(outputPath, TEMP_FILE_NAME);

            try
            {
                using (FileStream fs = new FileStream(tempFilePath, FileMode.CreateNew, FileAccess.Write))
                {
                    fs.WriteByte(0xff);
                }
                if (File.Exists(tempFilePath))
                {
                    File.Delete(tempFilePath);
                    hasAccess = true;
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error creating temp file to output directory: " + ex.ToString());
                messageBoxWrapper.ShowErrorDialog(NO_ACCESS_PERMISSION);
                hasAccess = false;
            }
            return hasAccess;
        }

        public void ValidateMicroServicePorts(Msi.Install install)
        {
            bool aliasPortValidated, authPortValidated, fileManagerPortValidated, gatewayPortValidated, mlPortValidated, reportPortValidated, validatorPortValidated, visionPortValidated, projectPortValidated, portalPortValidated;
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            install.SetProperty(Resource.PORT_NOT_IN_USE, "0");
            string installationType = install.GetProperty(Resource.INSTALLTYPE);

            aliasPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_ALIAS);
            authPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_AUTHENTICATION);
            fileManagerPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_FILEMANAGER);
            gatewayPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_GATEWAY);
            mlPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_ML);
            reportPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_REPORT);
            validatorPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_VALIDATOR);
            visionPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_VISIOBOT);
            projectPortValidated = Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_PROJECT);
            portalPortValidated = installationType.Equals("EXPRESS", StringComparison.InvariantCultureIgnoreCase) ? Helper.IsPortAvailableToUse(Resource.PORT_SERVICE_PORTAL) : true;

            if (!aliasPortValidated ||
                !authPortValidated ||
                !fileManagerPortValidated ||
                !gatewayPortValidated ||
                !mlPortValidated ||
                !reportPortValidated ||
                !validatorPortValidated ||
                !visionPortValidated ||
                !projectPortValidated ||
                !portalPortValidated)
            {
                messageBoxWrapper.ShowErrorDialog(Helper.GetPortInUseErrorMessage(aliasPortValidated, authPortValidated, fileManagerPortValidated, gatewayPortValidated, mlPortValidated, reportPortValidated, validatorPortValidated, visionPortValidated, projectPortValidated, portalPortValidated));
            }
            else
                install.SetProperty(Resource.PORT_NOT_IN_USE, "1");

        }
    }
}