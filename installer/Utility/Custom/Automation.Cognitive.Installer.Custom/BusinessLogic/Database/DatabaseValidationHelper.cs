﻿using Automation.Cognitive.Installer.Custom;
using Automation.Cognitive.Installer.Custom.Model;
using Automation.Cognitive.Installer.BusinessLogic.Security.ActiveDirectoryAuthenticators;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom
{
    public class DatabaseValidationHelper
    {
        public bool IsValidDBConfigurationFields(DatabaseConfigurations dbConfiguration)
        {
            if (dbConfiguration == null
               || string.IsNullOrWhiteSpace(dbConfiguration.DatabaseServerName)
               || string.IsNullOrWhiteSpace(dbConfiguration.DatabaseUserName)
               || string.IsNullOrWhiteSpace(dbConfiguration.DatabasePassword)
               || string.IsNullOrWhiteSpace(dbConfiguration.DatabaseName)
               || dbConfiguration.DatabasePort <= 0)
            {
                return false;
            }
            return true;
        }

        public bool IsPortInRange(int port, int portMin, int portMax)
        {
            return port >= portMin && port <= portMax;
        }

        public bool IsValidDatabaseCredentials(DatabaseConfigurations configuration)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnectionString(configuration)))
                {
                    conn.Open();
                    conn.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error connecting to SQL Server: " + ex.StackTrace);
                return false;
            }
        }

        public bool IsValidActiveDirectoryUser(string domainName, string userName, string securedata)
        {
            try
            {
                IActiveDirectoryCredentialValidator[] activeDirectoryAuthenticators = new IActiveDirectoryCredentialValidator[] {
                new RapidLdapActiveDirectoryCredentialValidator(),
                new LdapConnectionActiveDirectoryCredentialValidator(),
                new PrincipalContextActiveDirectoryCredentialValidator()
           };

                foreach (var credentialValidator in activeDirectoryAuthenticators)
                {
                    Helper.LogToFile($"Start validating using '{credentialValidator.GetType().Name}' ...");

                    if (credentialValidator.ValidateNoSSL(domainName, userName, securedata))
                    {
                        Helper.LogToFile($"Succeed using No SSL call.");
                        return true;
                    }
                    else
                    {
                        Helper.LogToFile($"Failed using No SSL call.");
                    }

                    if (credentialValidator.ValidateWithSSL(domainName, userName, securedata))
                    {
                        Helper.LogToFile($"Succeed using with SSL call.");
                        return true;
                    }
                    else
                    {
                        Helper.LogToFile($"Failed using SSL call.");
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("IsValidUserCredentials Exception : " + ex.Message);
            }
            return false;
        }

        public bool IsValidAdminUser(DatabaseConfigurations configuration)
        {
            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(GetConnectionString(configuration));

                if (sqlConnection == null)
                {
                    return false;
                }

                sqlConnection.Open();
                string command = string.Format("SELECT p.name AS [login],p.type ,p.type_desc ,p.is_disabled,CONVERT(VARCHAR(10), p.create_date, 101) AS [created],CONVERT(VARCHAR(10), p.modify_date, 101) AS[update] FROM sys.server_principals p JOIN sys.syslogins s ON p.sid = s.sid WHERE p.type_desc IN ('SQL_LOGIN', 'WINDOWS_LOGIN', 'WINDOWS_GROUP') AND p.name NOT LIKE '##%'AND s.sysadmin = 1");

                List<string> adminUsersList = new List<string>();
                SqlCommand sqlcommand = new SqlCommand(command, sqlConnection);
                using (SqlDataReader reader = sqlcommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        adminUsersList.Add(reader.GetString(0));
                    }
                }
                sqlConnection.Close();
                if (configuration.IsWindowsAuthentication)
                {
                    return adminUsersList.Contains(Environment.UserDomainName + "\\" + Environment.UserName);
                }
                else
                    return adminUsersList.Contains(configuration.DatabaseUserName);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string GetConnectionString(DatabaseConfigurations configuration)
        {
            string connectionString = string.Empty;
            if (configuration.IsWindowsAuthentication)
            {
                connectionString = string.Format(
                                Resource.DBConnectionString_WindowsAuthentication,
                                configuration.DatabaseServerName,
                                configuration.DatabasePort.ToString(),
                                configuration.DatabaseName);
            }
            else
            {
                connectionString = string.Format(
                                Resource.DBConnectionString_StandardAuthentication,
                                configuration.DatabaseServerName,
                                configuration.DatabasePort.ToString(),
                                configuration.DatabaseName,
                                configuration.DatabaseUserName,
                                configuration.DatabasePassword);
            }
            return connectionString;
        }

        public string GetSQLExpressConnectionString(DatabaseConfigurations configuration)
        {
            string connectionString = string.Empty; ;
            connectionString = string.Format(
                            Resource.EXPRESS_SQL_CONNECTION_STRING,
                            configuration.DatabaseServerName,
                            "master",
                            configuration.DatabaseUserName,
                            configuration.DatabasePassword);

            return connectionString;
        }
    }
}