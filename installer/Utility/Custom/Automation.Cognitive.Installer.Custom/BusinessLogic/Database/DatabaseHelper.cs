﻿using System;
using System.Collections.Generic;
using Automation.Cognitive.Installer.Custom.Model;
using InstallShield.Interop;

using System.Data.SqlClient;
using System.Data;
using System.Net;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Database
{
    public class DatabaseHelper
    {
        private const string DATABASE_EXISTS_QUERY = "SELECT 1 FROM {0} WHERE name='{1}'";
        private const string DB_SYSTEMTABLE_DATABASE_NAME = "sys.databases";
        private const string CREATE_DATABASE_QUERY = "CREATE DATABASE \"{0}\"";
        public const string CONFIGURATION_KEY_OUTPUTFOLDER = "CognitivePlatformOutputPath";
        private const string SYSTEM = "SYSTEM";
        const char USERNAME_DELIMITER = '\\';
        private readonly List<string> databaseNames;
        private ComponentConfigurations componentConfiguration;

        private DatabaseValidationHelper _databaseValidationHelper;
        protected Msi.Install install;
        private Helper _helper;
        private DatabaseConfigurations databaseConfiguration;
        private MessageBoxWrapper messageBoxWrapper;
        public const string AAE_NOT_INSTALLED = "Please make sure that Enterprise Client is installed on this machine and a user currently logged in to it. This is a prerequisite for IQ Bot Installation.";

        public string ControlRoomLink { get; set; }

        public DatabaseConfigurations DatabaseConfiguration
        {
            get
            {
                return databaseConfiguration;
            }
        }

        #region public

        public DatabaseHelper()
        {
        }

        public DatabaseHelper(DatabaseConfigurations databaseConfiguration)
        {
            this.databaseConfiguration = databaseConfiguration;
            _databaseValidationHelper = new DatabaseValidationHelper();
        }

        public DatabaseHelper(int msiHandle)
        {
            install = Msi.CustomActionHandle(msiHandle);
            _databaseValidationHelper = new DatabaseValidationHelper();
            messageBoxWrapper = new MessageBoxWrapper(install);
            _helper = new Helper();
            databaseConfiguration = getDatabaseConfigurations();
            databaseNames = new List<string> { "FileManager", "AliasData", "ClassifierData", "Configurations", "MLData" };
            componentConfiguration = new ComponentConfigurations(msiHandle);
        }

        public bool ValidateDatabaseConfigurationsDialog(int msiHandle)
        {
            bool isValidationSuccess = false;
            try
            {
                isValidationSuccess = validateDatabaseConfiguration(msiHandle, install);
                install.SetProperty(Resource.DATABASE_CONFIGURATION_DIALOG_VALIDATION_PASSED, isValidationSuccess.ToString());
            }
            catch (Exception ex)
            {
                messageBoxWrapper.ShowErrorDialog(ex.Message);
                Helper.LogToFile("ValidateDatabaseConfigurationsDialog:" + ex.Message);
            }
            return isValidationSuccess;
        }

        public bool CreateDatabase()
        {
            bool isDatabaseExists = false;
            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(GetConnectionString(DatabaseConfiguration));

                if (sqlConnection == null)
                {
                    return false;
                }

                sqlConnection.Open();
                foreach (string databaseName in databaseNames)
                {
                    string commandText = string.Format(DATABASE_EXISTS_QUERY, DB_SYSTEMTABLE_DATABASE_NAME, databaseName);
                    using (SqlCommand cmd = new SqlCommand(commandText, sqlConnection))
                    {
                        isDatabaseExists = cmd.ExecuteScalar() != null;
                    }
                    if (!isDatabaseExists)
                    {
                        commandText = string.Format(CREATE_DATABASE_QUERY, databaseName/*databaseConfiguration.DatabaseName*/);
                        SqlCommand command = new SqlCommand(commandText);
                        command.Connection = sqlConnection;
                        command.ExecuteNonQuery();
                    }
                }
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                Helper.LogToFile("CreateDatabase: " + ex.Message);
                return false;
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }

            Helper.LogToFile("CreateDatabase: Database configurations validated successfully");
            return true;
        }


        public bool InsertSettingstoConfigurationDatabase()
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            bool status = false;
            DatabaseConfiguration.DatabaseName = "Configurations";
            string connectionString = string.Empty;
            connectionString = GetConnectionString(DatabaseConfiguration);

            SqlConnection sqlConnection = null;
            CognitiveConfiguration cogConfiguration = GetCognitiveConfiguration();
            CognitiveConfiguration existingNodeConfiguration = ReadNodeConfigration(Helper.GetFullyQualifiedDomainName(), false);
            CognitiveConfiguration primaryNodeConfiguration = ReadNodeConfigration(string.Empty, true);

            if (!string.IsNullOrEmpty(primaryNodeConfiguration.NodeName) && !primaryNodeConfiguration.NodeName.Equals(Helper.GetFullyQualifiedDomainName(), StringComparison.InvariantCultureIgnoreCase))
                cogConfiguration.IsPrimaryNode = false;

            string commandText = null;
            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    if (!string.IsNullOrEmpty(existingNodeConfiguration.NodeName) && existingNodeConfiguration.NodeName.Equals(Helper.GetFullyQualifiedDomainName(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        commandText = "UPDATE [dbo].[CognitivePlatformNodes] Set [LoadBalancerURL]=@LoadBalancerURL ,[ControlRoomURL]=@ControlRoomURL,[ClientPlatformUrl]=@ClientPlatformUrl,[IsExpressInstallation]=@IsExpressInstallation,[OutputPath]=@OutputPath ,[InstalledOn]=@InstalledOn,[IsPrimaryNode]=@IsPrimaryNode where NodeName=@NodeName";
                    }
                    else
                    {
                        commandText = "INSERT INTO [dbo].[CognitivePlatformNodes]([NodeName],[LoadBalancerURL],[ControlRoomURL],[ClientPlatformUrl],[IsExpressInstallation],[OutputPath],[InstalledOn],[IsPrimaryNode]) VALUES(@NodeName,@LoadBalancerURL,@ControlRoomURL,@ClientPlatformUrl,@IsExpressInstallation,@OutputPath,@InstalledOn,@IsPrimaryNode)";
                    }

                    SqlCommand command = new SqlCommand(commandText, sqlConnection);

                    command.Parameters.AddWithValue("@NodeName", cogConfiguration.NodeName);
                    command.Parameters.AddWithValue("@LoadBalancerURL", cogConfiguration.LoadBalancerUrl);
                    command.Parameters.AddWithValue("@ControlRoomURL", cogConfiguration.ControlRoomUrl);
                    command.Parameters.AddWithValue("@ClientPlatformUrl", cogConfiguration.ClientPlatformUrl);
                    command.Parameters.AddWithValue("@IsExpressInstallation", cogConfiguration.IsExpressInstallation);
                    command.Parameters.AddWithValue("@OutputPath", cogConfiguration.OutputPath);
                    command.Parameters.AddWithValue("@InstalledOn", cogConfiguration.InstallOn);
                    command.Parameters.AddWithValue("@IsPrimaryNode", cogConfiguration.IsPrimaryNode);

                    command.ExecuteNonQuery();
                    status = true;
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error configuring Configuration Database: " + ex.ToString());
                messageBoxWrapper.ShowErrorDialog("Error configuring Configuration Database, please check logs for more details.");
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }

            return status;
        }

        public CognitiveConfiguration ReadNodeConfigration(string NodeName, bool isPrimaryNodeConfigRead)
        {
            DatabaseConfiguration.DatabaseName = "Configurations";
            string connectionString = string.Empty;
            connectionString = GetConnectionString(DatabaseConfiguration);

            SqlConnection sqlConnection = null;
            string commandText = string.Empty;
            CognitiveConfiguration cogConfiguration = new CognitiveConfiguration();
            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    if (isPrimaryNodeConfigRead)
                        commandText = "Select [NodeName],[LoadBalancerURL],[ControlRoomURL],[ClientPlatformUrl],[IsExpressInstallation],[OutputPath],[InstalledOn],[IsPrimaryNode] from [dbo].[CognitivePlatformNodes] where IsPrimaryNode=@IsPrimarynode";
                    else
                        commandText = "Select [NodeName],[LoadBalancerURL],[ControlRoomURL],[ClientPlatformUrl],[IsExpressInstallation],[OutputPath],[InstalledOn],[IsPrimaryNode] from [dbo].[CognitivePlatformNodes] where NodeName=@NodeName";
                    SqlCommand command = new SqlCommand(commandText, sqlConnection);
                    if (isPrimaryNodeConfigRead)
                        command.Parameters.AddWithValue("@IsPrimarynode", true);
                    else
                        command.Parameters.AddWithValue("@NodeName", NodeName);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            cogConfiguration.NodeName = reader["NodeName"] != null ? reader["NodeName"].ToString() : string.Empty;
                            cogConfiguration.LoadBalancerUrl = reader["LoadBalancerURL"] != null ? reader["LoadBalancerURL"].ToString() : string.Empty;
                            cogConfiguration.ControlRoomUrl = reader["ControlRoomURL"] != null ? reader["ControlRoomURL"].ToString() : string.Empty;
                            cogConfiguration.ClientPlatformUrl = reader["ClientPlatformUrl"] != null ? reader["ClientPlatformUrl"].ToString() : string.Empty;
                            cogConfiguration.IsExpressInstallation = reader["IsExpressInstallation"] != null ? Convert.ToBoolean(reader["IsExpressInstallation"]) : false;
                            cogConfiguration.OutputPath = reader["OutputPath"] != null ? reader["OutputPath"].ToString() : string.Empty;
                            cogConfiguration.InstallOn = reader["InstalledOn"] != null ? reader["InstalledOn"].ToString() : string.Empty;
                            cogConfiguration.IsPrimaryNode = reader["IsPrimaryNode"] != null ? Convert.ToBoolean(reader["IsPrimaryNode"]) : false;
                        }
                    }

                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("ReadNodeConfigration: Error: " + ex.ToString());
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
            return cogConfiguration;
        }

        public List<Configurations> ReadConfigurations()
        {
            DatabaseConfiguration.DatabaseName = "Configurations";
            string connectionString = GetConnectionString(DatabaseConfiguration);

            List<Configurations> configurationList = new List<Configurations>();

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                string commandText = "Select [key],[value] from [dbo].[Configurations]";
                SqlCommand command = new SqlCommand(commandText, sqlConnection);


                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string key = reader["key"] != null ? reader["key"].ToString() : string.Empty;
                        string value = reader["value"] != null ? reader["value"].ToString() : string.Empty;
                        configurationList.Add(new Configurations(key, value));
                    }

                    if (reader != null) reader.Close();
                }

                if (sqlConnection != null) sqlConnection.Close();
            }

            return configurationList;
        }

        public void ClearConfigurationEntryForCurrentNode(string instancename, string username, string port, string databaseSecureData, string installtype)
        {
            string connectionString = string.Empty;
            string nodeName = Helper.GetFullyQualifiedDomainName();
            if (installtype.Equals("EXPRESS", StringComparison.InvariantCultureIgnoreCase))
                connectionString = string.Format(
                                Resource.EXPRESS_SQL_CONNECTION_STRING,
                                instancename,
                                "Configurations",
                                username,
                                databaseSecureData);
            else
                connectionString = string.Format(
                                Resource.DBConnectionString_StandardAuthentication,
                                instancename,
                                port,
                                "Configurations",
                                username,
                                databaseSecureData);

            SqlConnection sqlConnection = null;
            string commandText = string.Empty;
            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    commandText = "DELETE FROM [dbo].[CognitivePlatformNodes] where NodeName=@NodeName";
                    SqlCommand command = new SqlCommand(commandText, sqlConnection);
                    command.Parameters.AddWithValue("@NodeName", nodeName);
                    command.ExecuteNonQuery();
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("ClearConfigurationEntryForCurrentNode: Error: " + ex.ToString());
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        private CognitiveConfiguration GetCognitiveConfiguration()
        {
            CognitiveConfiguration cogConfiguration = new CognitiveConfiguration();
            cogConfiguration.NodeName = Helper.GetFullyQualifiedDomainName();
            cogConfiguration.ControlRoomUrl = install.GetProperty(Resource.CONTROLROOM_URL);
            cogConfiguration.ClientPlatformUrl = Helper.BuildURL(install.GetProperty(Resource.CLIENT_SECURITY_TYPE).Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase) ? "https" : "http", install.GetProperty(Resource.CLIENT_HOSTNAME), install.GetProperty(Resource.CLIENT_PORT));
            cogConfiguration.InstallOn = DateTime.UtcNow.ToString();
            cogConfiguration.IsExpressInstallation = install.GetProperty(Resource.INSTALLTYPE).Equals("EXPRESS", StringComparison.InvariantCultureIgnoreCase) ? true : false;
            cogConfiguration.IsPrimaryNode = true;
            cogConfiguration.OutputPath = install.GetProperty(Resource.OUTPUT_PATH);
            cogConfiguration.LoadBalancerUrl = componentConfiguration.GetCognitiveBaseURL();

            return cogConfiguration;
        }

        public void ValidateSQLExpressInstanceConnection()
        {
            try
            {
                string connectionString = _databaseValidationHelper.GetSQLExpressConnectionString(DatabaseConfiguration);
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();

                if (connection.State != ConnectionState.Open)
                {
                    throw new DatabaseExeption(Resource.EXPRESS_SQL_CONNECTION_ERR_TEXT);
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error connecting to default instance of SQL Express: " + ex.ToString());
                throw new DatabaseExeption(Resource.EXPRESS_SQL_CONNECTION_ERR_TEXT);
            }
        }
        public void UpdateIQBotConfigurationForExpressInstallation()
        {
            string cognitivePlatformUrl = componentConfiguration.GetCognitiveBaseURL();
            string cognitiveOutputPath = install.GetProperty(Resource.OUTPUT_PATH);
            bool isUpdate = CRExpressHasIQbotCategory();
            UpdateIQBotCategoryForExpressInstallation(Resource.CONTROLROOM_IQBOT_CATEGORY_OUTPUTPATH, cognitiveOutputPath, isUpdate);
            UpdateIQBotCategoryForExpressInstallation(Resource.CONTROLROOM_IQBOT_CATEGORY_PLATFORM, cognitivePlatformUrl, isUpdate);


        }
        public void UpdateIQBotCategoryForExpressInstallation(string key, string value, bool update)
        {
            Helper.LogToFile(string.Format("UpdateIQBotCategoryForExpressInstallation: Key:{0}, Value:{1}, Update:{2}", key, value, update.ToString()));
            string connectionString = string.Empty;
            SqlConnection sqlConnection = null;
            DatabaseConfiguration.DatabaseName = "CRDB";
            connectionString = GetConnectionString(DatabaseConfiguration);

            string commandText = null;
            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    if (update)
                    {
                        commandText = "UPDATE [CRDB].[dbo].[centralizedconfiguration] Set [Category]=@Category,[Key]=@Key,[Value]=@Value,[CreatedOn]=@CreatedOn,[CreatedBy]=@CreatedBy,[UpdatedOn]=@UpdatedOn,[UpdatedBy]=@UpdatedBy,[DomainId]=@DomainId,[RowVersion]=@RowVersion where Category=@Category AND [Key]=@Key";
                    }
                    else
                    {
                        commandText = "INSERT INTO [dbo].[centralizedconfiguration]([Category],[Key],[Value],[CreatedOn],[CreatedBy],[UpdatedOn],[UpdatedBy],[DomainId],[RowVersion]) VALUES(@Category,@Key,@Value,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy,@DomainId,@RowVersion)";
                    }

                    SqlCommand command = new SqlCommand(commandText, sqlConnection);
                    Helper.LogToFile("UpdateIQBotCategoryForExpressInstallation: command: " + commandText);
                    command.Parameters.AddWithValue("@Category", "IQBot");
                    command.Parameters.AddWithValue("@Key", key);
                    command.Parameters.AddWithValue("@Value", value);
                    command.Parameters.AddWithValue("@CreatedOn", DateTime.UtcNow);
                    command.Parameters.AddWithValue("@CreatedBy", 1);
                    command.Parameters.AddWithValue("@UpdatedOn", DateTime.UtcNow);
                    command.Parameters.AddWithValue("@UpdatedBy", 1);
                    command.Parameters.AddWithValue("@DomainId", 1);
                    command.Parameters.AddWithValue("@RowVersion", 0);

                    command.ExecuteNonQuery();
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error configuring Configuration Database: " + ex.ToString());
                throw;
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public bool CRExpressHasIQbotCategory()
        {
            bool hasIQBotCategories = false;
            DatabaseConfiguration.DatabaseName = "CRDB";
            string connectionString = string.Empty;
            connectionString = GetConnectionString(DatabaseConfiguration);
            SqlConnection sqlConnection = null;
            string commandText = string.Empty;
            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    commandText = "Select COUNT(*) FROM [CRDB].[dbo].[centralizedconfiguration] where Category=@Category";
                    SqlCommand command = new SqlCommand(commandText, sqlConnection);
                    command.Parameters.AddWithValue("@Category", "IQBot");
                    Int32 count = (Int32)command.ExecuteScalar();
                    sqlConnection.Close();

                    if (count == 2)
                        hasIQBotCategories = true;
                    else if (count == 0)
                        hasIQBotCategories = false;
                    else
                        throw new ApplicationException("IQBot Platform URL and Output Path configuration is corrupted in Current Installation of Control Room. Please contact administrator for further troubleshooting.");
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("CRExpressHasIQbotCategory: Error: " + ex.ToString());
                messageBoxWrapper.ShowErrorDialog("IQBot Platform URL and Output Path configuration is corrupted in Current Installation of Control Room. Please contact administrator for further troubleshooting.");
                throw;
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
            return hasIQBotCategories;
        }

        public string GetDynamicPortForSQLExpress()
        {
            string portNumber = string.Empty;
            DatabaseConfiguration.DatabaseName = "master";
            string connectionString = string.Empty;
            connectionString = GetConnectionString(DatabaseConfiguration);
            SqlConnection sqlConnection = null;
            string commandText = string.Empty;
            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    commandText = Resource.EXPRESS_SQL_DYNAMIC_PORT_SELECT_QUERY;
                    SqlCommand command = new SqlCommand(commandText, sqlConnection);
                    portNumber = Convert.ToString(command.ExecuteScalar());
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("GetDynamicPortForSQLExpress: Error: " + ex.ToString());
                throw;
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
            return portNumber;
        }

        public bool InsertOutputPath()
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            bool status = false;
            DatabaseConfiguration.DatabaseName = "Configurations";
            string connectionString = string.Empty;
            connectionString = GetConnectionString(DatabaseConfiguration);

            SqlConnection sqlConnection = null;
            CognitiveConfiguration cogConfiguration = GetCognitiveConfiguration();

            string commandText = null;

            try
            {
                using (sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    if (isOutputPathAvailable(sqlConnection))
                    {
                        commandText = "UPDATE [dbo].[Configurations] Set [Value] = @Value,[CreatedBy] = @CreatedBy,[CreatedOn] = @CreatedOn WHERE [Key] = @Key";
                    }
                    else
                    {
                        commandText = "INSERT INTO [dbo].[Configurations]([Key],[Value],[CreatedBy],[CreatedOn]) Values(@Key,@Value,@CreatedBy,@CreatedOn)";
                    }

                    SqlCommand command = new SqlCommand(commandText, sqlConnection);

                    command.Parameters.AddWithValue("@Key", CONFIGURATION_KEY_OUTPUTFOLDER);
                    command.Parameters.AddWithValue("@Value", cogConfiguration.OutputPath);
                    command.Parameters.AddWithValue("@CreatedBy", SYSTEM);
                    command.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    command.ExecuteNonQuery();
                    status = true;
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("Error configuring Configuration Database: " + ex.ToString());
                messageBoxWrapper.ShowErrorDialog("Error occured while setting up IQ Bot Output Path. Please refer logs.");
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == System.Data.ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
            return status;
        }

        private bool isOutputPathAvailable(SqlConnection sqlConnection)
        {
            string commandText = null;

            commandText = "Select COUNT(*) FROM [dbo].[Configurations] where [Key]=@key";
            SqlCommand command = new SqlCommand(commandText, sqlConnection);
            command.Parameters.AddWithValue("@key", CONFIGURATION_KEY_OUTPUTFOLDER);
            Int32 count = (Int32)command.ExecuteScalar();
            if (count > 0)
                return true;
            else
                return false;
        }

        #endregion public

        #region private

        public string GetConnectionString(DatabaseConfigurations dbConfiguration)
        {
            string connectionString = string.Empty;
            string installtype = install.GetProperty(Resource.INSTALLTYPE);

            if (installtype.Equals("EXPRESS", StringComparison.InvariantCultureIgnoreCase))
                connectionString = string.Format(
                                Resource.EXPRESS_SQL_CONNECTION_STRING,
                                dbConfiguration.DatabaseServerName,
                                dbConfiguration.DatabaseName,
                                dbConfiguration.DatabaseUserName,
                                dbConfiguration.DatabasePassword);
            else if (dbConfiguration.IsWindowsAuthentication)
            {
                connectionString = string.Format(
                                Resource.DBConnectionString_WindowsAuthentication,
                                dbConfiguration.DatabaseServerName,
                                dbConfiguration.DatabasePort.ToString(),
                                dbConfiguration.DatabaseName);
            }
            else
            {
                connectionString = string.Format(
                                Resource.DBConnectionString_StandardAuthentication,
                                dbConfiguration.DatabaseServerName,
                                dbConfiguration.DatabasePort.ToString(),
                                dbConfiguration.DatabaseName,
                                dbConfiguration.DatabaseUserName,
                                dbConfiguration.DatabasePassword);
            }
            return connectionString;
        }

        private DatabaseConfigurations getDatabaseConfigurationsforCustomInstallation()
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            DatabaseConfigurations configurations = new DatabaseConfigurations();
            try
            {
                int dbPort = 0;
                configurations.DatabaseServerName = install.GetProperty(Resource.DATABASE_SERVER);
                configurations.DatabaseName = "master";
                configurations.DatabasePassword = _helper.FormatStringValue(install.GetProperty(Resource.DATABASE_PASSWORD));
                configurations.DatabaseUserName = _helper.FormatStringValue(install.GetProperty(Resource.DATABASE_USERNAME));

                bool result = int.TryParse(install.GetProperty(Resource.DATABASE_PORT), out dbPort);
                configurations.DatabasePort = dbPort;
            }
            catch (Exception ex)
            {
                Helper.LogToFile("getDatabaseConfigurations:" + ex.Message);
            }
            return configurations;
        }

        private DatabaseConfigurations getDatabaseConfigurationsForExpressInstallation()
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            DatabaseConfigurations configurations = new DatabaseConfigurations();
            try
            {
                int dbPort = 0;
                configurations.DatabaseServerName = string.Format("{0}\\{1}", Dns.GetHostName(), Resource.EXPRESS_SQL_INSTANCENAME);
                configurations.DatabaseName = "master"; //_helper.FormatStringValue(install.GetProperty(Resource.DATABASE_NAME));
                configurations.DatabasePassword = Resource.EXPRESS_SQL_SECUREDATA;
                configurations.DatabaseUserName = Resource.EXPRESS_SQL_USERNAME;

                bool result = int.TryParse((Resource.EXPRESS_SQL_PORT), out dbPort);
                configurations.DatabasePort = dbPort;
            }
            catch (Exception ex)
            {
                Helper.LogToFile("getDatabaseConfigurations:" + ex.Message);
            }
            return configurations;
        }

        private DatabaseConfigurations getDatabaseConfigurations()
        {
            DatabaseConfigurations configurations = new DatabaseConfigurations();
            try
            {
                int dbPort = 0;
                configurations.DatabaseServerName = install.GetProperty(Resource.DATABASE_SERVER);
                configurations.DatabaseName = "master";
                configurations.DatabasePassword = _helper.FormatStringValue(install.GetProperty(Resource.DATABASE_PASSWORD));
                configurations.DatabaseUserName = _helper.FormatStringValue(install.GetProperty(Resource.DATABASE_USERNAME));
                if (_helper.FormatStringValue(install.GetProperty(Resource.IS_DATABASE_WINDOWS_AUTH)).Equals("1"))
                    configurations.IsWindowsAuthentication = true;
                else
                    configurations.IsWindowsAuthentication = false;
                dbPort = int.Parse(install.GetProperty(Resource.DATABASE_PORT));
                configurations.DatabasePort = dbPort;
                Helper.LogToFile(string.Format("Database Configurations: {0}", configurations.ToString()));
            }
            catch (Exception ex)
            {
                Helper.LogToFile("getDatabaseConfigurations:" + ex.Message);
            }
            return configurations;
        }

        private bool validateDatabaseConfiguration(int msiHandle, Msi.Install install)
        {
            try
            {
                MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
                bool isValidDbCredentials = _databaseValidationHelper.IsValidDatabaseCredentials(DatabaseConfiguration);
                if (!isValidDbCredentials)
                {
                    messageBoxWrapper.ShowErrorDialog(Resource.InvalidDBCredentialsOrPort);
                    return false;
                }

                if (DatabaseConfiguration.IsWindowsAuthentication)
                {
                    Helper.LogToFile("Validating Domain User Credentials.");
                    string userName, domainName = string.Empty;
                    string[] array = DatabaseConfiguration.DatabaseUserName.Split(USERNAME_DELIMITER);
                    if (array.Length != 2)
                    {
                        messageBoxWrapper.ShowErrorDialog(Resource.VALIDATION_DOMAINUSER_INVALIDUSERNAME);
                        return false;
                    }
                    domainName = array[0];
                    if (string.IsNullOrWhiteSpace(domainName))
                    {
                        messageBoxWrapper.ShowErrorDialog(Resource.VALIDATION_DOMAINUSER_INVALIDUSERNAME);
                        return false;
                    }
                    userName = array[1];
                    if (!_databaseValidationHelper.IsValidActiveDirectoryUser(domainName, userName, DatabaseConfiguration.DatabasePassword))
                    {
                        messageBoxWrapper.ShowErrorDialog(Resource.VALIDATION_DOMAINUSER_INVALIDCREDENTIALS);
                        return false;
                    }
                }

                bool dbAccessPermission = _databaseValidationHelper.IsValidAdminUser(DatabaseConfiguration);
                if (!dbAccessPermission)
                {
                    messageBoxWrapper.ShowErrorDialog(Resource.InsufficientAccessPrivileges);
                    return false;
                }
                if (CreateDatabase())
                {
                    install.SetProperty(Resource.IS_SPECIFIED_DATABASE_DETECTED, isValidDbCredentials.ToString());
                    install.SetProperty(Resource.CONNECTION_STRING, _databaseValidationHelper.GetConnectionString(DatabaseConfiguration));
                    return true;
                }
            }
            catch (Exception ex)
            {
                Helper.LogToFile("databaseValidations:" + ex.Message);
                return false;
            }
            return false;
        }



        #endregion private
    }
}