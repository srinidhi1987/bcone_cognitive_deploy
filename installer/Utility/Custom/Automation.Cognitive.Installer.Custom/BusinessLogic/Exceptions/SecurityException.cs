﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions
{
    public class SecurityException : Exception
    {
        public SecurityException() : base()
        {
        }

        public SecurityException(string message) : base(message)
        {
        }

        public SecurityException(string message, Exception e) : base(message, e)
        {
        }
    }
}
