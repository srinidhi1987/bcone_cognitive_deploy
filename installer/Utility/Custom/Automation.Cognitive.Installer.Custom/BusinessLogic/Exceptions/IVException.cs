﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions
{
    public class IVException : Exception
    {
        public IVException() : base()
        {
        }

        public IVException(string message) : base(message)
        {
        }

        public IVException(string message, Exception e) : base(message, e)
        {
        }
    }
}
