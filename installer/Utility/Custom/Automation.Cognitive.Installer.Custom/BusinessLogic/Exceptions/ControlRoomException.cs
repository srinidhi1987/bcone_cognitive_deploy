﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions
{
    public class ControlRoomException : Exception
    {
        private string _message;
        public ControlRoomException(string message) : base(message)
        {
            _message = message;
        }

        public string Message
        {
            get
            {
                return _message;
            }
        }
    }
}
