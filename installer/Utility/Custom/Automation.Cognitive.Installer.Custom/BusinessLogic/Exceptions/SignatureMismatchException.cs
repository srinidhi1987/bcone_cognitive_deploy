﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic.Exceptions
{
    public class SignatureMismatchException : Exception
    {
        public SignatureMismatchException() : base()
        {
        }

        public SignatureMismatchException(string message) : base(message)
        {
        }

        public SignatureMismatchException(string message, Exception e) : base(message, e)
        {
        }
    }
}
