﻿using InstallShield.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Automation.Cognitive.Installer.Custom.Model;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using Automation.Cognitive.Installer.Custom.BusinessLogic.Security;
using Automation.Cognitive.Installer.Custom.Common;

namespace Automation.Cognitive.Installer.Custom.BusinessLogic
{
    public class ComponentConfigurations
    {
        protected Msi.Install install;
        private string installdir;
        private const string RABBITMQ_ADDNODE = "AddNodeToRabbitMQCluster.bat";
        private const string RABBITMQ_MODIFYCOOKIE = "ModifyRabbitMQNodeErlangCookie.bat";
        private const string RABBITMQ_REMOVENODE = "RemoveNodeFromRabbitMQCluster.bat";

        public ComponentConfigurations(int msiHandle)
        {
            install = Msi.CustomActionHandle(msiHandle);
        }

        public ComponentConfigurations()
        {
        }

        public void ConfigureComonents()
        {
            MessageBoxWrapper messageBoxWrapper = new MessageBoxWrapper(install);
            bool isSSLEnabled;
            try
            {
                PrepareSettingsConfigurationFile();
                installdir = install.GetProperty("INSTALLDIR").Trim();
                string sslOffloading = install.GetProperty(Resource.IS_SSL_OFFLOADING).Trim();
                string clientSecurityType = install.GetProperty(Resource.CLIENT_SECURITY_TYPE).Trim();

                string classifierPath = Path.Combine(installdir, "Workers", "Classifier", "CognitiveServiceConfiguration.json");
                string visionbotEnginePath = Path.Combine(installdir, "Workers", "VisionBotEngine", "CognitiveServiceConfiguration.json");
                string mlTranslationPath = Path.Combine(installdir, "ML", "translationsvc", "CognitiveServiceConfiguration.json");
                string mlWebPath = Path.Combine(installdir, "ML", "webservice", "CognitiveServiceConfiguration.json");
                string ocrEngineSettingFilePath = Path.Combine(installdir, "Configurations", "OCREngineSettings.json");
                string configurationFilePath = Path.Combine(installdir, "Configurations", "configuration.txt");

                isSSLEnabled = clientSecurityType.Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase);

                if (isSSLEnabled)
                {
                    SSLSecurityProvider securityProvider = new SSLSecurityProvider(install);
                    Helper.LogToFile("Setting up secure environment..");
                    securityProvider.ExctractPrivateKeyAndCopyCertificate();
                    // enable this when we are encrypting backend interservice communication with self signed certificate. 
                    // securityProvider.GenerateSelfSignedCRTandKeyFiles();
                    // securityProvider.GenerateSelfSignedJSKFile();                    

                    Helper.LogToFile("Secure environment setup successfully.");
                }

                replaceFileContent(visionbotEnginePath, "http://localhost", string.Format("http://{0}", Helper.GetFullyQualifiedDomainName()));
                replaceFileContent(configurationFilePath, "localhost", Helper.GetFullyQualifiedDomainName());
                configureServiceSettings(visionbotEnginePath, isSSLEnabled);

                //Copy same configured file to all locations
                File.Copy(visionbotEnginePath, classifierPath, true);
                File.Copy(visionbotEnginePath, mlTranslationPath, true);
                File.Copy(visionbotEnginePath, mlWebPath, true);

                configureOCREngineSettingFile(ocrEngineSettingFilePath);

                string gatewayUrl = install.GetProperty(Resource.GATEWAY_HOSTNAME);
                string gatewayPort = install.GetProperty(Resource.GATEWAY_PORT);
                configureFrontend(gatewayUrl, gatewayPort);


            }
            catch (Exception ex)
            {
                Helper.LogToFile("ConfigureComonents: Error: " + ex.ToString());
                messageBoxWrapper.ShowErrorDialog("Unable to apply product configurations during installation. Please check logs for more details.");
                throw;
            }

        }

        private void PrepareSettingsConfigurationFile()
        {
            Helper.LogToFile("PrepareSettingsConfigurationFile started");
            string secureConnectionToLB = string.Empty;
            string secureConnectionToPortal = string.Empty;
            string secureConnectionToCR = string.Empty;
            string installDir = install.GetProperty(Resource.INSTALLDIR);

            string dbServer = install.GetProperty(Resource.DATABASE_SERVER);
            string dbUser = install.GetProperty(Resource.DATABASE_USERNAME);
            string dbName = install.GetProperty(Resource.DATABASE_NAME);
            string dbPort = install.GetProperty(Resource.DATABASE_PORT);
            string crSecurityScheme = install.GetProperty(Resource.CONTROLROOM_SECURITY_TYPE);
            string crIP = install.GetProperty(Resource.CONTROLROOM_IP);
            string crPort = install.GetProperty(Resource.CONTROLROOM_PORT);
            string outputPath = install.GetProperty(Resource.OUTPUT_PATH);
            string gatewayAPIHostname = Helper.GetFullyQualifiedDomainName();
            string gatewayAPIPort = install.GetProperty(Resource.BACKEND_GATEWAY_PORT);
            string sslOffloading = install.GetProperty(Resource.IS_SSL_OFFLOADING);
            string loadBalancerHost = install.GetProperty(Resource.GATEWAY_HOSTNAME);
            string loadBalancerPort = install.GetProperty(Resource.GATEWAY_PORT);
            string clientSecurityType = install.GetProperty(Resource.CLIENT_SECURITY_TYPE);
            string clientHostName = install.GetProperty(Resource.CLIENT_HOSTNAME);
            string clientPort = install.GetProperty(Resource.CLIENT_PORT);
            string installtype = install.GetProperty(Resource.INSTALLTYPE);
            string isWinAuthEnabled = install.GetProperty(Resource.IS_DATABASE_WINDOWS_AUTH).Equals("1", StringComparison.InvariantCultureIgnoreCase) ? "true" : "false";
            string iqBotLogFolderPath = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), "Automation Anywhere IQBot Platform", "Logs");

            secureConnectionToLB = (sslOffloading.Equals("1", StringComparison.InvariantCultureIgnoreCase)) ? "true" : "false";

            if ((sslOffloading.Equals("1", StringComparison.InvariantCultureIgnoreCase)))
            {
                secureConnectionToPortal = "false";
            }
            else
            {
                secureConnectionToLB = secureConnectionToPortal = (clientSecurityType.Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase)) ? "true" : "false";
            }          

            secureConnectionToCR = (crSecurityScheme.Equals("https", StringComparison.InvariantCultureIgnoreCase)) ? "true" : "false";

            string settingsFilePath = Path.Combine(installDir, "Configurations", "Settings.txt");
            string formattedOutputPath = "\"" + outputPath + "\"";
            string formattedInstDir = "\"" + installDir + "\"";
            string formattediqBotLogFolderPath = "\"" + iqBotLogFolderPath + "\"";

            if (File.Exists(settingsFilePath))
            {
                string configurationString = string.Format($"SQLServerAddress={dbServer}{Environment.NewLine}UserName={dbUser}{Environment.NewLine}SQLPort={dbPort}{Environment.NewLine}SecureConnectionToCR={secureConnectionToCR}{Environment.NewLine}ControlRoomLink={crIP}{Environment.NewLine}ControlRoomPort={crPort}{Environment.NewLine}OutputPath={formattedOutputPath}{Environment.NewLine}GatewayHost={gatewayAPIHostname}{Environment.NewLine}GatewayPort={gatewayAPIPort}{Environment.NewLine}InstallDir={formattedInstDir}{Environment.NewLine}OCREngine=Tesseract4{Environment.NewLine}SecureConnectionToLB={secureConnectionToLB}{Environment.NewLine}LoadBalancerHost={loadBalancerHost}{Environment.NewLine}LoadBalancerPort={loadBalancerPort}{Environment.NewLine}secureConnectionToPortal={secureConnectionToPortal}{Environment.NewLine}ConfidenceThreshold=0{Environment.NewLine}InstallationType={installtype}{Environment.NewLine}SQLWindowsAuthentication={isWinAuthEnabled}{Environment.NewLine}CopyProductionFiles=true{Environment.NewLine}IQ_BOT_LOGS_PATH={formattediqBotLogFolderPath}{Environment.NewLine}VisionBotLockedTimeout=15{Environment.NewLine}DocumentLockedTimeout=900{Environment.NewLine}RabbitMQPort=5673{Environment.NewLine}IQBotBasePath=/IQBot/{Environment.NewLine}IQBotPortalPort={clientPort}");
                File.WriteAllText(settingsFilePath, configurationString);
            }
            else
                Helper.LogToFile("PrepareSettingsConfigurationFile: Settings file not found");

            Helper.LogToFile("PrepareSettingsConfigurationFile completed");
        }

        private void replaceFileContent(string filePath, string searchText, string replaceText)
        {
            Helper.LogToFile("configureServicesIP started");
            string text = File.ReadAllText(filePath);
            string newText = text.Replace(searchText, replaceText);
            File.WriteAllText(filePath, newText);
            Helper.LogToFile("configureServicesIP completed");
        }

        private void configureServiceSettings(string filePath, bool isSSLEnabled)
        {
            string json = File.ReadAllText(filePath);
            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            string connectionDBString, connectionMLDBString;
            string databaseServer = install.GetProperty(Resource.DATABASE_SERVER);
            string username = install.GetProperty(Resource.DATABASE_USERNAME);
            string port = install.GetProperty(Resource.DATABASE_PORT);

            connectionDBString = "Data Source =" + databaseServer + "," + port + ";User id = " + username + ";Database = ClassifierData;";
            connectionMLDBString = "DRIVER={SQL Server Native Client 11.0};SERVER=" + databaseServer + "," + port + ";database=MLData;UID=" + username + ";";

            jsonObj["ClassifierDBSettings"]["DbConnectionString"] = connectionDBString;
            jsonObj["MLDBSettings"]["DbConnectionString"] = connectionMLDBString;

            // enable this when we are encrypting backend interservice communication with self signed certificate. 
            //if (isSSLEnabled)
            //{
            //    string installDir = install.GetProperty(Resource.INSTALLDIR);
            //    string platformSSLCertFilePath = Path.Combine(installDir, Resource.PLATFORM_CERT_HOMEPATH, Resource.PLATFORM_CRT_CRT);
            //    string platformSSLkeyFilePath = Path.Combine(installDir, Resource.PLATFORM_CERT_HOMEPATH, Resource.PLATFORM_CRT_KEY);
            //    jsonObj["SSLSettings"]["RunWithSSL"] = true;
            //    jsonObj["SSLSettings"]["SSL_CERT_FilePath"] = platformSSLCertFilePath;
            //    jsonObj["SSLSettings"]["SSL_CERT_Key"] = platformSSLkeyFilePath;
            //}

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(filePath, output);
        }

        private void configureOCREngineSettingFile(string filePath)
        {
            string json = File.ReadAllText(filePath);
            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            string installDirPath = install.GetProperty("INSTALLDIR");
            string tesseract3 = Path.Combine(installDirPath, "Workers", "Classifier", "tessdata");
            string tesseract4 = Path.Combine(installDirPath, "Workers", "Classifier", "tessdata_4");

            jsonObj["OCREngineSettings"]["Tesseract3_Settings"]["TessdataPath"] = tesseract3;

            jsonObj["OCREngineSettings"]["Tesseract4_Settings"]["TessdataPath"] = tesseract4;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);

            File.WriteAllText(filePath, output);
        }

        public void configureFrontend(string gatewayHost, string port)
        {
            string path = Path.Combine(installdir, "Portal", "www", "js");
            string fileNamePattern = "main." + "*" + ".js";

            string[] fileNames = System.IO.Directory.GetFiles(path, fileNamePattern, System.IO.SearchOption.TopDirectoryOnly);

            string searchPattern = "localhost:3000";
            string replacePattern = string.Format(gatewayHost + ":" + port);

            replace(fileNames[0], searchPattern, replacePattern);
        }

        public void ConfigureRabbitMQClusterNodes(string primaryNodeName, bool addClusterToNode)
        {
            string currentNodeName = "rabbit@" + Helper.FQDNtoHostName(Dns.GetHostName());
            string supportDirPath = install.GetProperty(Resource.SUPPORTDIR);
            string addNodeScript = Path.Combine(supportDirPath, RABBITMQ_ADDNODE);
            string manageCookieScript = Path.Combine(supportDirPath, RABBITMQ_MODIFYCOOKIE);

            ProcessExecutor executor = new ProcessExecutor();
            executor.ModifyRabbitMQNodeErlangCookie(manageCookieScript, currentNodeName);
            if (addClusterToNode)
            {
                Helper.LogToFile("Adding node to rabbitmq cluster.");
                executor.AddNodeToRabbitMQCluster(addNodeScript, primaryNodeName);
            }
        }

        public void RemoveRabbitMQClusterNode()
        {
            string currentNodeName = "rabbit@" + Dns.GetHostName();
            string supportDirPath = install.GetProperty(Resource.SUPPORTDIR);
            string removeNodeScript = Path.Combine(supportDirPath, RABBITMQ_REMOVENODE);

            ProcessExecutor executor = new ProcessExecutor();
            executor.RemoveNodeFromRabbitMQCluster(removeNodeScript, currentNodeName);
        }

        private static void replace(string fileName, string searchPattern, string replacePattern)
        {
            string fileContent;
            Regex ex = new Regex(searchPattern);

            using (StreamReader reader = new StreamReader(fileName))
            {
                fileContent = reader.ReadToEnd();
                reader.Close();
            }

            fileContent = ex.Replace(fileContent, replacePattern);

            using (StreamWriter wr = new StreamWriter(fileName))
            {
                wr.Write(fileContent);
                wr.Close();
            }
        }

        public string GetCognitiveBaseURL()
        {
            string cognitiveBaseUrl = "<scheme>://<host>:<port>/";
            string host = install.GetProperty(Resource.GATEWAY_HOSTNAME);
            string port = install.GetProperty(Resource.GATEWAY_PORT);
            string sslOffloading = install.GetProperty(Resource.IS_SSL_OFFLOADING);
            string clientSecurityType = install.GetProperty(Resource.CLIENT_SECURITY_TYPE);

            if (sslOffloading.Equals("1", StringComparison.InvariantCultureIgnoreCase) || clientSecurityType.Equals("ISHTTPS", StringComparison.InvariantCultureIgnoreCase))
                cognitiveBaseUrl = cognitiveBaseUrl.Replace("<scheme>", "https");
            else
                cognitiveBaseUrl = cognitiveBaseUrl.Replace("<scheme>", "http");
            cognitiveBaseUrl = cognitiveBaseUrl.Replace("<host>", host);
            cognitiveBaseUrl = cognitiveBaseUrl.Replace("<port>", port);
            Helper.LogToFile("Cognitive Platform URL: " + cognitiveBaseUrl);
            return cognitiveBaseUrl;
        }

    }
}