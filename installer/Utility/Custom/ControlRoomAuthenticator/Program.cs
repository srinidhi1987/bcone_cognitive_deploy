﻿using Automation.CR.Web.Services.Client;
using Automation.CR.Web.Services.Client.Config;
using Newtonsoft.Json;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ControlRoomAuthenticator
{
    internal class Program
    {
        private static int Main(string[] args)
        {
            try
            {
                if (args.Length < 1)
                    return -1;
                if (args[0] == "UpdateIQBotProductInfo")
                {
                    UpdateIQBotProductInfo(args[1], args[2]);
                }
                else if (args[0] == "GetIQBotDNSInfo")
                {
                    GetIQBotDNSInfo();
                }

                return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return -1;
            }
        }

        public static void GetIQBotDNSInfo()
        {
            string applicationPath = Automation.Util.CommonMethods.GetAAApplicationPath();
            RestExecutorConfigurationProvider provider = new RestExecutorConfigurationProvider(applicationPath, null);
            RestExecutorConfiguration restConfiguration = provider.Get();
            CentralizedConfigurationClient ccClinet = new CentralizedConfigurationClient(restConfiguration);

            IDictionary<string, string> iqBotConfigurations = ccClinet.Get("IQBot");

            IDictionary<string, string> configurations = new Dictionary<string, string>();
            string hostName = "";
            string outputPath = "";

            if (iqBotConfigurations.ContainsKey("CognitivePlatformHost"))
            {
                hostName = iqBotConfigurations["CognitivePlatformHost"];
            }
            else
            {
                hostName = "";
            }

            if (iqBotConfigurations.ContainsKey("CognitivePlatformOutputPath"))
            {
                outputPath = iqBotConfigurations["CognitivePlatformOutputPath"];
            }
            else
            {
                outputPath = "";
            }

            configurations.Add("CognitivePlatformHost", hostName);
            configurations.Add("CognitivePlatformOutputPath", outputPath);

            Console.WriteLine(JsonConvert.SerializeObject(configurations));
        }

        public static void UpdateIQBotProductInfo(string dnsName, string outputPath)
        {
            string applicationPath = Automation.Util.CommonMethods.GetAAApplicationPath();
            RestExecutorConfigurationProvider provider = new RestExecutorConfigurationProvider(applicationPath, null);
            RestExecutorConfiguration restConfiguration = provider.Get();
            CentralizedConfigurationClient ccClinet = new CentralizedConfigurationClient(restConfiguration);

            IDictionary<string, string> iqBotConfigurations = ccClinet.Get("IQBot");

            if (iqBotConfigurations.ContainsKey("CognitivePlatformHost"))
            {
                ccClinet.Update("IQBot", "CognitivePlatformHost", dnsName);
            }
            else
            {
                ccClinet.Add("IQBot", "CognitivePlatformHost", dnsName);
            }

            if (iqBotConfigurations.ContainsKey("CognitivePlatformOutputPath"))
            {
                ccClinet.Update("IQBot", "CognitivePlatformOutputPath", outputPath);
            }
            else
            {
                ccClinet.Add("IQBot", "CognitivePlatformOutputPath", outputPath);
            }

            string controlRoomLink = restConfiguration.ApiUrl;

            Console.WriteLine(controlRoomLink);
        }
    }
}