# Automation Anywhere IQ Bot

## Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the team before making a change.

All the design changes must be approved by Product team, by either of Jasdip Singh, Bruno Selva or Derek Chan. 
All the architectural changes must be approved by Michael Sundell or Harshil Lodhiya. 

## Pull Request Process

- Ensure any build dependencies are maintained within "<WorkSpace>\installer\Resources\setup-support-files" Resources folder.
- Ensure any install dependencies are maintained within "<WorkSpace>\installer\Resources" Resources folder.
- To deliver any new resource or artifact through build or bundle it with windows installer, update configuration_*.bat file at "<workspace>\scripts" folder.
- Increase the version only if its Major version change. 
- Generate sample build and get verified with reviewer prior to generate Pull Request. 

## Branch Naming Convention: 

Using this format and name spacing our branch names will help organize them. With a single repo, we'll have to stay on top of maintain of our branches so it doesn't get out of control. This will also give us the ability to search branches easier as the repo grows.
Convention: {story_type}/{implementers_name}/{current_date}/{story_number}
example: feat/iamcutler/02282018/COG-1000

## Commit Message Convention: 

This format is a standard open source message style. We would benefit from this convention at a high level. Looking through the history, we can see the story, scope of work, and type of work without having to dig into the implementation itself.

Convention: {story_number}: {story_type}({scope}): {commit_message}

example: COG-1000: feat(projects): Introduced new API to fetch products by learning instance

specification: http://karma-runner.github.io/2.0/dev/git-commit-msg.html

