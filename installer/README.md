# Automation Anywhere IQ Bot

## Tech Stack
- InstallShield 2018 (Windown Installer)
- Microsoft .Net 4.6 (Custom Action Bridge)
- InstallScript (Installation Scripts)

## Development Server

- Server Setup2015-2 contains Installshield 2018 development IDE. 
- Jenkins Server 54.176.223.221 has Installshield Build Agent installed which is being used to compile build and generate installer from commandline. 

## Using Installshiled IDE

- Login to Setup2015-2 server. 
- Clone your feature branch in a folder. Make sure to copy all required setup support files to Installshield folder. 
- Open Installshield IDE. Click on start menu and type "Installshield" it will show Installshield 2018 in results. 
- Click on "Open an existing project".
- To build the installer, select "Build" from toolbar and click "Build SINGLE_EXE_IMAGE". 

## Command Line Interface (CLI)

- Note: The commandline build interface is ment to be used by Jenkins Job only. Please do not use by manually on the jenkins server. 
- "C:\Program Files (x86)\InstallShield\2018 SAB\System\IsCmdBld.exe" -p "<WorkSpace>\installer\Setup\InstallShield2015\CognitivePlatform.ism" -b "<WorkSpace>\installer\Setup\InstallShield2015" -Z ProductVersion="X.X.0.0"

## Output

- The output file will be generated at "<WorkSpace>\installer\Setup\PROJECT_ASSISTANT\SINGLE_EXE_IMAGE\DiskImages\DISK1" for IQ Bot Platform.
- The output file will be generated at "<WorkSpace>\installer\Setup\PROJECT_ASSISTANT\SINGLE_EXE_IMAGE\DiskImages\DISK1" for IQ Bot Platform.

## Contribution

- Please read CONTRIBUTING.md for details on the process for submitting pull requests.