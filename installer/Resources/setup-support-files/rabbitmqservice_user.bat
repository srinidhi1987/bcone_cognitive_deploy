@rem set username=%1
@rem set password=%2
attrib -r "%USERPROFILE%\.erlang.cookie" /s /d
robocopy "C:\Windows" "%USERPROFILE%" ".erlang.cookie" /IS
timeout /t 15

call xcopy "C:\Windows\.erlang.cookie" "%USERPROFILE%\.erlang.cookie" /Y/V/F/I
timeout /t 5

@rem set RUN_RABBITMQ=C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin
if NOT DEFINED RABBITMQ_HOME goto error
set RUN_RABBITMQ=%RABBITMQ_HOME%\sbin

net stop "RabbitMQ"
set rabbit_mq_config=%APPDATA%\RabbitMQ\rabbitmq.config
del "%rabbit_mq_config%"
( echo [{rabbit, [{tcp_listeners, [5673]}]}]. ) >> "%rabbit_mq_config%"
net start "RabbitMQ"
timeout /t 15

set username="messagequeue"
set password="passmessage"
cd "%RUN_RABBITMQ%"
start /B "rabbitMQusercreation" rabbitmqctl add_user %username% %password%
timeout /t 15
start /B "HostCreation" rabbitmqctl add_vhost test
timeout /t 15
start /B "SetPermissions" rabbitmqctl set_permissions -p test %username%  ".*" ".*" ".*"
timeout /t 15
pause
exit