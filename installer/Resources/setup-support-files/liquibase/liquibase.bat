@setlocal enableextensions
@cd /d "%~dp0"

@echo off

set SQLServerAddress=%1
set UserName=%2
set Password=%3
set SQLPort=%4
set InstallDir=%5
set InstallDIR=%InstallDIR:"=%


"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="configuration_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=Configurations --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="classifier_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=ClassifierData --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="project_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="filemanager_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="visionbot_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="validator_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=FileManager --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="alias_changelog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=AliasData --username=%UserName% --password=%Password% update
"%InstallDIR%JRE\1.8.0_161\bin\java.exe" -jar liquibase.jar --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --classpath="sqljdbc4-2.0.jar" --changeLogFile="ML_ChangeLog.xml" --url=jdbc:sqlserver://%SQLServerAddress%\\MSSQLSERVER:%SQLPort%;databaseName=MLData --username=%UserName% --password=%Password% update