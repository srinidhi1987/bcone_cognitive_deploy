::Script to remove node from the cluster
::Input to the script : rabbit mq node name ( rabbit@hostnameoftheserver )
set rabbit_mq_sbin="C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin"
cd /d %rabbit_mq_sbin%
set rabbit_mq_node_name=%1
if "%rabbit_mq_node_name%"=="" (
	echo "please enter rabbit mq node name"
	goto :ErrorOccurred
)
echo "stop broker"
call rabbitmqctl stop_app
echo "reset node"
call rabbitmqctl reset
echo "start app"
call rabbitmqctl start_app
echo "recreate the user messagequeue and set the tags" 
call rabbitmqctl add_user messagequeue passmessage
call rabbitmqctl add_vhost test
call rabbitmqctl set_permissions -p test messagequeue ".*" ".*" ".*"
call rabbitmqctl set_user_tags messagequeue administrator
exit /b 0