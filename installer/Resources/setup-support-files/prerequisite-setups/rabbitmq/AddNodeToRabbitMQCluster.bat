::Script to add a node to cluster
::Input to the script : rabbit mq node name ( rabbit@hostnameoftheserver )
set rabbit_mq_sbin="C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin"
cd /d %rabbit_mq_sbin%
set rabbit_mq_node_name=%1
if "%rabbit_mq_node_name%"=="" (
	echo "please enter rabbit mq node name"
	goto :ErrorOccurred
)
echo "stop broker"
call rabbitmqctl stop_app
echo "join node to cluster"
call rabbitmqctl join_cluster %rabbit_mq_node_name%
echo "start app"
call rabbitmqctl start_app
echo "mirror queues"
rabbitmqctl set_policy ha-all "." "{""ha-mode"":""all""}"
exit /b 0