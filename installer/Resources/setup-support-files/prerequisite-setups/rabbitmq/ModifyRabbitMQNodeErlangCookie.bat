::Script to add the cookie to the node
::Input to the script : 
  ::rabbit mq node name ( rabbit@hostnameoftheserver )
  ::cookie to be set in the node to be shared across cluster
set rabbit_mq_sbin="C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin"
echo %rabbit_mq_sbin%
cd /d %rabbit_mq_sbin%
set cookie_value=%1
set rabbit_mq_node_name=%2
if "%cookie_value%"=="" (
	echo "please enter cookie_value"
	goto :ErrorOccurred
)
if "%rabbit_mq_node_name%"=="" (
	echo "please enter rabbit mq node name"
	goto :ErrorOccurred
)
echo "Setting the erlang cookie "
call rabbitmqctl -n %rabbit_mq_node_name% eval "erlang:set_cookie(node(), '%cookie_value%')." 
echo "Setting the environment variable RABBITMQ_CTL_ERL_ARGS"
set "RABBITMQ_CTL_ERL_ARGS=-setcookie %cookie_value%"
setx RABBITMQ_CTL_ERL_ARGS "-setcookie %cookie_value%"
exit /b 0