@echo off
@setlocal enableextensions
@cd /d "%~dp0"
set SQLServerAddress=%1
set UserName=%2
set SQLPort=%3
set ControlRoomLink=%4
set ControlRoomPort=%5
set OutputPath=%6
set GatewayHost=%7
set GatewayPort=%8
set InstallDIR=%9
shift
shift
shift
set SecureConnectionToLB=%7
set LoadBalancerHost=%8
set LoadBalancerPort=%9

set config=%InstallDIR%Configurations\Settings.txt"
break>%config%
@echo SQLServerAddress=%SQLServerAddress%>> %config%
@echo UserName=%UserName%>> %config%
@echo SQLPort=%SQLPort%>> %config%
@echo ControlRoomLink=%ControlRoomLink%>> %config%
@echo ControlRoomPort=%ControlRoomPort%>> %config%
@echo OutputPath=%OutputPath%>> %config%
@echo GatewayHost=%GatewayHost%>> %config%
@echo GatewayPort=%GatewayPort%>> %config%
@echo InstallDIR=%InstallDIR%">> %config%
@echo OCREngine=Tesseract4>> %config%
@echo SecureConnectionToLB=%SecureConnectionToLB%>> %config%
@echo LoadBalancerHost=%LoadBalancerHost%>> %config%
@echo LoadBalancerPort=%LoadBalancerPort%>> %config%

exit