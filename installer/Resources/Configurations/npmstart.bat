@echo off
@setlocal enableextensions
@cd /d "%~dp0"

for /f "delims== tokens=1,2" %%G in (Settings.txt) do set %%G=%%H
set InstallDIR=%InstallDIR:"=%
@rem C:
set PortalPath=%InstallDIR%Portal
set RUN_NPM=C:\Program Files\nodejs\npm
cd %PortalPath%
set WinserPath=%PortalPath%\winser.cmd
call "%WinserPath%" -i -a -n "Automation Anywhere Cognitive Console" --startuptype auto --startcmd "node server\index.js" --env "NODE_ENV=production" --env "RUN_WITH_SSL=%secureConnectionToPortal%" --env "API_GATEWAY_PORT=%GatewayPort%" --env "API_GATEWAY_URL=%GatewayHost%" --env "CLIENT_COMPONENT_PUB_KEY=./keys/client-components.pub" --env "RUN_LB_WITH_SSL=%SecureConnectionToLB%" --env "API_LOADBALANCER_URL=%LoadBalancerHost%" --env "API_LOADBALANCER_PORT=%LoadBalancerPort%" --env "SSL_CERT_KEY=./keys/key.key" --env "SSL_CERT_FILE=./keys/cert.crt" --env "SSL_CA_CERT_FILE=./keys/ca.crt"  --env="PORT=%IQBotPortalPort%" --env "IQ_BOT_LOGS_PATH=%IQ_BOT_LOGS_PATH%" --env "USE_WEB_DESIGNER=true" --env "USE_WEB_VALIDATOR=true" --env "IQBOT_BASE_PATH=%IQBotBasePath%"

endlocal
:END BREAK
exit

