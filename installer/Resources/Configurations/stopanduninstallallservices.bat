@setlocal enableextensions
@cd /d "%~dp0"

@rem all microservices to be installed and started
start /min "microservices" microservices_stop.bat

@rem all workers and ML to be installed and started
start /min "WorkerService" workers_stop.bat

@rem all Client services to be installed and started
start /min "npm" npmstop.bat

exit