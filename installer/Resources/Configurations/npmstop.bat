@echo off
@setlocal enableextensions

nssm.exe stop "Automation Anywhere Cognitive Console"
nssm.exe remove "Automation Anywhere Cognitive Console" confirm

Exit