::Script to add the cookie to the node
::Input to the script : 
  ::rabbit mq node name ( rabbit@hostnameoftheserver )
  ::cookie to be set in the node to be shared across cluster
@echo off
set rabbit_mq_sbin="C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.6\sbin"
echo %rabbit_mq_sbin%
set cookie_value=KASFJKHJKSDFBFBMNSDF
set rabbit_mq_local_node_name=rabbit@%COMPUTERNAME%
echo rabbitmq local node name: %rabbit_mq_local_node_name%
attrib -r "C:\Windows\.erlang.cookie" /s /d
echo %cookie_value% > "C:\Windows\.erlang.cookie"
attrib -r "%USERPROFILE%\.erlang.cookie" /s /d
robocopy "C:\Windows" "%USERPROFILE%" ".erlang.cookie" /IS
timeout /t 15
call xcopy "C:\Windows\.erlang.cookie" "%USERPROFILE%\.erlang.cookie" /Y/V/F/I
timeout /t 5
set node_name=%1
IF "%node_name%"=="" (
	set /P input_node_name="Enter primary node name:"
	IF "%input_node_name%"=="" (
		echo "please enter primary rabbitmq node name"
		goto :ErrorOccurred
	) ELSE (
	set rabbit_mq_node_name=rabbit@%input_node_name% 
	)
) ELSE ( 
set rabbit_mq_node_name=rabbit@%node_name% 
)
echo "stop rabbitmq"
call net stop RabbitMQ /yes
echo "start rabbitmq"
call net start RabbitMQ
echo "start services"
start /B startservices.bat
cd /d %rabbit_mq_sbin%
echo "stop broker"
call rabbitmqctl stop_app
echo "join node to cluster"
call rabbitmqctl join_cluster %rabbit_mq_node_name%
echo "start app"
call rabbitmqctl start_app
echo "mirror queues"
rabbitmqctl -p test set_policy ha-all "." "{""ha-mode"":""all""}"
echo "cluster status"
rabbitmqctl cluster_status
exit /b 0
:ErrorOccurred
echo "Errors encountered during execution. Exited with status: %errorlevel%"