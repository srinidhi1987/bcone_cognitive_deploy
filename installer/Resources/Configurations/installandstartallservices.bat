@echo off
@setlocal enableextensions
@cd /d "%~dp0"

set username=%1
set password=%2

@rem all microservices to be installed and started
start /min /wait "microservices" microservices_start.bat %username% %password%

@rem all workers and ML to be installed and started
start /min /wait "WorkerService" workers_start.bat %username% %password%

@rem all Client services to be installed and started
start /min /wait "npm" npmstart.bat
exit