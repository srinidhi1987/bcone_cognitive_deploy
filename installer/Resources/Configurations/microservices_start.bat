@echo off
@setlocal enableextensions
@cd /d "%~dp0"

set domainusername=%1
set securedata=%2

for /f "delims== tokens=1,2" %%G in (Settings.txt) do set %%G=%%H
set InstallDIR=%InstallDIR:"=%
IF %SecureConnectionToCR% == true (
set "valuea=1"
)
IF %SecureConnectionToCR% == false ( 
set "valuea=0"
)
for /f "delims== tokens=1,2" %%G in (configuration.txt) do set %%G=%%H
@rem install Alias service
nssm.exe install "Automation Anywhere Cognitive Alias" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_alias.xml" "-Djava.library.path="""%InstallDIR%JDBC""" "-Dfile.encoding=UTF-8 -jar %AliasJar% %SQLServerAddress% %SQLPort% %AliasDatabase% %UserName% %AliasServicePort%"
nssm.exe set "Automation Anywhere Cognitive Alias" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Alias" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Alias" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Alias" Start SERVICE_AUTO_START

@rem install project service
nssm.exe install "Automation Anywhere Cognitive Projects" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_project.xml" "-Djava.library.path="""%InstallDIR%JDBC""" "-Dfile.encoding=UTF-8 -jar %ProjectJar% %SQLServerAddress% %SQLPort% %ProjectDatabase% %UserName% %ProjectServicePort% %ClassifierDatabase% %MLDatabase% %FileManagerServicePort% %VisionBotServicePort%" ""%OutputPath%
nssm.exe set "Automation Anywhere Cognitive Projects" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Projects" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Projects" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Projects" DependOnService "RabbitMQ"
nssm.exe set "Automation Anywhere Cognitive Projects" Start SERVICE_AUTO_START

@rem install Report service
nssm.exe install "Automation Anywhere Cognitive Report" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_reports.xml" "-Djava.library.path="""%InstallDIR%JDBC""" "-Dfile.encoding=UTF-8 -jar %ReportServiceJar% %SQLServerAddress% %SQLPort% %ReportServiceDatabase% %UserName% %ReportServicePort% %ProjectServicePort% %FileManagerServicePort% %ValidatorServicePort% %AliasServicePort%"
nssm.exe set "Automation Anywhere Cognitive Report" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Report" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Report" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Report" Start SERVICE_AUTO_START

@rem install Validator service
nssm.exe install "Automation Anywhere Cognitive Validator" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_validator.xml" "-Djava.library.path="""%InstallDIR%JDBC""" "-Dfile.encoding=UTF-8 -jar %ValidatorJar% %SQLServerAddress% %SQLPort% %ValidatorDatabase% %UserName% %ValidatorServicePort% %FileManagerServicePort% %ProjectServicePort%" ""%OutputPath%
nssm.exe set "Automation Anywhere Cognitive Validator" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Validator" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Validator" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Validator" DependOnService "RabbitMQ"
nssm.exe set "Automation Anywhere Cognitive Validator" Start SERVICE_AUTO_START

@rem install Visionbot service
nssm.exe install "Automation Anywhere Cognitive Visionbot Manager" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_visionbot.xml" "-Djava.library.path="""%InstallDIR%JDBC""" "-Dfile.encoding=UTF-8 -jar %VisionBotJar% %SQLServerAddress% %SQLPort% %VisionBotDatabase% %UserName% %VisionBotServicePort% %FileManagerServicePort% %ProjectServicePort% %ValidatorServicePort%"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" DependOnService "RabbitMQ"
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" Start SERVICE_AUTO_START

@rem install Filemanager service
nssm.exe install "Automation Anywhere Cognitive File Manager" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_filemanager.xml" "-Djava.library.path="""%InstallDIR%JDBC""" "-Dfile.encoding=UTF-8 -Xms256m -Xmx1024m -jar %FileManagerJar% %SQLServerAddress% %SQLPort% %FileManagerDatabase% %UserName% %ProjectHost% %ProjectServicePort% %VisionBotHost% %VisionBotServicePort% %FileManagerServicePort%" ""%OutputPath%
nssm.exe set "Automation Anywhere Cognitive File Manager" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive File Manager" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive File Manager" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive File Manager" DependOnService "RabbitMQ"
nssm.exe set "Automation Anywhere Cognitive File Manager" Start SERVICE_AUTO_START

@rem install Gateway-2 service
nssm.exe install "Automation Anywhere Cognitive Gateway-2" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_gateway-2.xml" "-Dfile.encoding=UTF-8 -Dserver.port=%GatewayPort% -Diqbot.databases.configurations.name=%ConfigurationsDatabase% -Dserver.tomcat.max-threads=%GatewayMaxThreads% -jar %GatewayJar% --spring.http.multipart.max-file-size=-1 --spring.http.multipart.max-request-size=-1"
nssm.exe set "Automation Anywhere Cognitive Gateway-2" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Gateway-2" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Gateway-2" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Gateway-2" Start SERVICE_AUTO_START

@rem install Application service
nssm.exe install "Automation Anywhere Cognitive Application" "%InstallDIR%JRE\1.8.0_161\bin\java.exe" "-Dlog4j.configurationFile=log4j2_application.xml" "-Djava.library.path=""%InstallDIR%JDBC"" -Dfile.encoding=UTF-8 -Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager -Dserver.port=%ApplicationServicePort% -Diqbot.databases.configurations.name=%ConfigurationsDatabase% -Diqbot.databases.configurations.userName=%UserName% -Diqbot.databases.configurations.host=%SQLServerAddress% -Diqbot.databases.configurations.port=%SQLPort% -jar %ApplicationJar%"
nssm.exe set "Automation Anywhere Cognitive Application" AppDirectory "%InstallDIR%Services"
nssm.exe set "Automation Anywhere Cognitive Application" Description "Provides Automation Anywhere Cognitive"
nssm.exe set "Automation Anywhere Cognitive Application" ImagePath """%InstallDIR%Configurations\nssm"""
nssm.exe set "Automation Anywhere Cognitive Application" Start SERVICE_AUTO_START

IF %SQLWindowsAuthentication% == true (
nssm.exe set "Automation Anywhere Cognitive Alias" ObjectName %domainusername% %securedata%
nssm.exe set "Automation Anywhere Cognitive Report" ObjectName %domainusername% %securedata%
nssm.exe set "Automation Anywhere Cognitive Validator" ObjectName %domainusername% %securedata%
nssm.exe set "Automation Anywhere Cognitive Visionbot Manager" ObjectName %domainusername% %securedata%
nssm.exe set "Automation Anywhere Cognitive File Manager" ObjectName %domainusername% %securedata%
nssm.exe set "Automation Anywhere Cognitive Projects" ObjectName %domainusername% %securedata%
nssm.exe set "Automation Anywhere Cognitive Application" ObjectName %domainusername% %securedata%
)

"%InstallDIR%ml\webservice\webservice.exe" --startup=auto install
"%InstallDIR%ml\translationsvc\translationsvc.exe" --startup=auto install

timeout 3
nssm.exe start "Automation Anywhere Cognitive Alias"
nssm.exe start "Automation Anywhere Cognitive Projects"
nssm.exe start "Automation Anywhere Cognitive Report"
nssm.exe start "Automation Anywhere Cognitive Validator"
nssm.exe start "Automation Anywhere Cognitive Visionbot Manager"
nssm.exe start "Automation Anywhere Cognitive File Manager"
nssm.exe start "Automation Anywhere Cognitive Gateway-2"
nssm.exe start "Automation Anywhere Cognitive Application"
net start "Automation Anywhere Cognitive MLWeb Service"
net start "Automation Anywhere Cognitive MLScheduler Service"
exit