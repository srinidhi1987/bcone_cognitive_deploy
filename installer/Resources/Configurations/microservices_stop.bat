@setlocal enableextensions
@cd /d "%~dp0"

nssm.exe stop "Automation Anywhere Cognitive Alias"
nssm.exe stop "Automation Anywhere Cognitive Projects"
nssm.exe stop "Automation Anywhere Cognitive File Manager"
nssm.exe stop "Automation Anywhere Cognitive Visionbot Manager"
nssm.exe stop "Automation Anywhere Cognitive Validator"
nssm.exe stop "Automation Anywhere Cognitive Report"
nssm.exe stop "Automation Anywhere Cognitive Gateway-2"
nssm.exe stop "Automation Anywhere Cognitive Application"
net stop "Automation Anywhere Cognitive MLWeb Service"
net stop "Automation Anywhere Cognitive MLScheduler Service"

nssm.exe remove "Automation Anywhere Cognitive Alias" confirm
nssm.exe remove "Automation Anywhere Cognitive Projects" confirm
nssm.exe remove "Automation Anywhere Cognitive File Manager" confirm
nssm.exe remove "Automation Anywhere Cognitive Visionbot Manager" confirm
nssm.exe remove "Automation Anywhere Cognitive Validator" confirm
nssm.exe remove "Automation Anywhere Cognitive Report" confirm
nssm.exe remove "Automation Anywhere Cognitive Gateway-2" confirm
nssm.exe remove "Automation Anywhere Cognitive Application" confirm
SC DELETE "Automation Anywhere Cognitive MLWeb Service"
SC DELETE "Automation Anywhere Cognitive MLScheduler Service"

exit