@setlocal enableextensions
@echo off
@cd /d "%~dp0"

set username=%1
set password=%2

for /f "delims== tokens=1,2" %%G in (Settings.txt) do set %%G=%%H
set InstallDIR=%InstallDIR:"=%

sc create "Automation Anywhere Cognitive VisionbotEngine Service" binPath= "%InstallDIR%Workers\VisionBotEngine\Automation.Cognitive.Worker.Visionbot.exe" start= auto
sc create "Automation Anywhere Cognitive Classifier Service" binPath= "%InstallDIR%Workers\Classifier\Automation.Cognitive.DocumentClassifier.exe" start= auto

IF %SQLWindowsAuthentication% == true (
sc config "Automation Anywhere Cognitive VisionbotEngine Service" obj= %username% password= %password%
sc config "Automation Anywhere Cognitive Classifier Service" obj= %username% password= %password%
)

sc config "Automation Anywhere Cognitive Classifier Service" depend=RabbitMQ
sc config "Automation Anywhere Cognitive VisionbotEngine Service" depend=RabbitMQ

timeout 3
net start "Automation Anywhere Cognitive Classifier Service"
net start "Automation Anywhere Cognitive VisionbotEngine Service"
exit