@echo off
@setlocal enableextensions
@cd /d "%~dp0"
 
nssm.exe stop "Automation Anywhere Cognitive Alias"
nssm.exe stop "Automation Anywhere Cognitive Projects"
nssm.exe stop "Automation Anywhere Cognitive File Manager"
nssm.exe stop "Automation Anywhere Cognitive Visionbot Manager"
nssm.exe stop "Automation Anywhere Cognitive Validator"
nssm.exe stop "Automation Anywhere Cognitive Report"
nssm.exe stop "Automation Anywhere Cognitive Gateway-2"
nssm.exe stop "Automation Anywhere Cognitive Application"
nssm.exe stop "Automation Anywhere Cognitive Console"
net stop "Automation Anywhere Cognitive VisionbotEngine Service"
net stop "Automation Anywhere Cognitive Classifier Service"
net stop "Automation Anywhere Cognitive MLWeb Service"
net stop "Automation Anywhere Cognitive MLScheduler Service"

nssm.exe start "Automation Anywhere Cognitive File Manager"
nssm.exe start "Automation Anywhere Cognitive Alias"
nssm.exe start "Automation Anywhere Cognitive Projects"
nssm.exe start "Automation Anywhere Cognitive Visionbot Manager"
nssm.exe start "Automation Anywhere Cognitive Validator"
nssm.exe start "Automation Anywhere Cognitive Report"
nssm.exe start "Automation Anywhere Cognitive Gateway-2"
nssm.exe start "Automation Anywhere Cognitive Application"
nssm.exe start "Automation Anywhere Cognitive Console"
net start "Automation Anywhere Cognitive VisionbotEngine Service"
net start "Automation Anywhere Cognitive MLWeb Service"
net start "Automation Anywhere Cognitive MLScheduler Service"
net start "Automation Anywhere Cognitive Classifier Service"

exit







