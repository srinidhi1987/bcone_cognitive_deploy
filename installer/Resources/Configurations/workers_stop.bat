@setlocal enableextensions
@cd /d "%~dp0"

net stop "Automation Anywhere Cognitive VisionbotEngine Service"
net stop "Automation Anywhere Cognitive Classifier Service"

SC DELETE "Automation Anywhere Cognitive VisionbotEngine Service"
SC DELETE "Automation Anywhere Cognitive Classifier Service"

Exit