////////////////////////////////////////////////////////////////////////////////
//                                                                            
//  This template script provides the code necessary to build an entry-point 
//  function to be called in an InstallScript custom action. 
//                                                                            
//                                                                            
//    File Name:  Setup.rul                                                   
//                                                                            
//  Description:  InstallShield script                                        
//
////////////////////////////////////////////////////////////////////////////////

// Include Ifx.h for built-in InstallScript function prototypes, for Windows 
// Installer API function prototypes and constants, and to declare code for 
// the OnBegin and OnEnd events.
#include "ifx.h"

// The keyword export identifies MyFunction() as an entry-point function.
// The argument it accepts must be a handle to the Installer database.
prototype STRING GetRegistryValue(STRING,STRING);
export prototype CheckPrerequisites(HWND);
export prototype InstallPrerequisites(HWND);
export prototype WriteSettingsToStartFile(HWND);
export prototype StartNpm(HWND);

prototype Is_Framework46();
prototype JRE_Install();
prototype IS_JRE_Install(STRING);
prototype Is_NodeJs_Install();
prototype SetJavaHomePath(BOOL);
prototype SetJavaHomeEnvironment(STRING);
prototype Is_RabbitMQInstalled();
prototype SetNodeJsEnvironment();
prototype SetRabbitMQEnvironment();

//#define HWND_BROADCAST			0xFFFF
#define WM_SETTINGCHANGE		0x001A
#define SMTO_ABORTIFHUNG		0x0002
#define SMTO_TIMEOUT			50

prototype   BOOL USER32.SendMessageTimeout(HWND, SHORT, SHORT, POINTER, SHORT, SHORT, POINTER);

// To Do:  Declare global variables, define constants, and prototype user-
//         defined and DLL functions here.


// To Do:  Create a custom action for this entry-point function:
// 1.  Right-click on "Custom Actions" in the Sequences/Actions view.
// 2.  Select "Custom Action Wizard" from the context menu.
// 3.  Proceed through the wizard and give the custom action a unique name.
// 4.  Select "Run InstallScript code" for the custom action type, and in
//     the next panel select "MyFunction" (or the new name of the entry-
//     point function) for the source.
// 5.  Click Next, accepting the default selections until the wizard
//     creates the custom action.
//
// Once you have made a custom action, you must execute it in your setup by
// inserting it into a sequence or making it the result of a dialog's
// control event.

///////////////////////////////////////////////////////////////////////////////
//                                                                           
// Function:  MyFunction
//                                                                           
//  Purpose:  This function will be called by the script engine when
//            Windows(TM) Installer executes your custom action (see the "To
//            Do," above).
//                                                                           
///////////////////////////////////////////////////////////////////////////////


function STRING GetRegistryValue(keypath,key)
STRING retValue;
NUMBER nvType,nSize;
begin
		RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
		RegDBGetKeyValueEx(keypath,key,nvType,retValue,nSize);
		
		return retValue;
end;





function CheckPrerequisites(hMSI)
BOOL framework_46, isJREInstall,isNodejs,isRabbitMQInstalled;
STRING Temp;
NUMBER size;
begin
	
	framework_46 = Is_Framework46();
	if(framework_46) then
		MsiSetProperty(ISMSI_HANDLE, @IS_FRAMEWORK_INSTALLED, "1");
	endif;
	
	
	isJREInstall = JRE_Install();
	if(isJREInstall) then 
		SetJavaHomePath(FALSE);
	 MsiSetProperty(ISMSI_HANDLE, @IS_JRE_INSTALLED, "1");	
	endif;
	
	isNodejs = Is_NodeJs_Install();
	if(isNodejs) then
	   SetNodeJsEnvironment();
	   MsiSetProperty(ISMSI_HANDLE, @IS_Nodejs_INSTALLED, "1");
	endif;
	
	 isRabbitMQInstalled =  Is_RabbitMQInstalled();
	 if(isRabbitMQInstalled) then 
		SetRabbitMQEnvironment();
		MsiSetProperty(ISMSI_HANDLE, @IS_RABBITMQ_INSTALLED, "1");
	 endif;
	 
	 
	//MsiSetProperty(ISMSI_HANDLE, @IS_ALL_PREREQUISITES_PASSED, "0");
	
	/*if(framework_46 && isJREInstall && isNodejs) then
		MsiSetProperty(ISMSI_HANDLE,@IS_ALL_PREREQUISITES_PASSED,"1");
	endif;*/
	
	//MsiSetProperty(ISMSI_HANDLE,@IS_ALL_PREREQUISITES_PASSED,"1");
	//MsiGetProperty(ISMSI_HANDLE,"IS_ALL_PREREQUISITES_PASSED",Temp,size);
	//MessageBox(Temp,INFORMATION);
end;

function Is_Framework46()
BOOL retValue;
STRING dotNet_Key,strValue;
NUMBER nValue;
begin
		retValue = FALSE;
		dotNet_Key ="SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\full";
		
		RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
		
		if (RegDBKeyExist (dotNet_Key) >= 0) then
			
			strValue = GetRegistryValue(dotNet_Key, "Release");
			
            StrToNum(nValue, strValue);
            
			if (nValue>= 393295) then
				
				/// .NET 4.6 or higher version
				retValue = TRUE;
				
			endif;
			
		endif;
		
		return retValue;
end;

function JRE_Install()
	BOOL jreFound;
	
begin
	jreFound = FALSE;
	MsiSetProperty(ISMSI_HANDLE, @JRE_VERSION, "0");
				
	if (SYSINFO.bIsWow64) then
		jreFound = IS_JRE_Install("SOFTWARE\\Wow6432Node\\JavaSoft\\Java Runtime Environment");
	endif;
	
	if(!jreFound) then
		jreFound = IS_JRE_Install("SOFTWARE\\JavaSoft\\Java Runtime Environment");
	endif;
			
	return jreFound;
end;

function IS_JRE_Install(jreKey)
	BOOL retValue;
	STRING jreCurrentVersion;
	NUMBER nvType, nSize, nJREVersion;
	
begin
	retValue = FALSE;
	
	MsiSetProperty(ISMSI_HANDLE, @JRE_REGISTRY_KEY, "");
				
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	RegDBGetKeyValueEx(jreKey,"CurrentVersion", nvType, jreCurrentVersion, nSize);
	
	
	StrToNum(nJREVersion, jreCurrentVersion);
		
	if (nJREVersion > 0 ) then
	
		MsiSetProperty(ISMSI_HANDLE, @JRE_VERSION, jreCurrentVersion);
		
		if(jreCurrentVersion = "1.5") then
			REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
			return FALSE;
		endif;
		
		MsiSetProperty(ISMSI_HANDLE, @JRE_REGISTRY_KEY, jreKey);
		retValue = TRUE;
	endif;
		MsiSetProperty(ISMSI_HANDLE, @JRE_REGISTRY_KEY, jreKey);
		REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
	return retValue;
end;

function Is_NodeJs_Install()
STRING nodejs_key,strValue;
NUMBER nValue;
BOOL retValue;
begin
	retValue = FALSE;
	nodejs_key ="SOFTWARE\\Node.js";
	
	MsiSetProperty(ISMSI_HANDLE, @NODEJS_REGISTRY_KEY, "");
				
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	
	if (RegDBKeyExist (nodejs_key) >= 0) then	
	
		strValue = GetRegistryValue(nodejs_key, "Version");	
		
		if(VerCompare(strValue,"6.9.0",VERSION) < 0) then 
			REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
			return FALSE;
		endif;
		
		StrToNum(nValue, strValue);
		MsiSetProperty(ISMSI_HANDLE, @NODEJS_REGISTRY_KEY, nodejs_key);
		retValue = TRUE;
	endif;
		MsiSetProperty(ISMSI_HANDLE, @NODEJS_REGISTRY_KEY, nodejs_key);
		REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
			
	return retValue;
end;


function Is_RabbitMQInstalled()
STRING rabbitMQ_Key,rabbitMqCurrentVersion;
NUMBER nRabbitMqCurrentVersion,nvType,nSize;
BOOL retValue;

begin
	/*retValue = FALSE;
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	
	rabbitMQ_Key ="SOFTWARE\\Ericsson\\Erlang\\ErlSrv\\1.1\\RabbitMQ";
	
	if (RegDBKeyExist (rabbitMQ_Key) >= 0) then	
		retValue = TRUE;
	endif;
	
	MsiSetProperty(ISMSI_HANDLE, @RABBITMQ_REGISTRY_KEY, rabbitMQ_Key);
	
	REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
	
	return retValue;
	*/
	retValue = FALSE;
	
	MsiSetProperty(ISMSI_HANDLE, @RABBITMQ_REGISTRY_KEY, "");
				
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	rabbitMQ_Key = "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\RabbitMQ";
	RegDBGetKeyValueEx(rabbitMQ_Key,"DisplayVersion", nvType, rabbitMqCurrentVersion, nSize);
	
	
	StrToNum(nRabbitMqCurrentVersion, rabbitMqCurrentVersion);
		
	if (nRabbitMqCurrentVersion > 0 ) then
	
		MsiSetProperty(ISMSI_HANDLE, @RABBITMQ_VERSION, rabbitMqCurrentVersion);
		MsiSetProperty(ISMSI_HANDLE, @RABBITMQ_REGISTRY_KEY, rabbitMQ_Key);
		retValue = TRUE;
	endif;
		MsiSetProperty(ISMSI_HANDLE, @RABBITMQ_REGISTRY_KEY, rabbitMQ_Key);
		REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
	return retValue;
	
	
end;


function InstallPrerequisites(hMSI)
STRING SupportDir,jreVersion,filePath,erlangfilePath, rabbitMqFilePath,commandArgs,pm2FilePath;
BOOL isJREInstalled,isNodeJsInstalled,isRabbitMQInstalled;
NUMBER nSize;
begin
	
	MsiGetProperty(ISMSI_HANDLE,"SUPPORTDIR",SupportDir,nSize);
	LAAW_SHELLEXECUTEVERB = "runas";
	isJREInstalled = JRE_Install();
	MsiGetProperty(ISMSI_HANDLE, @JRE_VERSION, jreVersion, nSize);
	if(!isJREInstalled) then
			if(jreVersion = "1.5") then
				MessageBox("Unsupported Java Runtime Environment(JRE) 1.5 detected. \n Currently not supported on this version of JRE. Please uninstall JRE 1.5 and run this setup again. \nNOTE: JRE 1.8 will be installed upon resuming setup.", INFORMATION);
				
				return FALSE;
			endif;
			LaunchAppAndWait(SupportDir ^ "jre-8u91-windows-i586.exe","/s",WAIT);
			SetJavaHomePath(TRUE);
			
				
	endif;
	
	//RabbitMQ and Errlang
	erlangfilePath = SupportDir ^ "erlangInstall.bat";
	if (Is(FILE_EXISTS, erlangfilePath) = 1) then
			LaunchAppAndWait(erlangfilePath,"/s",LAAW_OPTION_WAIT);
	endif;
			
	isRabbitMQInstalled =  Is_RabbitMQInstalled();
	if(!isRabbitMQInstalled) then 
  
		rabbitMqFilePath = SupportDir ^ "rabbitmqservice.bat";
  
		if (Is(FILE_EXISTS, rabbitMqFilePath) = 1) then
			LaunchAppAndWait(rabbitMqFilePath,"/s",LAAW_OPTION_WAIT);
		endif;
		SetRabbitMQEnvironment();
	endif;
  
  //Node js
	isNodeJsInstalled = Is_NodeJs_Install();
	
	if(!isNodeJsInstalled) then
		
			filePath = SupportDir ^ "node-v6.10.2-x64.msi";
			
			LaunchAppAndWait("msiexec", "/i " + filePath + " /quiet /qn /norestart",WAIT);		
			SetNodeJsEnvironment();
	endif;

 
		   
  
  
  //
  /*erlangfilePath = SupportDir ^ "otp_win64_19.3.exe";
  
  if(LaunchAppAndWait(erlangfilePath ,"/s" + "/D=C:\Program Files\erl8.3",WAIT) > 0) then 
			MessageBox("Orlang installed",INFORMATION);
		else
			MessageBox("Orlang fail",INFORMATION);
		endif;
  
  
  rabbitMqFilePath = SupportDir ^ "rabbitmq-server-3.6.9.exe";
  commandArgs =  "/s" +"/NCRC" + "/D=C:\Program Files\RabbitMQ Server\"";
  
  if(LaunchAppAndWait(rabbitMqFilePath ,commandArgs,WAIT) > 0) then 
			MessageBox("RabbitMQ installed",INFORMATION);
		else
			MessageBox("RabbitMQ fail",INFORMATION);
		endif;*/
		
	/*if(LaunchAppAndWait(SupportDir ^ "otp_win64_19.1.exe" ,"/S",WAIT) > 0) then 
			MessageBox("Orlang installed",INFORMATION);
		else
			MessageBox("Orlang fail",INFORMATION);
		endif;
	
	//RabbitMQ
		if(LaunchAppAndWait(SupportDir ^ "rabbitmq-server-3.6.6.exe","/S",WAIT) > 0) then
			MessageBox("RabbitMQ installed",INFORMATION);
		else
			MessageBox("RabbitMQ fail to install", INFORMATION);
		endif;*/
		
		
		
end;

function SetJavaHomePath(updateJavaHomePath)
	STRING nValue, jreKeyPath,registryKey, jreVersionKey, javaHome, storedJavaHomePath;
	NUMBER nvType, nSize, nJREVersion;
	
begin
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	MsiGetProperty(ISMSI_HANDLE, @JRE_REGISTRY_KEY, jreKeyPath, nSize);
		
	//MessageBox(jreKeyPath,INFORMATION);
	//MessageBox(@JRE_REGISTRY_KEY,INFORMATION);
	
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	RegDBGetKeyValueEx(jreKeyPath,"CurrentVersion", nvType, nValue, nSize);
	
	StrToNum(nJREVersion, nValue);
		//MessageBox("-1",INFORMATION);
		//MessageBox(nValue,INFORMATION);
	if (nJREVersion > 0 ) then
			
		jreVersionKey = jreKeyPath ^ nValue;
		//MessageBox("-2",INFORMATION);
			
		if(RegDBGetKeyValueEx(jreVersionKey,"JavaHome",nvType,javaHome,nSize) = 0) then
			registryKey = "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
				//MessageBox("0",INFORMATION);
				//MessageBox(javaHome,INFORMATION);
			if(RegDBGetKeyValueEx(registryKey,"JAVA_HOME",nvType,storedJavaHomePath,nSize) < 0) then
			//	MessageBox("1",INFORMATION);
			//MessageBox(registryKey,INFORMATION);
				SetJavaHomeEnvironment(javaHome);
			else
				if(updateJavaHomePath || !Is(PATH_EXISTS, storedJavaHomePath)) then
				//	MessageBox("2",INFORMATION);
				//	MessageBox(storedJavaHomePath,INFORMATION);
					SetJavaHomeEnvironment(javaHome);
				endif;
			endif;			
		endif;			
	endif;
	REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
end;

function SetJavaHomeEnvironment(javaHomeValue)
	STRING  registryKey, szEnv;
	WPOINTER pResult, pEnv;
begin
	
	//MessageBox("Start",INFORMATION);
	registryKey = "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
	
	RegDBSetKeyValueEx(registryKey,"JAVA_HOME",REGDB_STRING, javaHomeValue, -1);
				
	MsiSetProperty(ISMSI_HANDLE, @IS_JAVA_HOME_SET , "1");
				
	szEnv = "Environment"; 
	pEnv = &szEnv; 
	
	if(!SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0, pEnv, SMTO_ABORTIFHUNG, SMTO_TIMEOUT, pResult)) then
		
	endif;
//	MessageBox("End",INFORMATION);			
end;

function SetNodeJsEnvironment()
STRING nodejsHome,nodejsKeyPath,registryKey,szEnv;
NUMBER nvType,nSize;
WPOINTER pResult, pEnv;
begin
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	MsiGetProperty(ISMSI_HANDLE, @NODEJS_REGISTRY_KEY, nodejsKeyPath, nSize);
	
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	
	if(RegDBGetKeyValueEx(nodejsKeyPath,"InstallPath",nvType,nodejsHome,nSize) = 0) then
		registryKey = "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
						
		RegDBSetKeyValueEx(registryKey,"NODEJS_HOME",REGDB_STRING, nodejsHome, -1);
				
		MsiSetProperty(ISMSI_HANDLE, @IS_JAVA_HOME_SET , "1");
				
		szEnv = "Environment"; 
		pEnv = &szEnv; 
	
		if(!SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0, pEnv, SMTO_ABORTIFHUNG, SMTO_TIMEOUT, pResult)) then
			
		endif;
		
	endif;
	
	REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
end;

function SetRabbitMQEnvironment()
STRING rabbitMqKeyPath,rabbitMqDisplayVersion,rabbitMqHome,registryKey, szEnv;
NUMBER nvType,nSize;
WPOINTER pResult, pEnv;
begin
	REGDB_OPTIONS = REGDB_OPTIONS | REGDB_OPTION_WOW64_64KEY;
	MsiGetProperty(ISMSI_HANDLE, @RABBITMQ_REGISTRY_KEY, rabbitMqKeyPath, nSize);
	
	RegDBSetDefaultRoot (HKEY_LOCAL_MACHINE);
	if(RegDBGetKeyValueEx(rabbitMqKeyPath,"DisplayVersion",nvType,rabbitMqDisplayVersion,nSize) = 0) then
		registryKey = "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment";
		rabbitMqHome = "C:\\Program Files\\RabbitMQ Server\\rabbitmq_server-" + rabbitMqDisplayVersion;
		
		RegDBSetKeyValueEx(registryKey,"RABBITMQ_HOME",REGDB_STRING, rabbitMqHome, -1);
		szEnv = "Environment"; 
		pEnv = &szEnv; 
	
		if(!SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0, pEnv, SMTO_ABORTIFHUNG, SMTO_TIMEOUT, pResult)) then
			
		endif;
	
	endif;
	REGDB_OPTIONS = REGDB_OPTIONS & ~REGDB_OPTION_WOW64_64KEY;
	
end;

function StartNpm(hMSI)
	STRING szNpmBatch,szCommandNpm,SupportDir,InstallDir;
	NUMBER nSize;
begin
	MsiGetProperty(ISMSI_HANDLE,"SUPPORTDIR",SupportDir,nSize);
	MsiGetProperty(ISMSI_HANDLE,"INSTALLDIR",InstallDir,nSize);
	
	szNpmBatch = InstallDir ^ "Configurations\\npmstart.bat";
	szCommandNpm = "\"" + INSTALLDIR;
	if (Is(FILE_EXISTS, szNpmBatch) = 1) then
		LAAW_SHELLEXECUTEVERB = "runas";
		LaunchAppAndWait(szNpmBatch,szCommandNpm,LAAW_OPTION_NOWAIT);
	else
		//MessageBox("NPM not started", INFORMATION);
	endif;
	
	
end;



function WriteSettingsToStartFile(hMSI)
STRING szProgram ,szServicePath,szCommandArgs,SupportDir,InstallDir;
STRING dbserver,dbusername,dbpswd,dbname,dbport,rabbitMqUN,rabbitMqPswd,controlRoomLink,controlroomIP,outputPath,controlroomPort;
STRING startServiceShortcut,stopServiceShortcut,shortcutIconPath;
NUMBER nSize;
begin
		//Write database settings and other settings to startCofiguration.txt file to restart services if require.
		MsiGetProperty(ISMSI_HANDLE,"SUPPORTDIR",SupportDir,nSize);
		MsiGetProperty(ISMSI_HANDLE,"INSTALLDIR",InstallDir,nSize);
				
		MsiGetProperty(ISMSI_HANDLE, @DATABASE_SERVER, dbserver, nSize);
		MsiGetProperty(ISMSI_HANDLE, @DATABASE_USERNAME, dbusername, nSize);
		MsiGetProperty(ISMSI_HANDLE, @DATABASE_PASSWORD, dbpswd, nSize);
		MsiGetProperty(ISMSI_HANDLE, @DATABASE_NAME, dbname, nSize);
		MsiGetProperty(ISMSI_HANDLE, @DATABASE_PORT, dbport, nSize);
		MsiGetProperty(ISMSI_HANDLE, @CONTROLROOM_IP ,controlroomIP,nSize);
		MsiGetProperty(ISMSI_HANDLE, @CONTROLROOM_PORT,controlroomPort,nSize);
		MsiGetProperty(ISMSI_HANDLE, @OUTPUT_PATH , outputPath,nSize);
		
	
		szServicePath = SupportDir ^ "ServicesSettings.bat";
		shortcutIconPath = InstallDir ^ "app.ico";
		//szCommandArgs = dbserver + " " + dbusername + " " + dbpswd + " " + dbport + " " + rabbitMqUN + " " + rabbitMqPswd + " " +"\""+InstallDir;
		szCommandArgs = dbserver + " " + dbusername + " " + dbport + " " + controlroomIP + " " + controlroomPort + " " + outputPath + " " + "\""+InstallDir;
				
		if (Is(FILE_EXISTS, szServicePath) = 1) then
			LAAW_SHELLEXECUTEVERB = "runas";
			//LaunchAppAndWait(szNpmBatch,szCommandNpm,LAAW_OPTION_NOWAIT);
			LaunchAppAndWait(szServicePath,szCommandArgs,LAAW_OPTION_WAIT);
			//startServiceShortcut = "\"" + INSTALLDIR+"\\Configurations\\startservices.bat" +"\"" ;
			//stopServiceShortcut = "\"" + INSTALLDIR+"\\Configurations\\stopservices.bat" +"\"" ;
		
			//CreateShortcut(FOLDER_DESKTOP,"Start IQ Bot",startServiceShortcut,"",shortcutIconPath,0,"",CS_OPTION_FLAG_REPLACE_EXISTING);
			//CreateShortcut(FOLDER_DESKTOP,"Stop IQ Bot",stopServiceShortcut,"",shortcutIconPath,0,"",CS_OPTION_FLAG_REPLACE_EXISTING);
		else
			//MessageBox(szServicePath, INFORMATION);
			//MessageBox("Services not found", INFORMATION);
		endif;
		
end;

/*function RabbitMQUserCreation(hMSI)
STRING szBatchPath,SupportDir;
NUMBER nSize;
begin
MsiGetProperty(ISMSI_HANDLE,"SUPPORTDIR",SupportDir,nSize);
	szBatchPath = SupportDir ^ "rabbitmqservice_user.bat";
	 
	if (Is(FILE_EXISTS, szBatchPath) = 1) then
		
		LaunchAppAndWait(szBatchPath,"/s",LAAW_OPTION_WAIT);
	endif;
end;*/



