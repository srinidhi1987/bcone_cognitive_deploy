@ECHO OFF

REM SET WorkspaceLocation=%1

set SevenZipExe="C:\Program Files\7-Zip\7z.exe"
set ZipName="cognitive-release"
set WorkspaceLocation="C:\Jenkins\workspace\cognitive-installer-job"
set ShareLocation="C:\IQBot\Builds"

set UnzipLocation="%WorkspaceLocation%\%ZipName%"
set MergeLocation="%UnzipLocation%\BuiltItems"
set ProtectionArtifactsLocation="%WorkspaceLocation%\ProtectionArtifacts\Cognitive"

set DateTimeStamp=%DATE:/=-%@%TIME::=-%

::===================================================
echo "*** START: placing protected binaries ***'

echo '--- Classifier Worker ---'

xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.dll %MergeLocation%\workers\classifier
::xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.Validation.dll %MergeLocation%\workers\classifier
xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.DocumentClassifier.exe %MergeLocation%\workers\classifier

echo '--- Visionbot Worker ---'

xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.dll %MergeLocation%\workers\visionbotengine
::xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.Validation.dll %MergeLocation%\workers\visionbotengine

echo '--- Designer App ---'

xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.dll %MergeLocation%\client\designer
::xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.Validation.dll %MergeLocation%\client\designer

echo '--- Validator App ---'

::xcopy /Y/V/F/q %ProtectionArtifactsLocation%\Automation.Cognitive.VisionBotEngine.Validation.dll %MergeLocation%\client\validator

echo '*** END: protected binaries placed ***'

::===========================================
echo "*** START: Create Protected Release Zip ***"

%SevenZipExe% a -tzip "%ShareLocation%\%ZipName%_Protected_%DateTimeStamp%.zip" "%MergeLocation%"

echo "*** END: Creating Protected zip ***"