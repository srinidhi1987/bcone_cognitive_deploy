@ECHO OFF

SET data=%1
REM SET data="C:\Jenkins\workspace\cognitive-installer-job"#"C:\Jenkins\workspace\cognitive-installer-job\SetupArtifacts"#RC-1.0-1#"\\datashare\Tempshare\DailyBuild\Cognitive"#"5.0.0.0"#"1"
FOR /f "tokens=1,2,3,4,5,6 delims=#" %%a IN ("%data%") do SET WorkspaceLocation=%%a&SET SetupArtifactesLocation=%%b&SET AutomatedSetupBranch=%%c&SET SetupDestinationPath=%%d&SET BuildVersion=%%e&SET BuildRCVersion=%%f

SET SetupMachineName=SETUP1-2015
SET SetupComponentList="configurations,workers"
SET InstallShieldProjectName="CognitivePlatform"
SET SetupExeName="Automation_Anywhere_IQ_BOT"

::SET SetupDestinationPath="\\192.168.2.252\TempShare\DailyBuild\AATFSSetups"
SET BuildAutomationProject=%WorkspaceLocation%\ciBuildAutomation\CIBuildAutomation.csproj


echo ========================INPUT PARAMETERS
echo WorkspaceLocation=%WorkspaceLocation%
echo SetupArtifactesLocation=%SetupArtifactesLocation%
echo AutomatedSetupBranch=%AutomatedSetupBranch%
echo BuildVersion=%BuildVersion%
echo BuildRCVersion=%BuildRCVersion%
echo ========================VARIABLES IN BATCH FILE
echo SetupMachineName=%SetupMachineName%
echo SetupComponentList=%SetupComponentList%
echo InstallShieldProjectName=%InstallShieldProjectName%
echo SetupExeName=%SetupExeName%
echo SetupDestinationPath=%SetupDestinationPath%
echo BuildAutomationProject=%BuildAutomationProject%


C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe %BuildAutomationProject% /fl /fl1 /flp:logfile=IQBotInfolog.txt /flp1:v=minimal;logfile=IQBotErrorlog.txt;errorsonly /t:StartSetup /p:SetupMachineName=%SetupMachineName%;WorkspaceLocation=%WorkspaceLocation%;SetupArtifactesLocation=%SetupArtifactesLocation%;AutomatedSetupBranch=%AutomatedSetupBranch%;SetupComponentList=%SetupComponentList%;SetupExeName=%SetupExeName%;InstallShieldProjectName=%InstallShieldProjectName%;SetupDestinationPath=%SetupDestinationPath%;BuildVersion=%BuildVersion%;BuildRCVersion=%BuildRCVersion%

