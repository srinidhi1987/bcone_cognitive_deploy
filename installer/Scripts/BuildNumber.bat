@echo off
SETLOCAL ENABLEDELAYEDEXPANSION
set buildNumberFile=%1

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "datestamp=%YY%%MM%%DD%"

set /p buildNumberFromFile=<%buildNumberFile%

set "currentBuildDate=%buildNumberFromFile:~0,6%"

IF "%currentBuildDate%"=="%datestamp%" (
	set sequenceNumber=%buildNumberFromFile:~6,2%
	set /a "sequenceNumber+=1"
	IF !sequenceNumber! lss 10 set "sequenceNumber=0!sequenceNumber!"
	echo %currentBuildDate%!sequenceNumber!>%buildNumberFile%
) else (
	echo %datestamp%01>%buildNumberFile%
)