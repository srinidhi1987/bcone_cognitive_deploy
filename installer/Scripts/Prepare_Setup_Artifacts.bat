@ECHO OFF
setlocal enableDelayedExpansion

REM set data="C:\Jenkins\workspace\cognitive-installer-job"#"C:\Jenkins\workspace\cognitive-installer-job\SetupArtifacts"
SET data=%1
FOR /f "tokens=1,2 delims=#" %%a IN ("%data%") do SET WorkspaceLocation=%%a&SET SetupArtifactsLocation=%%b
echo WorkspaceLocation=%WorkspaceLocation%
echo SetupArtifactsLocation=%SetupArtifactsLocation%



echo "START: emptying %SetupArtifactsLocation%..."

del /q "%SetupArtifactsLocation%\*"
FOR /D %%p IN ("%SetupArtifactsLocation%\*.*") DO rmdir "%%p" /s /q

echo "DONE emptying"



echo "START: copy 'configurations'..."

mkdir "%SetupArtifactsLocation%\configurations"

xcopy /Y/V/F/s/q %WorkspaceLocation%\cognitive-installer\Resources\configurations "%SetupArtifactsLocation%\configurations"

echo "DONE copying 'configurations'"



echo "START: copy 'workers'..."

mkdir "%SetupArtifactsLocation%\workers"

xcopy /Y/V/F/s/q %WorkspaceLocation%\cognitive-installer\Resources\workers "%SetupArtifactsLocation%\workers"

echo "DONE copying 'workers'"