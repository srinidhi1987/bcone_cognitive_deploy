@ECHO OFF
setlocal enableDelayedExpansion

set data="C:\Jenkins\workspace\cognitive-installer-job"#"C:\Jenkins\workspace\cognitive-installer-job\ProtectionArtifacts"
::SET data=%1
FOR /f "tokens=1,2 delims=#" %%a IN ("%data%") do SET WorkspaceLocation=%%a&SET ProtectionArtifactsLocation=%%b
echo ProtectionArtifactsLocation=%ProtectionArtifactsLocation%
echo WorkspaceLocation=%WorkspaceLocation%

SET BuildAutomationPTargetPath="%WorkspaceLocation%\ciBuildAutomation\CIBuildAutomation.csproj"

echo BuildAutomationPTargetPath %BuildAutomationPTargetPath%
SET ProtectionMachineName="\\Protection3"
SET SourceLocation=%ProtectionArtifactsLocation%
SET ComponentDestinationLocation="Cognitive:ATMX:Cognitive"
SET ProtectedBinariesDestinationPath="\ProtectionConfiguration\Binaries"
SET ProtectionSequenceDestinationPath="\ProtectionConfiguration\ProtectionSequence\ProtectionSequence.csv"
SET StartProtectionSourceLocation="\Protection\StartProtection.txt"
SET StartProtectionDestinationLocation="\Protection\AA"

C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe %BuildAutomationPTargetPath% /fl /fl1 /flp:logfile=IQBotInfolog.txt /flp1:v=minimal;logfile=IQBotErrorlog.txt;errorsonly /t:StartProtection /p:ProtectionMachineName=%ProtectionMachineName%;SourceLocation=%SourceLocation%;ComponentDestinationLocation=%ComponentDestinationLocation%;ProtectedBinariesDestinationPath=%ProtectedBinariesDestinationPath%;ProtectionSequenceDestinationPath=%ProtectionSequenceDestinationPath%;StartProtectionSourceLocation=%StartProtectionSourceLocation%;StartProtectionDestinationLocation=%StartProtectionDestinationLocation%;WorkspaceLocation=%WorkspaceLocation%

