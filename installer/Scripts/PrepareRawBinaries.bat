@echo off

set SevenZipExe="C:\Program Files\7-Zip\7z.exe"
set ZipName="cognitive-release"
set WorkspaceLocation="C:\Jenkins\workspace\cognitive-installer-job"

set PrerequisiteLocation="%WorkspaceLocation%\cognitive-installer\Resources"
set UnzipLocation="%WorkspaceLocation%\%ZipName%"
set MergeLocation="%UnzipLocation%\BuiltItems"

set ShareLocation="C:\IQBot\Builds"
set ArchiveLocation="%ShareLocation%\Archive"

::===========================================
echo "*** START: Archive old shared zips ***"

mkdir "%ArchiveLocation%"
move /Y "%ShareLocation%\%ZipName%*.zip" "%ArchiveLocation%\"

echo "*** END: Old zips archived ***"

::===================================================
echo "*** START: Download Zip from Build Machine ***"

echo %FTP_PASS%
echo open 13.232.0.83 >> ftp &echo user admin %FTP_PASS% >> ftp &echo binary >> ftp &echo get %ZipName%.zip >> ftp &echo bye >> ftp &ftp -n -v -s:ftp &del ftp

echo "*** END: Zip downloaded ***"

::=========================================================
echo "*** START: Copy downloaded zip to share location ***"

set DateTimeStamp=%DATE:/=-%@%TIME::=-%

xcopy /Y/V/F/q "%WorkspaceLocation%\%ZipName%.zip" "%ShareLocation%\"
ren "%ShareLocation%\%ZipName%.zip" "%ZipName%_Build_%DateTimeStamp%.zip"

echo "*** END: Zip copied to share location ***"

::=========================================
echo "*** START: Unzipping Release Zip ***"

del /q "%UnzipLocation%\*"
FOR /D %%p IN ("%UnzipLocation%\*.*") DO rmdir "%%p" /s /q

%SevenZipExe% x "%WorkspaceLocation%\%ZipName%.zip" -o"%UnzipLocation%"

echo "*** END: Unzipping ***"

::===================================
echo "*** START: Merge Resources ***"

echo 'Starting: Configurations....'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\configurations %MergeLocation%\configurations
echo 'Ending: Configurations'

echo 'Starting: Services....'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\services %MergeLocation%\services
echo 'Ending: Services'

echo 'Starting: Clients....'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\client\designer %MergeLocation%\client\designer
xcopy /Y/V/F/s/q %PrerequisiteLocation%\client\validator %MergeLocation%\client\validator
echo 'Ending: Clients'

echo 'Starting: ML....'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\ml\translationsvc %MergeLocation%\ml\translationsvc
xcopy /Y/V/F/s/q %PrerequisiteLocation%\ml\webservice %MergeLocation%\ml\webservice
echo 'Ending: ML'

echo 'Starting: Workers....'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\workers\classifier %MergeLocation%\workers\classifier
%SevenZipExe% x "%PrerequisiteLocation%\workers\tessdata.zip" -o"%MergeLocation%\workers\classifier"
%SevenZipExe% x "%PrerequisiteLocation%\workers\tessdata_4.zip" -o"%MergeLocation%\workers\classifier"
xcopy /Y/V/F/s/q %PrerequisiteLocation%\workers\visionbotengine %MergeLocation%\workers\visionbotengine
%SevenZipExe% x "%PrerequisiteLocation%\workers\tessdata.zip" -o"%MergeLocation%\workers\visionbotengine"
%SevenZipExe% x "%PrerequisiteLocation%\workers\tessdata_4.zip" -o"%MergeLocation%\workers\visionbotengine"
echo 'Ending: Workers'

echo 'Starting: Setup-support-files...'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\setup-support-files %MergeLocation%\setup-support-files
echo 'Ending: Setup-support-files'

echo 'Starting: Portal...'
xcopy /Y/V/F/s/q %PrerequisiteLocation%\portal %MergeLocation%\portal
echo 'Ending: Portal'

echo "*** END: Resources copied ***"

::===========================================
echo "*** START: Create Full Release Zip ***"

%SevenZipExe% a -tzip "%ShareLocation%\%ZipName%_Full_%DateTimeStamp%.zip" "%MergeLocation%"

echo "*** END: Creating full zip ***"