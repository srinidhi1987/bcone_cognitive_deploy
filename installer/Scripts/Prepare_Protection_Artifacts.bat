@ECHO OFF
setlocal enableDelayedExpansion

REM set data="C:\Jenkins\workspace\cognitive-installer-job"#"C:\Jenkins\workspace\cognitive-installer-job\ProtectionArtifacts"
SET data=%1
FOR /f "tokens=1,2 delims=#" %%a IN ("%data%") do SET WorkspaceLocation=%%a&SET ProtectionArtifactsLocation=%%b
echo WorkspaceLocation=%WorkspaceLocation%
echo ProtectionArtifactsLocation=%ProtectionArtifactsLocation%


echo "START: copy CSVs..."

del /q "%ProtectionArtifactsLocation%\*"

xcopy /Y/V/F/q %WorkspaceLocation%\cognitive-installer\Protection\*.csv %ProtectionArtifactsLocation%\

echo "END: CSV copied"



echo "START: copy Binaries..."

del /q "%ProtectionArtifactsLocation%\Cognitive\*"
FOR /D %%p IN ("%ProtectionArtifactsLocation%\Cognitive\*.*") DO rmdir "%%p" /s /q

xcopy /Y/V/F/q %WorkspaceLocation%\cognitive-release\BuiltItems\workers\classifier\Automation.Cognitive.VisionBotEngine.dll %ProtectionArtifactsLocation%\Cognitive\
::xcopy /Y/V/F/q %WorkspaceLocation%\cognitive-release\BuiltItems\workers\classifier\Automation.Cognitive.VisionBotEngine.Validation.dll %ProtectionArtifactsLocation%\Cognitive\
xcopy /Y/V/F/q %WorkspaceLocation%\cognitive-release\BuiltItems\workers\classifier\Automation.Cognitive.DocumentClassifier.exe %ProtectionArtifactsLocation%\Cognitive\

echo "END: binaries copied"