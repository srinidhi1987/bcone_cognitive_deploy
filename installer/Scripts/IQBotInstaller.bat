set workspace=%1
set buildNumber=%2
set buildZipFileName=cognitive-release_Protected*.zip
set buildLocation=\\COGNITIVESRV\Builds
set destination=%workspace%\cognitive-installer
set sevenZExe=%ProgramFiles%\7-Zip\7z.exe
set buildExtracationLocation=D:\iqbot-artifacts\BuiltItems
set resourceFolder=%destination%\Setup\Resources
set resourceFolderClient=%destination%\ClientSetup\Resources
set setupSupportFilesFolder=%destination%\Setup\InstallShield2015\Setup-Support-Files
set buildNumberFilePath=%destination%\buildnumber.txt
set buildNumberFileBatPath=%destination%\Scripts\BuildNumber.bat
set installshieldExePath=C:\Program Files (x86)\InstallShield\2015\System\IsCmdBld.exe
set serverIsmPath=%destination%\Setup\InstallShield2015\CognitivePlatform.ism
set clientIsmPath=%destination%\ClientSetup\CognitiveComponents\IQ Bot Client Components.ism
set serverBuildCreationPath=%destination%\Setup\PROJECT_ASSISTANT\SINGLE_EXE_IMAGE\DiskImages\DISK1
set clientBuildCreationPath=%destination%\ClientSetup\PROJECT_ASSISTANT\SINGLE_EXE_IMAGE\DiskImages\Disk1
set branch=develop
set version=5.2.0
set buildCopyPath=\\Win7prox64-SS\Setups\%version%\%branch%
set serverBuildName=Automation_Anywhere_IQ_BOT_%version%
set clientBuildName=Automation_Anywhere_IQ_Bot_Client_Components_%version%

del /q "%buildExtracationLocation%\..\*"
FOR /D %%p IN ("%buildExtracationLocation%\..\*.*") DO rmdir "%%p" /s /q
xcopy "%buildLocation%\%buildZipFileName%" "%buildExtracationLocation%\.." /r /i /y
"%sevenZExe%" x "%buildExtracationLocation%\..\%buildZipFileName%" -o"%buildExtracationLocation%\..\" -y

echo Starting Server Build

del /q "%resourceFolder%\*"
xcopy "%destination%\Resources\*.*" "%resourceFolder%" /r /i /y

del /q "%resourceFolder%\JRE\1.8.0_161\*"
FOR /D %%p IN ("%resourceFolder%\JRE\1.8.0_161\*.*") DO rmdir "%%p" /s /q
xcopy "%destination%\Resources\JRE\1.8.0_161\*" "%resourceFolder%\JRE\1.8.0_161\" /r /s /i /y

del /q "%resourceFolder%\portal\*"
FOR /D %%p IN ("%resourceFolder%\portal\*.*") DO rmdir "%%p" /s /q
xcopy "%destination%\Resources\portal\*" "%resourceFolder%\portal\" /r /s /i /y
xcopy "%buildExtracationLocation%\portal\*" "%resourceFolder%\portal\" /r /s /i /y

del /q "%resourceFolder%\ml\*"
FOR /D %%p IN ("%resourceFolder%\ml\*.*") DO rmdir "%%p" /s /q
xcopy "%buildExtracationLocation%\ml\*" "%resourceFolder%\ml\" /r /s /i /y

del /q "%resourceFolder%\workers\*"
FOR /D %%p IN ("%resourceFolder%\workers\*.*") DO rmdir "%%p" /s /q
xcopy "%buildExtracationLocation%\workers\*" "%resourceFolder%\workers\" /r /s /i /y

del /q "%resourceFolder%\services\*"
FOR /D %%p IN ("%resourceFolder%\services\*.*") DO rmdir "%%p" /s /q
xcopy "%buildExtracationLocation%\services\*" "%resourceFolder%\services\" /r /s /i /y
xcopy "%buildExtracationLocation%\configurations\*" "%resourceFolder%\configurations\" /r /s /i /y

del /q "%resourceFolder%\utilities\*"
FOR /D %%p IN ("%resourceFolder%\utilities\*.*") DO rmdir "%%p" /s /q
xcopy "%buildExtracationLocation%\utilities\*" "%resourceFolder%\utilities\" /r /s /i /y

xcopy "%buildExtracationLocation%\setup-support-files\controlroom\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\docs\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\installshield\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\liquibase\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\prerequisite-setups\jre\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\prerequisite-setups\node\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\prerequisite-setups\rabbitmq\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\openssl\*" "%setupSupportFilesFolder%" /r /i /y
xcopy "%buildExtracationLocation%\setup-support-files\*.*" "%setupSupportFilesFolder%" /r /i /y

rem cd %destination%
rem git pull origin %branch%

rem call "%buildNumberFileBatPath%" "%buildNumberFilePath%"

rem set /p buildNumber=<"%buildNumberFilePath%"
echo %buildNumber%

echo ^<?xml version="1.0" encoding="utf-8"?^>^<Product^>^<Name^>Automation Anywhere IQ Bot^</Name^>^<Version^>%version%.0-%buildNumber%^</Version^>^<Releases^>^<Version^>^</Version^>^<Notes^>^</Notes^>^</Releases^>^</Product^> > "%resourceFolder%\ProductReleaseInfo.xml"

rem "%installshieldExePath%" -p "%serverIsmPath%"
rem echo F|xcopy "%serverBuildCreationPath%\%serverBuildName%.exe" "%buildCopyPath%\%serverBuildName%.exe" /r /i /y
rem echo F|xcopy "%serverBuildCreationPath%\%serverBuildName%.exe" "%buildCopyPath%\backup\%serverBuildName%_%buildNumber%.exe" /r /i /y

echo Starting Client Build
del /q "%resourceFolderClient%\*"
FOR /D %%p IN ("%resourceFolderClient%\*.*") DO rmdir "%%p" /s /q

xcopy "%destination%\resources\*.*" "%resourceFolderClient%" /r /i /y
xcopy "%buildExtracationLocation%\client\validator\*" "%resourceFolderClient%\validator\" /r /s /i /y
xcopy "%buildExtracationLocation%\client\designer\*" "%resourceFolderClient%\designer\" /r /s /i /y

rem call "%buildNumberFileBatPath%" "%buildNumberFilePath%"

rem set /p buildNumber=<"%buildNumberFilePath%"
echo %buildNumber%

echo ^<?xml version="1.0" encoding="utf-8"?^>^<Product^>^<Name^>Automation Anywhere IQ Bot Client Components^</Name^>^<Version^>%version%.0-%buildNumber%^</Version^>^<Releases^>^<Version^>^</Version^>^<Notes^>^</Notes^>^</Releases^>^</Product^> > "%resourceFolderClient%\ProductReleaseInfo.xml"

rem "%installshieldExePath%" -p "%clientIsmPath%"
rem echo F|xcopy "%clientBuildCreationPath%\%clientBuildName%.exe" "%buildCopyPath%\%clientBuildName%.exe" /r /i /y
rem echo F|xcopy "%clientBuildCreationPath%\%clientBuildName%.exe" "%buildCopyPath%\backup\%clientBuildName%_%buildNumber%.exe" /r /i /y
