@ECHO OFF

SET data=%1
rem SET data="C:\Jenkins\workspace\cognitive-installer-job"#"C:\Jenkins\workspace\cognitive-installer-job\SetupArtifacts"#develop-0#"\\datashare\Tempshare\DailyBuild\Client"#"5.0.0.0"#"1"
FOR /f "tokens=1,2,3,4,5,6 delims=#" %%a IN ("%data%") do SET WorkspaceLocation=%%a&SET SetupArtifactesLocation=%%b&SET SetupBranch=%%c&SET SetupDestinationPath=%%d&SET BuildVersion=%%e&SET BuildRCVersion=%%f


SET ProductName=CICognitive
SET SetupMachineName=SETUP1-2015
SET SetupComponentList=""
REM /* SET SetupComponentList = coma separated component name (folders at installation location, e.g for 9x AAE: Client,ControlRoom,ProcessInVision) */

SET InstallShieldProjectName="IQ Bot Client Components"
REM /* SET InstallShieldProjectName = installshield ism project name */

SET SetupExeName="Automation_Anywhere_IQ_Bot_Client_Components"


SET InstallShieldLocation=%WorkspaceLocation%\cognitive-installer\ClientSetup\CognitiveComponents
SET BuildAutomationLocation=%WorkspaceLocation%
SET BuildAutomationProject=%WorkspaceLocation%\ciBuildAutomation\CIBuildAutomation.csproj
SET AutomatedSetupBranch=%ProductName%\%SetupBranch%

echo ========================INPUT PARAMETERS
echo InstallShieldLocation=%InstallShieldLocation%
echo SetupArtifactesLocation=%SetupArtifactesLocation%
echo AutomatedSetupBranch=%AutomatedSetupBranch%
echo BuildVersion=%BuildVersion%
echo BuildRCVersion=%BuildRCVersion%
echo ========================VARIABLES IN BATCH FILE
echo SetupMachineName=%SetupMachineName%
echo SetupComponentList=%SetupComponentList%
echo InstallShieldProjectName=%InstallShieldProjectName%
echo SetupExeName=%SetupExeName%
echo SetupDestinationPath=%SetupDestinationPath%
echo BuildAutomationProject=%BuildAutomationProject%


C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe %BuildAutomationProject% /fl /fl1 /flp:logfile=AAEInfolog.txt /flp1:v=minimal;logfile=AAEErrorlog.txt;errorsonly /t:StartSetup /p:SetupMachineName=%SetupMachineName%;InstallShieldLocation=%InstallShieldLocation%;BuildAutomationLocation=%BuildAutomationLocation%;SetupArtifactesLocation=%SetupArtifactesLocation%;AutomatedSetupBranch=%AutomatedSetupBranch%;SetupComponentList=%SetupComponentList%;SetupExeName=%SetupExeName%;InstallShieldProjectName=%InstallShieldProjectName%;SetupDestinationPath=%SetupDestinationPath%;BuildVersion=%BuildVersion%;BuildRCVersion=%BuildRCVersion%
